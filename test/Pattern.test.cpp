#include "FL/Pattern.hpp"
#include "IoStreamOverloads.hpp"
#include "catch2/catch_all.hpp"
#include <ranges>

namespace
{
	using ssv = std::span<std::u8string_view const>;
	using vsv = std::vector<std::u8string_view>;
}

TEST_CASE("Pattern can represent a pattern", "[FL]")
{
	auto const get_value = [](FL::PatternValue const & v) { return v; };
	SECTION("Empty")
	{
		auto const pattern = FL::Pattern{};
		REQUIRE(pattern.Size() == 0);
		REQUIRE(not pattern.IsValid());
	}
	SECTION("Non-empty")
	{
		auto const pattern = FL::Pattern{
			/*0*/ u8"hello",
			/*1*/ u8"",
			/*2*/ u8"this",
			/*3*/ u8"",
			/*4*/ u8"is",
			/*5*/ u8"",
			/*6*/ u8"cool",
		};
		REQUIRE(pattern.Size() == 7);
		REQUIRE(pattern.IsValid());
		CHECK(pattern[0] == u8"hello");
		CHECK(pattern[2] == u8"this");
		CHECK(pattern[4] == u8"is");
		CHECK(pattern[6] == u8"cool");
		auto pattern_range = pattern | std::views::transform(get_value);
		CHECK(vsv{ pattern_range.begin(), pattern_range.end() } == vsv{ u8"hello", u8"", u8"this", u8"", u8"is", u8"", u8"cool" });
		auto const pattern_subs = std::array<FL::PatternSub, 3>{
			FL::PatternSub{
				.index = 1,
				.value = u8"-=-",
			},
			FL::PatternSub{
				.index = 3,
				.value = u8"-|-",
			},
			FL::PatternSub{
				.index = 5,
				.value = u8"-+-",
			}
		};
		CHECK(pattern.Substitute(pattern_subs).AsU8String() == std::u8string(u8"hello-=-this-|-is-+-cool"));
		CHECK(pattern.Front() == FL::PatternValue{ u8"hello" });
		CHECK(pattern.Back() == FL::PatternValue{ u8"cool" });
	}
	SECTION("Non-empty with unknown start and end parts")
	{
		auto const pattern = FL::Pattern{
			/*0*/ u8"",
			/*1*/ u8"hello",
			/*2*/ u8"",
			/*3*/ u8"this",
			/*4*/ u8"",
			/*5*/ u8"is",
			/*6*/ u8"",
			/*7*/ u8"cool",
			/*8*/ u8"",
		};
		REQUIRE(pattern.Size() == 9);
		REQUIRE(pattern.IsValid());
		CHECK(pattern[0] == std::u8string(u8""));
		CHECK(pattern[1] == std::u8string(u8"hello"));
		CHECK(pattern[2] == std::u8string(u8""));
		CHECK(pattern[3] == std::u8string(u8"this"));
		CHECK(pattern[4] == std::u8string(u8""));
		CHECK(pattern[5] == std::u8string(u8"is"));
		CHECK(pattern[6] == std::u8string(u8""));
		CHECK(pattern[7] == std::u8string(u8"cool"));
		CHECK(pattern[8] == std::u8string(u8""));
		auto pattern_range = pattern | std::views::transform(get_value);
		CHECK(vsv{ pattern_range.begin(), pattern_range.end() } == vsv{ u8"", u8"hello", u8"", u8"this", u8"", u8"is", u8"", u8"cool", u8"" });
		/**
		 * @brief Typically substitutions would be built up, and then applied all at once.
		 */
		auto const pattern_subs = std::array{
			FL::PatternSub{
				.index = 0,
				.value = u8"123",
			},
			FL::PatternSub{
				.index = 2,
				.value = u8"-=-",
			},
			FL::PatternSub{
				.index = 4,
				.value = u8"-|-",
			},
			FL::PatternSub{
				.index = 6,
				.value = u8"-+-",
			}
		};
		CHECK(pattern.Substitute(pattern_subs).AsU8String() == std::u8string(u8"123hello-=-this-|-is-+-cool"));
		CHECK(pattern.Front() != u8"hello");
		CHECK(pattern.Back() != u8"cool");
	}
}