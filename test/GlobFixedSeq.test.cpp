#include "FL/GlobFixedSeq.hpp"
#include "FL/GlobParts.hpp"
#include "catch2/catch_all.hpp"
#include <catch2/catch_test_macros.hpp>

TEST_CASE("GlobFixedSeq can check for prefixes", "[FL]")
{
	FL::GlobPartList list{
		FL::GlobPartExact{u8"hello"},
		FL::GlobPartExact{u8"world"},
		FL::GlobPartExact{u8"food"},
		FL::GlobPartExact{u8"abc"},
	};
	FL::GlobFixedSeq seq{ list };

	CHECK(seq.Size() == 17);
	CHECK(seq.Describe() == "('hello')('world')('food')('abc')");
	CHECK(seq.ShouldSave() == false);
	CHECK(seq.HasPrefix(u8"helloworldfoodabcthis", true));
	CHECK(seq.Find(u8"greenhelloworldfoodabc", true) == 5);
	CHECK(seq.Find(u8"greenhellohelloworldfoodabc", true) == 10);
}

TEST_CASE("GetFixedSeqPrefix can extract prefixes", "[FL]")
{
	FL::GlobPartList list{
		FL::GlobPartExact{u8"hello"},
		FL::GlobPartExact{u8"world"},
		FL::GlobWildcard{},
		FL::GlobPartExact{u8"mate"},
	};
	FL::GlobFixedSeq seq = GetFixedSeqPrefix(list);

	CHECK(seq.Size() == 10);
	CHECK(seq.Describe() == "('hello')('world')");
	CHECK(seq.ShouldSave() == false);
	CHECK(seq.HasPrefix(u8"helloworldthis", true));
	CHECK(seq.Find(u8"greenhelloworld", true) == 5);
	CHECK(seq.Find(u8"greenhellohelloworld", true) == 10);
}

TEST_CASE("GetFixedSeqPrefix can extract suffixes", "[FL]")
{
	FL::GlobPartList list{
		FL::GlobPartExact{u8"mate"},
		FL::GlobWildcard{},
		FL::GlobPartExact{u8"hello"},
	};
	FL::GlobFixedSeq seq = GetFixedSeqSuffix(list);

	CHECK(seq.Size() == 5);
	CHECK(seq.Describe() == "('hello')");
	CHECK(seq.ShouldSave() == false);
	CHECK(seq.HasPrefix(u8"helloworldthis", true));
	CHECK(seq.Find(u8"greenhelloworld", true) == 5);
	CHECK(seq.Find(u8"greenhellohelloworld", true) == 5);
}

TEST_CASE("GetFixedSeqPrefix cannot extract empty suffixes", "[FL]")
{
	FL::GlobPartList list{
		FL::GlobPartExact{u8"mate"},
		FL::GlobWildcard{}
	};
	FL::GlobFixedSeq seq = GetFixedSeqSuffix(list);

	CHECK(seq.Size() == 0);
	CHECK(seq.Describe() == "");
	CHECK(seq.ShouldSave() == false);
	CHECK(seq.HasPrefix(u8"helloworldthis", true));
	CHECK(seq.Find(u8"greenhelloworld", true) == FL::glob_npos);
	CHECK(seq.Find(u8"greenhellohelloworld", true) == FL::glob_npos);
}