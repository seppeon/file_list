#include "FL/Semver.hpp"
#include "../private/IoStreamOverloads.hpp"
#include "catch2/catch_all.hpp"

namespace
{
	struct Test
	{
		std::string_view value;
		FL::Semver expected;
	};

	const Test valid_semvers[]
	{
		{
			.value = "0.0.4",
			.expected = { .major = 0, .minor = 0, .patch = 4 }
		},
		{
			.value = "1.2.3",
			.expected = { .major = 1, .minor = 2, .patch = 3 }
		},
		{
			.value = "10.20.30",
			.expected = { .major = 10, .minor = 20, .patch = 30 }
		},
		{
			.value = "1.1.2-prerelease+meta",
			.expected = { .major = 1, .minor = 1, .patch = 2, .prerelease = "prerelease", .meta = "meta" }
		},
		{
			.value = "1.1.2+meta",
			.expected = { .major = 1, .minor = 1, .patch = 2, .meta = "meta" }
		},
		{
			.value = "1.1.2+meta-valid",
			.expected = { .major = 1, .minor = 1, .patch = 2, .meta = "meta-valid" }
		},
		{
			.value = "1.0.0-alpha",
			.expected = { .major = 1, .minor = 0, .patch = 0, .prerelease = "alpha" }
		},
		{
			.value = "1.0.0-beta",
			.expected = { .major = 1, .minor = 0, .patch = 0, .prerelease = "beta" }
		},
		{
			.value = "1.0.0-alpha.beta",
			.expected = { .major = 1, .minor = 0, .patch = 0, .prerelease = "alpha.beta" }
		},
		{
			.value = "1.0.0-alpha.beta.1",
			.expected = { .major = 1, .minor = 0, .patch = 0, .prerelease = "alpha.beta.1" }
		},
		{
			.value = "1.0.0-alpha.1",
			.expected = { .major = 1, .minor = 0, .patch = 0, .prerelease = "alpha.1" }
		},
		{
			.value = "1.0.0-alpha0.valid",
			.expected = { .major = 1, .minor = 0, .patch = 0, .prerelease = "alpha0.valid" }
		},
		{
			.value = "1.0.0-alpha.0valid",
			.expected = { .major = 1, .minor = 0, .patch = 0, .prerelease = "alpha.0valid" }
		},
		{
			.value = "1.0.0-alpha-a.b-c-somethinglong+build.1-aef.1-its-okay",
			.expected = { .major = 1, .minor = 0, .patch = 0, .prerelease = "alpha-a.b-c-somethinglong", .meta = "build.1-aef.1-its-okay" }
		},
		{
			.value = "1.0.0-rc.1+build.1",
			.expected = { .major = 1, .minor = 0, .patch = 0, .prerelease = "rc.1", .meta = "build.1" }
		},
		{
			.value = "2.0.0-rc.1+build.123",
			.expected = { .major = 2, .minor = 0, .patch = 0, .prerelease = "rc.1", .meta = "build.123" }
		},
		{
			.value = "1.2.3-beta",
			.expected = { .major = 1, .minor = 2, .patch = 3, .prerelease = "beta" }
		},
		{
			.value = "10.2.3-DEV-SNAPSHOT",
			.expected = { .major = 10, .minor = 2, .patch = 3, .prerelease = "DEV-SNAPSHOT" }
		},
		{
			.value = "1.2.3-SNAPSHOT-123",
			.expected = { .major = 1, .minor = 2, .patch = 3, .prerelease = "SNAPSHOT-123" }
		},
		{
			.value = "1.0.0",
			.expected = { .major = 1, .minor = 0, .patch = 0 }
		},
		{
			.value = "2.0.0",
			.expected = { .major = 2, .minor = 0, .patch = 0 }
		},
		{
			.value = "1.1.7",
			.expected = { .major = 1, .minor = 1, .patch = 7 }
		},
		{
			.value = "2.0.0+build.1848",
			.expected = { .major = 2, .minor = 0, .patch = 0, .meta = "build.1848" }
		},
		{
			.value = "2.0.1-alpha.1227",
			.expected = { .major = 2, .minor = 0, .patch = 1, .prerelease = "alpha.1227" }
		},
		{
			.value = "1.0.0-alpha+beta",
			.expected = { .major = 1, .minor = 0, .patch = 0, .prerelease = "alpha", .meta = "beta" }
		},
		{
			.value = "1.2.3----RC-SNAPSHOT.12.9.1--.12+788",
			.expected = { .major = 1, .minor = 2, .patch = 3, .prerelease = "---RC-SNAPSHOT.12.9.1--.12", .meta = "788" }
		},
		{
			.value = "1.2.3----R-S.12.9.1--.12+meta",
			.expected = { .major = 1, .minor = 2, .patch = 3, .prerelease = "---R-S.12.9.1--.12", .meta = "meta" }
		},
		{
			.value = "1.2.3----RC-SNAPSHOT.12.9.1--.12",
			.expected = { .major = 1, .minor = 2, .patch = 3, .prerelease = "---RC-SNAPSHOT.12.9.1--.12" }
		},
		{
			.value = "1.0.0+0.build.1-rc.10000aaa-kk-0.1",
			.expected = { .major = 1, .minor = 0, .patch = 0, .meta = "0.build.1-rc.10000aaa-kk-0.1" }
		},
		// This will fail, uint32_t cannot store these, I believe this acceptable.
		// "99999999999999999999999.999999999999999999.99999999999999999",
		{
			.value = "1.0.0-0A.is.legal",
			.expected = { .major = 1, .minor = 0, .patch = 0, .prerelease = "0A.is.legal" }
		},
	};

	std::string_view constexpr invalid_semvers[]
	{
		"1",
		"1.2",
		"1.2.3-0123",
		"1.2.3-0123.0123",
		"1.1.2+.123",
		"+invalid",
		"-invalid",
		"-invalid+invalid",
		"-invalid.01",
		"alpha",
		"alpha.beta",
		"alpha.beta.1",
		"alpha.1",
		"alpha+beta",
		"alpha_beta",
		"alpha.",
		"alpha..",
		"beta",
		"1.0.0-alpha_beta",
		"-alpha.",
		"1.0.0-alpha..",
		"1.0.0-alpha..1",
		"1.0.0-alpha...1",
		"1.0.0-alpha....1",
		"1.0.0-alpha.....1",
		"1.0.0-alpha......1",
		"1.0.0-alpha.......1",
		"01.1.1",
		"1.01.1",
		"1.1.01",
		"1.2",
		"1.2.3.DEV",
		"1.2-SNAPSHOT",
		"1.2.31.2.3----RC-SNAPSHOT.12.09.1--..12+788",
		"1.2-RC-SNAPSHOT",
		"-1.0.3-gamma+b7718",
		"+justmeta",
		"9.8.7+meta+meta",
		"9.8.7-whatever+meta+meta",
		"99999999999999999999999.999999999999999999.99999999999999999----RC-SNAPSHOT.12.09.1--------------------------------..12",
	};

constexpr std::string_view start_find_semver =
R"(13.2.0
Copyright (C) 2023 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.)";

constexpr std::string_view middle_find_semver =
R"(gcc (Rev2, Built by MSYS2 project) 13.2.0
Copyright (C) 2023 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or 1.2.3 FITNESS FOR A PARTICULAR PURPOSE.)";

constexpr std::string_view end_find_semver =
R"(gcc (Rev2, Built by MSYS2 project) 13.2.0)";
}

constexpr std::string_view all_semvers =
R"(In the world of software, versions are crucial. For instance, consider the software Alpha which had its initial release as 1.0.0-alpha.
After a few bug fixes and minor changes, it moved to 1.0.1-beta.2. Post the beta testing phase, the software was officially released as 1.0.2.
Over time, with significant updates and new features, it evolved to 2.0.0, and currently, it’s at version 3.5.2+20130313144700.

Another software, Beta, had a different journey. It started with version 0.1.0-alpha+001, moved to 1.0.0+20130313144700,
and then jumped to 2.1.1-beta+exp.sha.5114f85. Currently, it’s at version 3.0.0-x.Y.Z.92.

Remember, in semver, versions follow the pattern of MAJOR.MINOR.PATCH, and labels like ‘alpha’, ‘beta’ are pre-release identifiers.
)";

TEST_CASE("Parsed the current version", "[FL::GetCurrentVersion]")
{
	CHECK(FL::GetCurrentVersion() == FL::Semver{.major = FL_LIB_MAJOR_VERSION, .minor = FL_LIB_MINOR_VERSION, .patch = FL_LIB_PATCH_VERSION});
}

TEST_CASE("Semver parser can handle all the test from semver.org on regexes", "[FL::Semver]")
{
	for (auto const [valid_semver, expected] : valid_semvers)
	{
		auto const result = FL::GetSemverFromString(valid_semver);
		INFO(valid_semver);
		CHECK(result.IsValid());
		CHECK(std::string(valid_semver) == result.ToString());
		CHECK(result.major == expected.major);
		CHECK(result.minor == expected.minor);
		CHECK(result.patch == expected.patch);
		CHECK(result.prerelease == expected.prerelease);
		CHECK(result.meta == expected.meta);
	}
	for (auto const invalid_semver : invalid_semvers)
	{
		auto const result = FL::GetSemverFromString(invalid_semver);
		INFO(invalid_semver);
		CHECK(not result.IsValid());
	}
}

TEST_CASE("Semver find", "[FL::FindFirstSemverString]")
{
	SECTION("Find semver at the start")
	{
		auto const semver = FL::FindFirstSemverString(start_find_semver);
        REQUIRE(semver.IsValid());
		CHECK(semver.index == 0);
		CHECK(semver.length == 6);
		CHECK(semver.semver == FL::Semver{ .major = 13, .minor = 2, .patch = 0 });
	}
	SECTION("Find semver in the middle")
	{
		auto const semver = FL::FindFirstSemverString(middle_find_semver);
        REQUIRE(semver.IsValid());
		CHECK(semver.length == 6);
		CHECK(semver.index == 35);
		CHECK(semver.semver == FL::Semver{ .major = 13, .minor = 2, .patch = 0 });
	}
	SECTION("Find semver in the end")
	{
		auto const semver = FL::FindFirstSemverString(end_find_semver);
        REQUIRE(semver.IsValid());
		CHECK(semver.length == 6);
		CHECK(semver.index == 35);
		CHECK(semver.semver == FL::Semver{ .major = 13, .minor = 2, .patch = 0 });
	}
    SECTION("Extract all semvers")
    {
        auto const all_semver = FL::FindAllSemverStrings(all_semvers);
        CHECK(all_semver == FL::SemverStringResultList{
            FL::SemverStringResult{ .index = all_semvers.find("1.0.0-alpha"), .length = 11, .semver = FL::SemverFromString("1.0.0-alpha").semver },
            FL::SemverStringResult{ .index = all_semvers.find("1.0.1-beta.2"), .length = 12, .semver = FL::SemverFromString("1.0.1-beta.2").semver },
            FL::SemverStringResult{ .index = all_semvers.find("1.0.2"), .length = 5, .semver = FL::SemverFromString("1.0.2").semver },
            FL::SemverStringResult{ .index = all_semvers.find("2.0.0"), .length = 5, .semver = FL::SemverFromString("2.0.0").semver },
            FL::SemverStringResult{ .index = all_semvers.find("3.5.2+20130313144700"), .length = 20, .semver = FL::SemverFromString("3.5.2+20130313144700").semver },
            FL::SemverStringResult{ .index = all_semvers.find("0.1.0-alpha+001"), .length = 15, .semver = FL::SemverFromString("0.1.0-alpha+001").semver },
            FL::SemverStringResult{ .index = all_semvers.find("1.0.0+20130313144700"), .length = 20, .semver = FL::SemverFromString("1.0.0+20130313144700").semver },
            FL::SemverStringResult{ .index = all_semvers.find("2.1.1-beta+exp.sha.5114f85"), .length = 26, .semver = FL::SemverFromString("2.1.1-beta+exp.sha.5114f85").semver },
            FL::SemverStringResult{ .index = all_semvers.find("3.0.0-x.Y.Z.92"), .length = 14, .semver = FL::SemverFromString("3.0.0-x.Y.Z.92").semver },
        });
    }
}

TEST_CASE("Comparison operators", "[FL::Semver]")
{
	auto check = [](FL::Semver lhs, FL::Semver rhs)
	{
		auto lhs_mod = lhs;
		lhs_mod.meta = "this_is_ignored";
		CHECK(lhs == lhs);
		CHECK(lhs == lhs_mod);
		CHECK(lhs != rhs);
		CHECK(lhs < rhs);
		CHECK(lhs <= rhs);
		CHECK(rhs > lhs);
		CHECK(rhs >= lhs);
	};
	auto check_false = [&](FL::Semver lhs, FL::Semver rhs)
	{
		return check(rhs, lhs);
	};
	// Permutations on the wrong side.
    check_false(FL::Semver{0,0,1}, FL::Semver{0,0,0});
    check_false(FL::Semver{0,1,0}, FL::Semver{0,0,0});
    check_false(FL::Semver{0,1,1}, FL::Semver{0,0,0});
    check_false(FL::Semver{1,0,0}, FL::Semver{0,0,0});
    check_false(FL::Semver{1,0,1}, FL::Semver{0,0,0});
    check_false(FL::Semver{1,1,0}, FL::Semver{0,0,0});
    check_false(FL::Semver{1,1,1}, FL::Semver{0,0,0});

	// Permutations on the correct side.
    check(FL::Semver{0,0,0}, FL::Semver{0,0,1});
    check(FL::Semver{0,0,0}, FL::Semver{0,1,0});
    check(FL::Semver{0,0,0}, FL::Semver{0,1,1});
    check(FL::Semver{0,0,0}, FL::Semver{1,0,0});
    check(FL::Semver{0,0,0}, FL::Semver{1,0,1});
    check(FL::Semver{0,0,0}, FL::Semver{1,1,0});
    check(FL::Semver{0,0,0}, FL::Semver{1,1,1});

	// Permutations on the prerelease.
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{0,0,1, ".beta"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{0,1,0, ".beta"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{0,1,1, ".beta"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{1,0,0, ".beta"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{1,0,1, ".beta"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{1,1,0, ".beta"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{1,1,1, ".beta"});
    check_false(FL::Semver{2,0,0, ".beta"}, FL::Semver{0,0,1, ".alpha"});
    check_false(FL::Semver{2,0,0, ".beta"}, FL::Semver{0,1,0, ".alpha"});
    check_false(FL::Semver{2,0,0, ".beta"}, FL::Semver{0,1,1, ".alpha"});
    check_false(FL::Semver{2,0,0, ".beta"}, FL::Semver{1,0,0, ".alpha"});
    check_false(FL::Semver{2,0,0, ".beta"}, FL::Semver{1,0,1, ".alpha"});
    check_false(FL::Semver{2,0,0, ".beta"}, FL::Semver{1,1,0, ".alpha"});
    check_false(FL::Semver{2,0,0, ".beta"}, FL::Semver{1,1,1, ".alpha"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{0,0,1, ".beta"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{0,1,0, ".beta"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{0,1,1, ".beta"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,0,0, ".beta"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,0,1, ".beta"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,1,0, ".beta"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,1,1, ".beta"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{0,0,1, ".alpha"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{0,1,0, ".alpha"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{0,1,1, ".alpha"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,0,0, ".alpha"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,0,1, ".alpha"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,1,0, ".alpha"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,1,1, ".alpha"});
    check_false(FL::Semver{2,0,0, ".alpha.alpha"}, FL::Semver{0,0,1, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha.alpha"}, FL::Semver{0,1,0, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha.alpha"}, FL::Semver{0,1,1, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha.alpha"}, FL::Semver{1,0,0, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha.alpha"}, FL::Semver{1,0,1, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha.alpha"}, FL::Semver{1,1,0, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha.alpha"}, FL::Semver{1,1,1, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".beta.alpha"}, FL::Semver{0,0,1, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".beta.alpha"}, FL::Semver{0,1,0, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".beta.alpha"}, FL::Semver{0,1,1, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".beta.alpha"}, FL::Semver{1,0,0, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".beta.alpha"}, FL::Semver{1,0,1, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".beta.alpha"}, FL::Semver{1,1,0, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".beta.alpha"}, FL::Semver{1,1,1, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{0,0,1, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{0,1,0, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{0,1,1, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{1,0,0, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{1,0,1, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{1,1,0, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{1,1,1, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{0,0,1, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{0,1,0, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{0,1,1, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{1,0,0, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{1,0,1, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{1,1,0, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{1,1,1, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha.10"}, FL::Semver{0,0,1, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".alpha.10"}, FL::Semver{0,1,0, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".alpha.10"}, FL::Semver{0,1,1, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".alpha.10"}, FL::Semver{1,0,0, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".alpha.10"}, FL::Semver{1,0,1, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".alpha.10"}, FL::Semver{1,1,0, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".alpha.10"}, FL::Semver{1,1,1, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".beta.10"}, FL::Semver{0,0,1, ".alpha.8"});
    check_false(FL::Semver{2,0,0, ".beta.10"}, FL::Semver{0,1,0, ".alpha.8"});
    check_false(FL::Semver{2,0,0, ".beta.10"}, FL::Semver{0,1,1, ".alpha.8"});
    check_false(FL::Semver{2,0,0, ".beta.10"}, FL::Semver{1,0,0, ".alpha.8"});
    check_false(FL::Semver{2,0,0, ".beta.10"}, FL::Semver{1,0,1, ".alpha.8"});
    check_false(FL::Semver{2,0,0, ".beta.10"}, FL::Semver{1,1,0, ".alpha.8"});
    check_false(FL::Semver{2,0,0, ".beta.10"}, FL::Semver{1,1,1, ".alpha.8"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{0,0,1, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{0,1,0, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{0,1,1, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{1,0,0, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{1,0,1, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{1,1,0, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{1,1,1, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{0,0,1, ".alpha.8"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{0,1,0, ".alpha.8"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{0,1,1, ".alpha.8"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{1,0,0, ".alpha.8"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{1,0,1, ".alpha.8"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{1,1,0, ".alpha.8"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{1,1,1, ".alpha.8"});
    check_false(FL::Semver{2,0,0}, FL::Semver{0,0,1, ".beta"});
    check_false(FL::Semver{2,0,0}, FL::Semver{0,1,0, ".beta"});
    check_false(FL::Semver{2,0,0}, FL::Semver{0,1,1, ".beta"});
    check_false(FL::Semver{2,0,0}, FL::Semver{1,0,0, ".beta"});
    check_false(FL::Semver{2,0,0}, FL::Semver{1,0,1, ".beta"});
    check_false(FL::Semver{2,0,0}, FL::Semver{1,1,0, ".beta"});
    check_false(FL::Semver{2,0,0}, FL::Semver{1,1,1, ".beta"});
    check_false(FL::Semver{2,0,0}, FL::Semver{0,0,1, ".alpha"});
    check_false(FL::Semver{2,0,0}, FL::Semver{0,1,0, ".alpha"});
    check_false(FL::Semver{2,0,0}, FL::Semver{0,1,1, ".alpha"});
    check_false(FL::Semver{2,0,0}, FL::Semver{1,0,0, ".alpha"});
    check_false(FL::Semver{2,0,0}, FL::Semver{1,0,1, ".alpha"});
    check_false(FL::Semver{2,0,0}, FL::Semver{1,1,0, ".alpha"});
    check_false(FL::Semver{2,0,0}, FL::Semver{1,1,1, ".alpha"});
    check_false(FL::Semver{2,0,0}, FL::Semver{0,0,1, ".beta"});
    check_false(FL::Semver{2,0,0}, FL::Semver{0,1,0, ".beta"});
    check_false(FL::Semver{2,0,0}, FL::Semver{0,1,1, ".beta"});
    check_false(FL::Semver{2,0,0}, FL::Semver{1,0,0, ".beta"});
    check_false(FL::Semver{2,0,0}, FL::Semver{1,0,1, ".beta"});
    check_false(FL::Semver{2,0,0}, FL::Semver{1,1,0, ".beta"});
    check_false(FL::Semver{2,0,0}, FL::Semver{1,1,1, ".beta"});
    check_false(FL::Semver{2,0,0}, FL::Semver{0,0,1, ".alpha"});
    check_false(FL::Semver{2,0,0}, FL::Semver{0,1,0, ".alpha"});
    check_false(FL::Semver{2,0,0}, FL::Semver{0,1,1, ".alpha"});
    check_false(FL::Semver{2,0,0}, FL::Semver{1,0,0, ".alpha"});
    check_false(FL::Semver{2,0,0}, FL::Semver{1,0,1, ".alpha"});
    check_false(FL::Semver{2,0,0}, FL::Semver{1,1,0, ".alpha"});
    check_false(FL::Semver{2,0,0}, FL::Semver{1,1,1, ".alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{0,0,1, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{0,1,0, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{0,1,1, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{1,0,0, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{1,0,1, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{1,1,0, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{1,1,1, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{0,0,1, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{0,1,0, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{0,1,1, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{1,0,0, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{1,0,1, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{1,1,0, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{1,1,1, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{0,0,1, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{0,1,0, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{0,1,1, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{1,0,0, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{1,0,1, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{1,1,0, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{1,1,1, ".beta.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{0,0,1, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{0,1,0, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{0,1,1, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{1,0,0, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{1,0,1, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{1,1,0, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{1,1,1, ".alpha.alpha"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{0,0,1, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{0,1,0, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{0,1,1, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,0,0, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,0,1, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,1,0, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,1,1, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{0,0,1, ".alpha.8"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{0,1,0, ".alpha.8"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{0,1,1, ".alpha.8"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,0,0, ".alpha.8"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,0,1, ".alpha.8"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,1,0, ".alpha.8"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,1,1, ".alpha.8"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{0,0,1, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{0,1,0, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{0,1,1, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,0,0, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,0,1, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,1,0, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,1,1, ".beta.8"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{0,0,1, ".alpha.8"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{0,1,0, ".alpha.8"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{0,1,1, ".alpha.8"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,0,0, ".alpha.8"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,0,1, ".alpha.8"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,1,0, ".alpha.8"});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,1,1, ".alpha.8"});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{0,0,1});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{0,1,0});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{0,1,1});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{1,0,0});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{1,0,1});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{1,1,0});
    check_false(FL::Semver{2,0,0, ".alpha"}, FL::Semver{1,1,1});
    check_false(FL::Semver{2,0,0, ".beta"}, FL::Semver{0,0,1});
    check_false(FL::Semver{2,0,0, ".beta"}, FL::Semver{0,1,0});
    check_false(FL::Semver{2,0,0, ".beta"}, FL::Semver{0,1,1});
    check_false(FL::Semver{2,0,0, ".beta"}, FL::Semver{1,0,0});
    check_false(FL::Semver{2,0,0, ".beta"}, FL::Semver{1,0,1});
    check_false(FL::Semver{2,0,0, ".beta"}, FL::Semver{1,1,0});
    check_false(FL::Semver{2,0,0, ".beta"}, FL::Semver{1,1,1});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{0,0,1});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{0,1,0});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{0,1,1});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,0,0});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,0,1});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,1,0});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,1,1});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{0,0,1});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{0,1,0});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{0,1,1});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,0,0});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,0,1});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,1,0});
    check_false(FL::Semver{2,0,0, ".10"}, FL::Semver{1,1,1});
    check_false(FL::Semver{2,0,0, ".alpha.alpha"}, FL::Semver{0,0,1, ".beta"});
    check_false(FL::Semver{2,0,0, ".alpha.alpha"}, FL::Semver{0,1,0, ".beta"});
    check_false(FL::Semver{2,0,0, ".alpha.alpha"}, FL::Semver{0,1,1, ".beta"});
    check_false(FL::Semver{2,0,0, ".alpha.alpha"}, FL::Semver{1,0,0, ".beta"});
    check_false(FL::Semver{2,0,0, ".alpha.alpha"}, FL::Semver{1,0,1, ".beta"});
    check_false(FL::Semver{2,0,0, ".alpha.alpha"}, FL::Semver{1,1,0, ".beta"});
    check_false(FL::Semver{2,0,0, ".alpha.alpha"}, FL::Semver{1,1,1, ".beta"});
    check_false(FL::Semver{2,0,0, ".beta.alpha"}, FL::Semver{0,0,1, ".alpha"});
    check_false(FL::Semver{2,0,0, ".beta.alpha"}, FL::Semver{0,1,0, ".alpha"});
    check_false(FL::Semver{2,0,0, ".beta.alpha"}, FL::Semver{0,1,1, ".alpha"});
    check_false(FL::Semver{2,0,0, ".beta.alpha"}, FL::Semver{1,0,0, ".alpha"});
    check_false(FL::Semver{2,0,0, ".beta.alpha"}, FL::Semver{1,0,1, ".alpha"});
    check_false(FL::Semver{2,0,0, ".beta.alpha"}, FL::Semver{1,1,0, ".alpha"});
    check_false(FL::Semver{2,0,0, ".beta.alpha"}, FL::Semver{1,1,1, ".alpha"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{0,0,1, ".beta"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{0,1,0, ".beta"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{0,1,1, ".beta"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{1,0,0, ".beta"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{1,0,1, ".beta"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{1,1,0, ".beta"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{1,1,1, ".beta"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{0,0,1, ".alpha"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{0,1,0, ".alpha"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{0,1,1, ".alpha"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{1,0,0, ".alpha"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{1,0,1, ".alpha"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{1,1,0, ".alpha"});
    check_false(FL::Semver{2,0,0, ".10.alpha"}, FL::Semver{1,1,1, ".alpha"});
    check_false(FL::Semver{2,0,0, ".alpha.10"}, FL::Semver{0,0,1, ".beta"});
    check_false(FL::Semver{2,0,0, ".alpha.10"}, FL::Semver{0,1,0, ".beta"});
    check_false(FL::Semver{2,0,0, ".alpha.10"}, FL::Semver{0,1,1, ".beta"});
    check_false(FL::Semver{2,0,0, ".alpha.10"}, FL::Semver{1,0,0, ".beta"});
    check_false(FL::Semver{2,0,0, ".alpha.10"}, FL::Semver{1,0,1, ".beta"});
    check_false(FL::Semver{2,0,0, ".alpha.10"}, FL::Semver{1,1,0, ".beta"});
    check_false(FL::Semver{2,0,0, ".alpha.10"}, FL::Semver{1,1,1, ".beta"});
    check_false(FL::Semver{2,0,0, ".beta.10"}, FL::Semver{0,0,1, ".alpha"});
    check_false(FL::Semver{2,0,0, ".beta.10"}, FL::Semver{0,1,0, ".alpha"});
    check_false(FL::Semver{2,0,0, ".beta.10"}, FL::Semver{0,1,1, ".alpha"});
    check_false(FL::Semver{2,0,0, ".beta.10"}, FL::Semver{1,0,0, ".alpha"});
    check_false(FL::Semver{2,0,0, ".beta.10"}, FL::Semver{1,0,1, ".alpha"});
    check_false(FL::Semver{2,0,0, ".beta.10"}, FL::Semver{1,1,0, ".alpha"});
    check_false(FL::Semver{2,0,0, ".beta.10"}, FL::Semver{1,1,1, ".alpha"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{0,0,1, ".beta"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{0,1,0, ".beta"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{0,1,1, ".beta"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{1,0,0, ".beta"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{1,0,1, ".beta"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{1,1,0, ".beta"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{1,1,1, ".beta"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{0,0,1, ".alpha"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{0,1,0, ".alpha"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{0,1,1, ".alpha"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{1,0,0, ".alpha"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{1,0,1, ".alpha"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{1,1,0, ".alpha"});
    check_false(FL::Semver{2,0,0, ".10.10"}, FL::Semver{1,1,1, ".alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{0,0,1, ".beta"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{0,1,0, ".beta"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{0,1,1, ".beta"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{1,0,0, ".beta"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{1,0,1, ".beta"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{1,1,0, ".beta"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{1,1,1, ".beta"});
    check(FL::Semver{0,0,0, ".beta"}, FL::Semver{0,0,1, ".alpha"});
    check(FL::Semver{0,0,0, ".beta"}, FL::Semver{0,1,0, ".alpha"});
    check(FL::Semver{0,0,0, ".beta"}, FL::Semver{0,1,1, ".alpha"});
    check(FL::Semver{0,0,0, ".beta"}, FL::Semver{1,0,0, ".alpha"});
    check(FL::Semver{0,0,0, ".beta"}, FL::Semver{1,0,1, ".alpha"});
    check(FL::Semver{0,0,0, ".beta"}, FL::Semver{1,1,0, ".alpha"});
    check(FL::Semver{0,0,0, ".beta"}, FL::Semver{1,1,1, ".alpha"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{0,0,1, ".beta"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{0,1,0, ".beta"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{0,1,1, ".beta"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,0,0, ".beta"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,0,1, ".beta"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,1,0, ".beta"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,1,1, ".beta"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{0,0,1, ".alpha"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{0,1,0, ".alpha"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{0,1,1, ".alpha"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,0,0, ".alpha"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,0,1, ".alpha"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,1,0, ".alpha"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,1,1, ".alpha"});
    check(FL::Semver{0,0,0, ".alpha.alpha"}, FL::Semver{0,0,1, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".alpha.alpha"}, FL::Semver{0,1,0, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".alpha.alpha"}, FL::Semver{0,1,1, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".alpha.alpha"}, FL::Semver{1,0,0, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".alpha.alpha"}, FL::Semver{1,0,1, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".alpha.alpha"}, FL::Semver{1,1,0, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".alpha.alpha"}, FL::Semver{1,1,1, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".beta.alpha"}, FL::Semver{0,0,1, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".beta.alpha"}, FL::Semver{0,1,0, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".beta.alpha"}, FL::Semver{0,1,1, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".beta.alpha"}, FL::Semver{1,0,0, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".beta.alpha"}, FL::Semver{1,0,1, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".beta.alpha"}, FL::Semver{1,1,0, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".beta.alpha"}, FL::Semver{1,1,1, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{0,0,1, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{0,1,0, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{0,1,1, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{1,0,0, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{1,0,1, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{1,1,0, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{1,1,1, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{0,0,1, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{0,1,0, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{0,1,1, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{1,0,0, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{1,0,1, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{1,1,0, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{1,1,1, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".alpha.10"}, FL::Semver{0,0,1, ".beta.8"});
    check(FL::Semver{0,0,0, ".alpha.10"}, FL::Semver{0,1,0, ".beta.8"});
    check(FL::Semver{0,0,0, ".alpha.10"}, FL::Semver{0,1,1, ".beta.8"});
    check(FL::Semver{0,0,0, ".alpha.10"}, FL::Semver{1,0,0, ".beta.8"});
    check(FL::Semver{0,0,0, ".alpha.10"}, FL::Semver{1,0,1, ".beta.8"});
    check(FL::Semver{0,0,0, ".alpha.10"}, FL::Semver{1,1,0, ".beta.8"});
    check(FL::Semver{0,0,0, ".alpha.10"}, FL::Semver{1,1,1, ".beta.8"});
    check(FL::Semver{0,0,0, ".beta.10"}, FL::Semver{0,0,1, ".alpha.8"});
    check(FL::Semver{0,0,0, ".beta.10"}, FL::Semver{0,1,0, ".alpha.8"});
    check(FL::Semver{0,0,0, ".beta.10"}, FL::Semver{0,1,1, ".alpha.8"});
    check(FL::Semver{0,0,0, ".beta.10"}, FL::Semver{1,0,0, ".alpha.8"});
    check(FL::Semver{0,0,0, ".beta.10"}, FL::Semver{1,0,1, ".alpha.8"});
    check(FL::Semver{0,0,0, ".beta.10"}, FL::Semver{1,1,0, ".alpha.8"});
    check(FL::Semver{0,0,0, ".beta.10"}, FL::Semver{1,1,1, ".alpha.8"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{0,0,1, ".beta.8"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{0,1,0, ".beta.8"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{0,1,1, ".beta.8"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{1,0,0, ".beta.8"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{1,0,1, ".beta.8"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{1,1,0, ".beta.8"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{1,1,1, ".beta.8"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{0,0,1, ".alpha.8"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{0,1,0, ".alpha.8"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{0,1,1, ".alpha.8"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{1,0,0, ".alpha.8"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{1,0,1, ".alpha.8"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{1,1,0, ".alpha.8"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{1,1,1, ".alpha.8"});
    check(FL::Semver{0,0,0}, FL::Semver{0,0,1, ".beta"});
    check(FL::Semver{0,0,0}, FL::Semver{0,1,0, ".beta"});
    check(FL::Semver{0,0,0}, FL::Semver{0,1,1, ".beta"});
    check(FL::Semver{0,0,0}, FL::Semver{1,0,0, ".beta"});
    check(FL::Semver{0,0,0}, FL::Semver{1,0,1, ".beta"});
    check(FL::Semver{0,0,0}, FL::Semver{1,1,0, ".beta"});
    check(FL::Semver{0,0,0}, FL::Semver{1,1,1, ".beta"});
    check(FL::Semver{0,0,0}, FL::Semver{0,0,1, ".alpha"});
    check(FL::Semver{0,0,0}, FL::Semver{0,1,0, ".alpha"});
    check(FL::Semver{0,0,0}, FL::Semver{0,1,1, ".alpha"});
    check(FL::Semver{0,0,0}, FL::Semver{1,0,0, ".alpha"});
    check(FL::Semver{0,0,0}, FL::Semver{1,0,1, ".alpha"});
    check(FL::Semver{0,0,0}, FL::Semver{1,1,0, ".alpha"});
    check(FL::Semver{0,0,0}, FL::Semver{1,1,1, ".alpha"});
    check(FL::Semver{0,0,0}, FL::Semver{0,0,1, ".beta"});
    check(FL::Semver{0,0,0}, FL::Semver{0,1,0, ".beta"});
    check(FL::Semver{0,0,0}, FL::Semver{0,1,1, ".beta"});
    check(FL::Semver{0,0,0}, FL::Semver{1,0,0, ".beta"});
    check(FL::Semver{0,0,0}, FL::Semver{1,0,1, ".beta"});
    check(FL::Semver{0,0,0}, FL::Semver{1,1,0, ".beta"});
    check(FL::Semver{0,0,0}, FL::Semver{1,1,1, ".beta"});
    check(FL::Semver{0,0,0}, FL::Semver{0,0,1, ".alpha"});
    check(FL::Semver{0,0,0}, FL::Semver{0,1,0, ".alpha"});
    check(FL::Semver{0,0,0}, FL::Semver{0,1,1, ".alpha"});
    check(FL::Semver{0,0,0}, FL::Semver{1,0,0, ".alpha"});
    check(FL::Semver{0,0,0}, FL::Semver{1,0,1, ".alpha"});
    check(FL::Semver{0,0,0}, FL::Semver{1,1,0, ".alpha"});
    check(FL::Semver{0,0,0}, FL::Semver{1,1,1, ".alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{0,0,1, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{0,1,0, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{0,1,1, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{1,0,0, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{1,0,1, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{1,1,0, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{1,1,1, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{0,0,1, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{0,1,0, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{0,1,1, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{1,0,0, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{1,0,1, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{1,1,0, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{1,1,1, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{0,0,1, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{0,1,0, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{0,1,1, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{1,0,0, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{1,0,1, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{1,1,0, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{1,1,1, ".beta.alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{0,0,1, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{0,1,0, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{0,1,1, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{1,0,0, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{1,0,1, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{1,1,0, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{1,1,1, ".alpha.alpha"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{0,0,1, ".beta.8"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{0,1,0, ".beta.8"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{0,1,1, ".beta.8"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,0,0, ".beta.8"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,0,1, ".beta.8"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,1,0, ".beta.8"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,1,1, ".beta.8"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{0,0,1, ".alpha.8"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{0,1,0, ".alpha.8"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{0,1,1, ".alpha.8"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,0,0, ".alpha.8"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,0,1, ".alpha.8"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,1,0, ".alpha.8"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,1,1, ".alpha.8"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{0,0,1, ".beta.8"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{0,1,0, ".beta.8"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{0,1,1, ".beta.8"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,0,0, ".beta.8"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,0,1, ".beta.8"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,1,0, ".beta.8"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,1,1, ".beta.8"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{0,0,1, ".alpha.8"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{0,1,0, ".alpha.8"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{0,1,1, ".alpha.8"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,0,0, ".alpha.8"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,0,1, ".alpha.8"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,1,0, ".alpha.8"});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,1,1, ".alpha.8"});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{0,0,1});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{0,1,0});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{0,1,1});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{1,0,0});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{1,0,1});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{1,1,0});
    check(FL::Semver{0,0,0, ".alpha"}, FL::Semver{1,1,1});
    check(FL::Semver{0,0,0, ".beta"}, FL::Semver{0,0,1});
    check(FL::Semver{0,0,0, ".beta"}, FL::Semver{0,1,0});
    check(FL::Semver{0,0,0, ".beta"}, FL::Semver{0,1,1});
    check(FL::Semver{0,0,0, ".beta"}, FL::Semver{1,0,0});
    check(FL::Semver{0,0,0, ".beta"}, FL::Semver{1,0,1});
    check(FL::Semver{0,0,0, ".beta"}, FL::Semver{1,1,0});
    check(FL::Semver{0,0,0, ".beta"}, FL::Semver{1,1,1});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{0,0,1});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{0,1,0});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{0,1,1});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,0,0});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,0,1});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,1,0});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,1,1});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{0,0,1});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{0,1,0});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{0,1,1});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,0,0});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,0,1});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,1,0});
    check(FL::Semver{0,0,0, ".10"}, FL::Semver{1,1,1});
    check(FL::Semver{0,0,0, ".alpha.alpha"}, FL::Semver{0,0,1, ".beta"});
    check(FL::Semver{0,0,0, ".alpha.alpha"}, FL::Semver{0,1,0, ".beta"});
    check(FL::Semver{0,0,0, ".alpha.alpha"}, FL::Semver{0,1,1, ".beta"});
    check(FL::Semver{0,0,0, ".alpha.alpha"}, FL::Semver{1,0,0, ".beta"});
    check(FL::Semver{0,0,0, ".alpha.alpha"}, FL::Semver{1,0,1, ".beta"});
    check(FL::Semver{0,0,0, ".alpha.alpha"}, FL::Semver{1,1,0, ".beta"});
    check(FL::Semver{0,0,0, ".alpha.alpha"}, FL::Semver{1,1,1, ".beta"});
    check(FL::Semver{0,0,0, ".beta.alpha"}, FL::Semver{0,0,1, ".alpha"});
    check(FL::Semver{0,0,0, ".beta.alpha"}, FL::Semver{0,1,0, ".alpha"});
    check(FL::Semver{0,0,0, ".beta.alpha"}, FL::Semver{0,1,1, ".alpha"});
    check(FL::Semver{0,0,0, ".beta.alpha"}, FL::Semver{1,0,0, ".alpha"});
    check(FL::Semver{0,0,0, ".beta.alpha"}, FL::Semver{1,0,1, ".alpha"});
    check(FL::Semver{0,0,0, ".beta.alpha"}, FL::Semver{1,1,0, ".alpha"});
    check(FL::Semver{0,0,0, ".beta.alpha"}, FL::Semver{1,1,1, ".alpha"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{0,0,1, ".beta"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{0,1,0, ".beta"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{0,1,1, ".beta"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{1,0,0, ".beta"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{1,0,1, ".beta"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{1,1,0, ".beta"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{1,1,1, ".beta"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{0,0,1, ".alpha"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{0,1,0, ".alpha"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{0,1,1, ".alpha"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{1,0,0, ".alpha"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{1,0,1, ".alpha"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{1,1,0, ".alpha"});
    check(FL::Semver{0,0,0, ".10.alpha"}, FL::Semver{1,1,1, ".alpha"});
    check(FL::Semver{0,0,0, ".alpha.10"}, FL::Semver{0,0,1, ".beta"});
    check(FL::Semver{0,0,0, ".alpha.10"}, FL::Semver{0,1,0, ".beta"});
    check(FL::Semver{0,0,0, ".alpha.10"}, FL::Semver{0,1,1, ".beta"});
    check(FL::Semver{0,0,0, ".alpha.10"}, FL::Semver{1,0,0, ".beta"});
    check(FL::Semver{0,0,0, ".alpha.10"}, FL::Semver{1,0,1, ".beta"});
    check(FL::Semver{0,0,0, ".alpha.10"}, FL::Semver{1,1,0, ".beta"});
    check(FL::Semver{0,0,0, ".alpha.10"}, FL::Semver{1,1,1, ".beta"});
    check(FL::Semver{0,0,0, ".beta.10"}, FL::Semver{0,0,1, ".alpha"});
    check(FL::Semver{0,0,0, ".beta.10"}, FL::Semver{0,1,0, ".alpha"});
    check(FL::Semver{0,0,0, ".beta.10"}, FL::Semver{0,1,1, ".alpha"});
    check(FL::Semver{0,0,0, ".beta.10"}, FL::Semver{1,0,0, ".alpha"});
    check(FL::Semver{0,0,0, ".beta.10"}, FL::Semver{1,0,1, ".alpha"});
    check(FL::Semver{0,0,0, ".beta.10"}, FL::Semver{1,1,0, ".alpha"});
    check(FL::Semver{0,0,0, ".beta.10"}, FL::Semver{1,1,1, ".alpha"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{0,0,1, ".beta"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{0,1,0, ".beta"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{0,1,1, ".beta"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{1,0,0, ".beta"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{1,0,1, ".beta"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{1,1,0, ".beta"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{1,1,1, ".beta"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{0,0,1, ".alpha"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{0,1,0, ".alpha"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{0,1,1, ".alpha"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{1,0,0, ".alpha"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{1,0,1, ".alpha"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{1,1,0, ".alpha"});
    check(FL::Semver{0,0,0, ".10.10"}, FL::Semver{1,1,1, ".alpha"});
    check(FL::Semver{1,1,1, "beta"}, FL::Semver{1,1,1});
    check(FL::Semver{1,1,1, "beta"}, FL::Semver{1,1,1, "gamma"});
    check(FL::Semver{1,1,1, "beta.beta"}, FL::Semver{1,1,1, "gamma"});
    check(FL::Semver{1,1,1, "beta.beta"}, FL::Semver{1,1,1, "gamma.gamma"});
    check(FL::Semver{1,1,1, "beta.beta"}, FL::Semver{1,1,1, "beta.gamma"});
    check(FL::Semver{1,1,1, "beta"}, FL::Semver{1,1,1, "beta.10"});
    check(FL::Semver{1,1,1, "alpha"}, FL::Semver{1,1,1, "beta.alpha"});
    check(FL::Semver{1,1,1, "beta"}, FL::Semver{1,1,1, "beta.beta"});
    check(FL::Semver{1,1,1, "beta.9"}, FL::Semver{1,1,1, "beta.11"});
	check(FL::Semver{1,1,1, "1"}, FL::Semver{1,1,1, "a"});
    check(FL::Semver{1,1,1, "abc"}, FL::Semver{1,1,1, "abd"});
    check(FL::Semver{1,0,0, "beta"}, FL::Semver{1,0,0, "rc"});
	check(FL::Semver{1,0,0, "alpha"}, FL::Semver{1,0,0, "beta"});
	check(FL::Semver{1,0,0, "10"}, FL::Semver{1,0,0, "beta"});
	check(FL::Semver{1,0,0, "alpha.alpha"}, FL::Semver{1,0,0, "alpha.beta"});
	check(FL::Semver{1,0,0, "alpha.2"}, FL::Semver{1,0,0, "alpha.20"});
	check(FL::Semver{1,0,0, "alpha.1"}, FL::Semver{1,0,0, "alpha.alpha"});
	check(FL::Semver{1,0,0, "10"}, FL::Semver{1,0,0, "beta"});
	check(FL::Semver{1,0,0, "10"}, FL::Semver{1,0,0});
}