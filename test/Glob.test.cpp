#include "FL/Glob.hpp"
#include "IoStreamOverloads.hpp"
#include "Overloads.hpp"
#include "catch2/catch_all.hpp"
#include <source_location>

TEST_CASE("Glob can detect standard patterns", "[FL]")
{
	using R = std::vector<std::u8string>;

	auto check_parse = FL::Overloads{
		[](auto && glob, auto && str, R expect, std::source_location c = std::source_location::current())
		{
			auto const g = FL::Glob{glob};
			auto const result = g.Parse(str, true);
			auto const r = R{ result.begin(), result.end() };
			INFO(c.file_name() << ":" << c.line());
			CHECK(r == expect);
		},
		[](auto && glob, auto && str, std::nullopt_t, std::source_location c = std::source_location::current())
		{
			auto const g = FL::Glob{glob};
			auto const result = g.Parse(str, true);
			INFO(c.file_name() << ":" << c.line());
			CHECK(not result.IsValid());
		}
	};
	// Check the algorithm is greedy.
	check_parse(u8"?*abc", u8"tttabc", R{u8"t", u8"tt", u8"abc"});

	// Check the algorithm is greedy.
	check_parse(u8"*?abc", u8"tttabc", R{u8"tt", u8"t", u8"abc"});

	// Nothing matches nothing.
	check_parse(u8"", u8"", R{u8""});

	// Exact match matches exact match
	check_parse(u8"a", u8"a", R{u8"a"});

	// Glob can match empty.
	check_parse(u8"*", u8"", R{u8""});

	// Double glob is pointless and equivilent to the above.
	// CHECK(check_parse(u8"**"}.Describe() == "(*)");
	check_parse(u8"**", u8"", R{u8""});

	// Glob matches whole string.
	check_parse(u8"*", u8"hello", R{u8"hello"});

	// Double glob matches whole string and is stupid.
	check_parse(u8"**", u8"hello", R{u8"hello"});

	// Glob between matches can match a length of zero.
	check_parse(u8"hello*world", u8"helloworld", R{u8"hello", u8"", u8"world"});

	// Glob between two matches can match a space.
	check_parse(u8"hello*world", u8"hello world", R{u8"hello", u8" ", u8"world"});

	// Glob between two matches can have the first match between.
	check_parse(u8"hello*world", u8"hellohelloworld", R{u8"hello", u8"hello", u8"world"});

	// Can match with glob surrounded by exact matches.
	check_parse(u8"a*b*c", u8"abc", R{u8"a", u8"", u8"b", u8"", u8"c"});

	// Glob can match when seperated matches"
	check_parse(u8"*a*b*c*", u8" a b c ", R{u8" ", u8"a", u8" ", u8"b", u8" ", u8"c", u8" "});
	check_parse(u8"a*b*c", u8"a b c", R{u8"a", u8" ", u8"b", u8" ", u8"c"});
	check_parse(u8"a*b*c", u8"a  b  c", R{u8"a", u8"  ", u8"b", u8"  ", u8"c"});

	// Can match with glob at end.
	check_parse(u8"a*b*", u8"abc", R{u8"a", u8"", u8"b", u8"c"});

	// Glob can match when seperated matches when ending in glob.
	check_parse(u8"a*b*", u8"a b c", R{u8"a", u8" ", u8"b", u8" c"});
	check_parse(u8"a*b*", u8"a  b  c", R{u8"a", u8"  ", u8"b", u8"  c"});

	// Can match with glob at start.
	check_parse(u8"*b*c", u8"abc", R{u8"a", u8"b", u8"", u8"c"});

	// Glob can match seperated matches when starting in glob.
	check_parse(u8"*b*c", u8"a b c", R{u8"a ", u8"b", u8" ", u8"c"});
	check_parse(u8"*b*c", u8"a  b  c", R{u8"a  ", u8"b", u8"  ", u8"c"});

	// Single character glob can match one character.
	check_parse(u8"?", u8"a", R{u8"a"});

	// Single character glob cannot match 2.
	check_parse(u8"?", u8"ab", std::nullopt);

	// Single character glob cannot match 0.
	check_parse(u8"?", u8"", std::nullopt);

	// Two character glob can match one character.
	check_parse(u8"??", u8"aa", R{u8"a", u8"a"});

	// Two character glob cannot match 1.
	check_parse(u8"??", u8"b", std::nullopt);

	// Two character glob cannot match 3.
	check_parse(u8"??", u8"bbb", std::nullopt);

	// Two character glob cannot match 0.
	check_parse(u8"??", u8"", std::nullopt);

	// Cannot match globbers
	check_parse(u8"a*?b*?c", u8"abc", std::nullopt);
	check_parse(u8"a*?b*?c", u8"a b c", R{u8"a", u8"", u8" ", u8"b", u8"", u8" ", u8"c"});
	check_parse(u8"a?*b?*c", u8"abc", std::nullopt);
	check_parse(u8"a?*b?*c", u8"a b c", R{u8"a", u8" ", u8"", u8"b", u8" ", u8"", u8"c"});
	check_parse(u8"a*?b*?", u8"abc", std::nullopt);
	check_parse(u8"a*?b*?", u8"a b c", R{u8"a", u8"", u8" ", u8"b", u8" ", u8"c"});
	check_parse(u8"a?*b?*", u8"abc", std::nullopt);
	check_parse(u8"a?*b?*", u8"a b c", R{u8"a", u8" ", u8"", u8"b", u8" ", u8"c"});
	check_parse(u8"*?b*?c", u8"abc", std::nullopt);
	check_parse(u8"*?b*?c", u8"a b c", R{u8"a", u8" ", u8"b", u8"", u8" ", u8"c"});
	check_parse(u8"?*b?*c", u8"abc", std::nullopt);
	check_parse(u8"?*b?*c", u8"a b c", R{u8"a", u8" ", u8"b", u8" ", u8"", u8"c"});

	CHECK(FL::Glob{u8"[1-8]"}.Describe() == "(from '1' to '8')");
	CHECK(FL::Glob{u8"[ab]"}.Describe() == "(any 'ab')");
	CHECK(FL::Glob{u8"[1-8"}.Describe() == "('[1-8')");
	CHECK(FL::Glob{u8"[ab"}.Describe() == "('[ab')");

	// Can match ranges of chars.
	check_parse(u8"[1-8]", u8"0", std::nullopt);
	check_parse(u8"[1-8]", u8"1", R{u8"1"});
	check_parse(u8"[1-8]", u8"5", R{u8"5"});
	check_parse(u8"[1-8]", u8"8", R{u8"8"});
	check_parse(u8"[1-8]", u8"9", std::nullopt);

	// Can match one of chars.
	check_parse(u8"[ab]", u8"a", R{u8"a"});
	check_parse(u8"[ab]", u8"b", R{u8"b"});
	check_parse(u8"[ab]", u8"c", std::nullopt);

	// Replace can replace parts like capture groups
	auto subs = std::array{
		FL::PatternSub{
			.index = 1,
			.value = u8"1",
		},
		FL::PatternSub{
			.index = 3,
			.value = u8"2",
		},
		FL::PatternSub{
			.index = 5,
			.value = u8"3",
		}
	};
	auto r = FL::Glob{u8"foo:*,bar:*,laa:*"}.Parse(u8"foo:10,bar:20,laa:30", true).Substitute(subs);
	CHECK(r.AsU8String() == u8"foo:1,bar:2,laa:3");

	SECTION("Bug0001", "A centre located letter combination was not recognized.")
	{
		auto case_0001 = FL::Glob{u8"*[Ff]ile*.txt"};
		CHECK(case_0001.Describe() == "(*)(any 'Ff')('ile')(*)('.txt')");
		check_parse(case_0001, u8"all-files-ignored.txt", R{u8"all-", u8"f", u8"ile", u8"s-ignored", u8".txt"});
	}
}