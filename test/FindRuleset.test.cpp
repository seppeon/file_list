#include "FL/FindRuleset.hpp"
#include "FL/FileSearch.hpp"
#include "../private/IoStreamOverloads.hpp"
#include "catch2/catch_all.hpp"
#include <filesystem>
#include <source_location>

#include <iostream>

TEST_CASE("FindFilters can filter patterns", "[FL::FindRule]")
{
	SECTION("FindFilterGlob")
	{
		auto const filter = FL::FindFilterGlob(u8"hello*world", u8":1:2");
		// Note: Glob itself is tested in @ref Glob.test.cpp
		CHECK(filter(FL::FindFilterListResult{u8"hellobigworld"}) == FL::FindFilterListResult{u8"big", u8"world"});
	}
	SECTION("FindFilterGetLines")
	{
		auto const filter = FL::FindFilterGetLines();
		CHECK(filter(FL::FindFilterListResult{u8""}) == FL::FindFilterListResult{u8""});
		CHECK(filter(FL::FindFilterListResult{u8"", u8""}) == FL::FindFilterListResult{u8"", u8""});
		CHECK(filter(FL::FindFilterListResult{u8"\n"}) == FL::FindFilterListResult{u8"", u8""});
		CHECK(filter(FL::FindFilterListResult{u8"hello\nbig\nworld"}) == FL::FindFilterListResult{u8"hello", u8"big", u8"world"});
		CHECK(filter(FL::FindFilterListResult{u8"hello\nbig\nworld", u8"cat\ndog\nfish"}) == FL::FindFilterListResult{u8"hello", u8"big", u8"world", u8"cat", u8"dog", u8"fish"});
		CHECK(filter(FL::FindFilterListResult{u8"\nhello\nbig\nworld"}) == FL::FindFilterListResult{u8"", u8"hello", u8"big", u8"world"});
		CHECK(filter(FL::FindFilterListResult{u8"\nhello\nbig\nworld\n"}) == FL::FindFilterListResult{u8"", u8"hello", u8"big", u8"world", u8""});
	}
	SECTION("FindFilterTrimSpaces")
	{
		auto const filter = FL::FindFilterTrimSpaces();
		// Note: Glob itself is tested in @ref StrOps.test.cpp
		CHECK(filter(FL::FindFilterListResult{u8"abc"}) == FL::FindFilterListResult{u8"abc"});
		CHECK(filter(FL::FindFilterListResult{u8"  abc  "}) == FL::FindFilterListResult{u8"abc"});
	}
	const auto project_path = std::filesystem::path(std::source_location::current().file_name()).parent_path().parent_path();
	const auto test_dir_path = (project_path / "include").u8string();           ///< Use known files that won't be changing in the project.
	const auto docs_dir_path = (project_path / "cmake").u8string();             ///< Use known files that won't be changing in the project.
	const auto cmake_lists_path = (project_path / "CMakeLists.txt").u8string();	///< Use known files that won't be changing in the project.
	const auto gitignore_path = (project_path / "LICENSE.txt").u8string();       ///< Use known files that won't be changing in the project.
	SECTION("FindFilterRegularFileExists")
	{
		auto const filter = FL::FindFilterRegularFileExists();
		CHECK(filter(FL::FindFilterListResult{test_dir_path, docs_dir_path, u8"crayon", cmake_lists_path, gitignore_path}) == FL::FindFilterListResult{cmake_lists_path, gitignore_path});
	}
	SECTION("FindFilterDirectoryExists")
	{
		auto const filter = FL::FindFilterDirectoryExists();
		CHECK(filter(FL::FindFilterListResult{test_dir_path, docs_dir_path, u8"crayon", cmake_lists_path, gitignore_path}) == FL::FindFilterListResult{test_dir_path, docs_dir_path});
	}
}

TEST_CASE("FindInDirs can substitute", "[FL::FindRule]")
{
	FL::FindInDirs find_in_dir{ .dirs = { u8"$clumpy", u8"c:/" } };
	auto result = find_in_dir.Sub({{ u8"$clumpy", {u8"F:/Filey",u8"K:/Wombats"} }}).dirs;
	CHECK(result == std::vector<std::u8string>{u8"F:/Filey", u8"K:/Wombats", u8"c:/"});
}

TEST_CASE("FindPipe can take executable output, and feed it into various filters", "[FL::FindRule]")
{
	auto const opt_find_gpp = FL::FileSearchForExecutable("g++");
	if (opt_find_gpp.has_value())
	{
		FL::FindPipe pipe{
			.cmds = { 
				FL::Cmd{
					.cmd = u8"echo",
					.args = {
						{ u8"" }
					},
				},
				FL::Cmd{
					.cmd = opt_find_gpp->u8string(),
					.args = {	
						{ u8"-xc++" },
						{ u8"-Wp,-v" },
						{ u8"-" },
					}
				}
			},
			.filters = {
				FL::FindFilterGlob(u8"*#include <...> search starts here:*End of search list.*", u8":2"),
				FL::FindFilterGetLines(),
				FL::FindFilterTrimSpaces(),
				FL::FindFilterDirectoryExists(),
			},
			.expect_error = true,
		};
		// We expect to be able to find standard library headers.
		auto const find_paths = pipe.Find("vector");
		REQUIRE(find_paths.has_value());
		REQUIRE(not find_paths->empty());
		CHECK(std::filesystem::exists(*find_paths));
		CHECK(std::filesystem::is_regular_file(*find_paths));
	}
	else
	{
		WARN("`g++` couldn't be found, so some tests could not be run.");
	}
	auto const opt_find_gcc = FL::FileSearchForExecutable("gcc");
	if (opt_find_gcc.has_value())
	{
		FL::FindPipe pipe{
			.cmds = { 
				FL::Cmd{
					.cmd = u8"echo",
					.args = {
						{ u8"" }
					},
				},
				FL::Cmd{
					.cmd = opt_find_gpp->u8string(),
					.args = {	
						{ u8"-xc" },
						{ u8"-Wp,-v" },
						{ u8"-" },
					}
				}
			},
			.filters = {
				FL::FindFilterGlob(u8"*#include <...> search starts here:*End of search list.*", u8":2"),
				FL::FindFilterGetLines(),
				FL::FindFilterTrimSpaces(),
				FL::FindFilterDirectoryExists(),
			},
			.expect_error = true,
		};
		// We expect to be able to find standard library headers.
		auto const find_paths = pipe.Find("stdio.h");
		REQUIRE(find_paths.has_value());
		REQUIRE(not find_paths->empty());
		CHECK(std::filesystem::exists(*find_paths));
		CHECK(std::filesystem::is_regular_file(*find_paths));
	}
	else
	{
		WARN("`gcc` couldn't be found, so some tests could not be run.");
	}
}