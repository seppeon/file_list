#include "FL/StrOps.hpp"
#include "../private/IoStreamOverloads.hpp"
#include "catch2/catch_all.hpp"

TEST_CASE("TrimSpaces can trim spaces", "[FL::StrOps]")
{
	// Test combinations of zero, one and multiple spaces on either end.
	CHECK(FL::TrimSpaces(u8"") == std::u8string_view(u8""));
	CHECK(FL::TrimSpaces(u8"a") == std::u8string_view(u8"a"));
	CHECK(FL::TrimSpaces(u8"abc") == std::u8string_view(u8"abc"));
	CHECK(FL::TrimSpaces(u8" ") == std::u8string_view(u8""));
	CHECK(FL::TrimSpaces(u8" a") == std::u8string_view(u8"a"));
	CHECK(FL::TrimSpaces(u8" abc") == std::u8string_view(u8"abc"));
	CHECK(FL::TrimSpaces(u8"  ") == std::u8string_view(u8""));
	CHECK(FL::TrimSpaces(u8"  a") == std::u8string_view(u8"a"));
	CHECK(FL::TrimSpaces(u8"  abc") == std::u8string_view(u8"abc"));
	CHECK(FL::TrimSpaces(u8"  ") == std::u8string_view(u8""));
	CHECK(FL::TrimSpaces(u8" a ") == std::u8string_view(u8"a"));
	CHECK(FL::TrimSpaces(u8" abc ") == std::u8string_view(u8"abc"));
	CHECK(FL::TrimSpaces(u8"   ") == std::u8string_view(u8""));
	CHECK(FL::TrimSpaces(u8"  a ") == std::u8string_view(u8"a"));
	CHECK(FL::TrimSpaces(u8"  abc ") == std::u8string_view(u8"abc"));
	CHECK(FL::TrimSpaces(u8"   ") == std::u8string_view(u8""));
	CHECK(FL::TrimSpaces(u8" a  ") == std::u8string_view(u8"a"));
	CHECK(FL::TrimSpaces(u8" abc  ") == std::u8string_view(u8"abc"));
	CHECK(FL::TrimSpaces(u8"    ") == std::u8string_view(u8""));
	CHECK(FL::TrimSpaces(u8"  a  ") == std::u8string_view(u8"a"));
	CHECK(FL::TrimSpaces(u8"  abc  ") == std::u8string_view(u8"abc"));

	// Test what is considered a space, with the minimum set being tab and space.
	CHECK(FL::TrimSpaces(u8" ") == std::u8string_view(u8""));
	CHECK(FL::TrimSpaces(u8"\t") == std::u8string_view(u8""));
	CHECK(FL::TrimSpaces(u8" \t") == std::u8string_view(u8""));
	CHECK(FL::TrimSpaces(u8"\t\t") == std::u8string_view(u8""));
	CHECK(FL::TrimSpaces(u8"  ") == std::u8string_view(u8""));
}