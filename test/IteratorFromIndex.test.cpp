#include "FL/IteratorFromIndex.hpp"
#include "catch2/catch_all.hpp"

namespace
{
	struct Foo
	{
		std::size_t Size() const noexcept;
		char & operator[](int i) const noexcept;
	};
}

TEST_CASE("Iterators can be made from a class that only has an index and a size", "[FL]")
{
	STATIC_REQUIRE(std::same_as<FL::IteratorFromIndex<Foo>::difference_type, int>);
	STATIC_REQUIRE(std::same_as<FL::IteratorFromIndex<Foo>::reference, char &>);
	STATIC_REQUIRE(std::same_as<FL::IteratorFromIndex<Foo>::pointer, char*>);
	STATIC_REQUIRE(std::same_as<FL::IteratorFromIndex<Foo>::pointer, char*>);
	STATIC_REQUIRE(std::same_as<FL::IteratorFromIndex<Foo>::value_type, char>);
	STATIC_REQUIRE(std::same_as<FL::IteratorFromIndex<Foo>::iterator_category, std::random_access_iterator_tag>);
}