#include "FL/PatternTarget.hpp"
#include "Overloads.hpp"
#include <catch2/catch_all.hpp>
#include <ranges>
#include <string_view>

TEST_CASE("PatternTarget can apply patterns", "[TL::PatternTarget]")
{
	auto const to_vector = [](FL::PatternTarget const & target) -> std::vector<std::u8string_view>
	{
		auto const string_transform = FL::Overloads{
			[](std::size_t index)
			{
				return std::u8string_view{&(u8"0123456789"[index]), 1};
			},
			[](std::u8string_view string)
			{
				return string;
			}
		};
		std::vector<std::u8string_view> output;
		for (auto const & part : target)
		{
			output.push_back(std::visit(string_transform, part));
		}
		return output;
	};
	SECTION("out of range isn't valid")
	{
		FL::PatternTarget target{u8"hello:3this"};
		auto part = target.Apply({
			u8"hello",
			u8"world",
		});
		CHECK(target.GetCaptureCount() == 1);
		CHECK(not part.IsValid());
		CHECK(to_vector(target) == std::vector<std::u8string_view>{u8"hello", u8"3", u8"this"});
	}
	SECTION("in range is valid (taking the first index of pattern)")
	{
		FL::PatternTarget target{u8"hello:0this"};
		auto part = target.Apply({
			u8"blue",
			u8"world",
		});
		CHECK(target.GetCaptureCount() == 1);
		CHECK(part.IsValid());
		CHECK(part.AsU8String() == u8"hellobluethis");
		CHECK(to_vector(target) == std::vector<std::u8string_view>{u8"hello", u8"0", u8"this"});
	}
	SECTION("in range is valid (taking the last index of pattern)")
	{
		FL::PatternTarget target{u8"hello:1this"};
		auto part = target.Apply({
			u8"hello",
			u8"world",
		});
		CHECK(target.GetCaptureCount() == 1);
		CHECK(part.IsValid());
		CHECK(part.AsU8String() == u8"helloworldthis");
		CHECK(to_vector(target) == std::vector<std::u8string_view>{u8"hello", u8"1", u8"this"});
	}
	SECTION("in range is valid, with mark at end (taking the first index of pattern)")
	{
		FL::PatternTarget target{u8"hello:0"};
		auto part = target.Apply({
			u8"blue",
			u8"world",
		});
		CHECK(target.GetCaptureCount() == 1);
		CHECK(part.IsValid());
		CHECK(part.AsU8String() == u8"helloblue");
		CHECK(to_vector(target) == std::vector<std::u8string_view>{u8"hello", u8"0"});
	}
	SECTION("in range is valid, with mark at end (taking the last index of pattern)")
	{
		FL::PatternTarget target{u8"hello:1"};
		auto part = target.Apply({
			u8"hello",
			u8"world",
		});
		CHECK(target.GetCaptureCount() == 1);
		CHECK(part.IsValid());
		CHECK(part.AsU8String() == u8"helloworld");
		CHECK(to_vector(target) == std::vector<std::u8string_view>{u8"hello", u8"1"});
	}
	SECTION("in range is valid, with mark at start (taking the first index of pattern)")
	{
		FL::PatternTarget target{u8":0hello"};
		auto part = target.Apply({
			u8"blue",
			u8"world",
		});
		CHECK(target.GetCaptureCount() == 1);
		CHECK(part.IsValid());
		CHECK(part.AsU8String() == u8"bluehello");
		CHECK(to_vector(target) == std::vector<std::u8string_view>{u8"0", u8"hello"});
	}
	SECTION("in range is valid, with mark at start (taking the last index of pattern)")
	{
		FL::PatternTarget target{u8":1hello"};
		auto part = target.Apply({
			u8"hello",
			u8"world",
		});
		CHECK(target.GetCaptureCount() == 1);
		CHECK(part.IsValid());
		CHECK(part.AsU8String() == u8"worldhello");
		CHECK(to_vector(target) == std::vector<std::u8string_view>{u8"1", u8"hello"});
	}
	SECTION("identity pattern target doesn't modify")
	{
		FL::PatternTarget target{u8"hello"};
		auto part = target.Apply({
			u8"hello",
			u8"world",
		});
		CHECK(part.IsValid());
		CHECK(part.AsU8String() == u8"hello");
		CHECK(to_vector(target) == std::vector<std::u8string_view>{u8"hello"});
	}
	SECTION("empty pattern target doesn't modify")
	{
		FL::PatternTarget target{u8""};
		auto part = target.Apply({
			u8"hello",
			u8"world",
		});
		CHECK(part.IsValid());
		CHECK(part.AsU8String() == u8"");
		CHECK(to_vector(target) == std::vector<std::u8string_view>{});
	}
	SECTION("mark only pattern works")
	{
		FL::PatternTarget target{u8":0"};
		auto part = target.Apply({
			u8"hello",
			u8"world",
		});
		CHECK(target.GetCaptureCount() == 1);
		CHECK(part.IsValid());
		CHECK(part.AsU8String() == u8"hello");
		CHECK(to_vector(target) == std::vector<std::u8string_view>{u8"0"});
	}
	SECTION("Multiple captures")
	{
		FL::PatternTarget target{u8":0blue:1"};
		auto part = target.Apply({
			u8"hello",
			u8"world",
		});
		CHECK(target.GetCaptureCount() == 2);
		CHECK(part.IsValid());
		CHECK(part.AsU8String() == u8"helloblueworld");
		CHECK(to_vector(target) == std::vector<std::u8string_view>{u8"0", u8"blue", u8"1"});
	}
}