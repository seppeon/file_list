include(CMakePackageConfigHelpers)

# Avoid conflicting debug and release binaries
set(CMAKE_DEBUG_POSTFIX d)

# Create the install
set(CONFIG_FILE_TEMPLATE ${CMAKE_CURRENT_SOURCE_DIR}/cmake/Config.cmake.in)
set(CONFIG_FILE ${CMAKE_CURRENT_BINARY_DIR}/FLConfig.cmake)
set(VERSION_FILE ${CMAKE_CURRENT_BINARY_DIR}/FLConfigVersion.cmake)
set(CONFIG_FILE_INSTALL_DIR ${CMAKE_INSTALL_LIBDIR}/cmake/FL)

# generate the config file that includes the exports
configure_package_config_file(
	# Input
		"${CONFIG_FILE_TEMPLATE}"
	# Output
		"${CONFIG_FILE}"
	INSTALL_DESTINATION
		"${CONFIG_FILE_INSTALL_DIR}"
)
write_basic_package_version_file(
	"${VERSION_FILE}"
	VERSION "${PROJECT_VERSION}"
	COMPATIBILITY SameMajorVersion
)
# Set the install targets
install(
	TARGETS FL FL_Private
	EXPORT FLTargets
	COMPONENT Lib
)
install(
	TARGETS FL_fls FL_fmv FL_fres
	EXPORT FLApps
	COMPONENT Apps
)
install(
	DIRECTORY "include/FL"
	TYPE INCLUDE
	COMPONENT Lib
)
install(
	DIRECTORY docs
	TYPE DOC
	COMPONENT Lib
)
install(
	EXPORT FLTargets
	FILE FLTargets.cmake
	DESTINATION "${CONFIG_FILE_INSTALL_DIR}"
	NAMESPACE FL::
	COMPONENT Lib
)
install(
	FILES
		"${CONFIG_FILE}"
    	"${VERSION_FILE}"
    DESTINATION
		"${CONFIG_FILE_INSTALL_DIR}"
	COMPONENT Lib
)