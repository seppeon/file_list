#include "Fmv/App.hpp"
#include "App/MakeTable.hpp"
#include "App/Printers.hpp"
#include "FL/FL.hpp"
#include <ranges>

namespace Fmv
{
	int MoveFiles(FL::App::CliInputOutputSettings const & input_only_settings, FL::App::CliPrinter const & printer)
	{
		auto const & common_settings = input_only_settings.common_settings;
		auto const & inputs = input_only_settings.inputs;
		auto const & outputs = input_only_settings.outputs;
		auto const & nav_settings = common_settings.glob_settings;
		auto const opt_move_list = FL::BuildGlobMoveRequestList(inputs, outputs);
		if (not opt_move_list)
		{
			printer.Error("The number of inputs must match the number of outputs.");
			return -1;
		}
		auto const table_format = common_settings.print_format;
		auto const & move_request_list = *opt_move_list;
		auto const [move_operations_list, invalid_operation_list] = FL::BuildMoveOperationList(move_request_list, nav_settings);
		if (not invalid_operation_list.empty())
		{
			printer.Warning(ToString(invalid_operation_list, table_format) + u8"\n");
		}
		if (move_operations_list.empty())
		{
			printer.Warning("No move operations were performed.");
			return -2;
		}
		if (input_only_settings.dry_run)
		{
			if (common_settings.print_format == FL::App::PrintFormat::List)
			{
				for (auto const & [src, dst] : move_operations_list)
				{
					printer.Raw("Will move:\n\t - Source: ", src, "\n\t - Destination: ", dst);
				}
			}
			else
			{
				printer.Raw(ToString(move_operations_list, table_format));
			}
		}
		else
		{
			auto const move_operations_status_list = FL::ApplyMoveOperationList(move_operations_list, input_only_settings.override_existing, input_only_settings.mkdir);
			if (common_settings.print_format == FL::App::PrintFormat::List)
			{
				for (auto const & op : move_operations_status_list)
				for (std::size_t i = 0; i < move_operations_list.size(); ++i)
				{
					auto const move_status = move_operations_status_list[i];
					auto const & [src, dst] = move_operations_list[i];
					printer.Raw("Moved:\n\t - Source: ", src, "\n\t - Destination: ", dst, "\n\t - Status: ", ToString(move_status));
				}
			}
			else
			{
				printer.Raw(ToString(move_operations_list, move_operations_status_list, table_format));
			}
		}
		return 0;
	}
}