#include "Fmv/Cli.hpp"
#include "App/App.hpp"

namespace Fmv
{
	const FL::App::CliDef cli_def = {
		.title = {
			.name = "FileMove",
			.desc = "An application used to move a set of files from to a set of destinations."
		},
		.flags = {
			FL::App::GetCommonFlags(),
			FL::App::GetInputOutputFlags(),
		},
		.key_values = {
			FL::App::GetCommonKeyVals(),
			FL::App::GetInputKeyVals("The file to move."),
			FL::App::GetOutputKeyVals("The new destination of the file."),
		}
	};
}