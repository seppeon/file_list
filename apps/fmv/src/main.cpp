#include "Fmv/Cli.hpp"
#include "Fmv/App.hpp"

int main(int argc, char const * const * argv) try
{
	auto args = FL::App::CliArgs(argc, argv);
	auto const input_output_settings = FL::App::ExtractCliInputOutputSettings(args);
	FL::App::HandleCommonSettings(input_output_settings.common_settings, args, Fmv::cli_def);
	auto const printer = FL::App::CliPrinter{input_output_settings.common_settings};
	return Fmv::MoveFiles(input_output_settings, printer);
}
catch(std::exception const & msg)
{
	std::cerr << msg.what();
	return -1;
}

#ifdef __MINGW32__
extern "C" int _CRT_glob = 0;
#endif