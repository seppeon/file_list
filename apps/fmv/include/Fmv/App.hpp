#pragma once
#include "App/App.hpp"

namespace Fmv
{
	[[nodiscard]] int MoveFiles(FL::App::CliInputOutputSettings const & input_only_settings, FL::App::CliPrinter const & printer);
}