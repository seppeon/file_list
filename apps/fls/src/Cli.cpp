#include "Fls/Cli.hpp"

namespace Fls
{
	const FL::App::CliDef cli_def{
		.title = {
			.name = "FileList",
			.desc = "This is the FileList application, it is a filename searching tool."
		},
		.flags = {
			FL::App::GetCommonFlags(),
		},
		.key_values = {
			FL::App::GetCommonKeyVals(),
		},
		.opt_args = FL::App::CliArgsPack{
			.type = "path",
			.desc = "The paths, containing globs for which to search."
		},
	};
}