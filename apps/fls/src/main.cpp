#include "Fls/Cli.hpp"
#include "Fls/App.hpp"

int main(int argc, char const * const * argv) try
{
	auto args = FL::App::CliArgs(argc, argv);
	auto const args_settings = FL::App::ExtractCliArgsSettings(args);
	FL::App::HandleCommonSettings(args_settings.common_settings, args, Fls::cli_def);
	auto const printer = FL::App::CliPrinter{args_settings.common_settings};
	Fls::ListFiles(args_settings, printer);
}
catch(std::exception const & msg)
{
	std::cerr << msg.what();
	return -1;
}

#ifdef __MINGW32__
extern "C" int _CRT_glob = 0;
#endif