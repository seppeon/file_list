#include "Fls/App.hpp"
#include "App/MakeTable.hpp"
#include "FL/GlobFiles.hpp"

namespace Fls
{
	void ListFiles(FL::App::CliArgsSettings const & input_only_settings, FL::App::CliPrinter const & printer)
	{
		auto const & common_settings = input_only_settings.common_settings;
		auto const & args = input_only_settings.args;
		for (auto const & arg : args)
		{
			printer.Status("cwd: ", std::filesystem::current_path());
			printer.Status("finding: ", arg);
			auto files = FL::GlobFiles(arg, common_settings.glob_settings);	
			printer.Status("found count: ", files.size());
			if (not files.empty())
			{
				if (common_settings.print_format == FL::App::PrintFormat::List)
				{
					for (auto const & file : files)
					{
						printer.Raw(file.path.u8string());
					}
				}
				else
				{
					printer.Raw(FL::App::ToString(files, common_settings.print_format));
				}
			}
		}
	}
}