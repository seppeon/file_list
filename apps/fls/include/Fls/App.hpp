#pragma once
#include "App/App.hpp"

namespace Fls
{
	void ListFiles(FL::App::CliArgsSettings const & input_only_settings, FL::App::CliPrinter const & printer);
}