#include "Fres/Cli.hpp"
#include <iostream>

int main(int argc, char const * const * argv) try
{
	auto args = FL::App::CliArgs(argc, argv);
	auto opt_fres_settings = Fres::ProcessFresArgs(args);
	if (not opt_fres_settings)
	{
		return -1;
	}
	auto & fres_settings = *opt_fres_settings;
	auto opt_result = FL::FindWithRuleset(fres_settings.ruleset, fres_settings.file_to_resolve);
	if (opt_result.has_value())
	{
		std::cout << *opt_result << "\n";
		return 0;
	}
	else
	{
		std::cout << "File not found.\n";
		return -1;
	}
}
catch(std::exception const & msg)
{
	std::cerr << msg.what();
	return -1;
}

#ifdef __MINGW32__
extern "C" int _CRT_glob = 0;
#endif