#include "Fres/Cli.hpp"
#include "App/CliCommonSettings.hpp"
#include "App/CliDef.hpp"
#include "FL/FindRuleset.hpp"
#include <filesystem>
#include <ranges>
#include <string_view>
#include <array>
#include <iostream>

namespace Fres
{
	namespace
	{
		const std::array<std::string_view, 2> res_file_commands{ "--input", "-I" };
		constexpr std::string_view res_rulesets_command = "--rulesets";
		constexpr std::string_view res_ruleset_name_command = "--ruleset-name";
		constexpr std::string_view res_ruleset_help_command = "--ruleset-help";

		auto GetFresRulesetKeyValHelp() -> std::vector<FL::App::CliKeyVal>
		{
			return {
				FL::App::CliKeyVal{
					.keys{ std::string(res_rulesets_command) },
					.type{ "file" },
					.desc{ "The rulesets file that contains a ruleset with the name specified by `--ruleset-name <string>`." },
					.required = true
				},
				FL::App::CliKeyVal{
					.keys{ std::string(res_ruleset_name_command) },
					.type{ "string" },
					.desc{ "The ruleset name." },
					.required = true
				},
				FL::App::CliKeyVal{
					.keys{ res_file_commands.begin(), res_file_commands.end() },
					.type{ "file" },
					.desc{ "The file to resolve." },
					.required = true
				}
			};
		}

		auto GetFresRulesetFlagsHelp() -> std::vector<FL::App::CliFlag>
		{
			return {
				FL::App::CliFlag{
					.keys{ std::string(res_ruleset_help_command) },
					.desc{ "Get the help for a particular ruleset, the provided string must be a valid ruleset name." },
				}
			};
		}

		const FL::App::CliDef static_cli_def{
			.title = {
				.name = "FileResolve",
				.desc = "An application used to resolve a file from a set of given paths (useful for finding executables and DLLs)."
			},
			.flags = {
				FL::App::GetCommonFlags(),
				GetFresRulesetFlagsHelp(),
			},
			.key_values = {
				GetFresRulesetKeyValHelp()
			}
		};
	}

	auto BuildRulesetHelp(std::string ruleset_name, std::string ruleset_desc, std::span<FL::FindArg const> find_args) -> FL::App::CliDef
	{
		auto output = FL::App::CliDef{
			.title = {
				.name = std::move(ruleset_name),
				.desc = std::move(ruleset_desc),
			}
		};
		std::vector<FL::App::CliKeyVal> new_key_vals;
		for (auto const & find_arg : find_args)
		{
			auto & ref = new_key_vals.emplace_back(FL::App::CliKeyVal{
				.keys = {},
				.type = find_arg.type.ToString(),
				.desc = std::string{ find_arg.description.begin(), find_arg.description.end() },
				.required = find_arg.required,
			});
			for (auto const & command : find_arg.commands)
			{
				ref.keys.push_back(std::string(command.begin(), command.end()));
			}
		}
		output.key_values.key_vals.insert(output.key_values.key_vals.end(), new_key_vals.begin(), new_key_vals.end());
		return output;
	}

	auto ProcessFresArgs(FL::App::CliArgs & args) -> std::optional<FresArgs>
	{
		auto split = args.SplitArgs("--");
		auto opt_input_path = split.lhs.FindKeyValAndRemove(res_file_commands);
		auto opt_rulesets_path = split.lhs.FindKeyValAndRemove(std::array<std::string_view, 1>{ res_rulesets_command });
		auto ruleset_help_command = split.lhs.FindFlagAndRemove(std::array<std::string_view, 1>{ res_ruleset_help_command });
		auto opt_ruleset_name = split.lhs.FindKeyValAndRemove(std::array<std::string_view, 1>{ res_ruleset_name_command });
		auto common_settings = FL::App::ExtractCommonSettings(split.lhs);
		if (not FL::App::HandleCommonSettings(common_settings, split.lhs, static_cli_def))
		{
			return std::nullopt;
		}
		if (not opt_rulesets_path)
		{
			throw std::runtime_error("The ruleset path is required '--ruleset <path>'.");
		}
		if (not opt_ruleset_name)
		{
			throw std::runtime_error("The name of the particular ruleset (from the rulesets file) must be provided.");
		}
		auto const ruleset_path = *opt_rulesets_path;
		auto const ruleset_name = *opt_ruleset_name;
		if (not std::filesystem::exists(ruleset_path) or not std::filesystem::is_regular_file(ruleset_path))
		{
			throw std::runtime_error("The ruleset path must exist '" + std::string(ruleset_path) + "' cannot be found.");
		}
		auto const opt_ruleset = FL::FindRulesetsFromFile(ruleset_path);
		if (not opt_ruleset)
		{
			throw std::runtime_error("The ruleset file was corrupt or didn't exist.");
		}
		auto const rulesets = FL::PreprocessRulesets(*opt_ruleset);
		auto const * const ruleset_ptr = rulesets.GetRulesetByName(std::u8string(ruleset_name.begin(), ruleset_name.end()));
		if (not ruleset_ptr)
		{
			throw std::runtime_error("The ruleset name was invalid.");
		}
		auto ruleset = *ruleset_ptr;
		if (ruleset_help_command)
		{
			std::cout << BuildRulesetHelp(std::string(ruleset_name), "A ruleset loaded from: '" + std::string(ruleset_path) + "'.", ruleset.args).GetHelp() << "\n";
			return std::nullopt;
		}
		if (not opt_input_path)
		{
			throw std::runtime_error("No input file was provided '--input <path>'.");
		}

		// Extract the arguments, as per the definition "ruleset", it has already been preprocessed, the arguments are valid and complete.
		FL::FindRulesetArgsSubstitution arg_subs;
		for (auto const & ruleset_arg : ruleset.args)
		{
			auto from_u8sv_to_sv = [](std::u8string_view r) { return std::string_view(reinterpret_cast<const char *>(r.data()), r.size()); };
			auto from_sv_to_u8sv = [](std::string_view r) { return std::u8string_view(reinterpret_cast<const char8_t *>(r.data()), r.size()); };
			auto to_string_range = ruleset_arg.commands | std::ranges::views::transform( from_u8sv_to_sv );
			std::vector<std::string_view> args{ to_string_range.begin(), to_string_range.end() };
			auto & arg_sub = arg_subs[ruleset_arg.name];
			for (;;)
			{
				auto const opt_arg = split.rhs.FindKeyValAndRemove({args.begin(), args.end()});
				if (not opt_arg) break;
				auto const & arg = *opt_arg;
				arg_sub.push_back(std::u8string(from_sv_to_u8sv(arg)));
			}
		}
		ruleset.Sub(arg_subs);

		return FresArgs{
			.common_settings = std::move(common_settings),
			.ruleset_path = std::move(*opt_rulesets_path),
			.file_to_resolve = std::move(*opt_input_path),
			.ruleset_name = std::u8string(opt_ruleset_name->begin(), opt_ruleset_name->end()),
			.ruleset_args = std::move(split.rhs),
			.ruleset = ruleset,
		};
	}
}