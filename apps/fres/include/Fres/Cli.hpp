#pragma once
#include "App/CliDef.hpp"
#include "App/CliCommonSettings.hpp"
#include "App/CliArgs.hpp"
#include "FL/FindRuleset.hpp"
#include "FL/FindWithRuleset.hpp"

namespace Fres
{
	[[nodiscard]] auto BuildRulesetHelp(std::string ruleset_name, std::string ruleset_desc, std::span<FL::FindArg const> find_args) -> FL::App::CliDef;

	struct FresArgs
	{
		FL::App::CliCommonSettings common_settings;
		std::filesystem::path ruleset_path;
		std::filesystem::path file_to_resolve;
		std::u8string ruleset_name;
		FL::App::CliArgs ruleset_args;
		FL::PreprocessedFindRuleset ruleset;
	};

	[[nodiscard]] auto ProcessFresArgs(FL::App::CliArgs & args) -> std::optional<FresArgs>;
}