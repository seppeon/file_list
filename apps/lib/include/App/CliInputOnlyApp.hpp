#pragma once
#include "App/CliCommonSettings.hpp"
#include <string>
#include <vector>

namespace FL::App
{
	struct CliInputOnlySettings
	{
		CliCommonSettings common_settings; 
		std::vector<std::u8string> inputs;
	};
	/**
	 * @brief Find the CLI arguments for an input only application and remove those from the unhandled args list.
	 * 
	 * @param args The arguments to process.
	 * @return std::nullopt on help, CliInputOnlySettings on success.
	 */
	[[nodiscard]] auto ExtractCliInputOnlySettings(CliArgs & args) -> CliInputOnlySettings;
}