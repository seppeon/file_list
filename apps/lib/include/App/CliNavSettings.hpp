#pragma once
#include "App/CliArgs.hpp"
#include "App/CliDef.hpp"
#include "FL/GlobFiles.hpp"
#include <string>
#include <optional>

namespace FL::App
{
	/**
	 * @brief Process the CLI arguments relevant to `FL::NavSettings`.
	 * 
	 * @param args The cli arguments to process (these will be mutated).
	 * @return The glob file settings, or std::nullopt if there is an error.
	 */
	[[nodiscard]] auto ExtractNavSettings(CliArgs & args) -> std::optional<FL::NavSettings>;
	/**
	 * @brief Get the help object for the `FL::NavSettings` command line arguments (as a string).
	 * 
	 * @return The help object.
	 */
	[[nodiscard]] auto GetNavFlags() -> std::vector<CliFlag>;
}