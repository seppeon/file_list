#pragma once
#include "FL/Nav.hpp"
#include "FL/MoveFiles.hpp"
#include "App/MakeTable.hpp"
#include "FL/Pattern.hpp"
#include <string>

namespace FL::App
{
	[[nodiscard]] std::u8string ToString(FL::NavList const & files, PrintFormat format);
	
	[[nodiscard]] std::u8string ToString(FL::GlobMoveRequest const & pattern, PrintFormat format);

	[[nodiscard]] std::u8string ToString(FL::Pattern const & pattern, PrintFormat format);

	[[nodiscard]] std::u8string ToString(FL::PatternTarget const & pattern_target, PrintFormat format);

	[[nodiscard]] std::u8string ToString(FL::MoveOperationList const & move_operations, PrintFormat format);

	[[nodiscard]] std::u8string ToString(FL::MoveOperationList const & move_operations, FL::MoveOperationStatusList const & move_operation_status_list, PrintFormat format);

	[[nodiscard]] std::u8string ToString(FL::PatternMismatchList const & pattern_mismatch, PrintFormat format);
}