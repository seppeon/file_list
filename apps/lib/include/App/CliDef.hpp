#pragma once
#include <string>
#include <vector>
#include <optional>

namespace FL::App
{
	struct CliFlag
	{
		std::vector<std::string> keys;
		std::string desc;
	};

	struct CliFlags
	{
		auto begin() const noexcept { return flags.begin(); }
		auto end() const noexcept { return flags.end(); }
		bool empty() const noexcept { return flags.empty(); }

		CliFlags(std::initializer_list<std::vector<CliFlag>> flag_sets)
		{
			for (auto const & flag_set : flag_sets)
			{
				flags.insert(flags.end(), flag_set.begin(), flag_set.end());
			}
		}

		std::vector<CliFlag> flags;
	};

	struct CliKeyVal
	{
		std::vector<std::string> keys;
		std::string type;
		std::string desc;
		bool required = false;
	};

	struct CliKeyVals
	{
		auto begin() const noexcept { return key_vals.begin(); }
		auto end() const noexcept { return key_vals.end(); }
		bool empty() const noexcept { return key_vals.empty(); }

		CliKeyVals(std::same_as<CliKeyVal> auto const & ... key_val_sets)
			: key_vals{ key_val_sets... }
		{}

		CliKeyVals(std::initializer_list<std::vector<CliKeyVal>> key_val_sets)
		{
			for (auto const & flag_set : key_val_sets)
			{
				key_vals.insert(key_vals.end(), flag_set.begin(), flag_set.end());
			}
		}

		std::vector<CliKeyVal> key_vals;
	};

	struct CliArgsPack
	{
		std::string type;
		std::string desc;
	};

	struct CliTitle
	{
		std::string name;
		std::string desc;
	};

	struct CliDef
	{
		CliTitle title;
		CliFlags flags;
		CliKeyVals key_values;
		std::optional<CliArgsPack> opt_args;
		/**
		 * @brief Get a printable help text.
		 * 
		 * @return The printable help text as a string.
		 */
		[[nodiscard]] std::string GetHelp() const noexcept;
	};
}