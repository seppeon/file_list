#pragma once
#include "App/CliArgs.hpp"
#include "App/CliNavSettings.hpp"
#include "App/MakeTable.hpp"
#include "FL/GlobFiles.hpp"
#include <optional>

namespace FL::App
{
	struct CliCommonSettings
	{
		FL::NavSettings glob_settings; 
		bool verbose = false;
		PrintFormat print_format = PrintFormat::AnsiColoured;
		bool print_help = false;
		bool print_version = false;
	};
	/**
	 * @brief Find the common CLI arguments and remove those from the unhandled args list.
	 * 
	 * @param args The arguments to process.
	 * @return CliCommonSettings on success.
	 */
	[[nodiscard]] auto ExtractCommonSettings(CliArgs & args) -> CliCommonSettings;
	/**
	 * @brief Get the help object for the common settings flags.
	 * 
	 * @return The help object.
	 */
	[[nodiscard]] auto GetCommonFlags() -> std::vector<CliFlag>;
	/**
	 * @brief Get the help object for the common settings key/values.
	 * 
	 * @return The help object.
	 */
	[[nodiscard]] auto GetCommonKeyVals() -> std::vector<CliKeyVal>;
	/**
	 * @brief Handles the common settings prints and exit conditions.
	 *
	 * @return Whether or not the system should keep running.
	 */
	bool HandleCommonSettings(CliCommonSettings const & common_settings, CliArgs const & args, CliDef const & cli_def);
}