#pragma once
#include "App/CliCommonSettings.hpp"
#include <string>
#include <vector>

namespace FL::App
{
	struct CliInputOutputSettings
	{
		CliCommonSettings common_settings;
		bool mkdir = false;
		bool dry_run = false;
		bool override_existing = false;
		std::vector<std::u8string> inputs;
		std::vector<std::u8string> outputs;
	};
	/**
	 * @brief Get the help file flags for the `FL::CliInputOutputSettings` structure.
	 * 
	 * @return The flags
	 */
	[[nodiscard]] auto GetInputOutputFlags() -> std::vector<CliFlag>;
	/**
	 * 
	 * @brief Find the CLI arguments for an input only application and remove those from the unhandled args list.
	 * 
	 * @param args The arguments to process.
	 * @return std::nullopt on help, CliInputOnlySettings on success.
	 */
	[[nodiscard]] auto ExtractCliInputOutputSettings(CliArgs & args) -> CliInputOutputSettings;
}