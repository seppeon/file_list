#pragma once
#include "App/CliCommonSettings.hpp"
#include "FL/StrOps.hpp"
#include <iostream>
#include <ostream>
#include <sstream>
#include <string>

inline std::ostream & operator<<(std::ostream & o, std::u8string const & s)
{
	return o<<std::string_view(reinterpret_cast<const char *>(s.data()), s.size());
}

inline std::ostream & operator<<(std::ostream & o, std::u8string_view const & s)
{
	return o<<std::string_view(reinterpret_cast<const char *>(s.data()), s.size());
}

namespace FL::App
{
	struct CliPrinter
	{
		void Raw(auto const & ... args) const
		{
			Print(false, "raw: ", args...);
		}

		void Result(auto const & ... args) const
		{
			Print(enable_verbose, "result: ", args...);
		}

		void Info(auto const & ... args) const
		{
			Print(enable_verbose, "info: ", args...);
		}

		void Status(auto const & ... args) const
		{
			if (enable_verbose)
			{
				Print(enable_verbose, "status: ", args...);
			}
		}

		void Warning(auto const & ... args) const
		{
			Print(enable_verbose, "warning: ", args...);
		}

		void Error(auto const & ... args) const
		{
			Print(enable_verbose, "error: ", args...);
		}

		CliPrinter(CliCommonSettings const & settings)
			: enable_verbose(settings.verbose)
		{}
	private:
		static void Print(bool enable_verbose, std::string_view prefix, auto const & ... args)
		{
			auto const string = ToString(args...);
			if (enable_verbose)
			{
				auto const lines = GetLines(string);
				auto const indent = std::string(prefix.size(), ' ');
				for (auto const & line : lines)
				{
					std::cout << prefix << std::string(line.begin(), line.end()) << "\n";
					prefix = indent;
				}
			}
			else
			{
				std::cout << string << "\n";
			}
		}

		static std::u8string ToString(auto const & ... args)
		{
			std::stringstream ss;
			((ss << args), ...);
			auto out = ss.str();
			return { out.begin(), out.end() };
		}
		bool enable_verbose;
	};
}