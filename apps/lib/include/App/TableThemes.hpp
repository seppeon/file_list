#pragma once
#include "App/Table.hpp"

namespace FL::App
{
	inline constexpr auto coloured_row_strip_grid_table_format = AnsiColouredRowStripGridTableFormat
	{
		.padding = 1,
		.header = {
			.format = { .bold = true },
			.text_colour = App::table_colour_white,
			.background_colour = { 0, 60, 120 },
		},
		.odd_row = {
			.format = {},
			.text_colour = { 218, 224, 224 },
			.background_colour = { 30, 30, 30 },
		},
		.even_row = {
			.format = {},
			.text_colour = { 218, 224, 224 },
			.background_colour = {20, 20, 20 },
		},
	};

	inline constexpr auto ascii_strip_table_format = AsciiStripTableFormat{
		.padding = 1,
		.header = {},
		.rows = {},
	};
}