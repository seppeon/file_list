#pragma once
#include "App/CliDef.hpp"
#include "App/CliArgs.hpp"
#include <string>
#include <optional>

namespace FL::App
{
	/**
	 * @brief Process '--input' command line options.
	 * 
	 * @param args The arguments to process (this will be mutated).
	 * @return std::nullopt when an error occurs, the input values otherwise.
	 */
	[[nodiscard]] auto ExtractInput(CliArgs & args) -> std::optional<std::u8string>;
	/**
	 * @brief Process '--input' and '--output' command line options.
	 * 
	 * @param args The arguments to process (this will be mutated).
	 * @return std::nullopt when an error occurs, the input and output values otherwise.
	 */
	[[nodiscard]] auto ExtractOutput(CliArgs & args) -> std::optional<std::u8string>;
	/**
	 * @brief Get the help text for the input output arguments.
	 *
	 * @note A KeyVal is a key value pair, like `--input file.txt`
	 * 
	 * @param input_desc The description of the input parameter.
	 * @return The help text.
	 */
	[[nodiscard]] auto GetInputKeyVals(std::string input_desc) -> std::vector<CliKeyVal>;
	/**
	 * @brief Get the help text for the input output arguments.
	 *
	 * @note A KeyVal is a key value pair, like `--input file.txt`
	 * 
	 * @param output_desc The description of the output parameter.
	 * @return The help text.
	 */
	[[nodiscard]] auto GetOutputKeyVals(std::string output_desc) -> std::vector<CliKeyVal>;
}