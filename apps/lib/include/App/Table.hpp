#pragma once
#include <cstddef>
#include <cstdint>
#include <span>
#include <string_view>
#include <vector>
#include <string>

namespace FL::App
{
	struct [[nodiscard]] TableColour
	{
		std::uint8_t r = 0;
		std::uint8_t g = 0;
		std::uint8_t b = 0;
	};

	inline constexpr auto table_colour_white = TableColour{255, 255, 255};
	inline constexpr auto table_colour_black = TableColour{0, 0, 0};

	enum class CellAlign
	{
		Left = 0,
		Right = 1,
		Center = 2,
	};

	struct [[nodiscard]] TextFormat
	{
		bool bold = false;
		bool italics = false;
		bool underlined = false;
	};

	struct [[nodiscard]] CellFormat
	{
		TextFormat format = {};
		CellAlign alignment = CellAlign::Left;
		TableColour text_colour = table_colour_white;
		TableColour background_colour = table_colour_black;
	};

	struct [[nodiscard]] TableCell
	{
		std::size_t format = 0;
		std::u8string contents = u8"";
	};

	struct [[nodiscard]] RowRenderer
	{
		/**
		 * @brief Measure the width of the contents (excluding padding).
		 * 
		 * @param value The text of the cell.
		 * @param format The format index of the cell.
		 * @return The width in characters.
		 */
		virtual std::size_t Measure(std::u8string_view const & value, std::size_t format_index) const = 0;
		/**
		 * @brief Render a cell with the given value, format and with the given width.
		 * 
		 * @param value The text of the cell.
		 * @param format The format index of the cell.
		 * @param min_width The width of the cell.
		 * @return The rendered cell.
		 */
		virtual std::u8string RenderCell(std::u8string_view value, std::size_t format_index, std::size_t min_width) const = 0;
		/**
		 * @brief Finish 
		 * 
		 * @param value The text of the cell.
		 * @return std::u8string 
		 */
		virtual std::u8string FinishRow(std::u8string_view value, std::size_t row_index, std::span<std::size_t const> col_sizes) const = 0;

		explicit RowRenderer(std::size_t padding);
		virtual ~RowRenderer();
	protected:
		std::size_t m_padding = 0;
	};

	class [[nodiscard]] Table
	{
		using cell_type = TableCell;
		using row_type = std::vector<cell_type>;
		using min_column_width_type = std::vector<std::size_t>;
		using table_type = std::vector<row_type>;

		row_type m_header;
		table_type m_rows;
		RowRenderer const * m_renderer = nullptr;

		[[nodiscard]] auto RenderRow(row_type const & row, min_column_width_type const & min_column_widths) const;

		[[nodiscard]] auto GetColumnWidths() const -> min_column_width_type;

		[[nodiscard]] virtual auto GetFormatIndex(std::size_t x, std::size_t y, CellAlign align) const noexcept -> std::size_t = 0;
	public:
		Table(std::size_t cols, std::size_t rows, RowRenderer const & renderer);
		virtual ~Table();

		void SetCell(std::size_t x, std::size_t y, cell_type value);

		void SetCell(std::size_t x, std::size_t y, std::u8string value, CellAlign align = CellAlign::Left);

		void SetCell(std::size_t x, std::size_t y, std::u8string_view value, CellAlign align = CellAlign::Left);

		void SetCell(std::size_t x, std::size_t y, std::string_view value, CellAlign align = CellAlign::Left);

		void SetCellContent(std::size_t x, std::size_t y, std::u8string contents);

		void SetColumnFormat(std::size_t col_index, std::size_t format_index);

		void SetRowFormat(std::size_t row_index, std::size_t format_index);

		[[nodiscard]] auto Render() const -> std::u8string;
	};

	struct [[nodiscard]] AnsiColourRenderer : RowRenderer
	{
		/**
		 * @brief Measure the width of the contents (excluding padding).
		 * 
		 * @param cell The cell to measure.
		 * @return The width in characters.
		 */
		std::size_t Measure(std::u8string_view const & value, std::size_t format_index) const override;

		std::u8string RenderCell(std::u8string_view value, std::size_t format_index, std::size_t width) const override;

		std::u8string FinishRow(std::u8string_view value, std::size_t row_index, std::span<std::size_t const> col_sizes) const override;

		AnsiColourRenderer(std::vector<CellFormat> formats, std::size_t padding);
	private:
		std::vector<CellFormat> m_formats;
	};
	struct [[nodiscard]] AnsiColouredRowFormat
	{
		TextFormat format;
		TableColour text_colour = table_colour_white;
		TableColour background_colour = table_colour_black;
	};
	/**
	 * | Header      | Header     |
	 * |-------------|------------|
	 * | Row Odd     | Row Odd    |
	 * | Row Even    | Row Even   |
	 * | Row Odd     | Row Odd    |
	 */
	struct [[nodiscard]] AnsiColouredRowStripGridTableFormat
	{
		std::size_t padding;
		AnsiColouredRowFormat header;
		AnsiColouredRowFormat odd_row;
		AnsiColouredRowFormat even_row;
	};
	struct [[nodiscard]] AnsiColouredRowStripGridTable : Table
	{
		AnsiColouredRowStripGridTable(std::size_t cols, std::size_t rows, AnsiColouredRowStripGridTableFormat const & row_format);
	private:
		static auto GetFormats(AnsiColouredRowStripGridTableFormat const & row_format) -> std::vector<CellFormat>;

		auto GetFormatIndex(std::size_t x, std::size_t y, CellAlign align) const noexcept -> std::size_t override;

		AnsiColourRenderer m_renderer;
	};

	struct [[nodiscard]] AsciiStripTableFormat
	{
		std::size_t padding;
		TextFormat header;
		TextFormat rows;
	};

	struct MarkdownStripTableCellFormat
	{
		TextFormat format;
		CellAlign align;
	};

	struct MarkdownRowRenderer : RowRenderer
	{
		/**
		 * @brief Measure the width of the contents (excluding padding).
		 * 
		 * @param cell The cell to measure.
		 * @return The width in characters.
		 */
		std::size_t Measure(std::u8string_view const & value, std::size_t format_index) const override;

		std::u8string RenderCell(std::u8string_view value, std::size_t format_index, std::size_t width) const override;

		std::u8string FinishRow(std::u8string_view value, std::size_t row_index, std::span<std::size_t const> col_sizes) const override;

		MarkdownRowRenderer(std::vector<MarkdownStripTableCellFormat> formats, std::size_t padding);
	private:
		std::vector<MarkdownStripTableCellFormat> m_formats;
	};
	/**
	 * | Header      | Header     |
	 * |-------------|------------|
	 * | Row         | Row        |
	 */
	struct [[nodiscard]] MarkdownStripTable : Table
	{
		MarkdownStripTable(std::size_t cols, std::size_t rows, AsciiStripTableFormat const & format);
	private:
		static auto GetFormats(AsciiStripTableFormat const & row_format) -> std::vector<MarkdownStripTableCellFormat>;

		auto GetFormatIndex(std::size_t x, std::size_t y, CellAlign align) const noexcept -> std::size_t override;

		MarkdownRowRenderer m_renderer;
	};

	using CsvStripTableCellFormat = CellAlign;

	struct CsvRowRenderer : RowRenderer
	{
		/**
		 * @brief Measure the width of the contents (excluding padding).
		 * 
		 * @param cell The cell to measure.
		 * @return The width in characters.
		 */
		std::size_t Measure(std::u8string_view const & value, std::size_t format_index) const override;

		std::u8string RenderCell(std::u8string_view value, std::size_t format_index, std::size_t width) const override;

		std::u8string FinishRow(std::u8string_view value, std::size_t row_index, std::span<std::size_t const> col_sizes) const override;

		CsvRowRenderer(std::vector<CsvStripTableCellFormat> formats);
	private:
		std::vector<CsvStripTableCellFormat> m_formats;
	};
	/**
	 * Header, Header,
	 * Row, "R\"ow",
	 */
	struct [[nodiscard]] CsvStripTable : Table
	{
		CsvStripTable(std::size_t cols, std::size_t rows, AsciiStripTableFormat const & format);
	private:
		static auto GetFormats(AsciiStripTableFormat const & row_format) -> std::vector<CsvStripTableCellFormat>;

		auto GetFormatIndex(std::size_t x, std::size_t y, CellAlign align) const noexcept -> std::size_t override;

		CsvRowRenderer m_renderer;
	};
}