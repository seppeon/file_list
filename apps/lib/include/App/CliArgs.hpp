#pragma once
#include <span>
#include <vector>
#include <optional>
#include <string_view>

namespace FL::App
{
	/**
	 * @brief As structure representing a split set of arguments.
	 */
	template <typename T>
	struct CliArgsSplitResult
	{
		T lhs;
		T rhs;
	};
	/**
	 * @brief Manage the cli arguments.
	 */
	struct CliArgs
	{
		CliArgs() = default;
		/**
		 * @brief Get a vector of the cli arguments.
		 * 
		 * @param argc The number of argumnets.
		 * @param argv A pointer to the arguments.
		 */
		explicit CliArgs(int argc, const char * const * argv);
		/**
		 * @brief Check if a key/value pair is the first argument, extract it if it is.
		 * 
		 * @param 
		 */
		[[nodiscard]] auto PopFirstKeyValue(std::span<std::string_view const> key) -> std::optional<std::string_view>;
		/**
		 * @brief Checks if a flag is present, if it is, remove it from the remaining args.
		 * 
		 * @param flag The options for this flag.
		 * @retval true: When the flag is found.
		 * @retval false: When the flag isn't found.
		 */
		[[nodiscard]] bool FindFlagAndRemove(std::span<std::string_view const> flag);
		/**
		 * @brief Find the first key/value pair, and remove it from the argument list, returning the value removed.
		 * 
		 * @param key The key(s) options for this pair.
		 * @return std::nullopt if the key isn't found, the value if it is found.
		 */
		[[nodiscard]] auto FindKeyValAndRemove(std::span<std::string_view const> key) -> std::optional<std::string_view>;
		/**
		 * @brief Get a list span representing the unhandled arguments.
		 * @return A span of the unhandled arguments.
		 */
		[[nodiscard]] auto GetUnhandled() const -> std::span<std::string_view const>;
		/**
		 * 
		 * @brief Split the arguments into two, using a particular splitting token (usually "--")
		 * 
		 * @return Two CliArgs structures, one on either side of the splitting token.
		 */
		[[nodiscard]] auto SplitArgs(std::string_view token) -> CliArgsSplitResult<CliArgs>;
		/**
		 * @brief Remove all unhandled args.
		 */
		void Clear();
	private:

		std::vector<std::string_view> m_args;
	};
}