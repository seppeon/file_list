#pragma once
#include "App/Table.hpp"
#include <memory>
#include <optional>

namespace FL::App
{
	enum class PrintFormat
	{
		List,
		Csv,
		Markdown,
		AnsiColoured,
	};

	[[nodiscard]] auto TableFormatFromString(std::string_view input) -> PrintFormat;

	[[nodiscard]] auto MakeTable(std::size_t columns, std::size_t rows, PrintFormat format) -> std::unique_ptr<App::Table>;
}