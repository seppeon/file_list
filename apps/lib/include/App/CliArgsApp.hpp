#pragma once
#include "App/CliCommonSettings.hpp"
#include <string>
#include <vector>

namespace FL::App
{
	struct CliArgsSettings
	{
		CliCommonSettings common_settings;
		std::vector<std::string> args;
	};
	/**
	 * @brief Find the CLI arguments for an input only application and remove those from the unhandled args list.
	 * 
	 * @param args The arguments to process.
	 * @return std::nullopt on help, CliInputOnlySettings on success.
	 */
	[[nodiscard]] auto ExtractCliArgsSettings(CliArgs & args) -> CliArgsSettings;
}