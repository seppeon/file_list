#include "App/CliDef.hpp"

namespace FL::App
{
	namespace
	{
		std::size_t MaximumKeyLength(auto const & obj)
		{
			std::size_t max_size = 0;
			for (auto const & key : obj.keys)
			{
				max_size = std::max(max_size, key.size());
			}
			return max_size;
		}
	}

	static constexpr char indent[] = "    ";
	std::string CliDef::GetHelp() const noexcept
	{
		std::string output;
		output += std::string(title.name.size() + 2, '=') + "\n";
		output += " " + title.name + " \n";
		output += std::string(title.name.size() + 2, '-') + "\n";
		output += title.desc + "\n";

		if (not flags.empty())
		{
			output += "\n";
			output += "Flags:\n";
			output += "------\n";
			for (auto const & flag : flags)
			{
				for (auto const & key : flag.keys)
				{
					output += key + "\n";
				}
				output += indent + flag.desc + "\n\n";
			}
		}

		if (not key_values.empty())
		{
			output += "\n";
			output += "Key/Value pairs:\n";
			output += "----------------\n";
			for (auto const & key_val : key_values)
			{
				auto const max_flag_length = MaximumKeyLength(key_val);
				for (auto const & key : key_val.keys)
				{
					auto const padding = max_flag_length - key.size();
					output += std::string(padding, ' ') + key + " <" + key_val.type + ">\n";
				}
				output += indent + std::string(key_val.required ? "Required" : "Optional")  + ".\n";
				output += indent + key_val.desc + "\n\n";
			}
		}

		if (opt_args.has_value())
		{
			auto const & args = *opt_args;
			output += "\n";
			output += "Remaining args:\n";
			output += "---------------\n";
			output += "<args:" + args.type + "> ...\n";
			output += indent + args.desc + "\n";
		}

		return output + "\nPlease submit bug reports here https://gitlab.com/seppeon/file_list/-/issues.\n";
	}
}