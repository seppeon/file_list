#include "App/CliInputOnlyApp.hpp"
#include "App/CliInputOutput.hpp"
#include <stdexcept>

namespace FL::App
{
	auto ExtractCliInputOnlySettings(CliArgs & args) -> CliInputOnlySettings
	{
		auto common_settings = FL::App::ExtractCommonSettings(args);
		if (common_settings.print_help)
		{
			return CliInputOnlySettings{
				.common_settings = std::move(common_settings)
			};
		}
		std::vector<std::u8string> inputs;
		for (;;)
		{
			auto const opt_input = ExtractInput(args);
			if (not opt_input) break;
			inputs.push_back(std::move(*opt_input));
		}
		if (inputs.empty())
		{
			throw std::runtime_error("'--input <path>' is a required arguments.");
		}
		return CliInputOnlySettings{
			.common_settings = std::move(common_settings),
			.inputs = std::move(inputs),
		};
	}
}