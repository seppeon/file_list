#include "App/CliInputOutputApp.hpp"
#include "App/CliInputOutput.hpp"
#include <stdexcept>
#include <array>

namespace FL::App
{
	auto GetInputOutputFlags() -> std::vector<CliFlag>
	{
		return {
			CliFlag{ .keys{ "-M", "--mkdir" }, .desc{ "Whether or not the application is allowed to make directories." } },
			CliFlag{ .keys{ "-X", "--override" }, .desc{ "Whether or not the application is allowed to override existing files." } },
			CliFlag{ .keys{ "-D", "--dry-run" }, .desc{ "If this flag is present, no changes to the filesystem will be made." } }
		};
	}

	auto ExtractCliInputOutputSettings(CliArgs & args) -> CliInputOutputSettings
	{
		auto common_settings = FL::App::ExtractCommonSettings(args);
		if (common_settings.print_help)
		{
			return CliInputOutputSettings{
				.common_settings = std::move(common_settings)
			};
		}
		std::vector<std::u8string> inputs;
		std::vector<std::u8string> outputs;
		for (;;)
		{
			auto const opt_input = ExtractInput(args);
			auto const opt_output = ExtractOutput(args);
			if (not opt_input or not opt_output) break;
			inputs.push_back(std::move(*opt_input));
			outputs.push_back(std::move(*opt_output));
		}
		if (inputs.empty())
		{
			throw std::domain_error("'--output <path>' and '--input <path>' are required arguments.");
		}
		return CliInputOutputSettings{
			.common_settings = std::move(common_settings),
			.mkdir = args.FindFlagAndRemove(std::array<std::string_view, 2>{"--mkdir", "-M"}),
			.dry_run = args.FindFlagAndRemove(std::array<std::string_view, 2>{"--dry-run", "-D"}),
			.override_existing = args.FindFlagAndRemove(std::array<std::string_view, 2>{"--override", "-X"}),
			.inputs = std::move(inputs),
			.outputs = std::move(outputs),
		};
	}
}