#include "App/CliArgs.hpp"
#include <algorithm>

namespace FL::App
{
	namespace
	{
		auto GetContainsFn(std::span<std::string_view const> options)
		{
			return [options](std::string_view a)
			{
				return std::any_of(options.begin(), options.end(), [a](std::string_view b)
				{
					return a == b;
				});
			};
		}
	}

	CliArgs::CliArgs(int argc, const char * const * argv)
		: m_args{(argc > 0) ? std::vector<std::string_view>{argv + 1, argv+argc} : std::vector<std::string_view>{}}
	{
	}

	auto CliArgs::PopFirstKeyValue(std::span<std::string_view const> key) -> std::optional<std::string_view>
	{
		auto const contains = GetContainsFn(key);
		if (m_args.empty()) return std::nullopt;
		auto const first_arg = m_args.front();
		if (not contains(first_arg)) return std::nullopt;
		else return first_arg;
	}

	bool CliArgs::FindFlagAndRemove(std::span<std::string_view const> flag)
	{
		auto const erase_count = std::erase_if(m_args, GetContainsFn(flag));
		return erase_count != 0;
	}

	auto CliArgs::FindKeyValAndRemove(std::span<std::string_view const> key) -> std::optional<std::string_view>
	{
		auto const contains = GetContainsFn(key);
		for (std::size_t i = 1; i < std::size(m_args); ++i)
		{
			auto const a = m_args[i-1];
			auto const b = m_args[i];
			if (contains(a))
			{
				auto const it = m_args.begin() + (i - 1);
				m_args.erase(it, it + 2);
				return std::optional<std::string_view>{ b };
			}
		}
		return {};
	}

	auto CliArgs::GetUnhandled() const -> std::span<std::string_view const>
	{
		return { m_args };
	}

	auto CliArgs::SplitArgs(std::string_view token) -> CliArgsSplitResult<CliArgs>
	{
		auto const contains = [token](std::string_view input)
		{
			return input == token;
		};
		for (std::size_t i = 0; i < std::size(m_args); ++i)
		{
			if (contains(m_args[i]))
			{
				// From the first argument to the current position.
				auto lhs_begin = m_args.begin();
				auto lhs_end = lhs_begin + i;
				// From one past the current position to the end, 
				// its important to note, the current position is not the end.
				auto rhs_begin = lhs_end + 1;
				auto rhs_end = m_args.end();
				CliArgsSplitResult<CliArgs> output;
				output.lhs.m_args = { lhs_begin, lhs_end };
				output.rhs.m_args = { rhs_begin, rhs_end };
				return output;
			}
		}
		return {
			.lhs = *this,
			.rhs = {}
		};
	}

	void CliArgs::Clear()
	{
		m_args.clear();
	}
}