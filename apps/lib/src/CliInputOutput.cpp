#include "App/CliInputOutput.hpp"
using namespace std::string_view_literals;

namespace FL::App
{
	static constexpr auto input_keys = {"-I"sv, "--input"sv};
	static constexpr auto output_keys = {"-O"sv, "--output"sv};

	auto ExtractInput(CliArgs & args) -> std::optional<std::u8string>
	{
		auto const opt_input = args.FindKeyValAndRemove(input_keys);
		if (not opt_input)
		{
			return {};
		}
		return std::u8string{ opt_input->begin(), opt_input->end() };
	}

	auto ExtractOutput(CliArgs & args) -> std::optional<std::u8string>
	{
		auto const opt_output = args.FindKeyValAndRemove(output_keys);
		if (not opt_output)
		{
			return {};
		}
		return std::u8string(opt_output->begin(), opt_output->end());
	}

	auto GetInputKeyVals(std::string input_desc) -> std::vector<CliKeyVal>
	{
		return {
			CliKeyVal{
				.keys{ input_keys.begin(), input_keys.end() },
				.type = "path",
				.desc = input_desc,
				.required = true,
			},
		};
	}

	auto GetOutputKeyVals(std::string output_desc) -> std::vector<CliKeyVal>
	{
		return {
			CliKeyVal{
				.keys{ output_keys.begin(), output_keys.end() },
				.type = "path",
				.desc = output_desc,
				.required = true,
			},
		};
	}
}