#include "App/Table.hpp"
#include <cstddef>
#include <span>
#include <string_view>
#include <vector>
#include <string>
#include <cassert>

namespace FL::App
{
	namespace
	{
		std::size_t GetTotalPadding(std::u8string_view value, std::size_t width)
		{
			return width - std::min(value.size(), width);
		}

		std::u8string PadAlignLeft(std::u8string value, std::size_t width)
		{
			auto const total_padding = GetTotalPadding(value, width);
			auto const padding = std::u8string(total_padding, u8' ');
			return value + padding;
		}

		std::u8string PadAlignRight(std::u8string value, std::size_t width)
		{
			auto const total_padding = GetTotalPadding(value, width);
			auto const padding = std::u8string(total_padding, u8' ');
			return padding + value;
		}

		std::u8string PadAlignCenter(std::u8string value, std::size_t width)
		{
			auto const total_padding = GetTotalPadding(value, width);
			auto const padding_left_width = total_padding / 2;
			auto const padding_right_width = total_padding - padding_left_width;
			auto const padding_left = std::u8string(padding_left_width, u8' ');
			auto const padding_right = std::u8string(padding_right_width, u8' ');
			return padding_left + value + padding_right;
		}

		std::u8string PadAndAlign(std::u8string value, CellAlign align, std::size_t width)
		{
			assert(value.size() <= width);
			switch (align) {
			case CellAlign::Left: return PadAlignLeft(std::move(value), width);
			case CellAlign::Right: return PadAlignRight(std::move(value), width);
			case CellAlign::Center: return PadAlignCenter(std::move(value), width);
			// Well someone is an arse, lets provide the data, just incase they really need it
			// displayed, but don't exit it to look sane...
			default: return value;
			}
		}

		std::u8string ToString(auto const & value)
		{
			auto const temp = std::to_string(value);
			return { temp.begin(), temp.end() };
		}

		// https://stackoverflow.com/a/33206814
		std::string GetAnsiTextColour(TableColour const & colour)
		{
			std::string output = "38;2;";
			output += std::to_string(colour.r) + ";";
			output += std::to_string(colour.g) + ";";
			output += std::to_string(colour.b);
			return output;
		}

		std::string GetAnsiBackgroundColour(TableColour const & colour)
		{
			std::string output = "48;2;";
			output += std::to_string(colour.r) + ";";
			output += std::to_string(colour.g) + ";";
			output += std::to_string(colour.b);
			return output;
		}

		std::string GetAnsiFormat(TextFormat const & format)
		{
			std::string output = "";
			if (format.bold) { output += "1;"; }
			if (format.underlined) { output += "4;"; }
			if (format.italics) { output += "3;"; }
			if (not output.empty()) { output.pop_back(); }
			return output;
		}
	}

	RowRenderer::RowRenderer(std::size_t padding)
		: m_padding(padding)
	{}

	RowRenderer::~RowRenderer(){}

	Table::Table(std::size_t cols, std::size_t rows, RowRenderer const & renderer)
		: m_header(cols)
		, m_rows(rows, row_type(cols))
		, m_renderer(&renderer)
	{
	}
	Table::~Table()
	{}

	void Table::SetCell(std::size_t x, std::size_t y, cell_type value)
	{
		assert(y < m_rows.size());
		auto & row = m_rows[y];
		assert(x < row.size());
		m_rows[y][x] = std::move(value);
	}

	void Table::SetCell(std::size_t x, std::size_t y, std::u8string value, CellAlign align)
	{
		SetCell(x, y, TableCell{
			.format = GetFormatIndex(x, y, align),
			.contents = std::move(value),
		});
	}

	void Table::SetCell(std::size_t x, std::size_t y, std::u8string_view value, CellAlign align)
	{
		SetCell(x, y, std::u8string(value.begin(), value.end()), align);
	}

	void Table::SetCell(std::size_t x, std::size_t y, std::string_view value, CellAlign align)
	{
		SetCell(x, y, std::u8string(value.begin(), value.end()), align);
	}

	void Table::SetCellContent(std::size_t x, std::size_t y, std::u8string contents)
	{
		assert(y < m_rows.size());
		auto & row = m_rows[y];
		assert(x < row.size());
		m_rows[y][x].contents = std::move(contents);
	}

	void Table::SetColumnFormat(std::size_t col_index, std::size_t format_index)
	{
		for (auto & row : m_rows)
		{
			auto & cell = row[col_index];
			cell.format = format_index;
		}
	}

	void Table::SetRowFormat(std::size_t row_index, std::size_t format_index)
	{
		auto & row = m_rows[row_index];
		for (auto & cell : row)
		{
			cell.format = format_index;
		}
	}


	[[nodiscard]] auto Table::RenderRow(row_type const & row, min_column_width_type const & min_column_widths) const
	{
		assert(row.size() == min_column_widths.size());
		std::u8string output;
		for (std::size_t i = 0; i < min_column_widths.size(); ++i)
		{
			auto const & cell = row[i];
			auto const & min_width = min_column_widths[i];
			output += m_renderer->RenderCell(cell.contents, cell.format, min_width);
		}
		return output;
	}

	auto Table::GetColumnWidths() const -> min_column_width_type
	{
		auto const column_count = m_header.size();
		auto output = min_column_width_type(column_count);
		for (auto const & row : m_rows)
		{
			for (std::size_t i = 0; i < column_count; ++i)
			{
				output[i] = std::max(output[i], m_renderer->Measure(row[i].contents, row[i].format));
			}
		}
		return output;
	}

	auto Table::Render() const -> std::u8string
	{
		auto const min_column_widths = GetColumnWidths();
		std::u8string output;
		std::size_t row_index = 0;
		for (auto const & row : m_rows)
		{
			output += m_renderer->FinishRow(RenderRow(row, min_column_widths), row_index++, min_column_widths);
		}
		return output;
	}

	std::size_t AnsiColourRenderer::Measure(std::u8string_view const & value, std::size_t format_index) const
	{
		return value.size();
	}

	std::u8string AnsiColourRenderer::RenderCell(std::u8string_view value, std::size_t format_index, std::size_t width) const
	{
		auto const & cell_format = m_formats[format_index];
		std::string temp = "\033[";
		temp += GetAnsiTextColour(cell_format.text_colour);
		temp += ";" + GetAnsiBackgroundColour(cell_format.background_colour);
		if (cell_format.format.bold or cell_format.format.italics or cell_format.format.underlined)
		{
			temp += ";" + GetAnsiFormat(cell_format.format);
		}
		temp += "m";

		auto const padding = std::u8string(m_padding, u8' ');
		return std::u8string{ temp.begin(), temp.end() } + padding + PadAndAlign(std::u8string(value), cell_format.alignment, width) + padding;
	}

	std::u8string AnsiColourRenderer::FinishRow(std::u8string_view value, std::size_t /* row_index */, std::span<std::size_t const> col_sizes) const
	{
		static constexpr std::u8string_view const clr = u8"\033[0m";
		std::u8string output;
		output.reserve(value.size() + clr.size() + 1);
		output += value;
		output += clr;
		output += u8"\n";
		return output;
	}

	AnsiColourRenderer::AnsiColourRenderer(std::vector<CellFormat> formats, std::size_t padding)
		: RowRenderer(padding)
		, m_formats(std::move(formats))
	{}

	AnsiColouredRowStripGridTable::AnsiColouredRowStripGridTable(std::size_t cols, std::size_t rows, AnsiColouredRowStripGridTableFormat const & row_format)
		: Table(cols, rows, m_renderer)
		, m_renderer(GetFormats(row_format), row_format.padding)
	{
		for (std::size_t row = 0; row < rows; ++row)
		{
			SetRowFormat(row, GetFormatIndex(0, row, CellAlign::Left));
		}
	}

	auto AnsiColouredRowStripGridTable::GetFormats(AnsiColouredRowStripGridTableFormat const & row_format) -> std::vector<CellFormat>
	{
		auto const get = [](auto const & format, CellAlign align)
		{
			return CellFormat{
				.format = format.format,
				.alignment = align,
				.text_colour = format.text_colour,
				.background_colour = format.background_colour,
			};
		};
		return {
			get(row_format.header, CellAlign::Left),
			get(row_format.odd_row, CellAlign::Left),
			get(row_format.even_row, CellAlign::Left),
			get(row_format.header, CellAlign::Right),
			get(row_format.odd_row, CellAlign::Right),
			get(row_format.even_row, CellAlign::Right),
			get(row_format.header, CellAlign::Center),
			get(row_format.odd_row, CellAlign::Center),
			get(row_format.even_row, CellAlign::Center),
		};
	}

	auto AnsiColouredRowStripGridTable::GetFormatIndex(std::size_t /* x */, std::size_t y, CellAlign align) const noexcept -> std::size_t
	{
		auto const align_index = (static_cast<int>(align) * 3);
		if (y == 0) return align_index;
		auto const is_odd_row = (y % 2) == 1;
		if (is_odd_row) return align_index + 1;
		else return align_index + 2;
	}

	namespace
	{
		constexpr std::u8string_view markdown_bold_start = u8"**";
		constexpr std::u8string_view markdown_bold_end = u8"**";
		constexpr std::u8string_view markdown_italics_start = u8"*";
		constexpr std::u8string_view markdown_italics_end = u8"*";
		constexpr std::u8string_view markdown_underline_start = u8"<u>";
		constexpr std::u8string_view markdown_underline_end = u8"</u>";
		constexpr std::u8string_view markdown_cell_start = u8"|";
		constexpr std::u8string_view markdown_row_end = u8"|";


		auto MarkdownMeasureBold()
		{
			std::size_t output = 0;
			output += markdown_bold_start.size();
			output += markdown_bold_end.size();
			return output;
		}
		auto MarkdownMeasureItalics()
		{
			std::size_t output = 0;
			output += markdown_italics_start.size();
			output += markdown_italics_end.size();
			return output;
		}
		auto MarkdownMeasureUnderline()
		{
			std::size_t output = 0;
			output += markdown_underline_start.size();
			output += markdown_underline_end.size();
			return output;
		}
		auto MarkdownWrapBold(std::u8string value)
		{
			std::u8string output;
			output += markdown_bold_start;
			output += value;
			output += markdown_bold_end;
			return output;
		}
		auto MarkdownWrapItalics(std::u8string value)
		{
			std::u8string output;
			output += markdown_italics_start;
			output += value;
			output += markdown_italics_end;
			return output;
		}
		auto MarkdownWrapUnderline(std::u8string value)
		{
			std::u8string output;
			output += markdown_underline_start;
			output += value;
			output += markdown_underline_end;
			return output;
		}
	}

	MarkdownStripTable::MarkdownStripTable(std::size_t cols, std::size_t rows, AsciiStripTableFormat const & format)
		: Table(cols, rows, m_renderer)
		, m_renderer(GetFormats(format), format.padding)
	{
		for (std::size_t row = 0; row < rows; ++row)
		{
			SetRowFormat(row, GetFormatIndex(0, row, CellAlign::Left));
		}
	}

	auto MarkdownStripTable::GetFormats(AsciiStripTableFormat const & row_format) -> std::vector<MarkdownStripTableCellFormat>
	{
		auto const get = [](TextFormat format, CellAlign align)
		{
			return MarkdownStripTableCellFormat{
				.format = format,
				.align = align,
			};
		};
		return {
			get(row_format.header, CellAlign::Left),
			get(row_format.rows, CellAlign::Left),
			get(row_format.header, CellAlign::Right),
			get(row_format.rows, CellAlign::Right),
			get(row_format.header, CellAlign::Center),
			get(row_format.rows, CellAlign::Center),
		};
	}

	auto MarkdownStripTable::GetFormatIndex(std::size_t /* x */, std::size_t y, CellAlign align) const noexcept -> std::size_t
	{
		auto const is_header = y == 0;
		auto const align_index = (static_cast<int>(align) * 2);
		if (is_header) return align_index;
		return align_index + 1;
	}

	std::size_t MarkdownRowRenderer::Measure(std::u8string_view const & value, std::size_t format_index) const
	{
		auto const & format= m_formats[format_index].format;
		std::size_t output = 0;
		output += value.size();
		if (format.bold) output += MarkdownMeasureBold();
		if (format.italics) output += MarkdownMeasureItalics();
		if (format.underlined) output += MarkdownMeasureUnderline();
		return output;
	}

	std::u8string MarkdownRowRenderer::RenderCell(std::u8string_view value, std::size_t format_index, std::size_t width) const
	{
		auto const & [format, alignment] = m_formats[format_index];
		std::u8string output;
		output += value;
		if (format.bold) output = MarkdownWrapBold(output);
		if (format.italics) output = MarkdownWrapItalics(output);
		if (format.bold) output = MarkdownWrapUnderline(output);
		auto const padding = std::u8string(m_padding, u8' ');
		return std::u8string(markdown_cell_start) + padding + PadAndAlign(std::move(output), alignment, width) + padding;
	}

	std::u8string MarkdownRowRenderer::FinishRow(std::u8string_view value, std::size_t row_index, std::span<std::size_t const> col_sizes) const
	{
		std::u8string underline;
		if (row_index == 0)
		{
			for (auto const col_size : col_sizes)
			{
				underline += RenderCell(std::u8string(col_size, '-'), 0, col_size);
			}
			underline += std::u8string(markdown_row_end) + u8"\n";
		}
		return std::u8string(value) + std::u8string(markdown_row_end) + u8"\n" + underline;
	}

	MarkdownRowRenderer::MarkdownRowRenderer(std::vector<MarkdownStripTableCellFormat> formats, std::size_t padding)
		: RowRenderer(padding)
		, m_formats(std::move(formats))
	{
	}

	namespace
	{
		bool CsvStringHasComma(std::u8string_view input)
		{
			return input.find(u8',') != input.npos;
		}
		std::u8string EscapeCsvString(std::u8string_view input)
		{
			if (CsvStringHasComma(input))
			{
				std::u8string output;
				for (auto const c : input)
				{
					if (c == u8'"')
					{
						output += u8"\\";
					}
					output += c;
				}
				return u8'"' + output + u8'"';
			}
			else
			{
				return std::u8string(input);
			}
		}
		std::size_t MeasureEscapedCsvString(std::u8string_view input)
		{
			return EscapeCsvString(input).size();
		}
	}

	CsvStripTable::CsvStripTable(std::size_t cols, std::size_t rows, AsciiStripTableFormat const & format)
		: Table(cols, rows, m_renderer)
		, m_renderer(GetFormats(format))
	{
		for (std::size_t row = 0; row < rows; ++row)
		{
			SetRowFormat(row, GetFormatIndex(0, row, CellAlign::Left));
		}
	}

	auto CsvStripTable::GetFormats(AsciiStripTableFormat const & row_format) -> std::vector<CsvStripTableCellFormat>
	{
		return {
			CellAlign::Left,
			CellAlign::Right,
			CellAlign::Center,
		};
	}

	auto CsvStripTable::GetFormatIndex(std::size_t /* x */, std::size_t y, CellAlign align) const noexcept -> std::size_t
	{
		auto const is_header = y == 0;
		auto const align_index = static_cast<int>(align);
		if (is_header) return align_index;
		return align_index + 1;
	}

	std::size_t CsvRowRenderer::Measure(std::u8string_view const & value, std::size_t format_index) const
	{
		return MeasureEscapedCsvString(value);
	}

	std::u8string CsvRowRenderer::RenderCell(std::u8string_view value, std::size_t format_index, std::size_t width) const
	{
		auto const & alignment = m_formats[format_index];
		return EscapeCsvString(value) + u8",";
	}

	std::u8string CsvRowRenderer::FinishRow(std::u8string_view value, std::size_t row_index, std::span<std::size_t const> col_sizes) const
	{
		return std::u8string(value) + u8"\n";
	}

	CsvRowRenderer::CsvRowRenderer(std::vector<CsvStripTableCellFormat> formats)
		: RowRenderer(0)
		, m_formats(std::move(formats))
	{
	}
}