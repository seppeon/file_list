#include "App/CliArgsApp.hpp"
#include "App/CliInputOutput.hpp"
#include <stdexcept>

namespace FL::App
{
	auto ExtractCliArgsSettings(CliArgs & args) -> CliArgsSettings
	{
		auto common_settings = FL::App::ExtractCommonSettings(args);
		if (common_settings.print_help)
		{
			return CliArgsSettings{
				.common_settings = std::move(common_settings)
			};
		}
		auto const unhandled = args.GetUnhandled();
		args.Clear();
		return CliArgsSettings{
			.common_settings = std::move(common_settings),
			.args = { unhandled.begin(), unhandled.end() },
		};
	}
}