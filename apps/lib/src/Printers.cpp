#include "App/Printers.hpp"
#include "FL/MoveFiles.hpp"
#include "FL/PatternTarget.hpp"
#include "../private/Overloads.hpp"
#include <algorithm>
#include <iostream>
#include <string>
#include <sstream>
#include <map>
#include <string_view>

namespace FL::App
{
	static std::u8string ToString(std::filesystem::path const & p)
	{
		std::stringstream s;
		s << p;
		auto const o = s.str();
		return { o.begin(), o.end() };
	}

	static std::u8string ToString(auto const & v) requires (requires (decltype(v) v)
		{
			{ std::to_string(v) };
		})
	{
		auto r = std::to_string(v);
		return { r.begin(), r.end() };
	}

	std::u8string ToString(FL::NavList const & files, PrintFormat format)
	{
		std::size_t columns = 0;
		for (auto const & file : files)
		{
			columns = std::max(file.name.Size(), columns);
		}
		auto const table_ptr = MakeTable(columns + 1, files.size() + 1, format);

		// Add the header.
		std::size_t path_column = 0;
		{
			for (; path_column < columns; ++path_column) { table_ptr->SetCell(path_column, 0, ToString(path_column)); }
			table_ptr->SetCell(path_column, 0, "Path");
		}

		// Add the body.
		std::size_t row = 1;
		for (auto const & file : files)
		{
			auto const & pattern = file.name;
			auto const & path = file.path;
			std::size_t column = 0;
			for (auto const & part : pattern)
			{
				table_ptr->SetCell(column++, row, part);
			}
			table_ptr->SetCell(path_column, row, ToString(path));
			++row;
		}
		return table_ptr->Render();
	}

	std::u8string ToString(FL::GlobMoveRequest const & pattern, PrintFormat format)
	{
		auto const columns = std::size_t(2);
		auto const rows = std::size_t(2);
		auto const table_ptr = MakeTable(columns, 2, format);
		table_ptr->SetCell(0, 0, "Source");
		table_ptr->SetCell(1, 0, "Destination");
		table_ptr->SetCell(0, 1, pattern.src);
		table_ptr->SetCell(1, 1, pattern.dst);
		return table_ptr->Render();
	}

	std::u8string ToString(FL::Pattern const & pattern, PrintFormat format)
	{
		auto const columns = pattern.Size();
		auto const rows = std::size_t(2);
		auto const table_ptr = MakeTable(columns, 2, format);
		for (std::size_t col = 0; col < columns; ++col)
		{
			auto const value = pattern[col];
			table_ptr->SetCell(col, 0, ":" + std::to_string(col));
			table_ptr->SetCell(col, 1, value);
		}
		return table_ptr->Render();
	}

	std::u8string ToString(FL::PatternTarget const & pattern_target, PrintFormat format)
	{
		auto const columns = pattern_target.Size();
		auto const rows = std::size_t(2);
		auto const table_ptr = MakeTable(columns, 2, format);
		std::size_t col = 0;
		auto const string_transform = FL::Overloads{
			[](std::size_t index) -> std::string
			{
				return ":" + std::to_string(index);
			},
			[](std::u8string_view v) -> std::string
			{
				return {v.begin(), v.end()};
			}
		};
		for (auto const & value : pattern_target)
		{
			table_ptr->SetCell(col, 0, "[" + std::to_string(col) + "]");
			table_ptr->SetCell(col, 1, std::visit(string_transform, value));
			++col;
		}
		return table_ptr->Render();
	}

	std::u8string ToString(FL::MoveOperationList const & move_operations, PrintFormat format)
	{
		auto const table_ptr = MakeTable(3, move_operations.size() + 1, format);
		auto const set_row = [&](std::size_t row, std::u8string_view source, std::u8string_view destination)
		{
			table_ptr->SetCell(0, row, source);
			table_ptr->SetCell(1, row, destination);
		};
		set_row(0, u8"Source", u8"Destination");
		for (std::size_t i = 0; i < move_operations.size(); ++i)
		{
			auto const & [move_src, move_dst] = move_operations[i];
			set_row(i + 1, ToString(move_src), ToString(move_dst));
		}
		return table_ptr->Render();
	}

	std::u8string ToString(FL::MoveOperationList const & move_operations, FL::MoveOperationStatusList const & move_operation_status_list, PrintFormat format)
	{
		auto const & status_list = move_operation_status_list;
		if (move_operations.size() != status_list.size()) { return u8"Invalid inputs provided to App::ToString for FL::MoveOperationsListStatus."; }

		auto const table_ptr = MakeTable(3, status_list.size() + 1, format);
		auto const set_row = [&](std::size_t row, std::u8string_view source, std::u8string_view destination, std::u8string_view status)
		{
			table_ptr->SetCell(0, row, source);
			table_ptr->SetCell(1, row, destination);
			table_ptr->SetCell(2, row, status);
		};
		set_row(0, u8"Source", u8"Destination", u8"Status");
		for (std::size_t i = 0; i < status_list.size(); ++i)
		{
			auto const & [move_src, move_dst] = move_operations[i];
			auto const & status = status_list[i];
			set_row(i + 1, ToString(move_src), ToString(move_dst), FL::ToString(status));
		}
		return table_ptr->Render();
	}

	std::u8string ToString(FL::PatternMismatchList const & pattern_mismatch, PrintFormat format)
	{
		std::map<PatternTarget, std::vector<PatternMismatch const *>> pattern_mismatch_groups;
		for (auto const & pattern_mismatch : pattern_mismatch)
		{
			pattern_mismatch_groups[pattern_mismatch.pattern_target].push_back(&pattern_mismatch);
		}

		auto const header_transform = FL::Overloads{
			[&](std::size_t index) -> std::u8string
			{
				return u8":" + ToString(index);
			},
			[](std::u8string_view v) -> std::u8string
			{
				return std::u8string{v};
			}
		};

		std::u8string output;
		for (auto const & [pattern_target, pattern_mismatch_ptr_list] : pattern_mismatch_groups)
		{
			auto const column_count = pattern_target.Size();
			auto const table_ptr = MakeTable(column_count, pattern_mismatch_ptr_list.size() + 1, format);

			// Print the table header for this pattern target.
			std::size_t col = 0;
			for (auto const & part : pattern_target)
			{
				table_ptr->SetCell(col++, 0, std::visit(header_transform, part));
			}

			// Print the table body, applying the glob result to the pattern target.
			std::size_t row = 1;
			for (auto const *pattern_mismatch_ptr : pattern_mismatch_ptr_list)
			{
				auto const & [move_request_index, glob_result, pattern_target] = *pattern_mismatch_ptr;
				auto const body_transform = FL::Overloads{
					[&](std::size_t index) -> std::u8string
					{
						if (index < glob_result.Size())
						{
							return u8"[" + std::u8string(glob_result[index]) + u8"]";
						}
						else return u8"out of range";
					},
					[](std::u8string_view v) -> std::u8string
					{
						return {v.begin(), v.end()};
					}
				};
				std::size_t col = 0;
				for (auto const & part : pattern_target)
				{
					table_ptr->SetCell(col, row, std::visit(body_transform, part));
					++col;
				}
				++row;
			}

			output += u8"This pattern had an error:\n";
			output += ToString(pattern_target, format) + u8"\n";
			output += u8"There were out of range indexes:\n";
			output += table_ptr->Render() + u8"\n";
		}
		output += u8"adjust your --output argument.";
		return output;
	}
}