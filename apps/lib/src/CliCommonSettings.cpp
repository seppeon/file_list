#include "App/CliCommonSettings.hpp"
#include "App/MakeTable.hpp"
#include "App/Table.hpp"
#include "FL/Semver.hpp"
#include <stdexcept>
#include <array>
#include <iostream>

namespace FL::App
{
	auto ExtractCommonSettings(CliArgs & args) -> CliCommonSettings
	{
		auto const has_help = args.FindFlagAndRemove(std::array<std::string_view, 2>{"--help", "?"});
		if (has_help) return CliCommonSettings{
			.print_help = has_help,
		};
		auto const has_version = args.FindFlagAndRemove(std::array<std::string_view, 1>{"--version"});
		if (has_version) return CliCommonSettings{
			.print_version = has_version,
		};

		auto const opt_settings = FL::App::ExtractNavSettings(args);
		if (not opt_settings)
		{
			throw std::runtime_error("invalid settings combination provided.");
		}

		auto opt_table_format = std::optional<PrintFormat>{};
		auto const opt_format_key_val = args.FindKeyValAndRemove(std::array<std::string_view, 2>{"--print_format", "-F"});
		auto const table_format = opt_format_key_val ? FL::App::TableFormatFromString(*opt_format_key_val) : PrintFormat::List;

		return CliCommonSettings{
			.glob_settings = *opt_settings,
			.verbose = args.FindFlagAndRemove(std::array<std::string_view, 2>{"--verbose", "-V"}),
			.print_format = table_format,
			.print_help = has_help,
			.print_version = has_version,
		};
	}

	auto GetCommonFlags() -> std::vector<CliFlag>
	{
		auto output = FL::App::GetNavFlags();
		output.push_back(CliFlag{ .keys{ "-V", "--verbose" }, .desc{ "If this flag is present, verbose printing are enabled." } });
		output.push_back(CliFlag{.keys{ "--help", "?" }, .desc = "Print help."});
		return output;
	}

	auto GetCommonKeyVals() -> std::vector<CliKeyVal>
	{
		return {
			CliKeyVal{
				.keys{ "-F", "--print_format" },
				.type = "string",
				.desc = "Print the data as a `colour` table, `markdown`, `csv` or as a `list` (default).",
				.required = false,
			}
		};
	}

	bool HandleCommonSettings(CliCommonSettings const & common_settings, CliArgs const & args, CliDef const & cli_def)
	{

		if (common_settings.print_help)
		{
			std::cout << cli_def.GetHelp() << std::endl;
			return false;
		}
		if (common_settings.print_version)
		{
			std::cout << FL::GetCurrentVersion().ToString() << "\n";
			return false;
		}
		for (auto const & arg : args.GetUnhandled())
		{
			std::cerr << "warning: unknown argument '" << arg << "'\n";
		}
		return true;
	}
}