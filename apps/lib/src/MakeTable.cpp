#include "App/MakeTable.hpp"
#include "App/Table.hpp"
#include "App/TableThemes.hpp"
#include <memory>

namespace FL::App
{
	auto TableFormatFromString(std::string_view input) -> PrintFormat
	{
		if (input == "markdown") return PrintFormat::Markdown;
		if (input == "colour") return PrintFormat::AnsiColoured;
		if (input == "csv") return PrintFormat::Csv;
		return PrintFormat::List;
	}
	auto MakeTable(std::size_t columns, std::size_t rows, PrintFormat format) -> std::unique_ptr<App::Table>
	{
		switch (format)
		{
		case PrintFormat::AnsiColoured:
		{
			return std::make_unique<App::AnsiColouredRowStripGridTable>(columns, rows, App::coloured_row_strip_grid_table_format);
		}
		case PrintFormat::Csv:
		{
			return std::make_unique<App::CsvStripTable>(columns, rows, App::ascii_strip_table_format);
		}
		default:
		{
			return std::make_unique<App::MarkdownStripTable>(columns, rows, App::ascii_strip_table_format);
		}
		};
	}
}