#include "App/CliNavSettings.hpp"
#include <array>

namespace FL::App
{
	auto ExtractNavSettings( CliArgs & args ) -> std::optional<FL::NavSettings>
	{
		return FL::NavSettings{
			.enable_recursion = args.FindFlagAndRemove( std::array<std::string_view, 2>{ "--enable_recursion", "-R" } ),
			.case_sensitive = args.FindFlagAndRemove( std::array<std::string_view, 2>{ "--case_sensitive", "-C" } ),
			.show_hidden = args.FindFlagAndRemove( std::array<std::string_view, 2>{ "--show_hidden", "-H" } ),
			.absolute_paths = args.FindFlagAndRemove( std::array<std::string_view, 2>{ "--absolute_paths", "-A" } ),
			.allow_symlinks = args.FindFlagAndRemove( std::array<std::string_view, 2>{ "--allow_symlinks", "-S" } ),
		};
	}

	auto GetNavFlags() -> std::vector<CliFlag>
	{
		return {
			CliFlag{ .keys{ "-R", "--enable_recursion" }, .desc{ "Allow recursive globs, for example '/**/*', where the '**' indicates recursive searching will be performed in the directory." } },
			CliFlag{ .keys{ "-C", "--case_sensitive" }, .desc{ "If this flag is present, the search will be case sensitive." } },
			CliFlag{ .keys{ "-H", "--show_hidden" }, .desc{ "If this flag is present, the search will show hidden files." } },
			CliFlag{ .keys{ "-A", "--absolute_paths" }, .desc{ "If this flag is present, all resulting paths will be in absolute form." } },
			CliFlag{ .keys{ "-S", "--allow_symlinks" }, .desc{ "If this flag is present, symlinks will be followed." } },
		};
	}
}