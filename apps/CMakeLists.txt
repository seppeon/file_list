# Add all subdirectories.
file(GLOB APP_LIST_DIRS CONFIGURE_DEPENDS *)
file(GLOB APP_LIST_FILES CONFIGURE_DEPENDS LIST_DIRECTORIES false *)
list(REMOVE_ITEM APP_LIST_DIRS ${APP_LIST_FILES})
foreach (APP_LIST_DIR IN LISTS APP_LIST_DIRS)
	add_subdirectory(${APP_LIST_DIR})
endforeach()