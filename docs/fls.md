\page file_list FileList

FileList is a utility designed to assist you in searching for files using globs. This guide will introduce you to the basic usage of this tool.

## Flags

FileList uses several flags to customize your search:

- `-R` or `--enable_recursion`: Allows recursive globs, such as `/**/*`, where `**` indicates recursive searching in the directory.
- `-C` or `--case_sensitive`: Makes the search case sensitive.
- `-H` or `--show_hidden`: Shows hidden files in the search results.
- `-A` or `--absolute_paths`: Displays all resulting paths in absolute form.
- `-S` or `--allow_symlinks`: Follows symlinks during the search.
- `-V` or `--verbose`: Enables verbose printing.
- `-F` or `--print_format`: Print the list in a `colour` table, `markdown`, `csv` or as a `list` (default).

You can also use `--help` or `?` to print the help information.

## Usage

After the flags, you can specify one or more paths containing globs for which to search.

For example:

### A case sensitive search in one directory
```
fls -C /path/to/search/*
```

### A case insensitive search in all directories for a text file
```
fls -R /**/*.txt
```
Note: `-R` flag must be provided, or the `**` token will not perform a recursive search.

## Support

If you encounter any issues while using FileList, please submit bug reports on the [GitLab issues page](https://gitlab.com/seppeon/file_list/-/issues).

Happy searching!