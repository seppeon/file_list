\page fres_gcc_header_search_order GCC Header Search Order

There two rulesets for the search order to mimick GCC. 

## Quoted Includes
```CPP
#include "hello.hpp"
```
The rule order for this ruleset are:

 - The directory of the current file is searched first.
 - The directories specified by `-iquote` options are searched in order.
 - The directories specified by `-I` options are searched in order.
 - The directories specified by `CPATH` environmental variable.
 - The directories specified by `-isystem` options are searched in order.
 - The directories specified by `C_INCLUDE_PATH` for `C` and `CPLUS_INCLUDE_PATH` for `CPP`.
 - The "system directories" are scanned.
   - For `C++` found by issuing `echo "" | g++ -xc++ "-Wp,-v" -`.
   - For `C` found by issuing `echo "" | g++ -xc "-Wp,-v" -`.
 - The directories specified by `-idirafter` options are searched in order.

## Angle Bracket Includes
```CPP
#include <hello.hpp>
```
The rule order for this ruleset are:

 - The directories specified by `-I` options are searched in order.
 - The directories specified by `CPATH` environmental variable.
 - The directories specified by `-isystem` options are searched in order.
 - The directories specified by `C_INCLUDE_PATH` for `C` and `CPLUS_INCLUDE_PATH` for `CPP`.
 - The "system directories" are scanned.
   - For `C++` found by issuing `echo "" | g++ -xc++ "-Wp,-v" -`.
   - For `C` found by issuing `echo "" | g++ -xc "-Wp,-v" -`.
 - The directories specified by `-idirafter` options are searched in order.

# Links

 - https://gcc.gnu.org/onlinedocs/cpp/Search-Path.html
 - https://gcc.gnu.org/onlinedocs/gcc/Directory-Options.html
 - https://gcc.gnu.org/onlinedocs/cpp/Environment-Variables.html
 - \file cpp.json An example ruleset, providing the ability to resolve the path of C++ header files.