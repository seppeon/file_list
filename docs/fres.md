\page file_resolve FileResolve

An application used to resolve a file from a set of resolution rules. These rules are customisable by the user via a JSON file.

## Flags

FileResolve uses several flags to customize your search:

 - `-R` or `--enable_recursion`: Allow recursive globs, for example '/**/*', where the '**' indicates recursive searching will be performed in the directory.
 - `-C` or `--case_sensitive`: If this flag is present, the search will be case sensitive.
 - `-H` or `--show_hidden`: If this flag is present, the search will show hidden files.
 - `-A` or `--absolute_paths`: If this flag is present, all resulting paths will be in absolute form.
 - `-S` or `--allow_symlinks`: If this flag is present, symlinks will be followed.
 - `-V` or `--verbose`: If this flag is present, verbose printing are enabled.
 - `--help` or `?`: Print help.
 - `--ruleset-help`: Get the help for a particular "ruleset", the provided string must be a valid "ruleset" name.
 - `--rulesets <file>`: The rulesets file that contains a "ruleset" with the name specified by 
 - `--ruleset-name <string>`: The "ruleset" name.
 - `--input <file>` or `-I <file>`: The file to resolve.

The arguments `--rulesets`, and `--ruleset-name` are mandatory. If you would like help about a specific "ruleset", see the example below.

When you are done with the regular `fres` arguments finish your command with `--`, all arguments that proceed the first `--` will be passed into the `ruleset` as arguments.

## Usage

### Resolving a header file

In this example, I will demonstrate how you can use fres to resolve a C++ header file. Throughout the example, it is assumed the current working directory is this project's root directory.

If you were to take a command from a `compile_commands.json` file of this project, you may have the following:
```
clang++ -Iinclude -Iprivate -isystem ~/.conan2/p/nlohm552351c8663ae/p/include -std=++20 -o CMakeFiles\\FL.dir\\src\\Exact.cpp.obj -c src\\Exact.cpp",
```

Extract the `-isystem` and `-I` arguments, if you have `-quote` or `-idirafter` extract those too. Seperate out the arguments so that they are regularly structured:
```
-I include -I private -isystem ~/.conan2/p/nlohm552351c8663ae/p/include
```

If we are using the example ruleset, the above arguments can be provided to the `fres` application after the `--` on the command line. Please note, rulesets can support any arguments, and these arguments are only relevant to the example file.

In addition to the above arguments the following must be provided:
 - `--compiler`, we will use `g++`.
 - `--current-header`, the directory of the current header. This impacts the search order, since the directory of a file is searched first for quoted includes.

The command would ultimately look as follows:
```
fres --input FL/File.hpp --rulesets apps/fres\\rulesets\\cpp.json --ruleset-name gpp_quoted_includes -- --compiler g++ --current-header include/FL -I include -I private -isystem ~/.conan2/p/nlohm552351c8663ae/p/include
```
The immediate response of command should be the path to the file with the relative path `FL/File.hpp`.

### Getting help about a ruleset

To get help about a specific "ruleset", you can query help about that "ruleset":
```
fres --rulesets apps/fres/rulesets/cpp.json --ruleset-name gpp_quoted_includes --ruleset-help
```
This will produce a help menu that describes the inputs avaliable as specified by some "ruleset" file.

## Examples

The example ruleset, called `cpp.json` is a ruleset that mimicks the search order of headers by GCC. This search order, and that of clang's is documented below. Note, its likely these documents are not free of errors.

 - \subpage fres_gcc_header_search_order
 - \subpage fres_clang_header_search_order