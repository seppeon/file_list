\page fres_clang_header_search_order Clang Header Search Order

There two rulesets for the search order to mimick Clang. 

## Quoted Includes
```CPP
#include "hello.hpp"
```
The rule order for this ruleset are:

 - The directory of the current file is searched first.
 - The directories specified by `-iquote` options are searched in order.
 - The directories specified by `-I` options are searched in order.
 - The directories specified by `CPATH` environmental variable.
 - The directories specified by `-isystem` options are searched in order.
 - The directories specified by `C_INCLUDE_PATH` for `C` and `CPLUS_INCLUDE_PATH` for `CPP`.
 - The "system directories" are scanned.
   - For `C++` found by issuing `echo "" | clang++ -xc++ -Wp,-v -`.
   - For `C` found by issuing `echo "" | clang++ -xc -Wp,-v -`.
 - The directories specified by `-idirafter` options are searched in order.

## Angle Bracket Includes
```CPP
#include <hello.hpp>
```
The rule order for this ruleset are:

 - The directories specified by `-I` options are searched in order.
 - The directories specified by `CPATH` environmental variable.
 - The directories specified by `-isystem` options are searched in order.
 - The directories specified by `C_INCLUDE_PATH` for `C` and `CPLUS_INCLUDE_PATH` for `CPP`.
 - The "system directories" are scanned.
   - For `C++` found by issuing `echo "" | clang++ -xc++ -Wp,-v -`.
   - For `C` found by issuing `echo "" | clang++ -xc -Wp,-v -`.
 - The directories specified by `-idirafter` options are searched in order.
