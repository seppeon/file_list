\page file_move FileMove

FileMove is a utility that allows you to move a set of files to a set of destinations. It supports various options to customize the file moving process.

## Flags

FileMove supports various flags to customize its behavior:

- `-R` or `--enable_recursion`: Allow recursive globs.
- `-C` or `--case_sensitive`: Make the search case sensitive.
- `-H` or `--show_hidden`: Show hidden files in the search.
- `-A` or `--absolute_paths`: Display all resulting paths in absolute form.
- `-S` or `--allow_symlinks`: Follow symlinks during the search.
- `-V` or `--verbose`: Enable verbose printing.
- `-M` or `--mkdir`: Allow the application to make directories.
- `-X` or `--override`: Allow the application to override existing files.
- `-D` or `--dry-run`: Make no changes to the filesystem (useful for testing).
- `-F` or `--print_format`: Print the list in a `colour` table, `markdown`, `csv` or as a `list` (default).

You can also use `--help` or `?` to print the help information.

## Usage

### General move operation

The usage of FileMove involves specifying an input file and an output path:

```
fmv --input <path> --output <path>
```

Here, `<path>` should be replaced with the path of your input file and the desired output path.

### File move with dependant output path

Here's an example of how you can use FileMove:

Lets move all header files to a subdirectory `include/Fmv`, and if that directory doesn't exist, lets create it (`--mkdir`):
```
fmv --input *.hpp --output :0/include/Fmv/:1:2 --mkdir
```

You can provide any number of inputs and outputs, as long as the number of inputs matches the number of outputs. Each input will be paired with the corresponding output, read from the first argument to the last.

#### Explaination

In the `--output` field of the `fmv` command, you can use numbers to refer to sections of the input file path that were found by the glob. These numbers are prefixed by a colon, like `:0`, `:1`, `:2` and so on. This feature allows you to dynamically construct output paths based on the structure of your input file paths. It’s particularly useful when you’re dealing with a large number of files and you want to preserve some aspect of their original directory structure.

When you run a command like:
```
fmv --input *.hpp --output :0/include/Fmv/:1:2 --mkdir
```
*Note: The `--mkdir` flag allows files to be moved into a directory that didn't exist before running the command.*

On the a directory containing the following files:

 - `./CliArgs.hpp`
 - `./CliArgsApp.hpp`
 - `./CliCommonSettings.hpp`
 - `./CliDef.hpp`
 - `./CliNavSettings.hpp`
 - `./CliInputOnlyApp.hpp`
 - `./CliInputOutput.hpp`
 - `./CliInputOutputApp.hpp`
 - `./CliPrinter.hpp`
 - `./Printers.hpp`

Each file would be broken into the following sections, note the `.` for current directory:

|    | 0   | 1                   | 2      |
| -- | --- | ------------------- | ------ |
| 1  | `.` | `CliArgs`           | `.hpp` |
| 2  | `.` | `CliArgsApp`        | `.hpp` |
| 3  | `.` | `CliCommonSettings` | `.hpp` |
| 4  | `.` | `CliDef`            | `.hpp` |
| 5  | `.` | `CliNavSettings`   | `.hpp` |
| 6  | `.` | `CliInputOnlyApp`   | `.hpp` |
| 7  | `.` | `CliInputOutput`    | `.hpp` |
| 8  | `.` | `CliInputOutputApp` | `.hpp` |
| 9  | `.` | `CliPrinter`        | `.hpp` |
| 10 | `.` | `Printers`          | `.hpp` |

*Note: You can produce this table before running your command with the `-D` and `-V` flags.*

Substituting the numbered sections into the numbers found in `:0/include/Fmv/:1:2`, you get:
 - `./include/Fmv/CliArgs.hpp`
 - `./include/Fmv/CliArgsApp.hpp`
 - `./include/Fmv/CliCommonSettings.hpp`
 - `./include/Fmv/CliDef.hpp`
 - `./include/Fmv/CliNavSettings.hpp`
 - `./include/Fmv/CliInputOnlyApp.hpp`
 - `./include/Fmv/CliInputOutput.hpp`
 - `./include/Fmv/CliInputOutputApp.hpp`
 - `./include/Fmv/CliPrinter.hpp`
 - `./include/Fmv/Printers.hpp`

It will move all `.hpp` files from the current directory (`.`) to a directory called `/include/Fmv`.

## Support

If you encounter any issues while using FileMove, please submit a bug report on [GitLab](https://gitlab.com/seppeon/file_list/-/issues).

Please replace `<path>` with your actual paths when using the commands.

Enjoy using FileMove!