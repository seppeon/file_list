\page fl_lib Library

## Key Features

- **Globbing Support**: The library provides support for globbing, allowing you to use wildcard characters to match multiple files at once.

- **Resolving Executables and Dynamic Libraries**: The library offers functions to resolve the paths of executables and dynamic libraries.

- **Moving Files**: The library supports moving files from one location to another with ease. It also provides glob support and the ability to have the moved-to path depend on the input path.

## Getting Started

The following sections will show you how to get the library included in your project.

### Using FetchContent

```CMAKE
include(FetchContent)

FetchContent_Declare(
  FileList
  GIT_REPOSITORY https://gitlab.com/seppeon/file_list.git
  GIT_TAG <hash>
)
FetchContent_MakeAvailable(FileList)

add_executable(t test.cpp)
target_link_libraries(t PRIVATE FL::FL)
```
*Note: You must replace `<hash>` with your desired git hash or tag.*