#pragma once

#if defined(_WIN32) || defined(_WIN64)
    #define PLATFORM_IS_WINDOWS
#elif __APPLE__
    #define PLATFORM_IS_MAC
#elif linux
    #define PLATFORM_IS_LINUX
#else
#ifdef EMSCRIPTEN 
	#define PLATFORM_IS_WASM
#else
	#error "Unsupported platform"
#endif
#endif