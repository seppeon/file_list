#pragma once
#include "FL/Semver.hpp"
#include <ostream>
#include <vector>
#include <string>
#include <string_view>
#include <optional>

// Provided so that catch2 can provide diagnostics on failure.

inline std::ostream & operator << (std::ostream & o, FL::Semver const & v)
{
	return o << v.ToString();
}

inline std::ostream & operator << (std::ostream & o, FL::SemverStringResult const & v)
{
	return o << "{ index = " << v.index << ", length = " << v.length << ", semver = " << v.semver.ToString() << " }";
}


inline std::ostream & operator << (std::ostream & o, char8_t const & v)
{
	o << "'";
	o.put(static_cast<char>(v));
	o << "'";
	return o;
}

inline std::ostream & operator << (std::ostream & o, std::u8string_view const & v)
{
	return o.write(reinterpret_cast<const char *>(v.data()), v.size());
}

template <std::convertible_to<std::u8string_view> T>
inline std::ostream & operator << (std::ostream & o, T const & v)
{
	return o << std::u8string_view(v);
}

inline std::ostream & operator << (std::ostream & o, std::vector<std::u8string_view> const & v)
{
	o << "[";
	bool need_comma = false;
	for (auto const & l : v)
	{
		if (need_comma) o << ", ";
		o << '"';
		o.write(reinterpret_cast<const char *>(l.data()), l.size());
		o << '"';
		need_comma = true;
	}
	o << "]";
	return o;
}

template <typename T>
inline std::ostream & operator << (std::ostream & o, std::vector<T> const & v)
{
	o << "[";
	bool need_comma = false;
	for (auto const & l : v)
	{
		if (need_comma) o << ", ";
		o << '"';
		o << l;
		o << '"';
		need_comma = true;
	}
	o << "]";
	return o;
}

template <typename T>
inline std::ostream & operator << (std::ostream & o, std::optional<T> const & v)
{
	if (not v)
	{
		return o << "null";
	}
	else
	{
		return o << *v;
	}
}