#pragma once
#include "PlatformDetect.hpp"
#if defined(PLATFORM_IS_WINDOWS)
#if __has_include (<Windows.h>)
#include <Windows.h>
#elif __has_include (<windows.h>)
#include <windows.h>
#endif
#include <shlwapi.h>
#undef min
#undef max
#endif