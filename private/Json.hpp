#pragma once
#include "FL/FindRuleset.hpp"
#include <nlohmann/json.hpp>
#include <vector>
#include <optional>	
#include <source_location>
#include <string>
#include <string_view>
#include <map>
#include <optional>

namespace FL
{
	template <typename T>
	inline constexpr auto is_vector_v = false;
	template <typename ... Args>
	inline constexpr auto is_vector_v<std::vector<Args...>> = true;

	template <typename T>
	auto GetVectorOf(nlohmann::json const & data) -> std::optional<std::vector<T>>;

	template <typename T>
	inline constexpr auto is_map_v = false;
	template <typename ... Args>
	inline constexpr auto is_map_v<std::map<std::u8string, Args...>> = true;

	template <typename T>
	inline constexpr auto is_optional_v = false;
	template <typename ... Args>
	inline constexpr auto is_optional_v<std::optional<Args...>> = true;

	inline std::nullopt_t NullOpt(std::source_location temp = std::source_location::current())
	{
		return std::nullopt;
	}

	template <typename T>
	auto GetMapOf(nlohmann::json const & data) -> std::optional<std::map<std::u8string, T>>;

	template <typename T>
	auto GetT(nlohmann::json const & data) -> std::optional<T>;

	inline bool IsString(nlohmann::json const & data) { return data.is_string(); }
	inline auto GetString(nlohmann::json const & data) -> std::optional<std::u8string>
	{
		if (not IsString(data)) return NullOpt();
		auto temp = data.template get<std::string>();
		return std::u8string{ temp.begin(), temp.end() };
	}

	inline bool IsBoolean(nlohmann::json const & data) { return data.is_boolean(); }
	inline auto GetBool(nlohmann::json const & data) -> std::optional<bool>
	{
		if (not IsBoolean(data)) return NullOpt();
		return data.template get<bool>();
	}

	inline bool IsInt64(nlohmann::json const & data) { return data.is_number() and data.is_number_integer(); }
	inline auto GetInt64(nlohmann::json const & data) -> std::optional<std::int64_t>
	{
		if (not IsInt64(data)) return NullOpt();
		return data.template get<std::int64_t>();
	}

	inline bool IsUint64(nlohmann::json const & data) { return data.is_number() and data.is_number_unsigned(); }
	inline auto GetUint64(nlohmann::json const & data) -> std::optional<std::uint64_t>
	{
		if (not IsUint64(data)) return NullOpt();
		return data.template get<std::uint64_t>();
	}

	inline bool IsChar(nlohmann::json const & data) { return IsString(data) and (data.template get<std::string>().size() == 1); }
	inline auto GetChar(nlohmann::json const & data) -> std::optional<char8_t>
	{
		if (not IsChar(data)) return NullOpt();
		return GetString(data)->front();
	}

	inline bool IsDouble(nlohmann::json const & data) { return data.is_number() and data.is_number_float(); }
	inline auto GetDouble(nlohmann::json const & data) -> std::optional<double>
	{
		if (not IsDouble(data)) return NullOpt();
		return data.template get<double>();
	}

	inline bool IsFloat(nlohmann::json const & data) { return data.is_number() and data.is_number_float(); }
	inline auto GetFloat(nlohmann::json const & data) -> std::optional<float>
	{
		if (not IsFloat(data)) return NullOpt();
		return data.template get<float>();
	}

	template <typename T, typename = void>
	struct UserGetT;

	template <typename T>
	concept HasUserGetT = requires(nlohmann::json const & data)
	{
		{ UserGetT<T, void>::GetT(data) } -> std::same_as<std::optional<T>>;
	};

	template <typename T>
	auto GetT(nlohmann::json const & data) -> std::optional<T>
	{
		if constexpr (std::is_same_v<std::u8string, T>)
		{
			return GetString(data);
		}
		else if constexpr (std::is_same_v<bool, T>)
		{
			return GetBool(data);
		}
		else if constexpr (std::is_same_v<std::int64_t, T>)
		{
			return GetInt64(data);
		}
		else if constexpr (std::is_same_v<std::uint64_t, T>)
		{
			return GetUint64(data);
		}
		else if constexpr (std::is_same_v<char8_t, T>)
		{
			return GetChar(data);
		}
		else if constexpr (std::is_same_v<double, T>)
		{
			return GetDouble(data);
		}
		else if constexpr (std::is_same_v<float, T>)
		{
			return GetFloat(data);
		}
		else if constexpr (is_vector_v<T>)
		{
			return GetVectorOf<typename T::value_type>(data);
		}
		else if constexpr (is_map_v<T>)
		{
			return GetVectorOf<typename T::mapped_type>(data);
		}
		else if constexpr (HasUserGetT<T>)
		{
			return UserGetT<T, void>::GetT(data);
		}
		else
		{
			return NullOpt();
		}
	}

	inline bool IsArray(nlohmann::json const & data) { return data.is_array(); }

	template <typename T>
	auto GetVectorOf(nlohmann::json const & data) -> std::optional<std::vector<T>>
	{
		if (not IsArray(data)) { return NullOpt(); }
		std::vector<T> output;
		for (auto const & elem : data)
		{
			auto opt_data = GetT<T>(elem);
			if (not opt_data) { return NullOpt(); }
			output.push_back(std::move(*opt_data));
		}
		return output;
	}

	template <typename T>
	auto GetMapOf(nlohmann::json const & data) -> std::optional<std::map<std::u8string, T>>
	{
		if (not data.is_object()) return NullOpt();
		auto const is_all_correct_type_test = data.items() | std::ranges::views::transform([](auto const & data){
			return GetT<T>(data).has_value();
		});
		auto const is_correct_type = std::all_of(is_all_correct_type_test.begin(), is_all_correct_type_test.end(), std::identity());
		if (not is_correct_type) return NullOpt();
		std::map<std::u8string, T> output;
		for (auto const & object : data.items())
		{
			auto const key_string = std::u8string(object.key().begin(), object.key().end());
			output[key_string] = GetT<T>(object.value());
		}
		return output;
	}

	template <typename T>
	auto GetKeyValuePairOf(nlohmann::json const & data, std::u8string_view key) -> std::optional<T>
	{
		if (not data.is_object()) return NullOpt();
		auto const key_str = std::string(key.begin(), key.end());
		if (not data.contains(key_str)) return NullOpt();
		return GetT<T>(data[key_str]);
	}

	inline auto HasKey(nlohmann::json const & data, std::u8string_view key) -> bool
	{
		if (not data.is_object()) return false;
		return data.contains(std::string(key.begin(), key.end()));
	}

	template <typename T>
	auto HasKeyOfType(nlohmann::json const & data, std::u8string_view key) -> bool
	{
		return HasKey(data, key) and GetT<T>(data).has_value();
	}

	template <typename T>
	inline constexpr auto constructor_of_avaliable_args = [](auto && ... args) -> T
	{
		return T{ *static_cast<decltype(args)&&>(args) ... };
	};

	template <typename T>
	struct Pair
	{
		std::u8string_view name;
		std::optional<T> default_value;
	};

	template <typename ... Ts>
	using PairValues = std::tuple<Ts...>;

	template <typename ... Ts>
	auto GetKeyValuePairs(nlohmann::json const & data, Pair<Ts> const & ... keys) -> std::optional<std::tuple<std::optional<Ts>...>>
	{
		auto const values = std::tuple
		{
			(
				HasKey(data, keys.name)
				? GetT<Ts>(data[std::string(keys.name.begin(), keys.name.end())])
				: keys.default_value
			)
			...
		};
		auto const has_required_keys = std::apply([&](std::optional<Ts> const & ... args)
		{
			return (
				(keys.default_value.has_value() or args.has_value()) and
				...
			);
		}, values);
		if (not has_required_keys) { return NullOpt(); }
		return values;
	}

	template <typename V>
	struct UserGetT<FindArgType, V>
	{
		static auto GetT(nlohmann::json const & data) -> std::optional<FindArgType>
		{
			auto const opt_string = GetString(data);
			if (not opt_string) { return NullOpt(); }
			auto const arg_type = ParseArgType(*opt_string);
			if (not arg_type.IsValid()) { return NullOpt(); }
			return arg_type;
		}
	};
	static_assert(HasUserGetT<FindArgType>);

	template <typename V>
	struct UserGetT<FindArgBaseType, V>
	{
		static auto GetT(nlohmann::json const & data) -> std::optional<FindArgBaseType>
		{
			auto const opt_string = GetString(data);
			if (not opt_string) { return NullOpt(); }
			for (auto const type : possible_base_arg_types)
			{
				if (ToU8String(type) == *opt_string)
				{
					return type;
				}
			}
			return NullOpt();
		}
	};
	static_assert(HasUserGetT<FindArgBaseType>);
	template <typename V>
	struct UserGetT<FindArg, V>
	{
		static auto GetT(nlohmann::json const & data) -> std::optional<FindArg>
		{
			auto const opt_key_value_pairs = GetKeyValuePairs(data,
				Pair<std::u8string>{u8"name"},
				Pair<std::vector<std::u8string>>{u8"commands", std::vector<std::u8string>{}},
				Pair<bool>{u8"required", false},
				Pair<std::u8string>{u8"description"},
				Pair<FindArgType>{u8"type"}
			);
			if (not opt_key_value_pairs) { return NullOpt(); }
			return std::apply(
				constructor_of_avaliable_args<FindArg>,
				*opt_key_value_pairs
			);
		}
	};
	static_assert(HasUserGetT<FindArg>);

	template <typename V>
	struct UserGetT<FindInDirs, V>
	{
		static auto GetT(nlohmann::json const & data) -> std::optional<FindInDirs>
		{				
			if (not HasKey(data, u8"directories")) return NullOpt();
			auto directories = data["directories"];
			if (IsArray(directories))
			{
				auto const opt_paths = FL::GetT<std::vector<std::u8string>>(directories);
				if (not opt_paths) return NullOpt();
				return FindInDirs{std::vector<std::u8string>{ opt_paths->begin(), opt_paths->end() }};
			}
			else if (IsString(directories))
			{
				auto const opt_path = FL::GetT<std::u8string>(directories);
				if (not opt_path) return NullOpt();
				return FindInDirs{std::vector<std::u8string>{ *opt_path }};
			}
			else
			{
				return NullOpt();
			}
		}
	};
	static_assert(HasUserGetT<FindInDirs>);

	template <typename V>
	struct UserGetT<FindInEnvVarPaths, V>
	{
		static auto GetT(nlohmann::json const & data) -> std::optional<FindInEnvVarPaths>
		{
			auto const opt_pairs = GetKeyValuePairs(data,
				Pair<std::u8string>(u8"variable"),
				Pair<char8_t>(u8"delim", CurrentEnvVarDelim())
			);
			if (not opt_pairs) return NullOpt();
			return std::apply(constructor_of_avaliable_args<FindInEnvVarPaths>, *opt_pairs);
		}
	};
	static_assert(HasUserGetT<FindInEnvVarPaths>);

	template <typename V>
	struct UserGetT<FindUsingRegKey, V>
	{
		static auto GetT(nlohmann::json const & data) -> std::optional<FindUsingRegKey>
		{
			auto const opt_pairs = GetKeyValuePairs(data,
				Pair<std::u8string>(u8"entry"),
				Pair<std::u8string>(u8"relative", u8"")
			);
			if (not opt_pairs) return NullOpt();
			return std::apply(constructor_of_avaliable_args<FindUsingRegKey>, *opt_pairs);
		}
	};
	static_assert(HasUserGetT<FindUsingRegKey>);

	template <typename V>
	struct UserGetT<FindUsingRegKeyVal, V>
	{
		static auto GetT(nlohmann::json const & data) -> std::optional<FindUsingRegKeyVal>
		{
			auto const opt_pairs = GetKeyValuePairs(data,
				Pair<std::u8string>(u8"entry"),
				Pair<std::u8string>(u8"value"),
				Pair<std::u8string>(u8"relative", u8"")
			);
			if (not opt_pairs) return NullOpt();
			return std::apply(constructor_of_avaliable_args<FindUsingRegKeyVal>, *opt_pairs);
		}
	};
	static_assert(HasUserGetT<FindUsingRegKeyVal>);

	template <typename V>
	struct UserGetT<FindFilters, V>
	{
		static auto GetT(nlohmann::json const & data) -> std::optional<FindFilters>
		{
			if (not IsArray(data)) { return NullOpt(); }
			FindFilters output;
			for (auto const & elem : data)
			{
				if (not HasKey(elem, u8"type")) { return NullOpt(); }
				auto const opt_type = GetString(elem["type"]);
				if (not opt_type) { return NullOpt(); }
				auto const & type = *opt_type;
				// TODO: Refactor this...
				if (type == u8"glob")
				{
					auto const opt_key_value_pairs = GetKeyValuePairs(elem,
						Pair<std::u8string>{u8"glob"},
						Pair<std::u8string>{u8"capture"},
						Pair<bool>{u8"case_sensitive", false}
					);
					if (not opt_key_value_pairs)
					{
						return NullOpt();
					}
					auto const & [opt_glob, opt_capture, opt_case_sensitive] = *opt_key_value_pairs;
					auto const & glob = *opt_glob;
					auto const & capture = *opt_capture;
					auto const & case_sensitive = *opt_case_sensitive;
					output.PushBack(FindFilterGlob(glob, capture, case_sensitive));
				}
				else if (type == u8"getlines")
				{
					output.PushBack(FindFilterGetLines());
				}
				else if (type == u8"trim_whitespace")
				{
					output.PushBack(FindFilterTrimSpaces());
				}
				else if (type == u8"directory_exists")
				{
					output.PushBack(FindFilterDirectoryExists());
				}
				else if (type == u8"file_exists")
				{
					output.PushBack(FindFilterRegularFileExists());
				}
				else
				{
					// Unknown filter type was provided.
					return NullOpt();
				}
			}
			return output;
		}
	};
	static_assert(HasUserGetT<FindFilters>);

	template <typename V>
	struct UserGetT<Cmd, V>
	{
		static auto GetT(nlohmann::json const & data) -> std::optional<Cmd>
		{
			auto opt_cmd = GetKeyValuePairOf<std::u8string>(data, u8"cmd");
			if (not opt_cmd) return NullOpt();
			auto opt_args = GetKeyValuePairOf<std::vector<std::u8string>>(data, u8"args");
			if (not opt_args) return NullOpt();
			return Cmd{
				.cmd = *opt_cmd,
				.args = { opt_args->begin(), opt_args->end() }
			};
		}
	};
	static_assert(HasUserGetT<Cmd>);

	template <typename V>
	struct UserGetT<FindPipe, V>
	{
		static auto GetT(nlohmann::json const & data) -> std::optional<FindPipe>
		{
			auto opt_cmd = GetKeyValuePairOf<std::vector<Cmd>>(data, u8"cmds");
			if (not opt_cmd) return NullOpt();
			auto output = FindPipe{
				.cmds = *opt_cmd,
				.expect_error = false,
			};
			auto opt_filters = GetKeyValuePairOf<FindFilters>(data, u8"filters");
			if (opt_filters)
			{
				output.filters = std::move(*opt_filters);
			}
			auto opt_expect_error = GetKeyValuePairOf<bool>(data, u8"expect_error");
			output.expect_error = opt_expect_error.value_or(false);
			return { std::move(output) };
		}
	};
	static_assert(HasUserGetT<FindPipe>);

	template <typename V>
	struct UserGetT<FindLibrary, V>
	{
		static auto GetT(nlohmann::json const & data) -> std::optional<FindLibrary>
		{
			return FindLibrary{};
		}
	};
	static_assert(HasUserGetT<FindLibrary>);

	template <typename V>
	struct UserGetT<FindExecutable, V>
	{
		static auto GetT(nlohmann::json const & data) -> std::optional<FindExecutable>
		{
			return FindExecutable{};
		}
	};
	static_assert(HasUserGetT<FindExecutable>);

	template <typename T, typename V>
	struct UserGetT<FindInherit<T>, V>
	{
		static auto GetT(nlohmann::json const & data) -> std::optional<FindInherit<T>>
		{
			auto opt_ruleset = GetKeyValuePairOf<std::u8string>(data, u8"ruleset");
			if (not opt_ruleset)
			{
				auto opt_value = FL::GetT<T>(data);
				if (not opt_value) return NullOpt();
				return FindInherit<T>{ std::move(*opt_value) };
			}
			return FindInherit<T>{ std::move(*opt_ruleset) };
		}
	};
	static_assert(HasUserGetT<FindInherit<std::u8string>>);

	template <typename V>
	struct UserGetT<FindRule, V>
	{
		static auto GetT(nlohmann::json const & data) -> std::optional<FindRule>
		{
			if (not HasKey(data, u8"type")) { return NullOpt(); }
			auto const type = GetString(data["type"]);
			for (auto const find_rule_type : possible_find_rule_types)
			{
				auto const find_rule_string = ToU8String(find_rule_type);
				if (type == find_rule_string)
				{
					// the type is `type`.
					switch (find_rule_type)
					{
					case FindRuleType::InDirs:
					{
						auto opt_temp = FL::GetT<FindInDirs>(data);
						if (not opt_temp) return NullOpt();
						return FindRule{ std::move(*opt_temp) };
					}
					case FindRuleType::InEnvVarPaths:
					{
						auto opt_temp = FL::GetT<FindInEnvVarPaths>(data);
						if (not opt_temp) return NullOpt();
						return FindRule{ std::move(*opt_temp) };
					}
					case FindRuleType::UsingRegKey:
					{
						auto opt_temp = FL::GetT<FindUsingRegKey>(data);
						if (not opt_temp) return NullOpt();
						return FindRule{ std::move(*opt_temp) };
					}
					case FindRuleType::UsingRegKeyVal:
					{
						auto opt_temp = FL::GetT<FindUsingRegKeyVal>(data);
						if (not opt_temp) return NullOpt();
						return FindRule{ std::move(*opt_temp) };
					}
					case FindRuleType::Library:
					{
						auto opt_temp = FL::GetT<FindLibrary>(data);
						if (not opt_temp) return NullOpt();
						return FindRule{ std::move(*opt_temp) };
					}
					case FindRuleType::Executable:
					{
						auto opt_temp = FL::GetT<FindExecutable>(data);
						if (not opt_temp) return NullOpt();
						return FindRule{ std::move(*opt_temp) };
					}
					case FindRuleType::Pipe:
					{
						auto opt_temp = FL::GetT<FindPipe>(data);
						if (not opt_temp) return NullOpt();
						return FindRule{ std::move(*opt_temp) };
					}
					};
				}
			}
			return NullOpt();
		}
	};
	static_assert(HasUserGetT<FindRule>);

	template <typename V>
	struct UserGetT<FindRuleset, V>
	{
		static auto GetT(nlohmann::json const & data) -> std::optional<FindRuleset>
		{
			auto const opt_key_value_pairs = GetKeyValuePairs(data,
				Pair<std::u8string>{u8"name"},
				Pair<FindRulesetArgs>{u8"args"},
				Pair<FindRulesetRules>{u8"rules"}
			);
			if (not opt_key_value_pairs) { return NullOpt(); }
			return std::apply(
				constructor_of_avaliable_args<FindRuleset>,
				*opt_key_value_pairs
			);
		}
	};
	static_assert(HasUserGetT<FindRuleset>);

	template <typename V>
	struct UserGetT<FindRulesets, V>
	{
		static auto GetT(nlohmann::json const & data) -> std::optional<FindRulesets>
		{
			auto const opt_key_value_pairs = GetKeyValuePairs(data,
				Pair<std::vector<FindRuleset>>{u8"rulesets"}
			);
			if (not opt_key_value_pairs) { return NullOpt(); }
			return std::apply(
				constructor_of_avaliable_args<FindRulesets>,
				*opt_key_value_pairs
			);
		}
	};
	static_assert(HasUserGetT<FindRulesets>);
}