#pragma once

namespace FL
{
	template<class...Fs>
	struct Overloads : Fs...
	{
		using Fs::operator()...;
	};
	template<class...Fs>
	Overloads(Fs...)->Overloads<Fs...>;
}