#pragma once
#include "FL/StrOps.hpp"
#include <cassert>
#include <string_view>
#include <vector>
#include <string>
#include <ranges>
#include <cstdint>
#include <limits>
#include "FL/IteratorFromIndex.hpp"
#include "WindowsHeader.hpp"

namespace FL
{
	/**
	 * @brief Represent a registry path.
	 */
	struct RegPath
	{
		using size_type = std::size_t;
		using difference = std::make_signed_t<size_type>;
		/**
		 * @brief Check if the path is valid, a default constructed path is invalid.
		 * 
		 * @return true When the path is valid, otherwise false.
		 */
		[[nodiscard]] auto IsValid() const noexcept -> bool
		{
			return not m_parts.empty();
		}
		/**
		 * @brief Get the number of parts in the path (the hive + the keys)
		 * 
		 * @return The number of parts.
		 */
		[[nodiscard]] auto Size() const noexcept -> difference
		{
			return static_cast<difference>(m_parts.size());
		} 
		/**
		 * @brief Get a particular part of the path.
		 * 
		 * @return The part of the path.
		 */
		[[nodiscard]] auto operator[](difference i) const noexcept -> std::u8string_view
		{
			assert((0 <= i and i < Size()));
			auto const [index, length] = GetPart(i);
			return std::u8string_view{m_contents.data() + index, length};
		}
		/**
		 * @brief A random access iterator type for the path.
		 */
		using const_iterator = IteratorFromIndex<RegPath>;
		/**
		 * @brief The begin iterator for path.
		 * 
		 * @return The iterator.
		 */
		[[nodiscard]] auto begin() const noexcept -> const_iterator
		{
			return const_iterator{*this, 0};
		}
		/**
		 * @brief The end iterator for path.
		 * 
		 * @return The iterator.
		 */
		[[nodiscard]] auto end() const noexcept -> const_iterator
		{
			return const_iterator{*this, Size()};
		}
		/**
		 * @brief Get the Hive of the path (basically the equivilent of the root directory).
		 * 
		 * @return A view of ths string representing the hive.
		 */
		[[nodiscard]] auto GetHive() const noexcept -> std::u8string_view
		{
			return operator[](0);
		}
		/**
		 * @brief A structure representing the keys of the path.
		 */
		struct Keys
		{
			const_iterator it;
			const_iterator it_end;
			/**
			* @brief The begin iterator for path key.
			* 
			* @return The iterator.
			*/
			[[nodiscard]] auto begin() const noexcept -> const_iterator
			{
				return it;
			}
			/**
			* @brief The end iterator for path key.
			* 
			* @return The iterator.
			*/
			[[nodiscard]] auto end() const noexcept -> const_iterator
			{
				return it_end;
			}
		};
		/**
		 * @brief Get the registry keys (the parts of the path after the hive).
		 * 
		 * @return A container of the keys.
		 */
		[[nodiscard]] auto GetKeys() const noexcept -> Keys
		{
			if (not IsValid()) return Keys{};
			return Keys{
				.it = ++begin(),
				.it_end = end(),
			};
		}
		/**
		 * @brief Construct the path from a string.
		 */
		explicit RegPath(std::u8string_view path)
		{
			auto partially_split = path | std::ranges::views::split(u8'\\');
			m_contents.reserve(path.size());
			m_parts.reserve(path.size() / sizeof(std::size_t));
			std::size_t offset = 0;
			for (auto partially_split_part : partially_split)
			{
				for (auto part : partially_split_part | std::ranges::views::split(u8'/'))
				{
					m_parts.push_back(offset += part.size());
					m_contents.append(part.begin(), part.end());
				}
			}
		}
	private:
		struct Part
		{
			std::size_t index;
			std::size_t length;
		};
		[[nodiscard]] Part GetPart(difference i) const noexcept
		{
			assert(IsValid());
			auto const low = (i == 0) ? 0 : m_parts[i-1];
			auto const high = m_parts[i];
			return Part{ .index = low, .length = high - low };
		}
		std::vector<std::size_t> m_parts;
		std::u8string m_contents;
	};
	/**
	 * @brief The type of data in a particular registry key.
	 * 
	 */
	enum class RegDataType
	{
		Binary,
		Dword,
		DwordLittleEndian,
		DwordBigEndian,
		ExpandSz,
		Link,
		MultiSz,
		None,
		Qword,
		QwordLittleEndian,
		Sz,
	};
	/**
	 * @brief A view of registry key, opened from the constructor, closed in the destructor.
	 */
	struct RegKey
	{
		/**
		 * @brief Construct a registry key from the registry path, opening the key.
		 * 
		 * @param path The path to the key.
		 */
		explicit RegKey(RegPath path)
			: m_path(std::move(path))
			, m_key(nullptr)
		{
			HKEY hkey_result = nullptr;
			if (m_path.IsValid())
			{
				HKEY hkey = GetHiveFromName(m_path.GetHive());
				if (hkey != nullptr)
				{
					for (auto part : m_path.GetKeys())
					{
						auto const subkey = std::string(part.begin(), part.end());
						auto const options = DWORD();
						auto const sam_desired = KEY_READ;
						if (::RegOpenKeyExA(hkey, subkey.data(), options, sam_desired, &hkey_result) != ERROR_SUCCESS)
						{
							return;
						}
						static_cast<void>(::RegCloseKey(hkey));
						hkey = hkey_result;
					}
				}
			}
			m_key = hkey_result;
		}

		struct sentinal{};
		struct iterator
		{
			using difference_type = std::make_signed_t<std::size_t>;
			struct value_type
			{
				std::u8string name;
				RegDataType type;
				std::vector<std::uint8_t> data;
			};
			using reference = const value_type &;
			using pointer = const value_type *;
			using const_reference = value_type;

			iterator & operator++()
			{
				thread_local static char value_name[std::numeric_limits<std::int16_t>::max()];
				DWORD value_len = std::size(value_name);
				DWORD type = 0;
				DWORD data_len = 0;
				if (RegEnumValueA(key, index, std::data(value_name), &value_len, NULL, &type, NULL, &data_len) != ERROR_SUCCESS)
				{
					key = nullptr;
					return *this;
				}
				value_type output
				{
					.name{ std::begin(value_name), std::begin(value_name) + value_len },
					.type = [&]
					{
						switch (type)
						{
						case REG_BINARY: return RegDataType::Binary;
						case REG_DWORD: return RegDataType::Dword;
					#if REG_DWORD_LITTLE_ENDIAN != REG_DWORD
						case REG_DWORD_LITTLE_ENDIAN: return RegDataType::DwordLittleEndian;
					#endif
						#if REG_DWORD_BIG_ENDIAN != REG_DWORD
						case REG_DWORD_BIG_ENDIAN: return RegDataType::DwordBigEndian;
						#endif
						case REG_EXPAND_SZ: return RegDataType::ExpandSz;
						case REG_LINK: return RegDataType::Link;
						case REG_MULTI_SZ: return RegDataType::MultiSz;
						case REG_QWORD: return RegDataType::Qword;
					#if REG_QWORD_LITTLE_ENDIAN != REG_QWORD
						case REG_QWORD_LITTLE_ENDIAN: return RegDataType::QwordLittleEndian;
					#endif
						case REG_SZ: return RegDataType::Sz;
						default: return RegDataType::None;
						}
					}()
				};
				value_len = std::size(value_name);
				output.data.resize(data_len);
				if (::RegEnumValueA(key, index, std::data(value_name), &value_len, NULL, &type, (BYTE *)output.data.data(), &data_len) != ERROR_SUCCESS)
				{
					key = nullptr;
					return *this;
				}
				current_output = std::move(output);
				++index;
				return *this;
			}
			void operator++(int)
			{
				++(*this);
			}
			reference operator*() const
			{
				return current_output;
			}
			pointer operator->() const
			{
				return &current_output;
			}

			friend bool operator==(iterator const & lhs, sentinal) noexcept
			{
				return lhs.key == nullptr;
			}
			friend bool operator!=(iterator const & lhs, sentinal) noexcept
			{
				return lhs.key != nullptr;
			}

			HKEY key = nullptr;
			std::size_t index = 0;
			value_type current_output;
		};
		static_assert(std::input_iterator<iterator>);
		/**
		 * @brief An input iterator to the first registry value.
		 * 
		 * @return The iterator.
		 */
		[[nodiscard]] auto begin() const noexcept -> iterator
		{
			auto it = iterator{ .key = m_key, .index = 0 };
			++it;
			return it; 
		}
		/**
		 * @brief A sentinal iterator used to signify the end of the registry values.
		 * 
		 * @return The iterator.
		 */
		[[nodiscard]] auto end() const noexcept -> sentinal
		{
			return {};
		}
		/**
		 * @brief Destroy the registry key, closing the key.
		 */
		~RegKey()
		{
			// Nothing can be done if this cannot be closed.
			if (m_key != nullptr)
			{
				static_cast<void>(::RegCloseKey(m_key));
			}
		}
	private:
		static HKEY GetHiveFromName(std::u8string_view name)
		{
			if (FL::Equal(name, u8"HKEY_CLASSES_ROOT", false)) return HKEY_CLASSES_ROOT;
			if (FL::Equal(name, u8"HKEY_CURRENT_CONFIG", false)) return HKEY_CURRENT_CONFIG;
			if (FL::Equal(name, u8"HKEY_CURRENT_CONFIG", false)) return HKEY_CURRENT_CONFIG;
			if (FL::Equal(name, u8"HKEY_CURRENT_USER", false)) return HKEY_CURRENT_USER;
			if (FL::Equal(name, u8"HKEY_CURRENT_USER_LOCAL_SETTINGS", false)) return HKEY_CURRENT_USER_LOCAL_SETTINGS;
			if (FL::Equal(name, u8"HKEY_LOCAL_MACHINE", false)) return HKEY_LOCAL_MACHINE;
			if (FL::Equal(name, u8"HKEY_PERFORMANCE_DATA", false)) return HKEY_PERFORMANCE_DATA;
			if (FL::Equal(name, u8"HKEY_PERFORMANCE_NLSTEXT", false)) return HKEY_PERFORMANCE_NLSTEXT;
			if (FL::Equal(name, u8"HKEY_PERFORMANCE_TEXT", false)) return HKEY_PERFORMANCE_TEXT;
			if (FL::Equal(name, u8"HKEY_USERS", false)) return HKEY_USERS;
			return nullptr;
		}

		RegPath m_path;
		HKEY m_key;
	};
}