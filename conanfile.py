from conan import ConanFile
from conan.tools.build import check_min_cppstd
from conan.tools.files import load
from conan.tools.cmake import CMakeToolchain, CMakeDeps, CMake, cmake_layout
import os
import re

required_conan_version = ">=1.50.0"

class FileListRecipe(ConanFile):
    implements = ["auto_shared_fpic"]
    name = "file_list"

    def set_version(self):
        content = load(self, os.path.join(self.recipe_folder, "CMakeLists.txt"))
        version = re.search("project\(FL VERSION (.*) LANGUAGES CXX\)", content).group(1)
        self.version = version.strip()

    # Optional metadata
    license = "MIT"
    author = "David Ledger davidledger@live.com.au"
    url = "https://gitlab.com/seppeon/file_list"
    homepage = "https://seppeon.gitlab.io/file_list/"
    description = "A library that helps you list, find and move files."
    topics = ("filesystem", "c++20", "file listing", "file resolving", "file moving")
    requires = "nlohmann_json/3.11.2", "catch2/3.4.0"
    build_policy = "missing"

    # Binary configuration
    settings = "os", "compiler", "build_type", "arch"
    options = {
		"shared": [True, False],
		"fPIC": [True, False]
	}
    default_options = {
		"shared": False,
		"fPIC": True
	}

    # Sources are located in the same place as this recipe, copy them to the recipe
    exports_sources = "CMakeLists.txt", "readme.md", "LICENSE.txt", "src/*", "cmake/*", "test/*", "include/*", "private/*", "apps/*", "docs/*"

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def generate(self):
        tc = CMakeToolchain(self)
        tc.generate()
        deps = CMakeDeps(self)
        deps.generate()

    def layout(self):
        cmake_layout(self)

    def validate(self):
        if self.settings.compiler.get_safe("cppstd"):
            check_min_cppstd(self, "20")

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def package(self):
        cmake = CMake(self)
        cmake.install()

    def package_info(self):
        self.cpp_info.set_property("cmake_file_name", "FL")
        self.cpp_info.set_property("cmake_target_name", "FL::FL")
        self.cpp_info.set_property("pkg_config_name", "FL")
        self.cpp_info.libs = [ "FL" ]