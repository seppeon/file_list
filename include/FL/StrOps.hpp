/**
 * @file StrOps.hpp
 * @brief Some basic string operations.
 */
#pragma once
#include <string_view>
#include <vector>

namespace FL
{
	/**
	 * @brief Compare two strings in a case sensitive way.
	 * 
	 * @todo Add full unicode and multiple language support.
	 * 
	 * @param lhs The first string to compare.
	 * @param rhs The second string to compare.
	 * @return Whether the strings are equal.
	 */
	[[nodiscard]] bool CaseSensitiveEqual(std::u8string_view lhs, std::u8string_view rhs) noexcept;
	/**
	 * @brief Compare two strings in a case insensitive way.
	 * 
	 * @todo Add full unicode and multiple language support.
	 * 
	 * @param lhs The first string to compare.
	 * @param rhs The second string to compare.
	 * @return Whether the strings are equal.
	 */
	[[nodiscard]] bool CaseInsensitiveEqual(std::u8string_view lhs, std::u8string_view rhs) noexcept;
	/**
	 * @brief Compare two strings in a case sensitive or case insensitive way.
	 * 
	 * @param lhs The first string to compare.
	 * @param rhs The second string to compare.
	 * @param case_sensitive Whether or not the string is case sensitive.
	 * @return Whether the strings are equal.
	 */
	[[nodiscard]] bool Equal(std::u8string_view lhs, std::u8string_view rhs, bool case_sensitive) noexcept;
	/**
	 * @brief Split a string into lines.
	 * 
	 * These are recognised newline combinations:
	 * @li '\\n',
	 * @li '\\r',
	 * @li '\\r\\n', and
	 * @li '\\n\\r'
	 * And will all be parsed as individual newlines.
	 * 
	 * @param input The input string.
	 * @return A list of lines. 
	 */
	[[nodiscard]] auto GetLines(std::u8string_view input) -> std::vector<std::u8string_view>;
	/**
	 * @brief Trim the leading and trailing spaces, as per `std::isspace`.
	 * 
	 * @return A trimmed string.
	 */
	[[nodiscard]] auto TrimSpaces(std::u8string_view input) -> std::u8string_view;
}