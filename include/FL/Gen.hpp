/**
 * @file Gen.hpp
 * @brief A coroutine return object.
 */
#pragma once
#include <coroutine>
#include <utility>
#include <type_traits>
#include <exception>
#include <iterator>

namespace FL
{
	/**
	 * @brief The return object type for a syncronous generator.
	 * @cond Doxygen_Suppress
	 * @tparam T The type that will be produced by the generator.
	 */
	template <typename T> requires (not std::is_void_v<T>)
	struct [[nodiscard]] Gen
	{
		struct promise_type
		{
			[[nodiscard]] auto get_return_object() noexcept -> Gen { return { *this }; }
			[[nodiscard]] auto initial_suspend() const noexcept -> std::suspend_always { return {}; }
			[[nodiscard]] auto final_suspend() const noexcept -> std::suspend_always { return {}; }

			void unhandled_exception()
			{
				last_exception = std::current_exception();
			}

			std::suspend_always yield_value(T const & value)
			{
				value_ptr = std::addressof(value);
				return {};
			}

			void return_void(){}

			T const & value()
			{
				rethrow_exception();
				return *value_ptr;
			}

			void rethrow_exception()
			{
				if (last_exception) { std::rethrow_exception(std::exchange(last_exception, std::exception_ptr())); }
			}

			T const * value_ptr = nullptr;
			std::exception_ptr last_exception = nullptr;
		};
		using promise_handle = std::coroutine_handle<promise_type>;

		struct sentinal{};
		struct iterator
		{
			using value_type = T;
			using reference = value_type const &;
			using pointer = value_type const *;
			using difference_type = std::make_signed_t<std::size_t>;

			iterator& operator++()
			{
				handle.resume();
				if (handle.done()) { handle.promise().rethrow_exception(); }
				return *this;
			}
			void operator++(int)
			{
				++(*this);
			}
			reference operator*() const
			{
				return handle.promise().value();
			}
			pointer operator->() const
			{
				return std::addressof(operator*());
			}
			friend bool operator==(iterator const & lhs, sentinal) noexcept
			{
				return not lhs.handle or lhs.handle.done();
			}
			friend bool operator!=(iterator const & lhs, sentinal rhs) noexcept
			{
				return not (lhs == rhs);
			}

			promise_handle handle;
		};
		static_assert(std::input_iterator<iterator>);

		[[nodiscard]] auto begin() const -> iterator
		{
			auto it = iterator{ m_handle };
			if (m_handle) { ++it; }
			return it;
		}
		[[nodiscard]] auto end() const -> sentinal { return {}; }

		// Default construct for container usage.
		Gen() = default;

		// No copies.
		Gen(Gen const &) = delete;
		Gen& operator=(Gen const &) = delete;

		// Moves are fine.
		Gen(Gen && gen) : m_handle(std::exchange(gen.m_handle, nullptr)){}
		Gen & operator=(Gen && gen) { std::swap(gen.m_handle, gen.m_handle); return *this; }

		// Dtor kills the coroutine frame.
		~Gen() { if (m_handle) { m_handle.destroy(); } }
	private:
		Gen(promise_type & promise) : m_handle{ promise_handle::from_promise(promise) } {}

		promise_handle m_handle{};
	};
	/**
	 * @endcond 
	 */
}