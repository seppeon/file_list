/**
 * @file Glob.hpp
 * @brief A filter that can perform glob operations on a given string.
 */
#pragma once
#include "FL/GlobFixedSeq.hpp"
#include "FL/GlobParts.hpp"
#include "FL/Filt.hpp"
#include <string>

namespace FL
{
	/**
	 * @brief Create a filter(`FL::Filt`) from a glob string..
	 *
	 * @note The glob recursive token "**" is not handled here, from the perspective of u8string processing it doesn't make sense. It will be treated as a single wildcard "*" (as will any number of subsequent wildcards).
	 */
	struct Glob : Filt
	{
		/**
		 * @brief Construct a `FL::Glob` from a glob u8string.
		 * 
		 * @param str The u8string.
		 */
		explicit Glob(std::u8string str);
		/**
		 * @brief Count the number of variable content sections.
		 * 
		 * This includes:
		 * @li [0-9] : range of characters.
		 * @li [ABC] : combinations of characters.
		 * @li ?     : single character wildcards.
		 * @li *     : multi-character wildcards.
		 * 
		 * @return The number of variable content sections.
		 */
		[[nodiscard]] auto GetVariableSectionCount() const noexcept -> std::size_t;
		/**
		 * @brief Get the number of parts in the glob.
		 * 
		 * @return The integer number of parts in the glob.
		 */
		[[nodiscard]] auto PartCount() const noexcept -> std::size_t;
		/**
		 * @brief Check if this glob requires an exact u8string match.
		 * 
		 * @retval true When the u8string given must match an exact u8string.
		 * @retval false When there are one or more variable sections.
		 */
		[[nodiscard]] auto RequiresExactMatch() const noexcept -> bool override;
		/**
		 * @brief Get the parts of the glob.
		 * 
		 * @return A u8string describing the glob.
		 */
		[[nodiscard]] auto Describe() const -> std::string override;
		/**
		 * @brief Get the parts of the glob, each wildcard counts as a split.
		 * 
		 * Given the following pattern:
		 * @li "hello*this"
		 * 
		 * With the following parts (you can view this via a call to describe):
		 * @li ["hello", "*", "this"]
		 * 
		 * The result of the call to `.Parse("helloclamthis")`, the result would be:
		 * @li ["hello", "clam", "this"]
		 * 
		 * @param input The u8string to split.
		 * @param case_sensitive Whether or not the parse will be case sensitive.
		 * @return The resulting pattern from the parse.
		 */
		[[nodiscard]] auto Parse(std::u8string_view input, bool case_sensitive) const -> FL::Pattern override;
	private:
		GlobPartList m_parts;
		FL::GlobFixedSeq m_prefix;
		FL::GlobFixedSeq m_suffix;
		FL::GlobFixedSeqList m_fixed;
		Pattern m_pattern;
	};
}