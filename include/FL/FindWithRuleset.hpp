/**
 * @file FindWithRuleset.hpp
 * @brief This file defines operations with an existing ruleset, such as preprocessing it or finding files with that ruleset.
 */
#pragma once
#include "FL/FindRuleset.hpp"
#include <filesystem>
#include <optional>
#include <string>
#include <vector>

namespace FL
{
	/**
	 * @brief The result type after pre-processing.
	 */
	struct PreprocessedFindRuleset
	{
		std::u8string name;          ///< The name of the ruleset.
		std::vector<FindArg> args;   ///< The arguments that are required to run the ruleset (provided externally).
		std::vector<FindRule> rules; ///< The rules that will be used to resolve the file.

		void Sub(FindRulesetArgsSubstitution const & sub);
	};
	/**
	 * @brief The result of pre-processing a ruleset.
	 * 
	 * This has two impacts:
	 * @li Any included files are placed into the ruleset.
	 * @li Any inherited sections are inserted.
	 */
	struct PreprocessedFindRulesets
	{
		std::vector<PreprocessedFindRuleset> rulesets;
		/**
		 * @brief Get a ruleset by name.
		 * 
		 * @param input The name of the ruleset (this is case sensitive).
		 * @return A pointer to the ruleset, or nullptr if it doesn't exist.
		 */
		[[nodiscard]] auto GetRulesetByName(std::u8string_view input) const noexcept -> PreprocessedFindRuleset const *;
		/**
		 * @brief Check if this is a valid ruleset, when invalid it is unusable.
		 * 
		 * @return true The ruleset is valid.
		 * @return false The ruleset is invalid.
		 */
		[[nodiscard]] bool IsValid() const noexcept;
		/**
		 * @brief Substitute variables into the rules.
		 * 
		 * @param sub The variables to use.
		 */
		void Sub(FindRulesetArgsSubstitution const & sub);
	};
	/**
	 * @brief Preprocess a ruleset, creating a new ruleset without inheritance fields or includes.
	 * 
	 * @param rulesets The old ruleset.
	 * @return The preprocessed ruleset.
	 */
	[[nodiscard]] auto PreprocessRulesets(FindRulesets const & rulesets) -> PreprocessedFindRulesets;
	/**
	 * @brief Get the arguments that are required by a particular ruleset.
	 * 
	 * @param ruleset The rules that will be used to find the file.
	 * @return A list of required arguments.
	 */
	[[nodiscard]] auto GetRequiredArgumentList(FindRuleset const & ruleset) -> std::vector<FindArg>;
	/**
	 * @brief Find a file using a given ruleset.
	 *
	 * @note It may not be immediately obvious why `rulesets` is passed instead of a single ruleset, this is because of ruleset inheritance, where one ruleset can inherit rules from another.
	 * 
	 * @param ruleset The rules that will be used to find the file.
	 * @param file The file that will be searched for (you can include relative paths, if you provide an absolute path, you already have the file...).
	 * @return The file path if it has been found, or std::nullopt if it couldn't be found.
	 */
	[[nodiscard]] auto FindWithRuleset(PreprocessedFindRuleset const & ruleset, std::filesystem::path const & file) -> std::optional<std::filesystem::path>;
}