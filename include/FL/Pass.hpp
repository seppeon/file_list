/**
 * @file Pass.hpp
 * @brief A `FL::Filt` object that always matches the entire given string.
 */
#pragma once
#include "FL/Filt.hpp"

namespace FL
{
	/**
	 * @brief A filter that always produces a string match.
	 */
	struct Pass : Filt
	{
		/**
		 * @brief Check if an filter match is required.
		 * 
		 * @retval true Always.
		 * @retval false Never.
		 */
		[[nodiscard]] auto RequiresExactMatch() const noexcept -> bool override;
		/**
		 * @brief Get the parts of the filter match.
		 * 
		 * @return A u8string describing the filter match.
		 */
		[[nodiscard]] auto Describe() const -> std::string override;
		/**
		 * @brief Get the parts of the filter match, each wildcard counts as a split.
		 * 
		 * Given the following pattern:
		 * @li "hello"
		 * 
		 * With the following parts (you can view this via a call to describe):
		 * @li ["hello"]
		 * 
		 * The result of the call to `.Parse("hello")`, the result would be:
		 * @li ["hello"]
		 * 
		 * @param input The u8string to split.
		 * @param case_sensitive Whether or not the parse will be case sensitive.
		 * @return The resulting pattern from the parse.
		 */
		[[nodiscard]] auto Parse(std::u8string_view input, bool case_sensitive) const -> FL::Pattern override;
		/**
		 * @brief Create an always passing filter.
		 */
		Pass() = default;
	};
}