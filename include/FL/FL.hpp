#pragma once
/**
 * @file FL.hpp
 * @brief Include all the library headers.
 */
#include "FL/Cmd.hpp"
#include "FL/Exact.hpp"
#include "FL/File.hpp"
#include "FL/FileOps.hpp"
#include "FL/Filt.hpp"
#include "FL/Gen.hpp"
#include "FL/Glob.hpp"
#include "FL/GlobFiles.hpp"
#include "FL/GlobFixedSeq.hpp"
#include "FL/GlobParts.hpp"
#include "FL/IteratorFromIndex.hpp"
#include "FL/MemberFunctionTraits.hpp"
#include "FL/Nav.hpp"
#include "FL/NavSettings.hpp"
#include "FL/Pass.hpp"
#include "FL/Pattern.hpp"
#include "FL/PatternTarget.hpp"
#include "FL/FileSearch.hpp"
#include "FL/StrOps.hpp"
#include "FL/MoveFiles.hpp"
/**
 * @brief The FileList namespace.
 */
namespace FL{}