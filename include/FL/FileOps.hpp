/**
 * @file FileOps.hpp
 * @brief Some general purpose file operations, which avoid throwing (unlike the equivilent in `std::filesystem::*`).
 */
#pragma once
#include <filesystem>

namespace FL
{
	/**
	 * @brief Check if a file is hidden.
	 * 
	 * This is treated differently across systems, this will be as per the wikipedia (https://en.wikipedia.org/wiki/Hidden_file_and_hidden_directory):
	 * @li unix-based-systems: Will check if the file is a dot file.
	 * @li windows based systems: Will check for the files hidden attribute.
	 * 
	 * @warning Assumes the file exists.
	 * 
	 * @retval true When the file is hidden.
	 * @retval false When the file is visible.
	 */
	[[nodiscard]] bool IsFileHidden(std::filesystem::path const & p);
	/**
	 * @brief Get the path in the case which the filesystem has stored it.
	 * 
	 * @param p The path to convert.
	 * @return The path in the correct case.
	 */
	[[nodiscard]] auto GetCaseSensitivePath(std::filesystem::path const & p) -> std::filesystem::path;
	/**
	 * @brief The result of fixing up a path.
	 *
	 * This is usually this is an identity function, where `fix_applied` is false. 
	 * A fix may be applied to excessively long paths, where some platforms(cough... Windows) require modifications to the path for long paths.
	 */
	struct FixResult
	{
		bool fix_applied; ///< Whether or not the path was adjusted, if it was you likely need to call UnfixLongPath before providing a path back to the user.
		std::filesystem::path result; ///< The path that should be used for accessing the filesystem.
	};
	/**
	 * @brief Get a path that can be used without excessive workarounds for long paths.
	 * 
	 * @note This function exists becuase windows will throw an exception if you try to perform an operation with a path that is longer than 260 characters.
	 * 
	 * @param p The path to fix.
	 * @return An adjusted path when needed, otherwise this is an identity function.
	 */
	[[nodiscard]] auto FixLongPath(std::filesystem::path p) -> FixResult;
	/**
	 * @brief Reverse the operation applied by `FL::FixLongPath`.
	 * 
	 * @note This function exists becuase windows will throw an exception if you try to perform an operation with a path that is longer than 260 characters.
	 * 
	 * @param p The path to fix.
	 * @return An original path when needed, otherwise this is an identity function.
	 */
	[[nodiscard]] auto UnfixLongPath(std::filesystem::path p) -> std::filesystem::path;
	/**
	 * @brief Check if a path points to a symlink.
	 * 
	 * @param p The path to check.
	 * @return true The path resolves to a symlink.
	 * @return false The path resolves to a non-symlink.
	 */
	[[nodiscard]] bool IsSymlink(std::filesystem::path const & p);
	/**
	 * @brief Check if a file exists.
	 * 
	 * @param p The path to check.
	 * @return true The file exists.
	 * @return false The file that doesn't exist.
	 */
	[[nodiscard]] bool FileExists(std::filesystem::path const & p);
	/**
	 * @brief Check if a file is a regular file.
	 * 
	 * @param p The path to check.
	 * @return true The path is a regular file.
	 * @return false The path is not a regular file.
	 */
	[[nodiscard]] bool IsRegularFile(std::filesystem::path const & p);
	/**
	 * @brief Check if a file is a directory.
	 * 
	 * @param p The path to check.
	 * @return true The path is a directory.
	 * @return false The path is not a directory.
	 */
	[[nodiscard]] bool IsDirectory(std::filesystem::path const & p);
	/**
	 * @brief Get the path of the current executable (not the current working directory).
	 * 
	 * @return The path of the current executable.
	 */
	[[nodiscard]] auto CurrentExePath() -> std::filesystem::path;
}