/**
 * @file FindExeVersion.hpp
 * @brief Some functions to get the current versions of some known applications.
 * 
 * This will likely be removed at some point, preferring a user customisable json file, defining how
 * versions are retrieved from whatever list of applications they find relevant.
 */
#pragma once
#include "Semver.hpp"
#include <filesystem>

namespace FL
{
	[[nodiscard]] auto GetCmakeVersion( std::filesystem::path exe = "cmake" ) -> FL::Semver;
	[[nodiscard]] auto GetGccVersion( std::filesystem::path exe = "gcc" ) -> FL::Semver;
	[[nodiscard]] auto GetGppVersion( std::filesystem::path exe = "g++" ) -> FL::Semver;
	[[nodiscard]] auto GetClangVersion( std::filesystem::path exe = "clang" ) -> FL::Semver;
	[[nodiscard]] auto GetClangPpVersion( std::filesystem::path exe = "clang++" ) -> FL::Semver;
	[[nodiscard]] auto GetNinjaVersion( std::filesystem::path exe = "ninja" ) -> FL::Semver;
}