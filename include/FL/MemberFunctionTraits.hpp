/**
 * @file MemberFunctionTraits.hpp
 * @brief Some type traits.
 */
#pragma once

namespace FL
{
	/**
	 * @brief Get some basic traits for a member function from the member function type.
	 * 
	 * @tparam T The member function type.
	 */
	template <typename T>
	struct MemberFunctionTraits;
	/**
	 * @cond Doxygen_Suppress
	 */
	template <typename...>
	struct TL {};
	template <typename>
	struct TLFirst { using type = void; };
	template <typename Front, typename...Args>
	struct TLFirst<TL<Front, Args...>> { using type = Front; };

	template <typename Obj, typename Ret, typename...Args>
	struct MemberFunctionTraits<Ret (Obj::*)(Args...)>
	{
		using return_type = Ret;
		using object_type = Obj;
		using args_type = TL<Args...>;
	};
	template <typename Obj, typename Ret, typename...Args>
	struct MemberFunctionTraits<Ret (Obj::*)(Args...) const>
	{
		using return_type = Ret;
		using object_type = Obj;
		using args_type = TL<Args...>;
	};
	template <typename Obj, typename Ret, typename...Args>
	struct MemberFunctionTraits<Ret (Obj::*)(Args...) volatile >
	{
		using return_type = Ret;
		using object_type = Obj;
		using args_type = TL<Args...>;
	};
	template <typename Obj, typename Ret, typename...Args>
	struct MemberFunctionTraits<Ret (Obj::*)(Args...) volatile const>
	{
		using return_type = Ret;
		using object_type = Obj;
		using args_type = TL<Args...>;
	};
	template <typename Obj, typename Ret, typename...Args>
	struct MemberFunctionTraits<Ret (Obj::*)(Args...) noexcept>
	{
		using return_type = Ret;
		using object_type = Obj;
		using args_type = TL<Args...>;
	};
	template <typename Obj, typename Ret, typename...Args>
	struct MemberFunctionTraits<Ret (Obj::*)(Args...) const noexcept>
	{
		using return_type = Ret;
		using object_type = Obj;
		using args_type = TL<Args...>;
	};
	template <typename Obj, typename Ret, typename...Args>
	struct MemberFunctionTraits<Ret (Obj::*)(Args...) volatile noexcept>
	{
		using return_type = Ret;
		using object_type = Obj;
		using args_type = TL<Args...>;
	};
	template <typename Obj, typename Ret, typename...Args>
	struct MemberFunctionTraits<Ret (Obj::*)(Args...) volatile const noexcept>
	{
		using return_type = Ret;
		using object_type = Obj;
		using args_type = TL<Args...>;
	};
	/**
	 * @endcond
	 */
}