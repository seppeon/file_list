/**
 * @file Exact.hpp
 * @brief A filter definition.
 */
#pragma once
#include "FL/Filt.hpp"

namespace FL
{
	/**
	 * @brief A filter that requires an exact string match.
	 */
	struct Exact : Filt
	{
		/**
		 * @brief Check if this exact string match requires an exact u8string match.
		 * 
		 * @retval true When the u8string given must match an exact u8string.
		 * @retval false When there are one or more variable sections.
		 */
		[[nodiscard]] auto RequiresExactMatch() const noexcept -> bool override;
		/**
		 * @brief Get the parts of the exact string match.
		 * 
		 * @return A u8string describing the exact string match.
		 */
		[[nodiscard]] auto Describe() const -> std::string override;
		/**
		 * @brief Get the parts of the exact string match, each wildcard counts as a split.
		 * 
		 * Given the following pattern:
		 * @li "hello"
		 * 
		 * With the following parts (you can view this via a call to describe):
		 * @li ["hello"]
		 * 
		 * The result of the call to `.Parse("hello")`, the result would be:
		 * @li ["hello"]
		 * 
		 * @param input The u8string to split.
		 * @param case_sensitive Whether or not the parse will be case sensitive.
		 * @return The resulting pattern from the parse.
		 */
		[[nodiscard]] auto Parse(std::u8string_view input, bool case_sensitive) const -> FL::Pattern override;
		/**
		 * @brief Create an exact match filter.
		 */
		explicit Exact(std::u8string contents);
	private:
		std::u8string m_contents;
	};
}