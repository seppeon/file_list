/**
 * @file File.hpp
 * @brief Abstraction of a file.
 */
#pragma once
#include <filesystem>
#include <vector>
#include <memory>
#include <span>

namespace FL
{
	/**
	 * @brief A caching read-only data structure which can be used to read a files contents/path.
	 */
	struct File
	{
		/**
		 * @brief Get the time that the file was last written to.
		 * 
		 * @return The time the file was last written.
		 */
		[[nodiscard]] auto LastModifiedTime() const -> std::filesystem::file_time_type;
		/**
		 * @brief Get the size of the file in bytes.
		 * 
		 * @return The size of the file in bytes.
		 */
		[[nodiscard]] auto Size() const -> std::size_t;
		/**
		 * @brief Get the path of the file.
		 * 
		 * @return The path of the file.
		 */
		[[nodiscard]] auto Path() const -> std::filesystem::path;
		/**
		 * @brief Get a view of the contents of the file.
		 * 
		 * @return A view of the contents of the file.
		 */
		[[nodiscard]] auto Read() const -> std::span<char const>;
		/**
		 * @brief Construct the file from a path.
		 *
		 * @warning This assumes the path exists.
		 * 
		 * @param path The path.
		 */
		 explicit File(std::filesystem::path path);
	private:
		std::filesystem::path m_path;
		mutable std::shared_ptr<std::vector<char>> m_contents;
	};
}