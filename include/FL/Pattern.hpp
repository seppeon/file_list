/**
 * @file Pattern.hpp
 * @brief A class which represents the result of an operation which breaks a given string into several parts.
 */
#pragma once
#include "IteratorFromIndex.hpp"
#include <span>
#include <string>
#include <vector>
#include <filesystem>

namespace FL
{
	struct Pattern;
	/**
	 * @brief Represents a single pattern section.
	 */
	using PatternValue = std::u8string_view;
	/**
	 * @brief Represent a pattern substitution
	 */
	struct PatternSub
	{
		using size_type = std::size_t; ///< The type of sizes.
		size_type index;     ///< The index of the substitution (the contents of this position will be replaced regardless of whether it was a known or unknown section.
		std::u8string value; ///< The value to move into the position of the substitution.
	};
	/**
	 * @brief A class representing a string split by some regex or glob operations, the capture groups substitutable.
	 */
	struct Pattern
	{
		using value_type = PatternValue;						///< Iterator required types.
		using reference = value_type;							///< Iterator required types.
		using size_type = std::size_t;							///< Iterator required types.
		using difference_type = std::make_signed_t<size_type>;	///< Iterator required types.
		/**
		 * @brief Default construct the `FL::Pattern`.
		 */
		Pattern() = default;
		/**
		 * @brief Construct the pattern from the constant sections.
		 * 
		 * @note To represent a pattern that starts in a non-constant section, provide an empty constant part as the first part (in constant_parts).
		 * @note To represent a pattern that ends in a non-constant section, provide an empty constant part as the last part (in constant_parts).
		 * 
		 * @param constant_parts A pointer to a span of constant parts for the pattern.
		 */
		explicit Pattern(std::span<std::u8string_view const> constant_parts);
		/**
		 * @brief Construct from initializer_list
		 * 
		 * @param constant_parts A list of constant parts in the pattern.
		 */
		Pattern(std::initializer_list<std::u8string_view> constant_parts);
		/**
		 * @brief Get the total length of the constant sections.
		 * 
		 * @return The total length of the constant sections.
		 */
		[[nodiscard]] auto GetTotalLength() const noexcept -> size_type;
		/**
		 * @brief Check if the pattern has any valid parts.
		 * 
		 * @return true The pattern has any valid parts.
		 * @return false The pattern has no valid parts.
		 */
		[[nodiscard]] auto IsValid() const noexcept -> bool;
		/**
		 * @brief Get the number of constant parts in the pattern.
		 * 
		 * @return The number of parts.
		 */
		[[nodiscard]] auto Size() const noexcept -> size_type;
		/**
		 * @brief Get the string view representing the constant section at an index.
		 * 
		 * @param i The index.
		 * @return The particular part string.
		 */
		[[nodiscard]] auto operator[](difference_type i) const noexcept -> reference;
		/**
		 * @brief Get the first part in the pattern.
		 * 
		 * @return The first part in the pattern.
		 */
		[[nodiscard]] auto Front() const noexcept -> value_type;
		/**
		 * @brief Get the last part in the pattern.
		 * 
		 * @return The last part in the pattern.
		 */
		[[nodiscard]] auto Back() const noexcept -> value_type;
		/**
		 * @brief Construct a string substituting the non-constant sections (the sections between each part in this pattern).
		 * 
		 * @note Time complexity: O(N)
		 * @note Space complexity: O(N)
		 * 
		 * @param substitutions The parts of the pattern to substitute.
		 * @return The resulting pattern after substitutions.
		 */
		[[nodiscard]] auto Substitute(std::span<PatternSub const> substitutions) const -> Pattern;
		/**
		 * @brief Pattern iterators are derived from the operator[] and Size() member functions, largely because it hides vector's impl and increases implementation flexibility.
		 */
		using const_iterator = IteratorFromIndex<Pattern const>;
		/**
		 * @brief Get the iterator to the begin part.
		 * 
		 * @return The iterator.
		 */
		[[nodiscard]] auto begin() const noexcept -> const_iterator;
		/**
		 * @brief Get the iterator to the end part.
		 * 
		 * @return The iterator.
		 */
		[[nodiscard]] auto end() const noexcept -> const_iterator;
		/**
		 * @brief Convert the pattern to a single string.
		 * 
		 * @return The string.
		 */
		[[nodiscard]] auto AsU8String() const noexcept -> std::u8string;
		/**
		 * @brief Convert the pattern to a path.
		 * 
		 * @return The string.
		 */
		[[nodiscard]] auto AsPath() const noexcept -> std::filesystem::path;
	private:
		std::vector<size_type> m_indexes;
		std::u8string m_parts;
	};
	/**
	 * @brief Describe a Pattern.
	 * 
	 * @param pattern The pattern to describe.
	 * @return The description.
	 */
	[[nodiscard]] std::string DescribePattern(Pattern const & pattern);
	/**
	 * @brief Describe a list of substitutions.
	 * 
	 * @param substitutions The list to describe.
	 * @return The description.
	 */
	[[nodiscard]] std::string DescribeSubstitutionList(std::span<PatternSub const> substitutions);
	/**
	 * @brief Describe a list of substitutions.
	 * 
	 * @param substitutions The list to describe.
	 * @return The description.
	 */
	[[nodiscard]] std::string DescribeSubstitutionVector(std::vector<PatternSub> const & substitutions);
}