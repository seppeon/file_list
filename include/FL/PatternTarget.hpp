/**
 * @file PatternTarget.hpp
 * @brief An object which can have a pattern applied, and will transform that pattern into a new pattern.
 */
#pragma once
#include "FL/Pattern.hpp"
#include <cstddef>
#include <iterator>
#include <variant>
#include <string>
#include <string_view>

namespace FL
{
	/**
	 * @brief The symbol used to prefix indexes of pattern parts.
	 */
	inline constexpr auto pattern_target_capture = u8':';
	/**
	 * @brief A pattern target is the place a pattern can be applied in order to produce a new pattern.
	 * 
	 * The colon character is defined as the "capture" character, it captures one of the replacements within the target.
	 * 
	 * Replacements are defined with a colon and an index `:0` for example would be replaced with the data at index 0 of a pattern. Dollar signs can be escaped with a 
	 * backslash, backslashes can be escaped with another backslash. A colon without subsequent digits does not require escaping (likely you should do it anyway).
	 * 
	 * Colon was choosen, as this is primarily a file renaming tool, and colon is not valid (or generally discouraged) within filenames on most platforms.
	 * 
	 * For example:
	 * 
	 * @li With this glob pattern `["hello", "*", "World"]`.
	 * @li Given the string `hello wonderful world`. The parse resulting in this pattern: `["hello", " wonderful ", "world"]`.
	 * @li If the pattern was applied to the target `goodbye:1mars` the produced pattern would be: `["goodbye", " wonderful ", "mars"]`.
	 * 
	 * This is useful replacing parts of paths, or parts of text.
	 * 
	 * @warning Indexes are 32 bit unsigned, indexes above this will be absolutely ignored, so if you are insane enough to meet this limitation, consider a two stage replacement.
	 */
	class PatternTarget
	{
		struct Part
		{
			bool is_length;              ///< whether or not the part represents length or index.
			std::size_t length_or_index; ///< when is_length is true this is the length consumed in contents, otherwise the index of the pattern.
		};
		std::vector<Part> m_parts;
		std::u8string m_contents;
		std::size_t m_capture_count = 0;
	public:
		/**
		 * @brief Construct a pattern target from a pattern string.
		 * 
		 * @param input The input string to parse.
		 */
		explicit PatternTarget(std::u8string_view input);
		/**
		 * @brief Get the number of parts in the pattern target.
		 * 
		 * @return The number of parts in the pattern target.
		 */
		[[nodiscard]] auto Size() const noexcept -> std::size_t;
		/**
		 * @brief Get the number of captures in the target.
		 * 
		 * @return The number of captures in the pattern target.
		 */
		[[nodiscard]] auto GetCaptureCount() const noexcept -> std::size_t;
		/**
		 * @brief Get the highest index requested from some pattern.
		 * 
		 * @return The index.
		 */
		[[nodiscard]] auto GetHighestPatternIndex() const noexcept -> std::size_t;
		/**
		 * @brief A bidirectional iterator type.
		 *
	 	 * @cond Doxygen_Suppress
	 	 * This is suppressed because nobody cares, as long as its a bidirectional iterator.
		 */
		struct const_iterator
		{
			using size_type = std::size_t;
			using value_type = std::variant<std::u8string_view, size_type>;
			using reference = value_type;
			using pointer = void;
			using difference_type = std::make_signed_t<size_type>;
			using iterator_tag = std::bidirectional_iterator_tag;

			const_iterator & operator++() noexcept;
			[[nodiscard("use ++it instead")]] const_iterator operator++(int) noexcept;
			const_iterator & operator--() noexcept;
			[[nodiscard("use ++it instead")]] const_iterator operator--(int) noexcept;
			[[nodiscard]] reference operator*() const noexcept;
			friend bool operator==(const_iterator const & l, const_iterator const & r) noexcept;
			friend bool operator!=(const_iterator const & l, const_iterator const & r) noexcept;

			const_iterator();
			const_iterator(size_type index, Part const * part_ptr, char8_t const * contents);
		private:
			static size_type GetLength(Part const * part_ptr) noexcept;

			size_type m_index = 0;
			Part const * m_part_ptr = nullptr;
			char8_t const * m_contents = nullptr;
		};
		/**
		 * @endcond
		 */
		static_assert(std::bidirectional_iterator<const_iterator>);
		/**
		 * @brief A bidirectional iterator to the first valid part.
		 * 
		 * @return The iterator.
		 */
		[[nodiscard]] auto begin() const noexcept -> const_iterator;
		/**
		 * @brief An bidirectional iterator to one after the last valid part.
		 * 
		 * @return The iterator.
		 */
		[[nodiscard]] auto end() const noexcept -> const_iterator;
		/**
		 * @brief Apply the pattern to the pattern target, producing a new pattern.
		 * 
		 * @note If the pattern target has pattern indexes outside the range of `pattern` this function will return an invalid pattern, testable with `.IsValid()`.
		 * 
		 * @param pattern The pattern to apply the target.
		 * @return Pattern The new pattern, or an invalid pattern on failure.
		 */
		[[nodiscard]] auto Apply(Pattern const & pattern) const -> Pattern;
		/**
		 * @brief Allow storage in ordered containers.
		 */
		friend bool operator<(PatternTarget const & l, PatternTarget const & r) noexcept;
	};
	/**
	 * @cond Doxygen_Suppress
	 * This is suppressed because nobody cares, as long as its a bidirectional iterator.
	 */
	[[nodiscard]] bool operator<(PatternTarget const & l, PatternTarget const & r) noexcept;
	[[nodiscard]] bool operator==(PatternTarget::const_iterator const & l, PatternTarget::const_iterator const & r) noexcept;
	[[nodiscard]] bool operator!=(PatternTarget::const_iterator const & l, PatternTarget::const_iterator const & r) noexcept;
	/**
	 * @endcond
	 */
}