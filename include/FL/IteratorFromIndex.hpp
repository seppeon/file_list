/**
 * @file IteratorFromIndex.hpp
 * @brief Create an iterator from a class supporting unary operator and size.
 */
#pragma once
#include "FL/MemberFunctionTraits.hpp"
#include <iterator>

namespace FL
{
	/**
	 * @brief Easily add random access iterator support to a class with operator[] and Size().
	 * 
	 * @cond Doxygen_Suppress
	 *
	 * @tparam T The type of the object to create an iterator for.
	 */
	template <typename T>
	class IteratorFromIndex
	{
		using self_type = IteratorFromIndex<T>;
		using unary_operator = MemberFunctionTraits<decltype(&T::operator[])>;
		using args_type = typename unary_operator::args_type;
	public:
		using difference_type = typename TLFirst<args_type>::type;
		using reference = typename unary_operator::return_type;
		using value_type = std::remove_reference_t<reference>;
		using pointer = std::conditional_t<std::is_reference_v<reference>, std::add_pointer_t<std::remove_reference_t<reference>>, void>;
		using iterator_category = std::random_access_iterator_tag;

		constexpr self_type & operator--() noexcept
		{
			--m_index;
			return *this;
		}
		constexpr self_type operator--(int) noexcept
		{
			auto out = *this;
			--out;
			return out;
		}
		constexpr self_type & operator-=(difference_type d) noexcept
		{
			m_index -= d;
			return *this;
		}
		constexpr self_type & operator++() noexcept
		{
			++m_index;
			return *this;
		}
		constexpr self_type operator++(int) noexcept
		{
			auto out = *this;
			++*this;
			return out;
		}
		constexpr self_type & operator+=(difference_type d) noexcept
		{
			m_index += d;
			return *this;
		}
		friend constexpr self_type operator+(self_type const & lhs, difference_type rhs) noexcept
		{
			return self_type{ *lhs.m_ptr, lhs.m_index + rhs};
		}
		friend constexpr self_type operator+(difference_type lhs, self_type const & rhs) noexcept
		{
			return self_type{ *rhs.m_ptr, lhs + rhs.m_index};
		}
		friend constexpr self_type operator-(self_type const & lhs, difference_type rhs) noexcept
		{
			return self_type{ *lhs.m_ptr, lhs.m_index - rhs};
		}
		friend constexpr difference_type operator-(self_type const & lhs, self_type const & rhs) noexcept
		{
			return lhs.m_index - rhs.m_index;
		}
		friend constexpr bool operator<(self_type const & lhs, self_type const & rhs) noexcept
		{
			return lhs.m_index < rhs.m_index;
		}
		friend constexpr bool operator>(self_type const & lhs, self_type const & rhs) noexcept
		{
			return lhs.m_index > rhs.m_index;
		}
		friend constexpr bool operator<=(self_type const & lhs, self_type const & rhs) noexcept
		{
			return lhs.m_index <= rhs.m_index;
		}
		friend constexpr bool operator>=(self_type const & lhs, self_type const & rhs) noexcept
		{
			return lhs.m_index >= rhs.m_index;
		}
		friend constexpr bool operator==(self_type const & lhs, self_type const & rhs) noexcept
		{
			return lhs.m_index == rhs.m_index;
		}
		friend constexpr bool operator!=(self_type const & lhs, self_type const & rhs) noexcept
		{
			return lhs.m_index != rhs.m_index;
		}
		constexpr reference operator*() const noexcept
		{
			return (*m_ptr)[m_index];
		}
		constexpr reference operator[](difference_type i) const noexcept
		{
			return (*m_ptr)[m_index + i];
		}
		/**
		 * @brief Construct the iterator from a reference to the container with the operator[] and size() and index.
		 */
		constexpr IteratorFromIndex(T const & ref, difference_type index) noexcept
			: m_index(index)
			, m_ptr( std::addressof(ref) )
		{}
		constexpr IteratorFromIndex() = default;
	private:
		difference_type m_index = 0;
		T const * m_ptr = nullptr;
	};
	/**
	 * @endcond 
	 */
}