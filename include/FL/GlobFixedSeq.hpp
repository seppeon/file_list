/**
 * @file GlobFixedSeq.hpp
 * @brief A set of utility functions and an abstraction of a set of constant width `FL::Filt` objects.
 */
#pragma once
#include "FL/GlobParts.hpp"
#include <span>
#include <string_view>

namespace FL
{
	/**
	 * @brief Represents a sequence of glob parts, each of which is a fixed length (the number of characters consumed is known).
	 *
	 * @note This is used as part of the implementation of glob, which is a `FL::Filt` producing a `FL::Pattern`. 
	 */
	struct GlobFixedSeq
	{
		/**
		 * @brief The sum of the sizes of the fixed sized parts.
		 * 
		 * @return The sum of sizes of this objects parts.
		 */
		[[nodiscard]] std::size_t Size() const noexcept;
		/**
		 * @brief Find this fixed sequence of parts within some input string.
		 * 
		 * @param input The input string.
		 * @param case_sensitive Is the search case sensitive?
		 * @return The index which the fixed sequence was found, or `glob_npos` if it is never found.
		 */
		[[nodiscard]] std::size_t Find(std::u8string_view input, bool case_sensitive) const noexcept;
		/**
		 * @brief Check if some string begins in the fixed sequence of parts.
		 * 
		 * @param input The input string.
		 * @param case_sensitive Is the search case sensitive?
		 * @return true when the `input` begins with this fixed sequence of parts.
		 */
		[[nodiscard]] bool HasPrefix(std::u8string_view input, bool case_sensitive) const noexcept;
		/**
		 * @brief Whether or not this will be saved in the `FL::Pattern` of the glob.
		 * 
		 * @return true This object should have any matches saved as parts of the pattern.
		 * @return false This object should not have anything saved as part of the produced pattern.
		 */
		[[nodiscard]] bool ShouldSave() const noexcept;
		/**
		 * @brief Get a string to describe this fixed section.
		 *
		 * @note This can be useful debugging patterns.
		 * 
		 * @return The string.
		 */
		[[nodiscard]] std::string Describe() const noexcept;

		std::span<GlobPartType const> parts; ///< A span that can be used to view some fixed width parts of the glob.
		std::size_t offset = 0; ///< The number of characters offset into the string.
	};
	static_assert(FixedLength<GlobFixedSeq>);
	/**
	 * @brief Given a list of glob parts, extract the prefix representing the fixed sequence.
	 * 
	 * @param parts A span of the parts to extract the prefix.
	 * @return The prefix of fixed width parts.
	 */
	[[nodiscard]] GlobFixedSeq GetFixedSeqPrefix(std::span<GlobPartType const> parts) noexcept;
	/**
	 * @brief Given a list of glob parts, extract the suffix representing the fixed sequence.
	 * 
	 * @param parts A span of the parts to extract the suffix.
	 * @return The suffix of fixed width parts.
	 */
	[[nodiscard]] GlobFixedSeq GetFixedSeqSuffix(std::span<GlobPartType const> parts) noexcept;

	using GlobFixedSeqList = std::vector<GlobFixedSeq>;
	/**
	 * @brief Get a list of fixed sequences from a given list of parts.
	 * 
	 * @param offset The number of characters offset into the sequence list (loaded into the .offset field in `GlobFixedSeq`, each element will be offset to be after the previous element).
	 * @param parts A span of parts to extract the fixed sequences.
	 * @return A list of fixed width sequences.
	 */
	[[nodiscard]] auto GetFixedSeqList(std::size_t offset, std::span<GlobPartType const> parts) noexcept -> GlobFixedSeqList;
	/**
	 * @brief Get the count of parts of a fixed sequence.
	 * 
	 * @param seq The fixed sequence.
	 * @return The count of parts.
	 */
	[[nodiscard]] auto GetFixedSeqPartsCount(GlobFixedSeq const & seq) noexcept -> std::size_t;
	/**
	 * @brief Get the count of parts of a fixed sequence list.
	 * 
	 * @param list The fixed sequence list.
	 * @return The count of parts.
	 */
	[[nodiscard]] auto GetFixedSeqListPartsCount(GlobFixedSeqList const & list) noexcept -> std::size_t;
	/**
	 * @brief Get the description of some fixed sequence list.
	 * 
	 * @param list The fixed sequence list.
	 * @return A string description of the list.
	 */
	[[nodiscard]] auto GetFixedSeqListDescribe(GlobFixedSeqList const & list) noexcept -> std::string;
}
