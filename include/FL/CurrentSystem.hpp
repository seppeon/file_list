#pragma once

namespace FL
{
	/**
	 * @brief Get the current deliminator for this system's environmental variables.
	 * 
	 * @note see: https://www.ibm.com/docs/en/informix-servers/12.10?topic=products-path-environment-variable
     *
	 * @return The deliminator.
	 */
	[[nodiscard]] auto CurrentEnvVarDelim() noexcept -> char8_t;
}