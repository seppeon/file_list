/**
 * @file FileSearch.hpp
 * @brief A set of classes and functions to perform resolution of a file using various techniques.
 */
#pragma once
#include "FL/CurrentSystem.hpp"
#include <string_view>
#include <optional>
#include <filesystem>

namespace FL
{
	/**
	 * @brief The result of a call to one of the file search functions.
	 * 
	 * It represents an optional path.
	 */
	using FileSearchResult = std::optional<std::filesystem::path>;
	/**
	 * @brief Attempt to find the file within the specific directory.
	 * 
	 * @param file The file to find.
	 * @param directory The directory.
	 * @return If the file found, the path, otherwise std::nullopt.
	 */
	[[nodiscard]] auto FileSearchDirectory(std::filesystem::path const & file, std::filesystem::path const & directory) -> FileSearchResult;
	/**
	 * @brief Attempt to find the file within the paths defined in an environmental variable.
	 * 
	 * @param file The file to find.
	 * @param var The environmental variable (perhaps PATH).
	 * @param delim The character used to seperate paths in the variable.
	 * @return If the file found, the path, otherwise std::nullopt.
	 */
	[[nodiscard]] auto FileSearchEnvironmentVariableDirs(std::filesystem::path const & file, std::u8string_view var, char8_t delim = CurrentEnvVarDelim()) -> FileSearchResult;
	/**
	 * @brief Attempt to find the file using the paths found in a registry entry.
	 * 
	 * @warning This will only check that the filename and extension matches the requested file, prefixes to the filename will not be checked.
	 * 
	 * @param file The file to find.
	 * @param entry The registry entry.
	 * @param relative The path which all values of the key will be relative to.
	 * @return If the file found, the path, otherwise std::nullopt.
	 */
	[[nodiscard]] auto FileSearchRegistryKey(std::filesystem::path const & file, std::u8string_view entry, std::filesystem::path const & relative = "") -> FileSearchResult;
	/**
	 * @brief Attempt to find the file using the paths found in a registry entry.
	 * 
	 * Useful variables on windows:
	 * @li Machine: `HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment`
	 * @li User: `KEY_CURRENT_USER\Environment`
	 * @warning This will do nothing on a unix system, provide additional search rules to cover this issue.
	 * 
	 * @param file The file to find.
	 * @param entry The registry key.
	 * @param value The value to look up from the key.
	 * @param relative The value will be relative to this path.
	 * @return If the file found, the path, otherwise std::nullopt.
	 */
	[[nodiscard]] auto FileSearchRegistryKeyValue(std::filesystem::path const & file, std::u8string_view entry, std::u8string_view value, std::filesystem::path const & relative = "") -> FileSearchResult;
	/**
	 * @brief Attempt to find the file using the system rules.
	 * 
	 * @note On windows this uses `LoadLibraryA` + `GetModuleFileNameA`.
	 * @note On linux this uses `lpconfig -p`, which is not always a complete list.
	 * 
	 * @param file The file to find.
	 * @return If the file found, the path, otherwise std::nullopt.
	 */
	[[nodiscard]] auto FileSearchForDynamicLibrary(std::filesystem::path const & file) -> FileSearchResult;
	/**
	 * @brief Attempt to find the file using the system's default exectuable loader rules.
	 * 
	 * @note On windows this will search using SearchPathA, then the directories in PATH, then common paths (Program Files, Program Files (x86).
	 * @note On linux, this will also search for directories in PATH, then common paths (/bin, /usr/bin).
	 * @note If an extension is not provided, this will attempt to find an executable using known common extensions.
	 * 
	 * @param file The file to find.
	 * @return If the file is found, the path otherwise std::nullopt.
	 */
	[[nodiscard]] auto FileSearchForExecutable(std::filesystem::path const & file) -> FileSearchResult;
}