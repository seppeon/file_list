/**
 * @file Filt.hpp
 * @brief A filter base class.
 */
#pragma once
#include "FL/Pattern.hpp"
#include <string>

namespace FL
{
	/**
	 * @brief The base class for any filter that can take a string and output a `FL::Pattern`. 
	 * 
	 * An example filter would be a regex filter, where the capture groups may be considered part of the pattern.
	 */
	struct Filt
	{
		/**
		 * @brief Check if this filter requires an exact string match.
		 * 
		 * @note This does not include case sensitivity.
		 * 
		 * @retval true When the string given must match an exact string.
		 * @retval false When there are one or more variable sections.
		 */
		[[nodiscard]] virtual auto RequiresExactMatch() const noexcept -> bool = 0;
		/**
		 * @brief Get the parts of the filter.
		 * 
		 * @return A string describing the parts of the filter.
		 */
		[[nodiscard]] virtual auto Describe() const -> std::string = 0;
		/**
		 * @brief Get the parts of the filter, each wildcard counts as a split.
		 * 
		 * Given the following glob pattern:
		 * @li "hello*this"
		 * 
		 * With the following parts (you can view this via a call to describe):
		 * @li ["hello", "*", "this"]
		 * 
		 * The result of the call to `.Parse("helloclamthis")`, the result would be:
		 * @li ["hello", "clam", "this"]
		 * 
		 * @param input The u8string to split.
		 * @param case_sensitive Whether or not the parse will be case sensitive.
		 * @return The resulting pattern from the parse.
		 */
		[[nodiscard]] virtual auto Parse(std::u8string_view input, bool case_sensitive) const -> FL::Pattern = 0;
	};
}