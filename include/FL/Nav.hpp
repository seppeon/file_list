/**
 * @file Nav.hpp
 * @brief An abstraction over navigation, when you have several concurrent working directories (as you do when you perform a glob, at each part of a globbed path, there are many possible paths to follow).
 */
#pragma once
#include "FL/Filt.hpp"
#include "FL/NavSettings.hpp"
#include <filesystem>
#include <set>

namespace FL
{
	/**
	 * @brief An item that represents each navigated file.
	 */
	struct NavItem
	{
		std::filesystem::path path;	///< Path of the current file.
		FL::Pattern name;			///< The pattern of the match up to the current file.
	};
	/**
	 * @brief A list of NavItems.
	 */
	using NavList = std::vector<NavItem>;
	/**
	 * @brief Less then operator for NavItem.
	 * 
	 * @return lhs < rhs
	 */
	[[nodiscard]] bool operator<(NavItem const & lhs, NavItem const & rhs) noexcept;
	/**
	 * @brief Greater then operator for NavItem.
	 * 
	 * @return lhs > rhs
	 */
	[[nodiscard]] bool operator>(NavItem const & lhs, NavItem const & rhs) noexcept;
	/**
	 * @brief Less or equal then operator for NavItem.
	 * 
	 * @return lhs <= rhs
	 */
	[[nodiscard]] bool operator<=(NavItem const & lhs, NavItem const & rhs) noexcept;
	/**
	 * @brief Greater or equal then operator for NavItem.
	 * 
	 * @return lhs >= rhs
	 */
	[[nodiscard]] bool operator>=(NavItem const & lhs, NavItem const & rhs) noexcept;
	/**
	 * @brief Equal then operator for NavItem.
	 * 
	 * @return lhs == rhs
	 */
	[[nodiscard]] bool operator==(NavItem const & lhs, NavItem const & rhs) noexcept;
	/**
	 * @brief Not equal then operator for NavItem.
	 * 
	 * @return lhs != rhs
	 */
	[[nodiscard]] bool operator!=(NavItem const & lhs, NavItem const & rhs) noexcept;
	/**
	 * @brief A tool for navigating a list of paths simultaneously (as required by any globbing tool).
	 */
	struct Nav
	{
		using opt_set = std::set<NavItem>; ///< The type used to store the currently viable directories and folders.
		/**
		 * @brief Open a specific directory.
		 * 
		 * @param name The name of the directory.
		 * @return true There are still options in the nav.
		 * @return false No more options.
		 */
		[[nodiscard]] bool Open(std::u8string_view name);
		/**
		 * @brief Apply a particular filter to the currently possible file options.
		 * 
		 * @param filt The filter to apply to recursively found directories.
		 * @return true There are still options in the nav.
		 * @return false No more options.
		 */
		[[nodiscard]] bool Apply(Filt const & filt);
		/**
		 * @brief Apply a filter recursively to the currently possible file options.
		 * 
		 * @param filt The filter to apply to recursively found directories.
		 * @return true There are still options in the nav.
		 * @return false No more options.
		 */
		[[nodiscard]] bool ApplyRecursive(Filt const & filt);
		/**
		 * @brief Add all recursive dirs.
		 * 
		 * @return true There are still options in the nav.
		 * @return false No more options.
		 */
		[[nodiscard]] bool AddRecursive();
		/**
		 * @brief Get a random access container of the current state of the nav.
		 * 
		 * @return The random access container.
		 */
		[[nodiscard]] auto CurrentNav() const noexcept -> NavList;
		/**
		 * @brief Setup the initial state of the nav.
		 * 
		 * @param settings The navigation settings used for all navigation operations.
		 * @param cwd The current working directory.
		 */
		explicit Nav(NavSettings const & settings, std::filesystem::path cwd = std::filesystem::current_path());
	private:
		NavSettings m_settings;
		opt_set m_options;
	};
}