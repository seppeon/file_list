/**
 * @file GlobParts.hpp
 * @brief A set of `FL::Filt` definitions, and some utility functions to operate on them.
 */
#pragma once
#include "Pattern.hpp"
#include <string_view>
#include <concepts>
#include <string>
#include <cstddef>
#include <variant> 
#include <vector>

namespace FL
{
	/**
	 * @brief Check if some type T is a valid glob part.
	 * 
	 * @tparam T The type.
	 */
	template <typename T>
	concept GlobPart = requires(T const & obj)
	{
		{ obj.ShouldSave() } noexcept -> std::same_as<bool>;
		{ obj.Describe() } -> std::same_as<std::string>;
	};
	/**
	 * @brief Check if some type T is a valid glob part and has the fixed length methods.
	 * 
	 * @tparam T The type.
	 */
	template <typename T>
	concept FixedLength = GlobPart<T> and requires(T const & obj, std::u8string_view v, bool case_sensitive)
	{
		{ obj.Size() } noexcept -> std::same_as<std::size_t>;
		{ obj.Find(v, case_sensitive) } noexcept -> std::same_as<std::size_t>;
		{ obj.HasPrefix(v, case_sensitive) } noexcept -> std::same_as<bool>;
	};
	/**
	 * @brief The result of find operations (`FL::GlobPart`) when they fail.
	 */
	inline constexpr std::size_t glob_npos = ~std::size_t{};
	/**
	 * @brief A glob part that is for an exact string.
	 */
	struct GlobPartExact
	{
		std::u8string part; ///< The text that must be found in some string.
		/**
		 * @brief The size in characters of this exact length and contents part.
		 * 
		 * @return The size.
		 */
		[[nodiscard]] std::size_t Size() const noexcept;
		/**
		 * @brief Find the `part` string in the given `input`.
		 * 
		 * @param input The input string.
		 * @param case_sensitive Is the search case sensitive?
		 * @return The index which the fixed sequence was found, or `glob_npos` if it is never found.
		 */
		[[nodiscard]] std::size_t Find(std::u8string_view input, bool case_sensitive) const noexcept;
		/**
		 * @brief Check if some string begins in with `part`.
		 * 
		 * @param input The input string.
		 * @param case_sensitive Is the search case sensitive?
		 * @return true when the `input` begins with `part`.
		 */
		[[nodiscard]] bool HasPrefix(std::u8string_view input, bool case_sensitive) const noexcept;
		/**
		 * @brief Whether or not this will be saved in the `FL::Pattern` of the glob.
		 * 
		 * @return true This object should have any matches saved as parts of the pattern.
		 * @return false This object should not have anything saved as part of the produced pattern.
		 */
		[[nodiscard]] bool ShouldSave() const noexcept;
		/**
		 * @brief Get a string to describe this part.
		 *
		 * @note This can be useful debugging patterns.
		 * 
		 * @return The string.
		 */
		[[nodiscard]] std::string Describe() const;
	};
	static_assert(FixedLength<GlobPartExact>);
	/**
	 * @brief A glob part that is for finding any one of several characters.
	 */
	struct GlobOneOfChar
	{
		std::u8string chars; ///< The characters that are allowed at the given position in some string.
		/**
		 * @brief The size of this part, which is always 1.
		 * 
		 * @return The size.
		 */
		[[nodiscard]] std::size_t Size() const noexcept;
		/**
		 * @brief Find one of the characters in `chars` in the given `input`.
		 * 
		 * @param input The input string.
		 * @param case_sensitive Is the search case sensitive?
		 * @return The index which the fixed sequence was found, or `glob_npos` if it is never found.
		 */
		[[nodiscard]] std::size_t Find(std::u8string_view input, bool case_sensitive) const noexcept;
		/**
		 * @brief Check if some string begins in with one of the characters in `chars`.
		 * 
		 * @param input The input string.
		 * @param case_sensitive Is the search case sensitive?
		 * @return true when the `input` begins with one of the characters in `chars`.
		 */
		[[nodiscard]] bool HasPrefix(std::u8string_view input, bool case_sensitive) const noexcept;
		/**
		 * @brief Whether or not this will be saved in the `FL::Pattern` of the glob.
		 * 
		 * @return true This object should have any matches saved as parts of the pattern.
		 * @return false This object should not have anything saved as part of the produced pattern.
		 */
		[[nodiscard]] bool ShouldSave() const noexcept;
		/**
		 * @brief Get a string to describe this part.
		 *
		 * @note This can be useful debugging patterns.
		 * 
		 * @return The string.
		 */
		[[nodiscard]] std::string Describe() const;
	};
	static_assert(FixedLength<GlobOneOfChar>);
	/**
	 * @brief A glob part for finding any character within a given range of ASCII characters.
	 */
	struct GlobRangeOfChar
	{
		char8_t low = '\0'; ///< The range of characters that are allowed at the given position in a string.
		char8_t high = '\0'; ///< The range of characters that are allowed at the given position in a string.
		/**
		 * @brief The size of this part, which is always 1.
		 * 
		 * @return The size.
		 */
		[[nodiscard]] std::size_t Size() const noexcept;
		/**
		 * @brief Find a character in the range of `low` to `high` in the given `input`.
		 * 
		 * @param input The input string.
		 * @param case_sensitive Is the search case sensitive?
		 * @return The index which the fixed sequence was found, or `glob_npos` if it is never found.
		 */
		[[nodiscard]] std::size_t Find(std::u8string_view input, bool case_sensitive) const noexcept;
		/**
		 * @brief Check if some string begins in with a character in the range of `low` to `high` inclusive.
		 * 
		 * @param input The input string.
		 * @param case_sensitive Is the search case sensitive?
		 * @return true when the `input` begins with a character in the range of `low` to `high` inclusive.
		 */
		[[nodiscard]] bool HasPrefix(std::u8string_view input, bool case_sensitive) const noexcept;
		/**
		 * @brief Whether or not this will be saved in the `FL::Pattern` of the glob.
		 * 
		 * @return true This object should have any matches saved as parts of the pattern.
		 * @return false This object should not have anything saved as part of the produced pattern.
		 */
		[[nodiscard]] bool ShouldSave() const noexcept;
		/**
		 * @brief Get a string to describe this part.
		 *
		 * @note This can be useful debugging patterns.
		 * 
		 * @return The string.
		 */
		[[nodiscard]] std::string Describe() const;
	};
	static_assert(FixedLength<GlobRangeOfChar>);
	/**
	 * @brief A glob part for finding any one character (it'll always find one for a non-empty string).
	 */
	struct GlobAnyOneChar
	{
		/**
		 * @brief The size of this part, which is always 1.
		 * 
		 * @return The size.
		 */
		[[nodiscard]] std::size_t Size() const noexcept;
		/**
		 * @brief Find the first character in the given `input`.
		 * 
		 * @param input The input string.
		 * @param case_sensitive Is the search case sensitive?
		 * @return The index which the fixed sequence was found, or `glob_npos` if it is never found.
		 */
		[[nodiscard]] std::size_t Find(std::u8string_view input, bool case_sensitive) const noexcept;
		/**
		 * @brief Check if some string begins in with one character (any character)
		 * 
		 * @param input The input string.
		 * @param case_sensitive Is the search case sensitive?
		 * @return true when the `input` begins with one character (any character)
		 */
		[[nodiscard]] bool HasPrefix(std::u8string_view input, bool case_sensitive) const noexcept;
		/**
		 * @brief Whether or not this will be saved in the `FL::Pattern` of the glob.
		 * 
		 * @return true This object should have any matches saved as parts of the pattern.
		 * @return false This object should not have anything saved as part of the produced pattern.
		 */
		[[nodiscard]] bool ShouldSave() const noexcept;
		/**
		 * @brief Get a string to describe this part.
		 *
		 * @note This can be useful debugging patterns.
		 * 
		 * @return The string.
		 */
		[[nodiscard]] std::string Describe() const;
	};
	static_assert(FixedLength<GlobAnyOneChar>);
	/**
	 * @brief A glob part that will match any characters up until the subsequent fixed sequences of parts matches something.
	 */
	struct GlobWildcard
	{
		/**
		 * @brief Whether or not this will be saved in the `FL::Pattern` of the glob.
		 * 
		 * @return true This object should have any matches saved as parts of the pattern.
		 * @return false This object should not have anything saved as part of the produced pattern.
		 */
		[[nodiscard]] bool ShouldSave() const noexcept;
		/**
		 * @brief Get a string to describe this part.
		 *
		 * @note This can be useful debugging patterns.
		 * 
		 * @return The string.
		 */
		[[nodiscard]] std::string Describe() const;
	};
	static_assert(GlobPart<GlobWildcard>);
	static_assert(not FixedLength<GlobWildcard>);

	using GlobPartType = std::variant<
		GlobPartExact,	// Unsaved
		GlobWildcard,	// Saved
		GlobAnyOneChar,	// Saved
		GlobOneOfChar,	// Saved
		GlobRangeOfChar	// Saved
	>;
	using GlobPartList = std::vector<GlobPartType>;
	/**
	 * @brief Determine whether the part is fixed length.
	 * 
	 * @param part The part to check.
	 * @retval true The part is fixed length.
	 * @retval false The part is not fixed length.
	 */
	[[nodiscard]] bool PartIsFixedLength(GlobPartType const & part) noexcept;
	/**
	 * @brief Produce a string describing the glob.
	 * 
	 * @param part The part to describe.
	 * @return The describing string.
	 */
	[[nodiscard]] auto PartDescribe(GlobPartType const & part) -> std::string;
	/**
	 * @brief Produce a string describing the part list.
	 * 
	 * @param list The list of parts to describe.
	 * @return The describing string.
	 */
	[[nodiscard]] auto PartListDescribe(GlobPartList const & list) -> std::string;
	/**
	 * @brief Get the fixed size of the part.
	 * @warning std::terminate will be called if this is used on non-fixed width parts.
	 * 
	 * @param part The part to get the size.
	 * @return The size of the part.
	 */
	[[nodiscard]] auto PartSize(GlobPartType const & part) -> std::size_t;
	/**
	 * @brief Find the index of the part in the input string.
	 * @warning std::terminate will be called if this is used on non-fixed width parts.
	 * 
	 * @param part The part to scan for in the string.
	 * @param input The string to scan inside.
	 * @param case_sensitive Whether or not the find will be case sensitive.
	 * @return The index in the string which the part was found, or `glob_npos` if it was never found.
	 */
	[[nodiscard]] auto PartFind(GlobPartType const & part, std::u8string_view input, bool case_sensitive) noexcept -> std::size_t;
	/**
	 * @brief Check if an input string has the prefix determined by the part.
	 * @warning std::terminate will be called if this is used on non-fixed width parts.
	 * 
	 * @param part The part defining the prefix.
	 * @param input The input string that will be searched.
	 * @param case_sensitive Whether or not the find will be case sensitive.
	 * @return true: When the prefix is found, otherwise false.
	 */
	[[nodiscard]] auto PartHasPrefix(GlobPartType const & part, std::u8string_view input, bool case_sensitive) noexcept -> bool;
	/**
	 * @brief Check if a particular part should be saved in the output list of a glob (similar to capture groups).
	 * 
	 * @note If it should be saved, the contents is not known for the part.
	 * @param part The part to check if it should be saved.
	 * @retval true The part, when matched will save the matched string.
	 * @retval false The part, when matched will not save the matched string.
	 */
	[[nodiscard]] auto PartShouldSave(GlobPartType const & part) noexcept -> bool;
	/**
	 * @brief A synonym for `FL::PartShouldSave`.
	 * @ref FL::PartShouldSave
	 * 
	 * @param part The part to check.
	 * @return true The part is constant.
	 * @return false The part is variable.
	 */
	[[nodiscard]] bool PartIsConstant(GlobPartType const & part) noexcept;
	/**
	 * @brief Get the total fixed length of a list of parts.
	 * 
	 * @param parts The parts to measure.
	 * @return The total .Size() of all the fixed length parts.
	 */
	[[nodiscard]] auto PartsTotalFixedSize(GlobPartList const & parts) noexcept -> std::size_t;
	/**
	 * @brief Count variable sized parts.
	 * 
	 * @param parts The parts to count.
	 * @return The number of variable length parts.
	 */
	[[nodiscard]] auto PartsCountVariableLength(GlobPartList const & parts) noexcept -> std::size_t;
	/**
	 * @brief Get string of part fixed constant part (usually a `GlobPartExact`).
	 * 
	 * @param part The part to count.
	 * @return The number of variable length parts.
	 */
	[[nodiscard]] auto PartAsConstant(GlobPartType const & part) noexcept -> std::u8string_view;
	/**
	 * @brief Get pattern from a parts list.
	 * 
	 * @return The pattern.
	 */
	[[nodiscard]] auto PatternFromPartList(GlobPartList const & part_list) -> Pattern;
}