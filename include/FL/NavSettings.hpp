/**
 * @file NavSettings.hpp
 * @brief An object with some basic settings to allow consistent handling of navigation throughout glob operations.
 */
#pragma once
#include "FL/Gen.hpp"
#include <filesystem>

namespace FL
{
	/**
	 * @brief These are the current avaliable setting for file navigating.
	 * 
	 * @warning Don't rely on the offsets of these members, they will change, you can rely on the order.
	 */
	struct NavSettings
	{
		bool enable_recursion = true;	///< Allow searches to search in subdirectories (when true).
		bool case_sensitive = true;		///< Files and directories should match case (when true).
		bool show_hidden = false;		///< Allow searches to find hidden files (when true).
		bool absolute_paths = false;	///< Produce absolute paths instead of relative paths (when true).
		bool allow_symlinks = false;	///< Follow symlinks (when true).
		/**
		 * @brief A syncronous generator that produces directories, exploring recursively.
		 * 
		 * @note std::runtime_error is thrown when the file is not "ok", as per `FileOk`.
		 * 
		 * @param p The path to the starting directory.
		 * @return A generator that can iterate through generators.
		 */
		[[nodiscard]] auto ExplorePathRecurse(std::filesystem::path const & p) const -> Gen<std::filesystem::path>;
		/**
		 * @brief A syncronous generator that produces directories, exploring in the directory `p`.
		 * 
		 * @param p The path to the directory to iterate.
		 * @return A generator that can iterate through generators.
		 */
		[[nodiscard]] auto ExplorePath(std::filesystem::path const & p) const -> Gen<std::filesystem::path>;
		/**
		 * @brief This is a particularly important function, since it asks if a file is ok given the visibility settings above (symlinks, hidden files etc...).
		 * 
		 * A file is ok if it:
		 * @li Exists and is not hidden and is not a symlink.
		 * @li Is a symlink and symlinks are allowed.
		 * @li Is hidden and hidden files are allowed.
		 * 
		 * @param p The path to the file.
		 * @return true The file is accessible.
		 * @return false The file is not accessible.
		 */
		[[nodiscard]] bool FileOk(std::filesystem::path const & p) const noexcept;
		/**
		 * @brief The file is a directory and ok.
		 * 
		 * @note `FL::NavSettings::FileOk` For information on what an ok path means.
		 * 
		 * @param p The path to check.
		 * @return true The file is a directory and ok.
		 * @return false The file is not ok or isn't a directory.
		 */
		[[nodiscard]] bool IsOkDirectory(std::filesystem::path const & p) const noexcept;
		/**
		 * @brief The file is a regular file and ok.
		 * 
		 * @note `FL::NavSettings::FileOk` For information on what an ok path means.
		 * 
		 * @param p The path to check.
		 * @return true The file is a regular file and ok.
		 * @return false The file is not ok or isn't a regular file.
		 */
		[[nodiscard]] bool IsOkRegularFile(std::filesystem::path const & p) const noexcept;
		/**
		 * @brief Get the printable path.
		 * 
		 * @return The path.
		 */
		[[nodiscard]] auto GetPrintablePath(std::filesystem::path const & p) const noexcept -> std::filesystem::path;
	};
}