/**
 * @file GlobFiles.hpp
 * @brief Some utility functions to perform globbing operations to find files.
 */
#pragma once
#include "FL/NavSettings.hpp"
#include "FL/Nav.hpp"

namespace FL
{
	/**
	 * @brief Apply a glob to find some files.
	 *
	 * @note This function supports glob's recursive `**` only when the settings.enable_recursion is true.
	 * @warning Case insensitive compare only works with english characters.
	 *
	 * @param glob The glob to apply.
	 * @param settings The file navigation settings to use when globbing.
	 * @return A list of the globbed files.
	 */
	[[nodiscard]] auto GlobFiles(std::u8string glob, NavSettings const & settings = {}) -> NavList;
	 /**
	  * @brief Apply a glob to find some files.
	  * 
	  * @note This function supports glob's recursive `**` only when the settings.enable_recursion is true.
	  *
	  * @param glob The glob to apply.
	  * @param settings The file navigation settings to use when globbing.
	  * @return A list of globbed files.
	  */
	[[nodiscard]] auto GlobFiles(std::string_view glob, NavSettings const & settings = {}) -> NavList;
}