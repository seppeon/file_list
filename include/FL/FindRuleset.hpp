/**
 * @file FindRuleset.hpp
 * @brief Defines the components needed to construct a find ruleset, an object which can be used to resolve a file location as per a set of rules.
 */
#pragma once
#include "FL/Cmd.hpp"
#include "FL/CurrentSystem.hpp"
#include "FL/FindFilters.hpp"
#include "FL/FileSearch.hpp"
#include <string>
#include <filesystem>
#include <unordered_map>
#include <vector>
#include <variant>
#include <optional>

namespace FL
{
	/**
	 * @brief The type that provided arguments are stored in.
	 */
	using FindRulesetArgsSubstitution = std::unordered_map<std::u8string, std::vector<std::u8string>>;
	/**
	 * @brief The type of a particular rule.
	 */
	enum class FindRuleType
	{
		InDirs,
		InEnvVarPaths,
		UsingRegKey,
		UsingRegKeyVal,
		Library,
		Executable,
		Pipe,
	};
	/**
	 * @brief A list of the possible find rule types.
	 */
	inline constexpr FindRuleType possible_find_rule_types[]
	{
		FindRuleType::InDirs,
		FindRuleType::InEnvVarPaths,
		FindRuleType::UsingRegKey,
		FindRuleType::UsingRegKeyVal,
		FindRuleType::Library,
		FindRuleType::Executable,
		FindRuleType::Pipe,
	};
	struct FindRule;
	/**
	 * @brief Check if a type could be a valid rule.
	 * 
	 * @tparam T The type to check.
	 */
	template <typename T>
	concept IsRule = requires(T obj, std::filesystem::path const & file, FindRulesetArgsSubstitution const & sub)
	{
		{ obj.Find(file) } -> std::same_as<FileSearchResult>;
		{ obj.Sub(sub) } -> std::same_as<T>;
	} and not std::same_as<std::remove_cvref_t<T>, FindRule>;
	/**
	 * @brief Convert `FindRuleType` to a string.
	 * 
	 * @param type The type to get a string for.
	 * @return The string.
	 */
	[[nodiscard]] auto ToU8String(FindRuleType type) -> std::u8string_view;
	/**
	 * @brief Convert `FindRuleType` to a string.
	 * 
	 * @param type The type to get a string for.
	 * @return The string.
	 */
	[[nodiscard]] auto ToString(FindRuleType type) -> std::string_view;
	/**
	 * @brief The type that represents an argument name.
	 */
	using FindArgName = std::u8string;
	/**
	 * @brief The types supported for arguments.
	 * 
	 * @note From this point on, all types other than `FindArgBaseType::List` will be referred to as fundamental data types.
	 */
	enum class FindArgBaseType
	{
		Int,        ///< Fundamental data type: A 64 bit signed integer.
		Uint,       ///< Fundamental data type: A 64 bit unsigned integer.
		String,     ///< Fundamental data type: A string.
		File,		///< Fundamental data type: The path to a file, checked immediately after reading the value.
		Directory,	///< Fundamental data type: The path to a directory, checked immediately after reading the value.
		Executable,	///< Fundamental data type: The path to a executable, checked immediately after reading the value.
		List,       ///< An list of values (a list of list is supported, `1,2,3;4,5,6;7,8,9`).
	};
	/**
	 * @brief A list of avaliable types for convience (since C++ doesn't have metaprogramming over enums).
	 */
	inline constexpr FindArgBaseType possible_base_arg_types[]
	{
		FindArgBaseType::Int,
		FindArgBaseType::Uint,
		FindArgBaseType::String,
		FindArgBaseType::File,
		FindArgBaseType::Directory,
		FindArgBaseType::Executable,
		FindArgBaseType::List,
	};
	/**
	 * @brief Convert `FindArgBaseType` to a string.
	 * 
	 * @param type The type to get a string for.
	 * @return The string.
	 */
	[[nodiscard]] auto ToU8String(FindArgBaseType type) -> std::u8string_view;
	/**
	 * @brief Convert `FindArgBaseType` to a string.
	 * 
	 * @param type The type to get a string for.
	 * @return The string.
	 */
	[[nodiscard]] auto ToString(FindArgBaseType type) -> std::string_view;
	/**
	 * @brief Store the type of some argument. 
	 */
	struct FindArgType
	{
		/**
		 * @brief Check if the type is a Int.
		 * 
		 * @return true When the type is a Int.
		 */
		[[nodiscard]] auto IsInt() const noexcept -> bool;
		/**
		 * @brief Check if the type is a Uint.
		 * 
		 * @return true When the type is a Uint.
		 */
		[[nodiscard]] auto IsUint() const noexcept -> bool;
		/**
		 * @brief Check if the type is a String.
		 * 
		 * @return true When the type is a String.
		 */
		[[nodiscard]] auto IsString() const noexcept -> bool;
		/**
		 * @brief Check if the type is a File.
		 * 
		 * @return true When the type is a File.
		 */
		[[nodiscard]] auto IsFile() const noexcept -> bool;
		/**
		 * @brief Check if the type is a Directory.
		 * 
		 * @return true When the type is a Directory.
		 */
		[[nodiscard]] auto IsDirectory() const noexcept -> bool;
		/**
		 * @brief Check if the type is a Executable.
		 * 
		 * @return true When the type is a Executable.
		 */
		[[nodiscard]] auto IsExecutable() const noexcept -> bool;
		/**
		 * @brief Check if the type is a List.
		 * 
		 * @return true When the type is a List.
		 */
		[[nodiscard]] auto IsList() const noexcept -> bool;
		/**
		 * @brief Check if the type is a 1dArray.
		 * 
		 * @return true When the type is a 1dArray.
		 */
		[[nodiscard]] auto Is1dArray() const noexcept -> bool;
		/**
		 * @brief Check whether this is a valid data type.
		 * 
		 * @return true This isn't valid.
		 * @return false This is valid.
		 */
		[[nodiscard]] auto IsValid() const noexcept -> bool;
		/**
		 * @brief Get the number of dimensions of this type.
		 * 
		 * @li `0`: for fundamental data types.
		 * @li `1`: for a single dimension list `list:string`.
		 * @li `2`: for multiple dimension lists `list:list:string`.
		 * 
		 * @return The number of dimensions.
		 */
		[[nodiscard]] auto GetDimensionCount() const noexcept -> std::size_t;
		/**
		 * @brief Get the base value type.
		 * 
		 * Example:
		 * @li `string`: Would be `string`.
		 * @li `array:string`: Would be `string`.
		 * @li `array:array:string`: Would be `string`.
		 * 
		 * @return The base value type.
		 */
		[[nodiscard]] auto GetBaseType() const noexcept -> FindArgBaseType;
		/**
		 * @brief Provide the typename as a string.
		 * 
		 * @return The string.
		 */
		[[nodiscard]] auto ToString() const noexcept -> std::string;
		/**
		 * @brief Provide the typename as a string.
		 * 
		 * @return The string.
		 */
		[[nodiscard]] auto ToU8String() const noexcept -> std::u8string;
		/**
		 * @brief Produce an invalid object.
		 */
		FindArgType() = default;
	private:
		friend auto ParseArgType(std::u8string_view input) -> FindArgType;

		std::vector<FindArgBaseType> m_type;
	};
	/**
	 * @brief Represents the result of calling `FL::ParseArgType`.
	 */
	struct ParseArgTypeResult
	{
		std::u8string_view remaining; ///< the unprocessed string.
		FindArgType result;           ///< The found found argument type.
		/**
		 * @brief Check if the result is valid.
		 * 
		 * @return true This isn't valid.
		 * @return false This is valid.
		 */
		[[nodiscard]] auto IsValid() const noexcept -> bool;
	};
	/**
	 * @brief Parse an argument type.
	 * 
	 * @param input The string.
	 * @return The argument type.
	 */
	[[nodiscard]] auto ParseArgType(std::u8string_view input) -> FindArgType;
	/**
	 * @brief The definition of a particular find argument.
	 */
	struct FindArg
	{
		std::u8string name;                  ///< The argument name.
		std::vector<std::u8string> commands; ///< The command line argument that identifies this command (not always needed).
		bool required = false;               ///< Whether or not this argument is required for valid input.
		std::u8string description;           ///< A description of the argument.
		FindArgType type;                    ///< The type of the argument.
	};
	/**
	 * @brief A rule for finding files in directories (this will ultimately create a series of single directory rules).
	 * 
	 * @note Json "type": "find_in_dirs"
	 */
	struct FindInDirs
	{
		std::vector<std::u8string> dirs;

		[[nodiscard]] auto Find(std::filesystem::path const & file) const -> FileSearchResult;
		[[nodiscard]] auto Sub(FindRulesetArgsSubstitution const & sub) const -> FindInDirs;
	};
	static_assert(IsRule<FindInDirs>);
	/**
	 * @brief A file for finding files in directories specified by an environmental variable.
	 * 
	 * @note Json "type": "find_in_env_var_paths"
	 */
	struct FindInEnvVarPaths
	{
		std::u8string variable;
		char8_t delim = CurrentEnvVarDelim();

		[[nodiscard]] auto Find(std::filesystem::path const & file) const -> FileSearchResult;
		[[nodiscard]] auto Sub(FindRulesetArgsSubstitution const & sub) const -> FindInEnvVarPaths;
	};
	static_assert(IsRule<FindInEnvVarPaths>);
	/**
	 * @brief A rule for finding files in directories specified by a registry key (the files are assumed in this case to be the key values).
	 * 
	 * @note Json "type": "find_using_reg_key"
	 */
	struct FindUsingRegKey
	{
		std::u8string entry;
		std::u8string relative;

		[[nodiscard]] auto Find(std::filesystem::path const & file) const -> FileSearchResult;
		[[nodiscard]] auto Sub(FindRulesetArgsSubstitution const & sub) const -> FindUsingRegKey;
	};
	static_assert(IsRule<FindUsingRegKey>);
	/**
	 * @brief A rule for finding files in directories specified by a registry key's value.
	 * 
	 * @note Json "type": "find_using_reg_key_val"
	 */
	struct FindUsingRegKeyVal
	{
		std::u8string entry;
		std::u8string value;
		std::u8string relative;

		[[nodiscard]] auto Find(std::filesystem::path const & file) const -> FileSearchResult;
		[[nodiscard]] auto Sub(FindRulesetArgsSubstitution const & sub) const -> FindUsingRegKeyVal;
	};
	static_assert(IsRule<FindUsingRegKeyVal>);
	/**
	 * @brief Search use the system to try locate libraries, then search common directories for library files.
	 * 
	 * @note Json "type": "find_library"
	 */
	struct FindLibrary
	{
		[[nodiscard]] auto Find(std::filesystem::path const & file) const -> FileSearchResult;
		[[nodiscard]] auto Sub(FindRulesetArgsSubstitution const & sub) const -> FindLibrary;
	};
	static_assert(IsRule<FindLibrary>);
	/**
	 * @brief A rule to use the system to try locate executables, then search common directoies for executable files.
	 * 
	 * @note Json "type": "find_executable"
	 */
	struct FindExecutable
	{
		[[nodiscard]] auto Find(std::filesystem::path const & file) const -> FileSearchResult;
		[[nodiscard]] auto Sub(FindRulesetArgsSubstitution const & sub) const -> FindExecutable;
	};
	static_assert(IsRule<FindExecutable>);
	/**
	 * @brief A rule which runs some executable, passes the output between several internal tools.
	 * 
	 * @note You cannot pipe information from one executable to another, just into the supported functions.
	 */
	struct FindPipe
	{
		std::vector<Cmd> cmds;     ///< The command that will be run, the output of each passed into the next.
		FindFilters filters; 	   ///< Stdout from the executable will be passed into these filters and then between the filters, from first to last, the output of the final filter must be a valid path.
		bool expect_error = false; ///< Whether or not the executation expects a non-zero return code.
		/**
		 * @brief Run the executable `cmd` with `args` and filter the output using `filters.
		 * 
		 * @return A list of paths.
		 */
		[[nodiscard]] auto Find(std::filesystem::path const & file) const -> FileSearchResult;
		[[nodiscard]] auto Sub(FindRulesetArgsSubstitution const & sub) const -> FindPipe;
	};
	static_assert(IsRule<FindPipe>);
	/**
	 * @brief The definition of a particular find rule.
	 */
	struct FindRule
	{
		FindRule() = default;
		FindRule(FindRule const &) = default;
		FindRule& operator=(FindRule const &) = default;
        /**
         * @brief Construct a Find Rule from a callable statisfying the concept @ref FL::IsRule
         * 
         * @tparam F The rule type.
         * @param rule The rule that will be used to resolve some paths.
         */
        template <IsRule F>
        FindRule(F && rule)
			: m_rule( new RuleImpl<std::remove_cvref_t<F>>{ std::forward<F>(rule) } )
        {}
		/**
		 * @brief Substitute some variables into the rules.
		 * 
		 * @return Whether the subtitution was accepted.
		 */
        /**
         * @brief Run a generic find rule.
         * 
         * @return The paths that will later be searched (or compared with).
         */
        [[nodiscard]] auto Find(std::filesystem::path const & path) const -> FL::FileSearchResult;
		/**
		 * @brief Substitute the arguments and produce a new, substituted rule.
		 *
		 * @return The updated rule.
		 */
		[[nodiscard]] auto Sub(FindRulesetArgsSubstitution const & sub) const -> FindRule;
	private:
        /**
         * @cond Doxygen_Suppress
         */
		struct RuleBase
		{
			virtual auto Find(std::filesystem::path const & path) const -> FileSearchResult = 0;
			virtual auto Sub(FindRulesetArgsSubstitution const & sub) const -> FindRule = 0;
			virtual ~RuleBase() {}
		};
		template <typename T, typename V = void>
		struct RuleImpl;
		template <IsRule T, typename V>
		struct RuleImpl<T, V> : RuleBase
		{
			auto Find(std::filesystem::path const & path) const -> FileSearchResult override { return m_rule.Find(path); }

			auto Sub(FindRulesetArgsSubstitution const & sub) const -> FindRule override
			{
				return FindRule{ m_rule.Sub(sub) };
			}

			RuleImpl(auto && f) : m_rule(std::forward<decltype(f)>(f)) {}
		private:
			T m_rule;
		};
        /**
         * @endcond
         */ 
        std::shared_ptr<RuleBase> m_rule; ///< A type erased rule, but only accepting a few things. 
	};
	static_assert(std::is_default_constructible_v<FindRule>);
	/**
	 * @brief Make whatever field able to inherit other fields.
	 * 
	 * @tparam T The type that is inheritable.
	 */
	template <typename T>
	using FindInherit = std::variant<
		std::u8string, ///< The name of the ruleset that will be inherited from.
		T
	>;
	using FindRulesetArgs = std::vector<FindInherit<FindArg>>;
	using FindRulesetRules = std::vector<FindInherit<FindRule>>;
	/**
	 * @brief Get the ruleset to resolve some file.
	 */
	struct FindRuleset
	{
		std::u8string name;     ///< The name of the ruleset.
		FindRulesetArgs args;   ///< The arguments that are required to run the ruleset (provided externally).
		FindRulesetRules rules; ///< The rules that will be used to resolve the file.
	};
	/**
	 * @brief Get a set of rulesets.
	 */
	struct FindRulesets
	{
		std::vector<FindRuleset> rulesets;
		/**
		 * @brief Get a ruleset by name.
		 * 
		 * @param input The name of the ruleset (this is case sensitive).
		 * @return A pointer to the ruleset, or nullptr if it doesn't exist.
		 */
		[[nodiscard]] auto GetRulesetByName(std::u8string_view input) const noexcept -> FindRuleset const *;
		/**
		 * @brief Check if this is a valid ruleset, when invalid it is unusable.
		 * 
		 * @return true The ruleset is valid.
		 * @return false The ruleset is invalid.
		 */
		[[nodiscard]] bool IsValid() const noexcept;
	};
	/**
	 * @brief Parse a json string and create a ruleset.
	 * 
	 * @param str The string to parse.
	 * @return A ruleset data structure, or std::nullopt if the string is malformed.
	 */
	[[nodiscard]] auto FindRulesetsFromJson(std::u8string_view str) -> std::optional<FindRulesets>;
	/**
	 * @brief Parse a json file and create a ruleset.
	 * 
	 * @param path The path to load.
	 * @return A ruleset data structure, or std::nullopt if the string is malformed.
	 */
	[[nodiscard]] auto FindRulesetsFromFile(std::filesystem::path path) -> std::optional<FindRulesets>;
}