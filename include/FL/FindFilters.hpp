#pragma once
#include <concepts>
#include <span>
#include <string_view>
#include <vector>
#include <memory>

namespace FL
{
	struct FindFilter;
	/// @brief A list of directories.
	using FindPathList = std::vector<std::u8string>;
	/**
	 * @brief Check if some callable `F` takes an argument of type `From` and produces one of type `To`.
	 * 
	 * @tparam F The callable to check.
	 * @tparam From The argument type.
	 * @tparam To The result type.
	 */
	template <typename F, typename From, typename To>
	concept FindFromToFn = requires(F fn, From from)
	{
		{ fn(from) } -> std::same_as<To>;
	} and not std::same_as<FindFilter, std::remove_cvref_t<F>>;
	/// @brief A single argument for a @ref FL::FindFilter.
	using FindFilterArg = std::u8string_view;
	/// @brief A list argument for a @ref FL::FindFilter.
	using FindFilterListArg = std::span<FindFilterArg const>;
	/// @brief A single result for a @ref FL::FindFilter.
	using FindFilterResult = std::u8string_view;
	/// @brief A list result for a @ref FL::FindFilter.
	using FindFilterListResult = std::vector<FindFilterResult>;
	/**
     * @brief An accepted type for the @ref FL::FindFilter.
	 * @ref FL::FindFilter
     * 
	 * @li arg: @ref FL::FindFilterListArg
     * @li result: @ref FL::FindFilterListResult : @ref FL::FindFilterListTransform
	 */
	template <typename F>
	concept FindFilterListTransform = FindFromToFn<F, FL::FindFilterListArg, FL::FindFilterListResult>;
	/**
     * @brief An accepted type for the @ref FL::FindFilter.
	 * @ref FL::FindFilter
     * 
	 * @li arg: @ref FL::FindFilterListArg
     * @li result: @ref FL::FindFilterResult : @ref FL::FindFilterCherryPick
	 */
	template <typename F>
	concept FindFilterCherryPick = FindFromToFn<F, FL::FindFilterListArg, FL::FindFilterResult>;
	/**
     * @brief An accepted type for the @ref FL::FindFilter.
	 * @ref FL::FindFilter
     * 
	 * @li arg: @ref FL::FindFilterArg
     * @li result: @ref FL::FindFilterListResult : @ref FL::FindFilterSplit
	 */
	template <typename F>
	concept FindFilterSplit = FindFromToFn<F, FL::FindFilterArg, FL::FindFilterListResult>;
	/**
     * @brief An accepted type for the @ref FL::FindFilter.
	 * @ref FL::FindFilter
     * 
	 * @li arg: @ref FL::FindFilterArg
     * @li result: @ref FL::FindFilterResult : @ref FL::FindFilterTransform
	 */
	template <typename F>
	concept FindFilterTransform = FindFromToFn<F, FL::FindFilterArg, FL::FindFilterResult>;
	/**
	 * @brief Take some input, provide some output.
	 * 
	 * The inputs must be one of these formats:
	 * @li @ref FL::FindFilterArg
	 * @li @ref FL::FindFilterListArg
	 * 
	 * The outputs must be one of these formats:
	 * @li @ref FL::FindFilterResult
	 * @li @ref FL::FindFilterListResult
	 * 
	 * @note Filters must provide a portion of the input to the output, they do not mutate the data (for example to_upper_case("hello")).
     * @note This class provides implicit constructors only, this is because these types have zero overlap and when there is overlap (due to overloads present in the callable) it will be caught.
	 */
	struct FindFilter
	{
        /**
         * @brief Construct a Find Filter from a callable statisfying the concept @ref FL::FindFilterListTransform
         * 
         * @tparam F The callable type.
         * @param list_transform The callable.
         */
        template <FindFilterListTransform F>
        FindFilter(F && list_transform)
            : m_fn( new FilterImpl<std::remove_cvref_t<F>>{ std::forward<F>(list_transform) } )
        {}
        /**
         * @brief Construct a Find Filter from a callable statisfying the concept @ref FL::FindFilterCherryPick
         * 
         * @tparam F The callable type.
         * @param cherry_pick The callable.
         */
        template <FindFilterCherryPick F>
        FindFilter(F && cherry_pick)
            : m_fn( new FilterImpl<std::remove_cvref_t<F>>{ std::forward<F>(cherry_pick) } )
        {}
        /**
         * @brief Construct a Find Filter from a callable statisfying the concept @ref FL::FindFilterSplit
         * 
         * @tparam F The callable type.
         * @param split The callable.
         */
        template <FindFilterSplit F>
        FindFilter(F && split)
            : m_fn( new FilterImpl<std::remove_cvref_t<F>>{ std::forward<F>(split) } )
        {}
        /**
         * @brief Construct a Find Filter from a callable statisfying the concept @ref FL::FindFilterTransform
         * 
         * @tparam F The callable type.
         * @param transform The callable.
         */
        template <FindFilterTransform F>
        FindFilter(F && transform)
            : m_fn( new FilterImpl<std::remove_cvref_t<F>>{ std::forward<F>(transform) } )
        {}
        /**
         * @brief Run a generic find filter.
         * 
         * @param arg The argument that will be filtered.
         * @return The result of the filtering.
         */
        [[nodiscard]] auto operator()(FL::FindFilterListArg arg) const -> FL::FindFilterListResult;
    private:
        /**
         * @cond Doxygen_Suppress
         */
        struct FilterBase
        {
            virtual auto Run(FindFilterListArg arg) const -> FindFilterListResult = 0;
            virtual ~FilterBase() {}
        };
        template <typename F>
        struct FilterImpl;
        template <FindFilterListTransform F>
        struct FilterImpl<F> : FilterBase
        {
            F fn;
			FilterImpl(auto && f) : fn(std::forward<decltype(f)>(f)) {}
            [[nodiscard]] auto Run(FindFilterListArg arg) const
				-> FindFilterListResult override
            {
                return fn(arg);
            }
        };
        template <FindFilterCherryPick F>
        struct FilterImpl<F> : FilterBase
        {
            F fn;
			FilterImpl(auto && f) : fn(std::forward<decltype(f)>(f)) {}
            [[nodiscard]] auto Run(FindFilterListArg arg) const
				-> FindFilterListResult override
            {
                return FindFilterListResult{ fn(arg) };
            }
        };
        template <FindFilterSplit F>
        struct FilterImpl<F> : FilterBase
        {
            F fn;
			FilterImpl(auto && f) : fn(std::forward<decltype(f)>(f)) {}
            [[nodiscard]] auto Run(FindFilterListArg arg) const
				-> FindFilterListResult override
            {
                FindFilterListResult output;
                for (auto const & v : arg)
                {
                    auto temp = fn(v);
                    output.insert(output.end(), temp.begin(), temp.end());
                }
                return output;
            }
        };
        template <FindFilterTransform F>
        struct FilterImpl<F> : FilterBase
        {
            F fn;
			FilterImpl(auto && f) : fn(std::forward<decltype(f)>(f)) {}
            [[nodiscard]] auto Run(FindFilterListArg arg) const
				-> FindFilterListResult override
            {
                FindFilterListResult output;
                output.reserve(arg.size());
                for (auto const & v : arg) { output.push_back(fn(v)); }
                return output;
            }
        };
        /**
         * @endcond
         */ 
        std::shared_ptr<FilterBase> m_fn; ///< A type erased function, but only accepting a few things. 
	};
    /**
     * @brief Create glob find filter callable.
     * 
     * @param glob The glob string.
     * @param capture The capture string.
	 * @param case_sensitive Whether or not the glob is case sensitive
	 * @return The filter.
     */
    [[nodiscard]] auto FindFilterGlob(std::u8string_view glob, std::u8string_view capture, bool case_sensitive = true) -> FindFilter;
	/**
	 * @brief Create a getlines find filter callable.
	 * 
	 * Implemented with @ref FL::GetLines
	 * 
	 * @return The filter.
	 */
	[[nodiscard]] auto FindFilterGetLines() -> FindFilter;
	/**
	 * @brief Create a filter that trims the leading and trailing spaces from a string.
	 * 
	 * Implemented with @ref FL::TrimSpaces
	 * 
	 * @return The filter.
	 */
	[[nodiscard]] auto FindFilterTrimSpaces() -> FindFilter;
	/**
	 * @brief Create a filter that will only let through elements that represent existing files.
	 * 
	 * @return the filter.
	 */
	[[nodiscard]] auto FindFilterRegularFileExists() -> FindFilter;	
	/**
	 * @brief Create a filter that will only let through elements that represent existing files.
	 * 
	 * @return the filter.
	 */
	[[nodiscard]] auto FindFilterDirectoryExists() -> FindFilter;
	/**
	 * @brief A list of find filters, which can be run, producing a list of directories.
	 */
	struct FindFilters
	{
		/**
		 * @brief Add a single filter to the list of filters.
		 * 
		 * @param filter The filter to add.
		 */
		void PushBack(FindFilter && filter);
		/**
		 * @brief Run the sequence of filters.
		 * 
		 * @param content The result of running whatever command had its output captured.
		 * @return The list of paths.
		 */
		[[nodiscard]] auto operator()(FindFilterArg content) const -> std::vector<std::u8string>;
		/**
		 * @brief Construct from a list of filters.
		 * 
		 * @param init_list Allow construction using initializer lists.
		 */
		FindFilters(std::initializer_list<FindFilter> init_list);
		/**
		 * @brief Default construction is just an empty filter.
		 */
		FindFilters() = default;
	private:
		std::vector<FindFilter> m_filters;
	};
}