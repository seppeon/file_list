/**
 * @file Cmd.hpp
 * @brief Utilities for interacting with, and executing command line applications.
 */
#pragma once
#include <concepts>
#include <cstdint>
#include <string>
#include <vector>
#include <filesystem>
#include <string_view>

namespace FL
{
	/**
	 * @brief Check if T is `bool`.
	 *
	 * This is used to avoid overload ambiguity due to implicit conversions. 
	 *
	 * @tparam T The type.
	 */
	template <typename T>
	concept Bool = std::same_as<std::remove_cvref_t<T>, bool>;
	/**
	 * @brief Check if a T is a signed integer (and not a bool).
	 *
	 * This is used to avoid overload ambiguity due to implicit conversions. 
	 * 
	 * @tparam T The type.
	 */
	template <typename T>
	concept SignedInt = std::signed_integral<T> and not std::same_as<std::remove_cvref_t<T>, bool>;
	/**
	 * @brief The tag type for a pipe on command line.
	 * 
	 * Example:
	 * echo "hello" | other_command
	 */
	struct CmdPipe{};
	/**
	 * @brief Represents a single argument of a command.
	 */
	class CmdArg
	{
		struct IntTag{};
		struct BoolTag{};
		struct Str{};
		struct U8Str{};

		CmdArg(IntTag, std::int64_t arg);
		CmdArg(BoolTag, bool arg, std::u8string_view true_str = u8"true", std::u8string_view false_str = u8"false");
	public:
		/**
		 * @brief An argument representing a cmd pipe.
		 * 
		 * Normally this would be escaped if provided by a string, so use this tag type.
		 * 
		 * @param pipe The pipe tag type.
		 */
		CmdArg(CmdPipe pipe);
		/**
		 * @brief Construct a new CmdArg from a signed integer.
		 * 
		 * @tparam T The type of the integer.
		 * @param arg The argument.
		 */
		template <typename T, std::enable_if_t<SignedInt<T>, bool> = true>
		CmdArg(T arg)
			: CmdArg(IntTag{}, arg)
		{}
		/**
		 * @brief Construct a CmdArg from a filename.
		 * 
		 * @param arg The filename.
		 */
		CmdArg(std::same_as<std::filesystem::path> auto const & arg)
			: CmdArg( arg.u8string() )
		{}
		/**
		 * @brief Construct a new CmdArg from a bool.
		 * 
		 * @tparam T The type of the bool (its always bool lol).
		 * @param arg The argument.
		 * @param true_str The text given to the command line when true is present in `arg`().
		 * @param false_str The text given to the command line when false is present in `arg`.
		 */
		template <typename T, std::enable_if_t<Bool<T>, bool> = true>
		CmdArg(T arg, std::u8string_view true_str = u8"true", std::u8string_view false_str = u8"false")
			: CmdArg(BoolTag{}, arg, true_str, false_str)
		{}
		/**
		 * @brief Construct a CmdArg from a unicode string.
		 * 
		 * @param arg The unicode string argument.
		 */
		CmdArg(std::u8string_view arg);
		/**
		 * @brief Construct a CmdArg from a string.
		 * 
		 * @param arg The string argument.
		 */
		CmdArg(std::string_view arg);

		std::u8string data; ///< The string that will be placed into the command line in place of the argument.
	};
	/**
	 * @brief Represents the result of running a command.
	 */
	struct CmdResult
	{
		int return_code = -1;            ///< The return code of the executable.
		std::u8string stdout_and_stderr; ///< The combination of stderr and stdout.
	};
	/**
	 * @brief Represent the type of the arguments of a command.
	 */
	using CmdArgs = std::vector<CmdArg>;
	/**
	 * @brief Represents a single command, and provides the result.
	 */
	struct Cmd
	{
		std::u8string cmd; ///< The executable that will be run.
		std::vector<CmdArg> args;  ///< The arguments to provide the executable.
		/**
		 * Convert a command to a string.
		 */
		[[nodiscard]] std::u8string ToU8String() const;
		/**
		 * @brief Run the command, returning stdout.
		 * 
		 * @return The return code, stdout and stderr.
		 */
		[[nodiscard]] CmdResult Run() const;
	};
	/**
	 * @brief Build a new command that pipes between two commands.
	 * 
	 * @param lhs The left hand operand.
	 * @param rhs The left hand operand.
	 * @return The produced command.
	 */
	[[nodiscard]] Cmd operator | (Cmd const & lhs, Cmd const & rhs);
	/**
	 * @brief Known terminal types.
	 */
	enum class TerminalType
	{
		WindowsCmd,
		WindowsPowerShell,
		Linux,
		Unknown,
	};
	/**
	 * @brief Get the current terminal name.
	 * 
	 * @return The current terminal name.
	 */
	[[nodiscard]] auto GetCurrentTerminalType() -> TerminalType;
}