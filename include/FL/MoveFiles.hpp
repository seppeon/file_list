/**
 * @file MoveFiles.hpp
 * @brief Provide functionality to move files based on a glob and a replacement pattern.
 */
#pragma once
#include "FL/NavSettings.hpp"
#include "FL/PatternTarget.hpp"
#include <span>
#include <string>
#include <string_view>
#include <vector>
#include <optional>
#include <filesystem>

namespace FL
{
	/**
	 * @brief The definition of a glob move request.
	 *
	 * This represents a glob find, paired with a string that
	 * transforms a matched string into a new path.
	 */
	struct GlobMoveRequest
	{
		std::u8string src;	///< The source pattern of a file.
		std::u8string dst;	///< The destination pattern of the file.
	};
	/**
	 * @brief A list of glob move request.
	 */
	using GlobMoveRequestList = std::vector<GlobMoveRequest>;
	/**
	 * @brief Pair a set of sources to a set of destinations.
	 *
	 * @warning `srcs.size()` must equal `dsts.size()`, or std::nullopt will be returned.
	 *
	 * @param srcs The source locations for the moves.
	 * @param dsts The destination locations for the moves.
	 * @return A list of move operations, or std::nullopt if matching the pairs wasn't possible.
	 */
	[[nodiscard]] auto BuildGlobMoveRequestList(std::span<std::u8string const> srcs, std::span<std::u8string const> dsts) -> std::optional<GlobMoveRequestList>;
	/**
	 * @brief The definition of a request for a move.
	 *
	 * From the filename `src` to the filename `dst`.
	 */
	struct MoveOperation
	{
		std::filesystem::path src;	///< The source pattern of a file.
		std::filesystem::path dst;	///< The destination pattern of the file.
	};
	/**
	 * @brief A list of move requests.
	 */
	using MoveOperationList = std::vector<MoveOperation>;
	/**
	 * @brief Represent a list of patterns that couldn't be matched when calling @ref FL::BuildMoveOperationList.
	 */
	struct PatternMismatch
	{
		std::size_t move_request_index;
		FL::Pattern glob_result;
		FL::PatternTarget pattern_target;
	};
	/**
	 * @brief A list results from calling @ref FL::BuildMoveOperationList, each indicating a failure to subsitute the found file's patterns (perhaps a FL::Glob) with the output pattern (A @ref FL::PatternTarget).
	 */
	using PatternMismatchList = std::vector<PatternMismatch>;
	/**
	 * @brief The result of the building of move operations.
	 */
	struct BuildMoveOperationListResult
	{
		MoveOperationList move_operations_list;
		PatternMismatchList invalid_pattern_list; ///< Indexes of the provided `glob_move_requests` that had glob/pattern target mismatches (likely a pattern target index is out of range).
	};
	/**
	 * @brief A function which builds a specific set of move operations 
	 * 
	 * @param glob_move_requests A list of glob removes.
	 * @param settings The settings that define navigation settings.
	 * @return A list of specific move operations that should be performed.
	 */
	[[nodiscard]] auto BuildMoveOperationList(std::span<GlobMoveRequest const> glob_move_requests, NavSettings const & settings) -> BuildMoveOperationListResult;
	/**
	 * @brief The status of a single move operation.
	 */
	enum class MoveOperationStatus
	{
		Success,
		MissingSource,
		ExistingFileAtDestination,
		CannotRenameFile,
		CannotRemoveFileAtDestination,
		CannotCreateRequiredDirectories,
	};
	/**
	 * @brief Convert MoveOperationStatus to a string.
	 *
	 * @param status The input enum.
	 * @return The string.
	 */
	[[nodiscard]] std::u8string_view ToString(MoveOperationStatus status) noexcept;
	/**
	 * @brief The result of applying some move operations.
	 */
	using MoveOperationStatusList = std::vector<MoveOperationStatus>;
	/**
	 * @brief Applies a move operation list.
	 *
	 * @param move_operation_list The list of move operations to perform.
	 * @param override_files Whether or not to allow the move operations to override existing files.
	 * @param make_directories Whether or not this is allowed to create directories.
	 * @return A move operation status structure.
	 */
	[[nodiscard]] auto ApplyMoveOperationList(std::span<MoveOperation const> move_operation_list, bool override_files, bool make_directories) -> MoveOperationStatusList;
}