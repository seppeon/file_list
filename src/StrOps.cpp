#include "FL/StrOps.hpp"
#include <algorithm>
#include <cctype>

namespace FL
{
	namespace
	{
		constexpr char8_t ToLowerCase( char8_t v ) noexcept
		{
			return ( ( u8'A' <= v ) and ( v <= u8'Z' ) ) ? u8'a' + ( v - u8'A' ) : v;
		}
	}

	bool CaseSensitiveEqual(std::u8string_view lhs, std::u8string_view rhs) noexcept
	{
		return (lhs == rhs);
	}

	bool CaseInsensitiveEqual(std::u8string_view lhs, std::u8string_view rhs) noexcept
	{
		// TODO: This clearly doesn't support unicode characters, and especially doesn't support
		//       anything with combining characters. Current support is ascii only.
		auto const compare = [](char8_t a, char8_t b) noexcept -> bool
		{
			return ToLowerCase(a) == ToLowerCase(b);
		};
		return std::equal(lhs.begin(), lhs.end(), rhs.begin(), rhs.end(), compare);
	}

	bool Equal(std::u8string_view lhs, std::u8string_view rhs, bool case_sensitive) noexcept
	{
		return case_sensitive ? CaseSensitiveEqual(lhs, rhs) : CaseInsensitiveEqual(lhs, rhs);
	}

	auto GetLines(std::u8string_view input) -> std::vector<std::u8string_view>
	{
		std::vector<std::u8string_view> output;
		std::size_t index = 0;
		std::size_t length = 0;
		bool has_newline = true;
		while ((index + length) < input.size())
		{
			auto const current = input.substr(index + length);
			if (current.starts_with(u8"\r\n") or current.starts_with(u8"\n\r"))
			{
				output.push_back(input.substr(index, length));
				index += length + 2;
				length = 0;
				has_newline = true;
			}
			else if (current.starts_with(u8"\r") or current.starts_with(u8"\n"))
			{
				output.push_back(input.substr(index, length));
				index += length + 1;
				length = 0;
				has_newline = true;
			}
			else
			{
				++length;
				has_newline = false;
			}
		}
		if (length > 0 or has_newline)
		{
			output.push_back(input.substr(index));
		}
		return output;
	}

	namespace
	{
		constexpr auto FirstMatchingPredicate(std::u8string_view input, std::size_t offset, auto pred) -> std::size_t
		{
			for (std::size_t i = offset; i < input.size(); ++i)
			{
				if (pred(input[i]))
				{
					return i;
				}
			}
			return input.size();
		}

		constexpr auto LastMatchingPredicate(std::u8string_view input, std::size_t offset, auto pred) -> std::size_t
		{
			auto const sz = input.size() - offset;
			for (std::size_t i = 0; i < (sz); ++i)
			{
				auto const index = (sz - 1) - i + offset;
				if (pred(input[index]))
				{
					return index + 1;
				}
			}
			return offset;
		}
	}

	auto TrimSpaces(std::u8string_view input) -> std::u8string_view
	{
		auto const is_non_space = [](char8_t c){ return not std::isspace(c); };
		auto const content_start = FirstMatchingPredicate(input, 0, is_non_space);
		auto const trailing_space_start = LastMatchingPredicate(input, content_start, is_non_space);
		return std::u8string_view{ input.data() + content_start, input.data() + trailing_space_start };
	}
}