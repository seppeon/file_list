#include "FL/Pass.hpp"

namespace FL
{
	auto Pass::RequiresExactMatch() const noexcept -> bool
	{
		return false;
	}

	auto Pass::Describe() const -> std::string
	{
		return "(pass)";
	}

	auto Pass::Parse(std::u8string_view input, bool case_sensitive) const -> FL::Pattern
	{
		return FL::Pattern{ input };
	}
}