#include "FL/PatternTarget.hpp"
#include "Overloads.hpp"

#include <algorithm>
#include <charconv>
#include <string_view>
#include <utility>

namespace FL
{
	namespace
	{
		struct CharParseResult
		{
			std::u8string_view rest{};
			char8_t c = '\0';

			constexpr bool IsValid() const noexcept
			{
				return c != '\0';
			}
		};
		template <char8_t c>
		[[nodiscard]] auto ParseChar( std::u8string_view str ) noexcept -> CharParseResult
		{
			if ( str.empty() ) return {};
			auto const v = str.front();
			if ( v != c ) return {};
			return CharParseResult{
				.rest = str.substr( 1 ),
				.c = v,
			};
		};

		struct U32ParseResult
		{
			std::u8string_view rest{};
			std::uint32_t value = 0;

			constexpr bool IsValid() const noexcept
			{
				return rest.data() != nullptr;
			}
		};
		[[nodiscard]] auto ParseU32( std::u8string_view str ) noexcept -> U32ParseResult
		{
			std::uint32_t output;
			auto const r = std::from_chars( reinterpret_cast<const char *>( str.data() ), reinterpret_cast<const char *>( str.data() + str.size() ), output, 10 );
			if ( r.ec != std::errc() ) return {};
			return U32ParseResult{
				.rest = str.substr( static_cast<std::size_t>( reinterpret_cast<const char8_t *>( r.ptr ) - str.data() ) ),
				.value = output,
			};
		}

		struct CaptureParseResult
		{
			std::u8string_view rest{};
			std::uint32_t index = 0;

			constexpr bool IsValid() const noexcept
			{
				return rest.data() != nullptr;
			}
		};
		[[nodiscard]] auto ParseCapture( std::u8string_view str ) noexcept -> CaptureParseResult
		{
			auto const capture_parse_result = ParseChar<pattern_target_capture>( str );
			if ( not capture_parse_result.IsValid() ) return {};
			auto const & [capture_parse_rest, _] = capture_parse_result;
			auto const index_parse_result = ParseU32( capture_parse_rest );
			if ( not index_parse_result.IsValid() ) return {};
			auto const & [index_parse_rest, index_parse_value] = index_parse_result;
			return CaptureParseResult{
				.rest = index_parse_rest,
				.index = index_parse_value,
			};
		}
	}

	PatternTarget::PatternTarget( std::u8string_view input )
	{
		m_capture_count = 0;
		std::size_t length = 0;
		bool escaped = false;
		auto & contents = this->m_contents;
		while ( not input.empty() )
		{
			auto const is_escaped = std::exchange( escaped, false );
			auto const c = input.front();
			if ( not is_escaped )
			{
				if ( c == u8'\\' )
				{
					escaped = true;
					// shortens input.
					input.remove_prefix( 1 );
					continue;
				}
				if ( c == pattern_target_capture )
				{
					auto const parse_capture_result = ParseCapture( input );
					if ( parse_capture_result.IsValid() )
					{
						auto const & [rest, index] = parse_capture_result;
						if (length != 0)
						{
							m_parts.push_back( Part{
								.is_length = true,
								.length_or_index = std::exchange(length, 0),
							});
						}
						m_parts.push_back( Part{
							.is_length = false,
							.length_or_index = index,
						} );
						++m_capture_count;
						// shortens input.
						input = rest;
						continue;
					}
				}
			}
			contents += c;
			++length;
			// shortens input.
			input.remove_prefix( 1 );
		}
		if (length != 0)
		{
			m_parts.push_back( Part{
				.is_length = true,
				.length_or_index = std::exchange(length, 0),
			});
		}
		// All paths shorten input, therefore this will terminate.
	}

	auto PatternTarget::Size() const noexcept -> std::size_t
	{
		return m_parts.size();
	}

	auto PatternTarget::GetCaptureCount() const noexcept -> std::size_t
	{
		return m_capture_count;
	}

	auto PatternTarget::GetHighestPatternIndex() const noexcept -> std::size_t
	{
		auto const get_index = Overloads
		{
			[](std::u8string_view v) -> std::size_t { return 0; },
			[](std::size_t index) -> std::size_t { return index; }
		};
		std::size_t index = 0;
		for (auto const & elem : *this)
		{
			index = std::max(std::visit(get_index, elem), index);
		}
		return index;
	}

	auto PatternTarget::const_iterator::operator++() noexcept -> const_iterator &
	{
		m_index += GetLength(m_part_ptr++);
		return *this;
	}
	auto PatternTarget::const_iterator::operator++(int) noexcept -> const_iterator
	{
		auto out = *this;
		++(*this);
		return out;
	}
	auto PatternTarget::const_iterator::operator--() noexcept -> const_iterator &
	{
		m_index -= GetLength(--m_part_ptr);
		return *this;
	}
	auto PatternTarget::const_iterator::operator--(int) noexcept -> const_iterator
	{
		auto out = *this;
		--(*this);
		return out;
	}
	auto PatternTarget::const_iterator::operator*() const noexcept -> reference
	{
		if (m_part_ptr->is_length)
		{
			// This is a printable part, a string.
			return std::u8string_view(m_contents + m_index, GetLength(m_part_ptr));
		}
		else
		{
			// This is an index.
			return m_part_ptr->length_or_index;
		}
	}

	bool operator<(PatternTarget const & l, PatternTarget const & r) noexcept
	{
		return std::lexicographical_compare(l.begin(), l.end(), r.begin(), r.end());
	}

	bool operator==(PatternTarget::const_iterator const & l, PatternTarget::const_iterator const & r) noexcept
	{
		return l.m_part_ptr == r.m_part_ptr;
	}

	bool operator!=(PatternTarget::const_iterator const & l, PatternTarget::const_iterator const & r) noexcept
	{
		return l.m_part_ptr != r.m_part_ptr;
	}

	PatternTarget::const_iterator::const_iterator()
	{}

	PatternTarget::const_iterator::const_iterator(size_type index, Part const * part_ptr, char8_t const * contents)
		: m_index(index)
		, m_part_ptr(part_ptr)
		, m_contents(contents)
	{}

	auto PatternTarget::const_iterator::GetLength(Part const * part_ptr) noexcept -> size_type
	{
		return part_ptr->is_length ? part_ptr->length_or_index : std::size_t(0);
	}

	auto PatternTarget::begin() const noexcept -> const_iterator
	{
		return const_iterator{
			/* index    */ 0,
			/* part_ptr */ m_parts.data(),
			/* contents */ m_contents.data(),
		};
	}

	auto PatternTarget::end() const noexcept -> const_iterator
	{
		return const_iterator{
			/* index    */ m_parts.size(),
			/* part_ptr */ m_parts.data() + m_parts.size(),
			/* contents */ m_contents.data() + m_contents.size(),
		};
	}

	auto PatternTarget::Apply( Pattern const & pattern ) const -> Pattern
	{
		std::vector<std::u8string_view> parts;
		std::size_t index = 0;
		for (auto const & part : m_parts){
			if (part.is_length)
			{
				auto const length = part.length_or_index;
				parts.push_back(std::u8string_view{ m_contents.data() + index, length });
				index += length;
			}
			else
			{
				auto const index = part.length_or_index;
				if (index >= pattern.Size()) return {};
				parts.push_back(pattern[index]);
			}
		}
		if (parts.empty())
		{
			parts.push_back(u8"");
		}
		return Pattern{parts};
	}
}