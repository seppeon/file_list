#include "FL/FindFilters.hpp"
#include "FL/Glob.hpp"
#include "FL/StrOps.hpp"
#include "FL/Pattern.hpp"
#include "FL/PatternTarget.hpp"
#include <numeric>

namespace FL
{
    auto FindFilterGlob(std::u8string_view glob, std::u8string_view capture, bool case_sensitive) -> FindFilter
	{
		struct Filter
		{
			bool case_sensitive;
			Glob globber;
			PatternTarget target;
			mutable Pattern applied_pattern;

			FindFilterListResult operator()(FindFilterArg arg) const
			{
				auto const parse_result = globber.Parse(arg, case_sensitive);
				if (not parse_result.IsValid()) return {};
				applied_pattern = target.Apply(parse_result);
				return {applied_pattern.begin(), applied_pattern.end()};
			}
		};
		static_assert(FL::FindFilterSplit<Filter>);
		Filter filter
		{
			.case_sensitive = case_sensitive,
			.globber{ Glob( std::u8string(glob) ) },
			.target{ PatternTarget( capture ) },
		};
		return FindFilter(std::move(filter));
	}

	auto FindFilterGetLines() -> FindFilter
	{
		return FindFilter{ &FL::GetLines };
	}

	auto FindFilterTrimSpaces() -> FindFilter
	{
		return FindFilter{ &FL::TrimSpaces };
	}

	auto FindFilterRegularFileExists() -> FindFilter
	{
		return FindFilter{
			+[](FindFilterListArg input) -> FindFilterListResult
			{
				FindFilterListResult output(input.begin(), input.end());
				std::erase_if(output, [](std::u8string_view path)
				{
					auto const test_path = std::filesystem::path(path);
					return not std::filesystem::exists(test_path) or not std::filesystem::is_regular_file(test_path);
				});
				return output;
			}
		};
	}

	auto FindFilterDirectoryExists() -> FindFilter
	{
		return FindFilter{
			+[](FindFilterListArg input) -> FindFilterListResult
			{
				FindFilterListResult output(input.begin(), input.end());
				std::erase_if(output, [](std::u8string_view path)
				{
					auto const test_path = std::filesystem::path(path);
					return not std::filesystem::exists(test_path) or not std::filesystem::is_directory(test_path);
				});
				return output;
			}
		};
	}

	auto FindFilter::operator()(FL::FindFilterListArg arg) const -> FindFilterListResult
	{
		return m_fn->Run(arg);
	}

	void FindFilters::PushBack(FindFilter && filter)
	{
		m_filters.push_back(std::move(filter));
	}

	auto FindFilters::operator()(FindFilterArg content) const -> FindPathList
	{
		auto const run_filter = [](FindFilterListResult arg, FindFilter const & filter) -> FindFilterListResult
		{
			return filter(arg);
		};
		auto const find_results = std::accumulate( m_filters.begin(), m_filters.end(), FindFilterListResult{ content }, run_filter );
		return { find_results.begin(), find_results.end() };
	}

	FindFilters::FindFilters(std::initializer_list<FindFilter> init_list)
		: m_filters(init_list)
	{}
}