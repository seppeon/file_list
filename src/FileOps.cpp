#include "FL/FileOps.hpp"
#include "PlatformDetect.hpp"
#include <iterator>
#include <vector>
#include <span>
#include <utility>

#if defined(PLATFORM_IS_WINDOWS)
#include "WindowsHeader.hpp"

namespace FL
{
	bool IsFileHidden(std::filesystem::path const & p)
	{
		auto const attributes = GetFileAttributesA(reinterpret_cast<const char *>(p.u8string().data()));
		if (attributes & FILE_ATTRIBUTE_SYSTEM) return false;
		return (attributes & FILE_ATTRIBUTE_HIDDEN);
	}

	static auto GetShortName(std::filesystem::path const & p) -> std::filesystem::path
	{
		std::wstring short_name;
		auto const required_len = GetShortPathNameW(p.c_str(), NULL, 0);
		short_name.resize(required_len);
		auto const len = GetShortPathNameW(p.c_str(), short_name.data(), short_name.size());
		short_name.resize(len);
		return short_name;
	}

	static auto GetLongName(std::filesystem::path const & p) -> std::filesystem::path
	{
		std::wstring long_name;
		auto const required_len = GetLongPathNameW(p.c_str(), NULL, 0);
		long_name.resize(required_len);
		auto const len = GetLongPathNameW(p.c_str(), long_name.data(), long_name.size());
		long_name.resize(len);
		return long_name;
	}

	auto GetCaseSensitivePath(std::filesystem::path const & p) -> std::filesystem::path
	{
		auto const new_name_len = static_cast<std::size_t>(std::distance(p.begin(), p.end()));
		auto const cased_path = GetLongName(GetShortName(p));
		// well this is absurd, std::filesystem is rubbish.
		auto const case_sensitive_path_parts = std::vector(cased_path.begin(), cased_path.end());
		auto const case_sensitive_path_parts_span = std::span(std::as_const(case_sensitive_path_parts));
		auto const equal_length_parts = case_sensitive_path_parts_span.last(std::min(case_sensitive_path_parts_span.size(), new_name_len));
		std::filesystem::path case_sensitive_parts;
		for (auto const & part : equal_length_parts) case_sensitive_parts /= part;
		return case_sensitive_parts;
	}

	static constexpr std::u8string_view long_path_prefix = u8"\\\\?\\";
	static bool IsLongWindowsPath(std::u8string_view p)
	{
		// Give a little headroom I'll be appending some chars.
		// not that it should be needed...
		return p.size() >= 250;
	}

	auto FixLongPath(std::filesystem::path p) -> FixResult
	{
		auto const suffix = p.u8string();
		if (IsLongWindowsPath(suffix) and not suffix.starts_with(long_path_prefix))
		{
			return {true, std::filesystem::path(std::u8string(long_path_prefix) + suffix)};
		}
		return {false, p};
	}

	auto UnfixLongPath(std::filesystem::path p) -> std::filesystem::path
	{
		auto const suffix = p.u8string();
		if (IsLongWindowsPath(suffix) and suffix.starts_with(long_path_prefix))
		{
			return std::filesystem::path(suffix.substr(long_path_prefix.size()));
		}
		return p;
	}

	auto CurrentExePath() -> std::filesystem::path
	{
		wchar_t path[MAX_PATH] = { 0 };
		GetModuleFileNameW(NULL, path, MAX_PATH);
		return GetLongName(path);
	}
}
#elif defined(PLATFORM_IS_LINUX) or defined(PLATFORM_IS_MAC)
#include <linux/limits.h>
#include <unistd.h>
namespace FL
{
	auto FixLongPath(std::filesystem::path p) -> FixResult
	{
		return { false, std::move(p) };
	}

	auto UnfixLongPath(std::filesystem::path p) -> std::filesystem::path
	{
		return std::move(p);
	}
	bool IsFileHidden(std::filesystem::path const & p)
	{
		auto const name = p.filename().u8string();
		return (name.size() > 1) and name.starts_with(u8".");
	}
	auto GetCaseSensitivePath(std::filesystem::path const & p) -> std::filesystem::path
	{
		return p;
	}
	auto CurrentExePath() -> std::filesystem::path
	{
		char result[PATH_MAX];
		ssize_t count = readlink("/proc/self/exe", result, PATH_MAX);
		return std::string(result, (count > 0) ? count : 0);
	}
}
#endif

namespace FL
{
	bool IsSymlink(std::filesystem::path const & p)
	{
		std::error_code ec;
		return std::filesystem::is_symlink(p, ec) and ec == std::error_code();
	}

	bool FileExists(std::filesystem::path const & p)
	{
		std::error_code ec;
		return std::filesystem::exists(p, ec) and ec == std::error_code();
	}

	bool IsRegularFile(std::filesystem::path const & p)
	{
		if (not FileExists(p)) return false;
		std::error_code ec;
		return std::filesystem::is_regular_file(p, ec) and ec == std::error_code();
	}

	bool IsDirectory(std::filesystem::path const & p)
	{
		if (not FileExists(p)) return false;
		std::error_code ec;
		return std::filesystem::is_directory(p, ec) and ec == std::error_code();
	}
}