#include "FL/File.hpp"
#include <filesystem>
#include <fstream>

namespace FL
{
	auto File::LastModifiedTime() const -> std::filesystem::file_time_type
	{
		return std::filesystem::last_write_time(m_path);
	}

	auto File::Size() const -> std::size_t
	{
		return std::filesystem::file_size(m_path);
	}

	auto File::Path() const -> std::filesystem::path
	{
		return m_path;
	}

	auto File::Read() const -> std::span<char const>
	{
		if (not m_contents)
		{
			using it = std::istreambuf_iterator<char>;
			using vec = std::vector<char>;
			std::ifstream t{ m_path, std::ios::binary };
			m_contents = std::shared_ptr<vec>{ new vec{ it(t), it() } };
		}
		return std::span<char const>{ m_contents->data(), m_contents->size() };
	}

	File::File(std::filesystem::path path)
		: m_path{ path }
	{
	}
}