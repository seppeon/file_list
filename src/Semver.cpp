#include "FL/Semver.hpp"
#include <charconv>
#include <ranges>
#include <algorithm>

namespace FL
{
	namespace
	{
		/**
		* @brief The current version as per the build system.
		*/
		inline constexpr auto current_version_string = std::string_view{ FL_LIB_VERSION };

		auto ParseUntilFalse(std::string_view input, auto test) -> std::string_view
		{
			if (input.empty()) return {};
			auto const * end_ptr = input.data() + input.size();
			std::size_t i = 0;
			for (; i < input.size(); ++i)
			{
				if (not test(input[i]))
				{
					break;
				}
			}
			if (i == input.size()) return std::string_view{ end_ptr, end_ptr };
			return (i != 0)
				? std::string_view{ &input[i], end_ptr }
				: std::string_view{};
		}

		auto IsLetter(char c) -> bool
		{
			return ('a' <= c and c <= 'z') or ('A' <= c and c <= 'Z');
		}

		auto IsDigit(char c) -> bool
		{
			return '0' <= c and c <= '9';
		}

		auto IsZero(char c) -> bool
		{
			return c == '0';
		}

		auto IsPlus(char c) -> bool
		{
			return c == '+';
		}

		auto IsMinus(char c) -> bool
		{
			return c == '-';
		}

		auto IsDot(char c) -> bool
		{
			return c == '.';
		}

		auto IsPositiveDigit(char c) -> bool
		{
			return IsDigit(c) and not IsZero(c);
		}

		auto ParseDigits(std::string_view input) -> std::string_view
		{
			return ParseUntilFalse(input, &IsDigit);
		}

		auto IsNonDigit(char c) -> bool
		{
			return IsLetter(c) or IsMinus(c);
		}

		auto IsValid(std::string_view input) -> bool
		{
			return input.data() != nullptr;
		}

		auto IsValid(std::from_chars_result input) -> bool
		{
			return input.ec == std::errc();
		}

		auto RemoveFront(std::string_view input) -> std::string_view
		{
			return std::string_view{ input.data() + 1, input.data() + input.size() };
		}

		auto ParseNumericIdentifier(std::string_view input) -> std::string_view
		{
			if (input.empty()) return {};
			auto const c = input.front();
			if (IsZero(c))
			{
				// If it leads in zero, its a single digit (as per semver grammar).
				return RemoveFront(input);
			}
			// After the non-zero, it must be a positive digit.
			if (not IsPositiveDigit(c)) return {};
			// If this is empty, it'll fail so no need to recheck.
			input = RemoveFront(input);
			auto const digits = ParseDigits(input);
			if (IsValid(digits)) return digits;
			return input;
		}

		auto ParseAlphanumericIdentifier(std::string_view input) -> std::string_view
		{
			auto found_non_digit = false;
			while (not input.empty() and IsValid(input))
			{
				auto const c = input.front();
				auto const is_non_digit = IsNonDigit(c);
				auto const is_digit = IsDigit(c);
				if (is_non_digit or is_digit)
				{
					found_non_digit |= is_non_digit;
					input = RemoveFront(input);
				}
				else break;
			}
			// grammar requires at minimum 1 non-digit.
			if (not found_non_digit) return {};
			return input;
		}

		auto ParseBuildIdentifier(std::string_view input) -> std::string_view
		{
			if (auto r = ParseAlphanumericIdentifier(input); IsValid(r)) return r;
			if (auto r = ParseDigits(input); IsValid(r)) return r;
			return {};
		}

		auto ParsePreReleaseIdentifier(std::string_view input) -> std::string_view
		{
			if (auto r = ParseAlphanumericIdentifier(input); IsValid(r)) return r;
			if (auto r = ParseNumericIdentifier(input); IsValid(r)) return r;
			return {};
		}

		auto ParseDotSeperatedBuildIdentifiers(std::string_view input) -> std::string_view
		{
			// <dot-separated build identifiers> ::= <build identifier>
			//                                     | <build identifier> "." <dot-separated build identifiers>
			// <build identifier>
			input = ParseBuildIdentifier(input);
			while (IsValid(input))
			{
				// "."
				if (input.empty()) return input;
				auto const c = input.front();
				if (not IsDot(c)) return input;
				auto const last_valid = input;
				input = RemoveFront(input);

				// <build identifier>
				auto const build_identifier_result = ParseBuildIdentifier(input);
				if (not IsValid(build_identifier_result)) return last_valid;
				input = build_identifier_result;
				if (input.empty()) return input;
			}
			return {};
		}

		auto ParseDotSeperatedPreReleaseIdentifiers(std::string_view input, auto identifier) -> std::string_view
		{
			// <dot-separated pre-release identifiers> ::= <pre-release identifier>
			//                                           | <pre-release identifier> "." <dot-separated pre-release identifiers>
			// <pre-release identifier>
			auto const * before = input.data();
			input = ParsePreReleaseIdentifier(input);
			auto const * after = input.data();
			while (IsValid(input))
			{
				identifier(std::string_view{before, after});
				if (input.empty()) return input;
				auto const last_valid = input;
				// "."
				auto const c = input.front();
				if (not IsDot(c)) return input;
				input = RemoveFront(input);

				// <pre-release identifier>
				before = input.data();
				auto const build_identifier_result = ParsePreReleaseIdentifier(input);
				if (not IsValid(build_identifier_result)) return last_valid;
				input = build_identifier_result;
				after = input.data();
				identifier(std::string_view{before, after});
				if (input.empty()) return input;
			}
			return {};
		}

		auto ParseBuild(std::string_view input) -> std::string_view
		{
			return ParseDotSeperatedBuildIdentifiers(input);
		}

		auto ParsePreRelease(std::string_view input) -> std::string_view
		{
			return ParseDotSeperatedPreReleaseIdentifiers(input, [](std::string_view){});
		}

		auto ParsePatch(std::string_view input) -> std::string_view
		{
			return ParseNumericIdentifier(input);
		}

		auto ParseMinor(std::string_view input) -> std::string_view
		{
			return ParseNumericIdentifier(input);
		}

		auto ParseMajor(std::string_view input) -> std::string_view
		{
			return ParseNumericIdentifier(input);
		}
	}

	auto ParseSemverResult::IsValid() const noexcept -> bool
	{
		return semver.IsValid();
	}

	auto SemverStringResult::IsValid() const noexcept -> bool
	{
		return semver.IsValid();
	}

	auto SemverFromString(std::string_view input) -> ParseSemverResult
	{
		// <major>
		auto const major_result = ParseMajor(input);
		if (not IsValid(major_result)) return ParseSemverResult{};
		auto const * const major_begin = input.data();
		auto const * const major_end = major_result.data();

		// .
		if (major_result.empty()) return ParseSemverResult{};
		auto const dot_1 = major_result.front();
		if (not IsDot(dot_1)) return ParseSemverResult{};
		auto const major_result_dot = RemoveFront(major_result);

		// <minor>
		auto const minor_result = ParseMinor(major_result_dot);
		if (not IsValid(minor_result)) return ParseSemverResult{};
		auto const * const minor_begin = major_result_dot.data();
		auto const * const minor_end = minor_result.data();

		// .
		if (minor_result.empty()) return ParseSemverResult{};
		auto const dot_2 = minor_result.front();
		if (not IsDot(dot_2)) return ParseSemverResult{};
		auto const minor_result_dot = RemoveFront(minor_result);

		// <patch>
		auto const patch_result = ParsePatch(minor_result_dot);
		if (not IsValid(patch_result)) return ParseSemverResult{};
		auto const * const patch_begin = minor_result_dot.data();
		auto const * const patch_end = patch_result.data();

		// At this point, we have a valid semver, lets build it.
		Semver output{};
		auto const major_parse_result = std::from_chars(major_begin, major_end, output.major);
		if (not IsValid(major_parse_result)) return ParseSemverResult{};
		auto const minor_parse_result = std::from_chars(minor_begin, minor_end, output.minor);
		if (not IsValid(minor_parse_result)) return ParseSemverResult{};
		auto const patch_parse_result = std::from_chars(patch_begin, patch_end, output.patch);
		if (not IsValid(patch_parse_result)) return ParseSemverResult{};

		// Optionally at this point, we can parse prerelease then build
		input = patch_result;
		if (input.empty())
		{
			return ParseSemverResult{
				.remaining = input,
				.semver = output,
			};
		}

		if (IsMinus(input.front()))
		{
			auto const after_minus = RemoveFront(input);
			auto const prerelease_result = ParsePreRelease(after_minus);
			if (IsValid(prerelease_result))
			{
				auto const prerelease_result_begin = after_minus.data();
				auto const prerelease_result_end = prerelease_result.data();
				output.prerelease.assign(prerelease_result_begin, prerelease_result_end);
				if (prerelease_result.empty())
				{
					return ParseSemverResult{
						.remaining = prerelease_result,
						.semver = output,
					};
				}
				input = prerelease_result;
				if (input.empty())
				{
					return ParseSemverResult{
						.remaining = input,
						.semver = output,
					};
				}
			}
		}

		if (IsPlus(input.front()))
		{
			auto const after_plus = RemoveFront(input);
			auto const build_result = ParseBuild(after_plus);
			if (IsValid(build_result))
			{
				auto const build_result_begin = after_plus.data();
				auto const build_result_end = build_result.data();
				output.meta.assign(build_result_begin, build_result_end);
				input = build_result;
			}
		}

		return ParseSemverResult{
			.remaining = input,
			.semver = output,
		};
	}

	auto GetSemverFromString(std::string_view input) -> Semver
	{
		auto const result = SemverFromString(input);
		if (not result.IsValid()) { return {}; }
		if (not result.remaining.empty()) { return {}; }
		return result.semver;
	}

	auto Semver::ToString() const noexcept -> std::string
	{
		std::string output;
		output += std::to_string(major);
		output += ".";
		output += std::to_string(minor);
		output += ".";
		output += std::to_string(patch);
		if (not prerelease.empty())
		{
			output += "-";
			output += prerelease;
		}
		if (not meta.empty())
		{
			output += "+";
			output += meta;
		}
		return output;
	}

	auto Semver::IsValid() const noexcept -> bool
	{
		return major != invalid_semver_version;
	}

	bool operator<(Semver const & lhs, Semver const & rhs)
	{
		auto const tup_lhs = std::tuple(lhs.major, lhs.minor, lhs.patch);
		auto const tup_rhs = std::tuple(rhs.major, rhs.minor, rhs.patch);
		if (tup_lhs < tup_rhs) return true;
		if (tup_lhs > tup_rhs) return false;
		auto const lhs_empty = lhs.prerelease.empty();
		auto const rhs_empty = rhs.prerelease.empty();
		if (not lhs_empty and rhs_empty) return true;
		if (lhs_empty and not rhs_empty) return false;
		auto const splitter = std::ranges::views::split('.');
		auto const transformer = std::ranges::views::transform([](auto && v) -> std::string_view
		{
			return std::string_view{ v.data(), v.size() };
		});
		auto prerelease_parts_lhs = lhs.prerelease | splitter | transformer;
		auto prerelease_parts_rhs = rhs.prerelease | splitter | transformer;
		auto lhs_it = prerelease_parts_lhs.begin();
		auto const lhs_it_end = prerelease_parts_lhs.end();
		auto rhs_it = prerelease_parts_rhs.begin();
		auto const rhs_it_end = prerelease_parts_rhs.end();

		while (lhs_it != lhs_it_end and rhs_it != rhs_it_end)
		{
			auto lhs = *lhs_it;
			auto rhs = *rhs_it;
			if (lhs != rhs)
			{
				auto const lhs_parse_numeric = ParseNumericIdentifier(lhs);
				auto const rhs_parse_numeric = ParseNumericIdentifier(rhs);
				if (IsValid(lhs_parse_numeric) and not IsValid(rhs_parse_numeric))
				{
					// numeric identifier has a lower precedence than non-numeric identifiers.
					return true;
				}
				else if (not IsValid(lhs_parse_numeric) and IsValid(rhs_parse_numeric))
				{
					// numeric identifier has a lower precedence than non-numeric identifiers.
					return false;
				}
				else if (IsValid(lhs_parse_numeric) and IsValid(rhs_parse_numeric))
				{
					std::uint32_t lhs_value = 0;
				std::uint32_t rhs_value = 0;
				// Already checked for validity.
				static_cast<void>(std::from_chars(lhs.data(), lhs.data() + lhs.size(), lhs_value));
				static_cast<void>(std::from_chars(rhs.data(), rhs.data() + rhs.size(), rhs_value));
				if (lhs_value < rhs_value)
				{
					return true;
				}
				if (lhs_value > rhs_value)
				{
					return false;
				}
			}
			else
			{
				// These are identifiers, compare them lexographically, as per ascii.
				auto const result = std::lexicographical_compare_three_way(lhs.begin(), lhs.end(), rhs.begin(), rhs.end());
				if (result == std::strong_ordering::less)
				{
					return true;
				}
				if (result == std::strong_ordering::greater)
				{
					return false;
				}
			}
		}
		++lhs_it;
		++rhs_it;
	}

		if (lhs_it == lhs_it_end)
		{
			if (rhs_it != rhs_it_end)
			{
				// Then the left hand side has higher precedence
				return true;
			}
		}
		// Then the right hand side has higher precedence.
		return false;
	}

	bool operator>(Semver const & lhs, Semver const & rhs)
	{
		return rhs < lhs;
	}

	bool operator<=(Semver const & lhs, Semver const & rhs)
	{
		return not (lhs > rhs);
	}

	bool operator>=(Semver const & lhs, Semver const & rhs)
	{
		return not (lhs < rhs);
	}

	bool operator==(Semver const & lhs, Semver const & rhs)
	{
		return ( lhs.major == rhs.major and lhs.minor == rhs.minor and lhs.patch == rhs.patch and lhs.prerelease == rhs.prerelease );
	}

	auto GetCurrentVersion() -> Semver
	{
		return GetSemverFromString(current_version_string); 
	}

	auto FindFirstSemverString(std::string_view input) -> SemverStringResult
	{
		constexpr std::size_t min_length = 5;
		if (input.size() < min_length) return {};
		auto const avalible_length = (input.size() - min_length);
		auto const end_it = input.data() + input.size();
		for (std::size_t i = 0; i <= avalible_length; ++i)
		{
			auto const parse_result = SemverFromString({input.data() + i, end_it});
			if (parse_result.IsValid())
			{
				return SemverStringResult{
					.index = i,
					.length = input.size() - (parse_result.remaining.size() + i),
					.semver = parse_result.semver,
				};
			}
		}
		return {};
	}

	auto FindAllSemverStrings(std::string_view input) -> SemverStringResultList
	{
		SemverStringResultList output;
		std::size_t index = 0;
		auto const * const it = input.data();
		auto const * const end_it = input.data() + input.size();
		while (end_it != it)
		{
			auto const result = FindFirstSemverString({it + index, end_it});
			auto const start_index = index + result.index;
			auto const end_index = start_index + result.length;
			if (result.IsValid())
			{
				index = end_index;
				output.push_back(SemverStringResult{
					.index = start_index,
					.length = result.length,
					.semver = result.semver,
				});
			}
			else break;
		}
		return output;
	}
}