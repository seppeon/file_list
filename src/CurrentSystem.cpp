#include "FL/CurrentSystem.hpp"

namespace FL
{
	auto CurrentEnvVarDelim() noexcept -> char8_t
	{
#if defined(PLATFORM_IS_WINDOWS)
		return u8';';
#elif defined(PLATFORM_IS_LINUX)
		return u8':';
#elif defined(PLATFORM_IS_MAC)
		return u8':';
#else
		return u8'\0';
#endif
	}
}