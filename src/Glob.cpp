#include "FL/Glob.hpp"
#include "FL/GlobFixedSeq.hpp"
#include "FL/GlobParts.hpp"
#include <span>
#include <string>
#include <utility>
#include <variant>
#include <algorithm>

namespace FL
{
	namespace
	{
		auto CreatePartList(std::u8string str) -> GlobPartList
		{
			std::u8string temp;
			std::vector<GlobPartType> parts;
			// Get a span including the null termination.
			auto submit = [&]
			{
				if (not temp.empty())
				{
					parts.push_back(GlobPartExact{std::exchange(temp, u8"")});
				}
			};
			auto wildcard = [&]
			{
				if (not parts.empty())
				{
					auto const & back = parts.back();
					if (std::holds_alternative<GlobWildcard>(back))
					{
						return;
					}
				}
				parts.push_back(GlobWildcard{});
			};
			auto any_one_char = [&]
			{
				parts.push_back(GlobAnyOneChar{});
			};
			auto range = [&](char8_t low, char8_t high)
			{
				parts.push_back(GlobRangeOfChar{ low, high });
			};

			struct Eat
			{
				char8_t c = u8'\0';
				bool is_escaped = false;
			};
			std::size_t i = 1;
			auto const str_span = std::span<char8_t const>{ str.data(), str.size() + 1 };
			auto eat_char = [&]() -> Eat
			{
				if (i >= str_span.size()) return {};
				auto const a = str_span[i-1];
				auto const is_escape = a == u8'\\';
				auto const b = str_span[i];
				i += (1 + static_cast<std::size_t>(is_escape));
				return {
					.c = (is_escape) ? b : a,
					.is_escaped = is_escape,
				};
			};
			auto backtrack = [&](Eat c)
			{
				if (c.c == u8'\0') return;
				i -= (1 + static_cast<std::size_t>(c.is_escaped));
			};
			auto normal_char = [&](char8_t c)
			{
				temp += c;
			};

			for (;;)
			{
				auto const [a, is_escaped] = eat_char();
				if (a == u8'\0') break;

				if (not is_escaped && a == u8'?')
				{
					submit();
					any_one_char();
				}
				else if (not is_escaped && a == u8'*')
				{
					submit();
					wildcard();
				}
				else if (not is_escaped && a == u8'[')
				{
					// Create a scope that can be returned from, to short circuit nested if statements.
					// We already know we have a [				
					std::vector<Eat> chars{};
					// Read until the close bracket ] (or the end.
					for (;;)
					{
						auto [t, t_escaped] = eat_char();
						if (t == '\0') break;
						chars.push_back({t, t_escaped});
						if (not t_escaped and t == ']') break;
					}
					// a-z] range of chars.
					//  - This does support the * or ? characters, they do not need escaping.
					if  (
							(chars.size() == 4) and 
							// Must have each of 4 sections valid.
							(chars[0].c and chars[1].c and chars[2].c and chars[3].c) and 
							//  low             bar            high           close
							// - and ] must not be escaped, and low must be smaller or equal to high.
							(
								(not chars[1].is_escaped and chars[1].c == u8'-') and
								(not chars[3].is_escaped and chars[3].c == u8']') and
								(chars[0].c <= chars[2].c)
							)
						)
					{
						// This is without a doubt a range of chars, lets add it and move the cursor.
						submit();
						range(chars[0].c, chars[2].c);
					}
					else if (
						// Need at least one character in the brackets, otherwise the glob is unparsable.
						(chars.size() >= 2) and
						// Ends in close.
						(not chars.back().is_escaped and chars.back().c == u8']')
						)
					{
						submit();
						chars.pop_back(); // pop the close character)
						auto & l = *std::get_if<GlobOneOfChar>(&parts.emplace_back(GlobOneOfChar{}));
						for (auto [c, _] : chars) l.chars += c;
					}
					else
					{
						// This was not a valid [*] block, parse it like normal chars.
						for (auto c : chars) backtrack(c);
						normal_char(a);
					}
				}
				else
				{
					normal_char(a);
				}
			}
			submit();
			return std::move(parts);
		}

		auto RemovePrefix(auto const & parts, auto const & prefix)
		{
			return std::span(std::as_const(parts)).subspan(prefix.parts.size());
		}
		auto RemovePrefixSuffix(auto const & parts, auto const & prefix, auto const & suffix)
		{
			auto const len = parts.size() - (prefix.parts.size() + suffix.parts.size());
			return RemovePrefix(parts, prefix).first(len);
		}
	}

	Glob::Glob(std::u8string str)
		: m_parts(CreatePartList(std::move(str)))
		, m_prefix(GetFixedSeqPrefix(m_parts))
		, m_suffix(GetFixedSeqSuffix(RemovePrefix(m_parts, m_prefix)))
		, m_fixed(GetFixedSeqList(0, RemovePrefixSuffix(m_parts, m_prefix, m_suffix)))
		, m_pattern(PatternFromPartList(m_parts))
	{
	}
	auto Glob::RequiresExactMatch() const noexcept -> bool
	{
		return m_parts.empty() or (PartCount() == 1) and (not PartShouldSave(m_parts.front()));
	}

	auto Glob::GetVariableSectionCount() const noexcept -> std::size_t
	{
		return std::count_if(std::begin(m_parts), std::end(m_parts), PartShouldSave);
	}

	auto Glob::PartCount() const noexcept -> std::size_t
	{
		return std::size(m_parts);
	}

	auto Glob::Describe() const -> std::string
	{
		std::string output;
		for (auto const & part : m_parts) output += std::visit([](auto const & p){ return p.Describe(); }, part);
		return output;
	}

	auto Glob::Parse(std::u8string_view input, bool case_sensitive) const -> FL::Pattern
	{
		if (m_parts.empty())
		{
			// An empty pattern only matches empty.
			return input.empty()
				? FL::Pattern{FL::Pattern::value_type{}}
				: FL::Pattern{};
		}
		// Create a change list, which has one element for each empty component in the pattern.
		auto change_list = std::vector<PatternSub>{};

		// The part counts for the various sections.
		auto const prefix_part_length = GetFixedSeqPartsCount(m_prefix);
		auto const fixed_part_length = [](auto v){ return (v > 0) ? (v + 1) : 0; }(GetFixedSeqListPartsCount(m_fixed));
		auto const suffix_part_length = GetFixedSeqPartsCount(m_suffix);
		auto const defined_part_size = (fixed_part_length + prefix_part_length + suffix_part_length);
		auto const post_fixed_length = (PartCount() - defined_part_size);
		// The ranges of the parts within the m_parts variable for the sections.
		auto prefix_index = std::size_t{0};
		auto const prefix_length = std::size_t{prefix_part_length};
		auto fixed_index = std::size_t{prefix_index + prefix_length};
		auto const fixed_length = std::size_t{fixed_part_length + post_fixed_length};
		auto suffix_index = std::size_t{fixed_index + fixed_length};
		auto const suffix_length = std::size_t{suffix_part_length};
		// The ranges of the input string for each section.
		auto const prefix_input_length = m_prefix.Size();
		auto const suffix_input_length = m_suffix.Size();
		auto const total_fixed_length = prefix_input_length + suffix_input_length;
		// Shortcircuit for impossible length.
		if (total_fixed_length > input.size())
		{
			return FL::Pattern{};
		}
		auto const fixed_input_length = input.size() - (prefix_input_length + suffix_input_length);
		auto const prefix_input_index = std::size_t{0};
		auto const suffix_input_index = input.size() - m_suffix.Size();
		auto const fixed_input_index = prefix_input_index + prefix_input_length;
		auto const process_fixed_section = [&change_list, case_sensitive](std::size_t part_index, std::u8string_view str, GlobFixedSeq const & fixed) -> bool
		{
			if (not fixed.parts.empty())
			{
				if (not fixed.HasPrefix(str, case_sensitive))
				{
					// Couldn't find the constant width prefix/suffix.
					return false;
				}
				for (auto const & part : fixed.parts)
				{
					auto const len = PartSize(part);
					auto const result = str.substr(0, len);
					if (PartShouldSave(part))
					{
						change_list.push_back(PatternSub{
							.index = part_index,
							.value = std::u8string(result),
						});
					}
					++part_index;
					str.remove_prefix(len);
				}
			}
			return true;
		};

		auto const process_body = [&change_list, post_fixed_length, case_sensitive](std::size_t part_index, std::u8string_view str, GlobFixedSeqList const & fixed_list) -> bool
		{
			for (auto & part : fixed_list)
			{
				// Find the first item from the offset.
				auto const result = part.Find(str, case_sensitive);
				if (result == glob_npos)
				{
					// Missing find a fixed section within the input.
					return false;
				}

				// Add the offset for the variable width part.
				// Its alright for this to be empty...
				change_list.push_back(PatternSub{
					.index = part_index,
					.value = std::u8string(str.substr(0, result)),
				});
				str.remove_prefix(result);
				++part_index;

				// Need to loop through the fixed width parts, as some may be fixed width 
				// but variable content, these need to be added to the change list
				for (auto const & subpart : part.parts)
				{
					auto const subpart_size = PartSize(subpart);
					if (PartShouldSave(subpart))
					{
						change_list.push_back(PatternSub{
							.index = part_index,
							.value = std::u8string(str.substr(0, subpart_size)),
						});
					}
					str.remove_prefix(subpart_size);
					++part_index;
				}
			}
			for (std::size_t i = 0; i < post_fixed_length; ++i)
			{
				// there is an unhandled variable length section at the end of the fixed section
				// Usually this would mean a wildcard char * is present, but it could me multiple * characters.
				// Someone may present multiple wildcard characters if for example "they have bad ideas"
				change_list.push_back(PatternSub{
					.index = part_index,
					.value = std::u8string(str),
				});
				str.remove_prefix(str.size());
				++part_index;
			}
			return str.empty();
		};

		// Each glob will be split into 3 parts:
		// part order: <prefix> <body> <suffix>
		// 
		// 		<prefix>: The prefix consists of the leading parts that have a defined length.
		// 		<body>    The body contains defined length sections split by variable length sections.
		// 		<suffix>: The suffix consists of the trailing prats that have defined length.

		// <prefix>
		if (not process_fixed_section(prefix_index, input.substr(prefix_input_index, prefix_input_length), m_prefix))
		{
			return {};
		}
		// <body>
		if (not process_body(fixed_index, input.substr(fixed_input_index, fixed_input_length), m_fixed))
		{
			return {};
		}
		// <suffix>
		if (not process_fixed_section(suffix_index, input.substr(suffix_input_index, suffix_input_length), m_suffix))
		{
			return {};
		}

		return m_pattern.Substitute(change_list);
	}
}