#include "FL/GlobFiles.hpp"

#include "FL/FileOps.hpp"
#include "FL/Glob.hpp"
#include "FL/Nav.hpp"

#include <filesystem>
#include <optional>
#include <string_view>
#include <utility>

namespace FL
{
	namespace
	{
		auto const & GetSeperator()
		{
			static auto const seperator = std::filesystem::path::string_type( 1, std::filesystem::path::preferred_separator );
			return seperator;
		}
		auto AddSlash(std::filesystem::path file)
		{
			return file += GetSeperator();
		}
	}

	auto GlobFiles( std::u8string str, NavSettings const & settings ) -> NavList
	{
		std::optional<Nav> opt_nav;
		auto const cwd = [&]() -> bool
		{
			// Example Path: ./hello
			//               ^
			opt_nav.emplace( Nav( settings, "." ) );
			return true;
		};
		auto const root_dir = [&]() -> bool
		{
			// Example Path: /hello
			//               ^
			opt_nav.emplace( Nav( settings, GetSeperator() ) );
			return true;
		};
		auto const recursive_cwd = [&]() -> bool
		{
			// Example Path: **/crayons/file.txt
			//               ^^
			auto & ref = opt_nav.emplace( Nav( settings, "." ) );
			return ref.AddRecursive();
		};
		auto const dir_cwd = [&]( std::u8string const & file ) -> bool
		{
			// Example Path: include/crayons/file.txt
			//               |<--->|
			auto & ref = opt_nav.emplace( Nav( settings, "." ) );
			Glob glob( file );
			return glob.RequiresExactMatch() ? ref.Open( file ) : ref.Apply( glob );
		};
		auto const file_cwd = [&]( std::u8string const & file ) -> bool
		{
			// Example Path: file.txt
			//               |<---->|
			auto & ref = opt_nav.emplace( Nav( settings, "." ) );
			Glob glob( file );
			return glob.RequiresExactMatch() ? ref.Open( file ) : ref.Apply( glob );
		};
		auto const root_path = [&]( std::u8string const & dir ) -> bool
		{
			// Exmple Path: c:/hello
			//               ^
			auto dir_path = AddSlash( dir );
			if ( not FL::FileExists( dir_path ) ) return false;
			opt_nav.emplace( Nav( settings, dir_path ) );
			return true;
		};
		auto const recursive = [&]() -> bool
		{
			// Example Path: c:/**/food
			//                  ^^
			if ( not opt_nav ) return false;
			auto & ref = *opt_nav;
			return ref.AddRecursive();
		};
		auto const ordinary_dir = [&]( std::u8string const & name ) -> bool
		{
			// Example Path: food/*hello*/more/path
			//                    |<--->|
			if ( not opt_nav ) return false;
			auto & ref = *opt_nav;
			Glob glob( name );
			return glob.RequiresExactMatch() ? ref.Open( name ) : ref.Apply( glob );
		};
		auto const ordinary_file = [&]( std::u8string const & name ) -> bool
		{
			// Example Path: food/*hello.txt
			//                    |<------>|
			if ( not opt_nav ) return false;
			auto & ref = *opt_nav;
			Glob glob( name );
			return glob.RequiresExactMatch() ? ref.Open( name ) : ref.Apply( glob );
		};

		// The buffer that is accumulated inside.
		std::u8string buffer;
		auto const is_recursive = [&]
		{
			return settings.enable_recursion and ( buffer == u8"**" );
		};
		auto const is_root_path = [&]
		{
			auto const path = std::filesystem::path( buffer );
			return path.has_root_name() and not path.has_root_directory();
		};
		auto submit_first = [&]() -> bool
		{
			// Explicit cwd?
			if ( buffer == u8"." and cwd() ) { return true; }
			// Explicit root directory, implicit drive?
			if ( buffer == u8"" and root_dir() ) { return true; }
			// Implicit cwd recursive search.
			if ( is_recursive() and recursive_cwd() ) { return true; }
			// Explicit root directory, explicit drive
			if ( is_root_path() and root_path( buffer ) ) { return true; }
			return false;
		};
		auto submit = [&, first = true](auto first_file) mutable -> bool
		{
			// Leading path sections have some unique behaviour.
			return (
				( std::exchange( first, false ) and ( submit_first() or first_file(buffer) ) ) or
				( is_recursive() and recursive() )
			);
		};

		// Case insensitive paths are compared in lower case.
		for ( auto const c : str )
		{
			if ( ( c == u8'/' or c == u8'\\' ) and ( submit(dir_cwd) or ordinary_dir( buffer ) ) )
			{
				// Successfully submitted the character.
				buffer.clear();
			}
			else { buffer += c; }
		}
		if ( not buffer.empty() )
		{
			if ( not( submit(file_cwd) or ordinary_file( buffer ) ) ) { return {}; }
		}
		if ( opt_nav )
		{
			auto & nav = *opt_nav;
			auto output = nav.CurrentNav();
			for ( auto & option : output ) { option.path = settings.GetPrintablePath( option.path ); }
			return output;
		}
		return {};
	}

	auto GlobFiles( std::string_view glob, NavSettings const & settings ) -> NavList
	{
		return GlobFiles( std::u8string( glob.begin(), glob.end() ), settings );
	}
}