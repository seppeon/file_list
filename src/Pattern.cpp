#include "FL/Pattern.hpp"
#include <numeric>
#include <cstring>
#include <cassert>

namespace FL
{
	std::u8string_view unknown_part = u8"";

	namespace
	{
		constexpr Pattern::size_type SumOfSizes(auto const & parts) noexcept
		{
			return std::accumulate(
				parts.begin(),
				parts.end(),
				Pattern::size_type{}, 
				[](Pattern::size_type a, std::u8string_view  b) noexcept -> Pattern::size_type
				{
					return a + b.size();
				}
			);
		}

		constexpr Pattern::size_type Exchange(Pattern::size_type & value, Pattern::size_type inc) noexcept
		{
			auto const output = value;
			value = inc;
			return output;
		}
		/**
		 * @brief Get the index range of the particular constant part.
		 */
		struct PartIndexRange
		{
			Pattern::size_type index = 0;
			Pattern::size_type length = 0;
		};
		/**
		 * @brief Get the range of indexes representing the particular part of the constant pattern.
		 * 
		 * @param i The index of the constant pattern.
		 * @return The index range.
		 */
		constexpr auto GetPartIndexRange(std::span<Pattern::size_type const> indexes, Pattern::size_type i) noexcept -> PartIndexRange
		{
			auto const low = (i == 0) ? 0 : indexes[i-1];
			auto const high = indexes[i];
			return PartIndexRange{ .index = low, .length = high - low };
		}
	}

	Pattern::Pattern(std::span<std::u8string_view const> parts)
		: m_indexes( parts.size(), 0 )
		, m_parts( SumOfSizes(parts), u8'\0' )
	{
		size_type indexes_index = 0;
		size_type parts_index = 0;
		for (auto const part : parts)
		{
			std::memcpy(m_parts.data() + parts_index, part.data(), part.size());
			parts_index += part.size();
			m_indexes[indexes_index++] = parts_index;
		}
	}

	Pattern::Pattern(std::initializer_list<std::u8string_view> constant_parts)
		: Pattern{ std::span<std::u8string_view const>{ constant_parts.begin(), constant_parts.end() } }
	{}

	auto Pattern::Size() const noexcept -> size_type
	{
		return m_indexes.size();
	}

	auto Pattern::begin() const noexcept -> const_iterator
	{
		return { *this, 0 };
	}

	auto Pattern::end() const noexcept -> const_iterator
	{
		return { *this, static_cast<difference_type>(Size()) };
	}

	auto Pattern::Front() const noexcept -> value_type
	{
		assert(IsValid());
		return operator[](0);
	}

	auto Pattern::Back() const noexcept -> value_type
	{
		assert(IsValid());
		return operator[](Size() - 1);
	}

	auto Pattern::GetTotalLength() const noexcept -> size_type
	{
		return m_parts.size();
	}

	auto Pattern::IsValid() const noexcept -> bool
	{
		return Size() != 0;
	}

	auto Pattern::operator[](difference_type i) const noexcept -> reference
	{
		auto const index_range = GetPartIndexRange(m_indexes, i);
		return PatternValue{ std::u8string_view{m_parts.data() + index_range.index, index_range.length} };
	}

	auto Pattern::Substitute(std::span<PatternSub const> substitutions) const -> Pattern
	{
		auto indexes = m_indexes;
		auto total_new_length = size_type(0);
		auto total_old_length = size_type(0);
		// Convert indexes to lengths
		size_type len = 0;
		for (auto & index : indexes)
		{
			index -= Exchange(len, index);
		}
		// Assign the new lengths
		for (auto const [i, value] : substitutions)
		{
			indexes[i] = value.size();
		}
		// Convert back to indexes
		size_type offset = 0;
		size_type total_length = 0;
		for (auto & index : indexes)
		{
			total_length += index;
			index = (offset += index);
		}
		// Copy the substitutions into the new indexes
		auto parts = std::u8string(total_length, '\0');
		// Copy in the old values
		Pattern output;
		output.m_indexes = std::move(indexes);
		output.m_parts = std::move(parts);
		for (size_type i = 0; i < Size(); ++i)
		{
			auto const src = operator[](i);
			auto * dst = output.m_parts.data() + GetPartIndexRange(output.m_indexes, i).index;
			std::memcpy(dst, src.data(), src.size());
		}
		for (auto const [i, value] : substitutions)
		{
			auto const [offset, _] = GetPartIndexRange(output.m_indexes, i);
			std::memcpy(output.m_parts.data() + offset, value.data(), value.size());
		}
		return output;
	}

	auto Pattern::AsU8String() const noexcept -> std::u8string
	{
		std::u8string output;
		for (auto const & value : *this)
		{
			output += value;
		}
		return output;
	}

	auto Pattern::AsPath() const noexcept -> std::filesystem::path
	{
		return std::filesystem::path(AsU8String());
	}

	std::string DescribePattern(Pattern const & pattern)
	{
		std::size_t i = 0;
		std::string output;
		for (auto const & part : pattern)
		{
			output += "[";
			output += std::to_string(i++) + ": '";
			output.append((const char *)part.data(), part.size());
			output += "']";
		}
		return output;
	}

	std::string DescribeSubstitutionList(std::span<PatternSub const> substitutions)
	{
		std::string output;
		for (auto const & sub : substitutions)
		{
			output += "[";
			output += std::to_string(sub.index) + ": '";
			output.append((const char *)sub.value.data(), sub.value.size());
			output += "']";
		}
		return output;
	}

	[[nodiscard]] std::string DescribeSubstitutionVector(std::vector<PatternSub> const & substitutions)
	{
		return DescribeSubstitutionList(std::span<PatternSub const>{substitutions.data(), substitutions.size()});
	}
}