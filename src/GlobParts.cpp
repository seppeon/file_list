#include "FL/GlobParts.hpp"
#include "FL/StrOps.hpp"
#include "Overloads.hpp"

#include <algorithm>
#include <cstddef>
#include <exception>
#include <span>
#include <string_view>

namespace FL
{
	namespace
	{
		std::size_t PreconditionFind( std::u8string_view input, auto precondition ) noexcept
		{
			for ( std::size_t i = 0; i < input.size(); ++i )
			{
				if ( precondition( input[i] ) ) { return i; }
			}
			return glob_npos;
		}

		std::u8string_view HasPrefix( FixedLength auto const & parser, std::u8string_view input )
		{
			auto const result = parser.Find( input.substr( 0, parser.Size() ) );
			return result != glob_npos ? input.substr( parser.Size() ) : std::u8string_view{};
		}

		bool Equal(char8_t a, char8_t b, bool case_sensitive) noexcept
		{
			return FL::Equal(std::u8string_view{&a, 1}, std::u8string_view{&b, 1}, case_sensitive);
		}

		auto MakeEqual(bool case_sensitive) noexcept
		{
			return [case_sensitive](char8_t a, char8_t b) { return Equal(a, b, case_sensitive); };
		}

		// TODO: Update with unicode library once one is found.
		//       This will only work for english, and have major issues in other locales.
		constexpr char8_t ToLowerCase( char8_t v ) noexcept
		{
			return ( ( u8'A' <= v ) and ( v <= u8'Z' ) ) ? u8'a' + ( v - u8'A' ) : v;
		}
	}
	////////////////////////////////////////////////////////////////////////////////////////
	std::size_t GlobPartExact::Size() const noexcept
	{
		return part.size();
	}
	std::size_t GlobPartExact::Find( std::u8string_view input, bool case_sensitive ) const noexcept
	{
		// TODO: This will need to be updated when full utf8 support is added.
		auto const equal = MakeEqual(case_sensitive);
		auto const it = std::search(input.begin(), input.end(), part.begin(), part.end(), equal);
		return ( it == input.end() ) ? glob_npos : (it - input.begin());
	}
	bool GlobPartExact::HasPrefix( std::u8string_view input, bool case_sensitive ) const noexcept
	{
		return Equal(part, input.substr(0, part.size()), case_sensitive);
	}
	bool GlobPartExact::ShouldSave() const noexcept
	{
		return false;
	}
	////////////////////////////////////////////////////////////////////////////////////////
	std::size_t GlobOneOfChar::Size() const noexcept
	{
		return 1;
	}
	std::size_t GlobOneOfChar::Find( std::u8string_view input, bool case_sensitive) const noexcept
	{
		auto const has_char = [&]( char8_t v )
		{
			auto const compare = MakeEqual(case_sensitive);
			auto const equal = [v, compare](char8_t b)
			{
				return compare(v, b);
			};
			return std::any_of( chars.begin(), chars.end(), equal );
		};
		return PreconditionFind( input, has_char );
	}
	bool GlobOneOfChar::HasPrefix( std::u8string_view input, bool case_sensitive ) const noexcept
	{
		if ( input.empty() ) return false;
		auto const compare = MakeEqual(case_sensitive);
		auto const equal = [v = input.front(), compare](char8_t b)
		{
			return compare(v, b);
		};
		return std::any_of( chars.begin(), chars.end(), equal );
	}
	bool GlobOneOfChar::ShouldSave() const noexcept
	{
		return true;
	}
	////////////////////////////////////////////////////////////////////////////////////////
	std::size_t GlobRangeOfChar::Size() const noexcept
	{
		return 1;
	}
	std::size_t GlobRangeOfChar::Find( std::u8string_view input, bool case_sensitive ) const noexcept
	{
		auto const in_char_range = [&]( char8_t v )
		{
			if (case_sensitive)
			{
				return ( low <= v ) and ( v <= high );
			}
			else
			{
				v = ToLowerCase(v);
				return ( ToLowerCase(low) <= v ) and ( v <= ToLowerCase(high) );
			}
		};
		return PreconditionFind( input, in_char_range );
	}
	bool GlobRangeOfChar::HasPrefix( std::u8string_view input, bool case_sensitive ) const noexcept
	{
		if ( input.empty() ) return false;
		auto const in_char_range = [&]( char8_t v )
		{
			if (case_sensitive)
			{
				return ( low <= v ) and ( v <= high );
			}
			else
			{
				v = ToLowerCase(v);
				return ( ToLowerCase(low) <= v ) and ( v <= ToLowerCase(high) );
			}
		};
		return in_char_range( input.front() );
	}
	bool GlobRangeOfChar::ShouldSave() const noexcept
	{
		return true;
	}
	////////////////////////////////////////////////////////////////////////////////////////
	std::size_t GlobAnyOneChar::Size() const noexcept
	{
		return 1;
	}
	std::size_t GlobAnyOneChar::Find( std::u8string_view input, bool case_sensitive ) const noexcept
	{
		return HasPrefix( input, case_sensitive ) ? 0 : glob_npos;
	}
	bool GlobAnyOneChar::HasPrefix( std::u8string_view input, bool case_sensitive ) const noexcept
	{
		static_cast<void>(case_sensitive);
		return not input.empty();
	}
	bool GlobAnyOneChar::ShouldSave() const noexcept
	{
		return true;
	}
	////////////////////////////////////////////////////////////////////////////////////////
	bool GlobWildcard::ShouldSave() const noexcept
	{
		return true;
	}
	////////////////////////////////////////////////////////////////////////////////////////
	std::string GlobPartExact::Describe() const
	{
		return reinterpret_cast<const char *>( ( u8"('" + part + u8"')" ).data() );
	}
	std::string GlobWildcard::Describe() const
	{
		return "(*)";
	}
	std::string GlobAnyOneChar::Describe() const
	{
		return "(?)";
	}
	std::string GlobOneOfChar::Describe() const
	{
		return reinterpret_cast<const char *>( ( u8"(any '" + chars + u8"')" ).data() );
	}
	std::string GlobRangeOfChar::Describe() const
	{
		return "(from '" + std::string( 1, low ) + "' to '" + std::string( 1, high ) + "')";
	}
	// General purpose functions
	bool PartIsFixedLength( GlobPartType const & part ) noexcept
	{
		return std::visit( Overloads{ []( FixedLength auto const & v )
							   {
								   return true;
							   },
							   []( auto const & )
							   {
								   return false;
							   } },
			part );
	}
	std::string PartDescribe( GlobPartType const & part )
	{
		return std::visit(
			[]( auto const & v )
			{
				return v.Describe();
			},
			part );
	}
	std::string PartListDescribe( GlobPartList const & list )
	{
		std::string output;
		for ( auto const & part : list ) output += PartDescribe( part );
		return output;
	}
	std::size_t PartSize( GlobPartType const & part )
	{
		return std::visit( Overloads{ []( FixedLength auto const & v )
							   {
								   return v.Size();
							   },
							   []( auto const & )
							   {
								   // This is a logic error, and should never happen.
								   std::terminate();
								   return std::size_t{};
							   } },
			part );
	}
	std::size_t PartFind( GlobPartType const & part, std::u8string_view input, bool case_sensitive ) noexcept
	{
		return std::visit( Overloads{ [&]( FixedLength auto const & v )
							   {
								   return v.Find( input, case_sensitive );
							   },
							   []( auto const & )
							   {
								   // This is a logic error, and should never happen.
								   std::terminate();
								   return std::size_t{};
							   } },
			part );
	}
	bool PartHasPrefix( GlobPartType const & part, std::u8string_view input, bool case_sensitive ) noexcept
	{
		return std::visit( Overloads{ [&]( FixedLength auto const & v ) -> bool
							   {
								   return v.HasPrefix( input, case_sensitive );
							   },
							   []( auto const & ) -> bool
							   {
								   // This is a logic error, and should never happen.
								   std::terminate();
								   return false;
							   } },
			part );
	}
	bool PartShouldSave( GlobPartType const & part ) noexcept
	{
		return std::visit(
			[]( auto const & v )
			{
				return v.ShouldSave();
			},
			part );
	}
	bool PartIsConstant( GlobPartType const & part ) noexcept
	{
		return not PartShouldSave( part );
	}
	auto PartsTotalFixedSize( GlobPartList const & parts ) noexcept -> std::size_t
	{
		std::size_t output = 0;
		for ( auto const & part : parts )
		{
			if ( PartIsFixedLength( part ) ) { output += PartSize( part ); }
		}
		return output;
	}
	auto PartsCountVariableLength( GlobPartList const & parts ) noexcept -> std::size_t
	{
		std::size_t output = 0;
		for ( auto const & part : parts )
		{
			if ( not PartIsFixedLength( part ) ) { ++output; }
		}
		return output;
	}

	auto PartAsConstant( GlobPartType const & part ) noexcept -> std::u8string_view
	{
		return std::visit( Overloads{ [&]( GlobPartExact const & v ) -> std::u8string_view
							   {
								   return v.part;
							   },
							   []( auto const & ) -> std::u8string_view
							   {
								   // This is a logic error, and should never happen.
								   std::terminate();
								   return u8"";
							   } },
			part );
	}

	auto PatternFromPartList( GlobPartList const & part_list ) -> Pattern
	{
		std::vector<std::u8string_view> output;
		for (auto const & part : part_list)
		{
			if (PartIsConstant(part))
			{
				output.push_back(PartAsConstant(part));
			}
			else
			{
				output.push_back(u8"");
			}
		}
		return Pattern{ output };
	}
}