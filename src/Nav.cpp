#include "FL/Nav.hpp"
#include "FL/Exact.hpp"
#include "FL/FileOps.hpp"
#include "FL/Pass.hpp"

namespace FL
{
	using std::filesystem::path;
	using opt_set = typename Nav::opt_set;

	bool operator<( NavItem const & lhs, NavItem const & rhs ) noexcept
	{
		return lhs.path < rhs.path;
	}
	bool operator>( NavItem const & lhs, NavItem const & rhs ) noexcept
	{
		return lhs.path > rhs.path;
	}
	bool operator<=( NavItem const & lhs, NavItem const & rhs ) noexcept
	{
		return lhs.path <= rhs.path;
	}
	bool operator>=( NavItem const & lhs, NavItem const & rhs ) noexcept
	{
		return lhs.path >= rhs.path;
	}
	bool operator==( NavItem const & lhs, NavItem const & rhs ) noexcept
	{
		return lhs.path == rhs.path;
	}
	bool operator!=( NavItem const & lhs, NavItem const & rhs ) noexcept
	{
		return lhs.path != rhs.path;
	}

	namespace
	{
		auto ConcatNavItem( std::filesystem::path subpath, FL::Pattern const & old_pattern, FL::Pattern const & sub_pattern ) -> NavItem
		{
			auto new_pattern = std::vector<std::u8string_view>( old_pattern.begin(), old_pattern.end() );
			new_pattern.insert( new_pattern.end(), sub_pattern.begin(), sub_pattern.end() );
			return NavItem{
				.path = subpath,
				.name = FL::Pattern{ new_pattern },
			};
		}
		auto ConcatNavItem( std::filesystem::path subpath, FL::Pattern const & old_pattern, std::u8string_view const & sub_pattern ) -> NavItem
		{
			auto new_pattern = std::vector<std::u8string_view>( old_pattern.begin(), old_pattern.end() );
			new_pattern.push_back( sub_pattern );
			return NavItem{
				.path = subpath,
				.name = FL::Pattern{ new_pattern },
			};
		}

		auto DefaultApplyPreCheck( std::filesystem::path const & )
		{
			return true;
		}

		auto Apply( NavSettings const & settings, opt_set const & options, Filt const & filt, auto explore, auto pre_check, bool retain_dirs = false ) -> opt_set
		{
			opt_set new_options;
			for ( auto const & [path, option_pattern] : options )
			{
				if ( settings.IsOkDirectory( path ) )
				{
					if (retain_dirs)
					{
						new_options.insert( ConcatNavItem( path, option_pattern, u8"" ) );
					}
					for ( auto const & subpath : explore( path ) )
					{
						if ( pre_check( subpath ) )
						{
							std::error_code ec;
							auto const filename = std::filesystem::relative( subpath, path, ec );
							auto const filename_u8str = filename.u8string();
							if ( ec != std::error_code() ) continue;
							auto const sub_pattern = filt.Parse( filename_u8str, settings.case_sensitive );
							if ( sub_pattern.IsValid() )
							{
								new_options.insert( ConcatNavItem( subpath, option_pattern, sub_pattern ) );
							}
						}
					}
				}
			}
			return new_options;
		}

		auto CaseSensitiveOpen( NavSettings const & settings, opt_set const & options, std::u8string_view const & name ) -> opt_set
		{
			opt_set new_options;
			for ( auto const & [path, option_pattern] : options )
			{
				if ( settings.IsOkDirectory( path ) )
				{
					auto const new_path = path / name;
					if ( settings.FileOk( new_path ) )
					{
						if ( settings.case_sensitive )
						{
							// Verify filename case.
							auto const case_sensitive_filename = FL::GetCaseSensitivePath( new_path ).filename();
							if ( case_sensitive_filename != name )
							{
								// File isn't the same case, skip it.
								continue;
							}
						}
						new_options.insert( ConcatNavItem( new_path, option_pattern, name ) );
					}
				}
			}
			return new_options;
		}
	}

	bool Nav::Open( std::u8string_view name )
	{
		auto fast_options = CaseSensitiveOpen( m_settings, m_options, name );
		if ( not fast_options.empty() )
		{
			m_options = std::move( fast_options );
			return true;
		}
		if ( not m_settings.case_sensitive )
		{
			// We didn't match the fast option, this leaves us searching through the directories for the specific name.
			// matcher will utilise the case sensitivity options, specifically not being case sensitive.
			Exact matcher{ std::u8string( name ) };
			auto const explore = [&]( std::filesystem::path const & p )
			{
				return m_settings.ExplorePath( p );
			};
			m_options = FL::Apply( m_settings, m_options, matcher, explore, DefaultApplyPreCheck );
		}
		return not m_options.empty();
	}

	bool Nav::Apply( Filt const & filt )
	{
		auto const explore = [&]( std::filesystem::path const & p )
		{
			return m_settings.ExplorePath( p );
		};
		m_options = FL::Apply( m_settings, m_options, filt, explore, DefaultApplyPreCheck );
		return not m_options.empty();
	}

	bool Nav::ApplyRecursive( Filt const & filt )
	{
		auto const is_dir = [&]( std::filesystem::path const & p )
		{
			return m_settings.IsOkDirectory( p );
		};
		auto const explore = [&]( std::filesystem::path const & p )
		{
			return m_settings.ExplorePathRecurse( p );
		};
		auto new_options = FL::Apply( m_settings, m_options, filt, explore, is_dir, true );
		m_options = std::move(new_options);
		return not m_options.empty();
	}

	bool Nav::AddRecursive()
	{
		auto const is_dir = [&]( std::filesystem::path const & p )
		{
			return m_settings.IsOkDirectory( p );
		};
		auto const explore = [&]( std::filesystem::path const & p )
		{
			return m_settings.ExplorePathRecurse( p );
		};
		auto new_options = FL::Apply( m_settings, m_options, Pass{}, explore, is_dir, true );
		m_options = std::move(new_options);
		return not m_options.empty();
	}

	auto Nav::CurrentNav() const noexcept -> NavList
	{
		return NavList( m_options.begin(), m_options.end() );
	}

	Nav::Nav( NavSettings const & settings, path cwd )
		: m_settings( settings )
		, m_options{ NavItem{
			  .path = cwd,
			  .name = { cwd.u8string() },
		  } }
	{
		volatile auto v = CurrentNav();
	}
}