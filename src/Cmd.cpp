#include "FL/Cmd.hpp"
#include <cstdio>
#include <array>
#include <stdexcept>
#include <string>
#include <concepts>
#include <utility>
#include <string_view>

#include <stdio.h>
#include <stdlib.h>
#include "WindowsHeader.hpp"
#ifdef PLATFORM_IS_WINDOWS
#define popen _popen
#define pclose _pclose
#endif

namespace FL
{
	namespace
	{
		auto GetEscapeChar()
		{
			switch (GetCurrentTerminalType())
			{
			case TerminalType::Linux: return u8'\\';
			case TerminalType::WindowsCmd: return u8'\\';
			case TerminalType::WindowsPowerShell: return u8'`';
			default: return u8'\\';
			}
		}

		std::u8string_view Escape(char8_t const & c)
		{
			switch(GetCurrentTerminalType())
			{
			case TerminalType::Linux:
			{
				if (c == u8'`')	 return u8"\\`";	
				if (c == u8'~')	 return u8"\\~";	
				if (c == u8'!')	 return u8"\\!";	
				if (c == u8'#')	 return u8"\\#";	
				if (c == u8'$')	 return u8"\\$";	
				if (c == u8'&')	 return u8"\\&";	
				if (c == u8'*')	 return u8"\\*";	
				if (c == u8'(')	 return u8"\\(";	
				if (c == u8')')	 return u8"\\)";	
				if (c == u8'\t') return u8"\\t";
				if (c == u8'{')	 return u8"\\{";	
				if (c == u8'[')	 return u8"\\[";	
				if (c == u8'|')	 return u8"\\|";	
				if (c == u8'\\') return u8"\\\\";
				if (c == u8';')	 return u8"\\;";	
				if (c == u8'\'') return u8"\\\'";
				if (c == u8'"')	 return u8"\\\"";
				if (c == u8'<')	 return u8"\\<";	
				if (c == u8'>')	 return u8"\\>";	
				if (c == u8'?')	 return u8"\\?";	
				if (c == u8' ')	 return u8"\\ ";	
				break;
			}
			case TerminalType::WindowsCmd:
			{
				if (c == '\n') return u8"\n";
				if (c == '\r') return u8"\r";
				if (c == '&') return u8"^&";
				if (c == ',') return u8"^,";
				if (c == '|') return u8"^|";
				if (c == ',') return u8"^,";
				if (c == '(') return u8"^(";
				if (c == ',') return u8"^,";
				if (c == ')') return u8"^)";
				if (c == ',') return u8"^,";
				if (c == '<') return u8"^<";
				if (c == ',') return u8"^,";
				if (c == '>') return u8"^>";
				if (c == ',') return u8"^,";
				if (c == '^') return u8"^^";
				if (c == '"') return u8"^\"";
				if (c == '\'') return u8"^'";
				break;
			}
			case TerminalType::WindowsPowerShell:
			{
				if (c == '\a') return u8"`a";
				if (c == '\b') return u8"`b";
				if (c == '\f') return u8"`f";
				if (c == '\n') return u8"`n";
				if (c == '\r') return u8"`r";
				if (c == '\t') return u8"`t";
				if (c == '\v') return u8"`v";
				break;
			}
			default: break;
			}
			return { &c, 1 };
		}

		std::u8string EscapeString(std::u8string_view arg)
		{
			if (arg.empty())
			{
				return u8"\"\"";
			}
			std::u8string output;
			for (auto const c : arg)
			{
				output += Escape(c);
			}
			return output;
		}

		std::u8string GetExe(std::u8string exe)
		{
			switch(GetCurrentTerminalType())
			{
			case TerminalType::Linux: return EscapeString(exe);
			case TerminalType::WindowsCmd: return EscapeString(exe);
			case TerminalType::WindowsPowerShell: return u8"." + EscapeString(exe);
			default: return EscapeString(exe);
			}
		}

		std::u8string ToString(std::integral auto arg)
		{
			auto temp = std::to_string(arg);
			return { temp.begin(), temp.end() };
		}

		std::u8string ToString(bool arg, std::u8string_view true_str, std::u8string_view false_str)
		{
			return EscapeString(arg ? true_str : false_str);
		}

		struct POpen
		{
			POpen() = delete;
			POpen(POpen &&) = delete;
			POpen(POpen const &) = delete;
			POpen& operator=(POpen &&) = delete;
			POpen& operator=(POpen const &) = delete;

			POpen(std::string cmd)
				: m_cmd(std::move(cmd + " 2>&1"))
				, m_ptr(popen(m_cmd.data(), "r"))
			{
			}
			POpen(std::u8string_view cmd)
				: POpen(std::string(cmd.begin(), cmd.end()))
			{
			}
			~POpen() { if (m_ptr) { pclose(m_ptr); } }

			[[nodiscard]] bool IsValid() const noexcept
			{
				return m_ptr != nullptr;
			}

			[[nodiscard]] CmdResult GetResult() noexcept
			{
				if (not m_ptr) return {};
				char8_t buffer[BUFSIZ];
				std::u8string output;
				size_t count = 0;
				do {
					if ((count = fread(std::data(buffer), 1, BUFSIZ, m_ptr)) > 0)
					{
						output.append(std::begin(buffer), std::next(std::begin(buffer), count));
					}
				} while(count > 0);
				return CmdResult{
					.return_code = pclose(std::exchange(m_ptr, nullptr)),
					.stdout_and_stderr = std::move(output),
				};
			}
		private:
			std::string m_cmd;
			FILE * m_ptr = nullptr;
		};

		[[nodiscard]] CmdResult Exe(std::u8string_view cmd)
		{
			POpen runner(cmd);
			if (not runner.IsValid()) return {};
			return runner.GetResult();
		}
	}
	CmdArg::CmdArg(CmdPipe)
		: data(u8"|")
	{}
	CmdArg::CmdArg(std::u8string_view arg)
		: data(EscapeString(arg))
	{}
	CmdArg::CmdArg(std::string_view arg)
		: CmdArg(std::u8string_view(reinterpret_cast<char8_t const *>(arg.data()), arg.size()))
	{}
	CmdArg::CmdArg(IntTag, std::int64_t arg)
		: data(ToString(arg))
	{}
	CmdArg::CmdArg(BoolTag, bool arg, std::u8string_view true_str, std::u8string_view false_str)
		: data(ToString(arg))
	{}

	std::u8string Cmd::ToU8String() const
	{
		std::u8string whole_command = GetExe(cmd);
		for (auto const & [ arg ] : args)
		{
			whole_command += u8" " + arg;
		}
		return whole_command;
	}

	CmdResult Cmd::Run() const
	{
		std::u8string whole_command = GetExe(cmd);
		for (auto const & [ arg ] : args)
		{
			whole_command += u8" " + arg;
		}
		return Exe(whole_command);
	}

	Cmd operator | (Cmd const & lhs, Cmd const & rhs)
	{
		Cmd output = lhs;
		output.args.push_back(CmdArg{CmdPipe{}});
		output.args.push_back(CmdArg{rhs.cmd});
		output.args.insert(output.args.end(), rhs.args.begin(), rhs.args.end());
		return output;
	}

	[[nodiscard]] auto GetCurrentTerminalType() -> TerminalType
	{
#if defined(PLATFORM_IS_WINDOWS)
		static const auto terminal_type = []
		{
			auto const [ps1_return_code_1, ps1_string_1] = Exe(u8"$PSVersionTable.BuildVersion");
			if (ps1_return_code_1 == 0) return TerminalType::WindowsPowerShell;
			auto const [ps1_return_code_2, ps1_string_2] = Exe(u8"$PSVersionTable.OS");
			if (ps1_return_code_2 == 0) return TerminalType::WindowsPowerShell;
			auto const [cmd_return_code, cmd_string] = Exe(u8"ver");
			if (cmd_return_code == 0) return TerminalType::WindowsCmd;
			return TerminalType::Unknown;
		}();
		return terminal_type;
#else
		return TerminalType::Linux;
#endif
	}
}