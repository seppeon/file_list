#include "FL/MoveFiles.hpp"
#include "FL/FileOps.hpp"
#include "FL/GlobFiles.hpp"
#include "FL/NavSettings.hpp"
#include "FL/PatternTarget.hpp"
#include <filesystem>
#include <system_error>

namespace FL
{
	std::u8string_view ToString(MoveOperationStatus status) noexcept
	{
		switch (status)
		{
		case MoveOperationStatus::Success: return u8"Success";
		case MoveOperationStatus::MissingSource: return u8"MissingSource";
		case MoveOperationStatus::ExistingFileAtDestination: return u8"ExistingFileAtDestination";
		case MoveOperationStatus::CannotRenameFile: return u8"CannotRenameFile";
		case MoveOperationStatus::CannotRemoveFileAtDestination: return u8"CannotRemoveFileAtDestination";
		case MoveOperationStatus::CannotCreateRequiredDirectories: return u8"CannotCreateRequiredDirectories";
		default: return u8"";
		}
	}

	auto BuildGlobMoveRequestList(std::span<std::u8string const> srcs, std::span<std::u8string const> dsts) -> std::optional<GlobMoveRequestList>
	{
		if (srcs.size() != dsts.size()) return std::nullopt;
		auto const count = std::size(srcs);
		GlobMoveRequestList output(count);
		for (std::size_t i = 0; i < count; ++i)
		{
			output[i] = GlobMoveRequest{
				.src = srcs[i],
				.dst = dsts[i],
			};
		}
		return output;
	}

	auto BuildMoveOperationList(std::span<GlobMoveRequest const> glob_move_requests, NavSettings const & settings) -> BuildMoveOperationListResult
	{
		BuildMoveOperationListResult output;
		for (std::size_t i = 0; i < glob_move_requests.size(); ++i)
		{
			auto const & [src_glob_pattern, dst_target_pattern] = glob_move_requests[i];
			auto const src_glob = FL::GlobFiles(src_glob_pattern, settings);
			auto const dst_pattern = FL::PatternTarget( dst_target_pattern );
			for (auto const & [src_file, src_pattern] : src_glob)
			{
				auto const dst_file_pattern = dst_pattern.Apply(src_pattern);
				if (dst_file_pattern.IsValid())
				{
					auto const dst_file = dst_file_pattern.AsPath();
					output.move_operations_list.push_back({
						.src = settings.absolute_paths ? std::filesystem::absolute(src_file) : src_file,
						.dst = settings.absolute_paths ? std::filesystem::absolute(dst_file) : dst_file,
					});
				}
				else
				{
					// The pattern couldn't be applied to the pattern target.
					output.invalid_pattern_list.push_back(PatternMismatch{
						.move_request_index = i,
						.glob_result = src_pattern,
						.pattern_target = dst_pattern,
					});
				}
			}
		}
		return output;
	}

	auto ApplyMoveOperationList(std::span<MoveOperation const> move_operation_list, bool override_files, bool make_directories) -> MoveOperationStatusList
	{
		MoveOperationStatusList output{move_operation_list.size(), MoveOperationStatus::Success};
		auto const len = move_operation_list.size();
		for (std::size_t i = 0; i < len; ++i)
		{
			auto const & [src, dst] = move_operation_list[i];
			if (not FL::FileExists(src))
			{
				// error: the source file doesn't exist.
				output[i] = MoveOperationStatus::MissingSource;
				continue;
			}

			if (make_directories)
			{
				if (dst.has_parent_path())
				{
					auto const dst_parent = dst.parent_path();
					if (not std::filesystem::exists(dst_parent))
					{
						std::error_code ec{};
						std::filesystem::create_directories(dst_parent, ec);
						if (ec != std::error_code{})
						{
							// error: create directories failed, cannot proceed.
							output[i] = MoveOperationStatus::CannotCreateRequiredDirectories;
							continue;
						}
					}
				}
			}
			else
			{
				if (dst.has_parent_path())
				{
					auto const dst_parent = dst.parent_path();
					if (not std::filesystem::exists(dst_parent))
					{
						// error: cannot create directories, cannot proceed.
						output[i] = MoveOperationStatus::CannotCreateRequiredDirectories;
						continue;
					}
				}
			}

			if (FL::FileExists(dst))
			{
				if (override_files)
				{
					std::error_code ec{};
					std::filesystem::remove(dst, ec);
					if (ec != std::error_code{})
					{
						// error: remove failed, cannot proceed.
						output[i] = MoveOperationStatus::CannotRemoveFileAtDestination;
						continue;
					}
				}
				else
				{
					// error: an existing file cannot be overriden.
					output[i] = MoveOperationStatus::CannotRemoveFileAtDestination;
					continue;
				}
			}
			std::error_code ec{};
			std::filesystem::rename(src, dst, ec);
			if (ec != std::error_code{})
			{
				// error: rename failed, this might be bad because a file may have already been removed.
				output[i] = MoveOperationStatus::CannotRenameFile;
				continue;
			}
		}
		return output;
	}
}