#include "FL/GlobFixedSeq.hpp"
#include "FL/GlobParts.hpp"
#include <algorithm>
#include <cstddef>
#include <iterator>
#include <numeric>

namespace FL
{
	bool GlobFixedSeq::ShouldSave() const noexcept
	{
		return std::any_of(
			std::begin(parts),
			std::end(parts),
			PartShouldSave
		);
	}
	std::string GlobFixedSeq::Describe() const noexcept
	{
		std::string output;
		for (auto const & part : parts)
		{
			output += PartDescribe(part);
		}
		return output;
	}

	std::size_t GlobFixedSeq::Size() const noexcept
	{
		return std::accumulate(
			std::begin(parts),
			std::end(parts),
			std::size_t{},
			[](std::size_t v, GlobPartType const & part) -> std::size_t
			{
				return v + PartSize(part);
			}
		);
	}
	std::size_t GlobFixedSeq::Find(std::u8string_view input, bool case_sensitive) const noexcept
	{
		if (parts.empty()) return glob_npos;
		auto const & front = parts.front();

		auto check_offset = [&](std::size_t const offset, std::u8string_view scanner) -> std::size_t
		{
			// Find the front item.
			auto const i = PartFind(front, scanner, case_sensitive);
			if (i == glob_npos) return glob_npos;
			scanner.remove_prefix(i + PartSize(front));

			// Check the tailing items.
			for (std::size_t i = 1; i < parts.size(); ++i)
			{
				auto const & tail = parts[i];
				if (not PartHasPrefix(tail, scanner, case_sensitive)) return glob_npos;
				scanner.remove_prefix(PartSize(tail));
			}
			return i + offset;
		};
		// Scan through the string, using the first item in the parts list 
		// as the item that will be searched for, all subsequent items just have 
		// a prefix check performed.
		for (std::size_t i = 0; i + Size() <= input.size(); ++i)
		{
			auto const result = check_offset(i, input.substr(i));
			if (result != glob_npos)
			{
				return result;
			}
		}
		return glob_npos;
	}
	bool GlobFixedSeq::HasPrefix(std::u8string_view input, bool case_sensitive) const noexcept
	{
		for (auto const & part : parts)
		{
			if (not PartHasPrefix(part, input, case_sensitive)) return false;
			input.remove_prefix(PartSize(part));
		}
		return true;
	}

	GlobFixedSeq GetFixedSeqPrefix(std::span<GlobPartType const> parts) noexcept
	{
		auto const index_of_first_variable_length = [&]
		{
			for (std::size_t i = 0; i < parts.size(); ++i)
			{
				if (not PartIsFixedLength(parts[i]))
				{
					return i;
				}
			}
			return parts.size();
		}();
		return GlobFixedSeq{ parts.subspan(0, index_of_first_variable_length) };
	}

	GlobFixedSeq GetFixedSeqSuffix(std::span<GlobPartType const> parts) noexcept
	{
		auto const index_of_last_fixed_length = [&]
		{
			for (std::size_t k = 0; k < parts.size(); ++k)
			{
				auto const i = parts.size() - (k + 1);
				if (not PartIsFixedLength(parts[i]))
				{
					return i + 1;
				}
			}
			return parts.size();
		}();
		return GlobFixedSeq{ parts.subspan(index_of_last_fixed_length) };
	}

	auto GetFixedSeqList(std::size_t offset, std::span<GlobPartType const> parts) noexcept -> GlobFixedSeqList
	{
		if (parts.empty())
		{
			return {};
		}
		std::vector<GlobFixedSeq> sequences;
		// Remove the wildcards, we don't consider them explicitly.
		if (not PartIsFixedLength(parts.front()))
		{
			parts = parts.subspan(1);
		}
		while (not parts.empty())
		{
			auto prefix = GetFixedSeqPrefix(parts);
			prefix.offset = offset;
			offset += prefix.Size();
			sequences.push_back(prefix);
			parts = parts.subspan(prefix.parts.size());
			// Remove the wildcards, we don't consider them explicitly.
			if (not parts.empty() and not PartIsFixedLength(parts.front()))
			{
				parts = parts.subspan(1);
			}
		}
		return { sequences };
	}

	auto GetFixedSeqPartsCount(GlobFixedSeq const & seq) noexcept -> std::size_t
	{
		return seq.parts.size();
	}

	auto GetFixedSeqListPartsCount(GlobFixedSeqList const & list) noexcept -> std::size_t
	{
		std::size_t fixed_parts =0;
		for (auto const & item : list)
		{
			fixed_parts += GetFixedSeqPartsCount(item);
		}
		auto const variable_parts = (std::max<std::size_t>(1, list.size()) - 1);
		return fixed_parts + variable_parts;
	}

	auto GetFixedSeqListDescribe(GlobFixedSeqList const & list) noexcept -> std::string
	{
		std::string output;
		auto it = list.begin();
		auto const end_it = list.end();
		if (it != end_it)
		{
			auto const & ref = *it;
			output += ref.Describe();
			++it;
			while (it != end_it)
			{
				auto const & mid_ref = *it;
				output += GlobWildcard{}.Describe();
				output += ref.Describe();
				++it;
			}
		}
		return output;
	}
}