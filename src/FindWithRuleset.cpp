#include "FL/FindWithRuleset.hpp"
#include "FL/FindRuleset.hpp"
#include "Overloads.hpp"
#include <set>
#include <string_view>
#include <unordered_map>
#include <variant>

namespace FL
{
	namespace
	{
		template <typename T>
		struct InsertOp
		{
			std::size_t index = 0;
			T const * ruleset = nullptr;
			// Reverse the order of sorting, so that insert operations don't impact indexes of previous insert operations.
			friend bool operator<(InsertOp const & lhs, InsertOp const & rhs) { return lhs.index > rhs.index; }
		};

		template <typename Elems, typename T>
		auto Inherit(Elems & elems, std::unordered_map<std::u8string_view, T const *> const & value_by_ruleset) -> bool
		{
			// Loop through elements, inserting inherited elements in the position inherited.
			// Gather all the inherit operations.
			std::set<InsertOp<T>> insert_operations;
			for (std::size_t i = 0; i < elems.size(); ++i)
			{
				auto const & elem = elems[i];
				auto const handle_elem = ::FL::Overloads{
					[&](std::u8string const & inheritied_find_argument)
					{
						auto const it = value_by_ruleset.find(inheritied_find_argument);
						if (it != value_by_ruleset.end())
						{
							insert_operations.insert(InsertOp{
								.index = i,
								.ruleset = it->second
							});
							return true;
						}
						else return false;
					},
					[](auto const &) { return true; }
				};
				auto const inherit_success = std::visit(handle_elem, elem);
				if (not inherit_success)
				{
					return false;
				}
			}
			// Apply the inherit operations.
			for (auto const & [i, ruleset_ptr] : insert_operations)
			{
				elems.erase(elems.begin() + i);
				elems.insert(elems.begin() + i, ruleset_ptr->begin(), ruleset_ptr->end());
			}
			return true;
		}

		template <typename T>
		auto Assume(std::vector<FindInherit<T>> const & elems)
		{
			std::vector<T> output;
			auto const handle_elem = ::FL::Overloads{
				[](std::u8string const &)
				{
					return true;
				},
				[&](T const & element)
				{
					output.push_back(element);
					return true;
				}
			};
			for (auto const & elem : elems) { std::visit(handle_elem, elem); }
			return output;
		}
	}

	void PreprocessedFindRuleset::Sub(FindRulesetArgsSubstitution const & sub)
	{
		for (auto & rule : rules)
		{
			rule = rule.Sub(sub);
		}
	}

	void PreprocessedFindRulesets::Sub(FindRulesetArgsSubstitution const & sub)
	{
		for (auto & ruleset : rulesets)
		{
			ruleset.Sub(sub);
		}
	}

	auto PreprocessRulesets(FindRulesets const & rulesets) -> PreprocessedFindRulesets
	{
		// Build some maps to lookup fields by name, for inheritance.
		std::unordered_map<std::u8string_view, FindRulesetArgs const *> args_by_name;
		std::unordered_map<std::u8string_view, FindRulesetRules const *> rules_by_name;
		for (auto const & ruleset : rulesets.rulesets)
		{
			args_by_name[ruleset.name] = std::addressof(ruleset.args);
			rules_by_name[ruleset.name] = std::addressof(ruleset.rules);
		}

		// Loop through rulesets, inheriting fields as needed.
		FindRulesets temp = rulesets;
		for (auto & [_, args, rules] : temp.rulesets)
		{
			// Inherit args
			if (not Inherit(args, args_by_name)) return {};
			// Inherit rules
			if (not Inherit(rules, rules_by_name)) return {};
		}
		PreprocessedFindRulesets output;
		for (auto & [name, args, rules] : temp.rulesets)
		{
			output.rulesets.push_back(PreprocessedFindRuleset
			{
				.name = name,
				.args = Assume(args),
				.rules = Assume(rules),
			});
		}
		return output;
	}

	auto GetRequiredArgumentList(FindRuleset const & ruleset) -> std::vector<FindArg>
	{
		std::vector<FindArg> output;
		auto const handle_elem = ::FL::Overloads{
			[](std::u8string const & inheritied_find_argument){},
			[&](FL::FindArg const & arg) { output.push_back(arg); }
		};
		for (auto const & arg : ruleset.args) { std::visit(handle_elem, arg); }
		return output;
	}

	auto FindWithRuleset(PreprocessedFindRuleset const & ruleset, std::filesystem::path const & file) -> std::optional<std::filesystem::path>
	{
		for (auto const & rule : ruleset.rules)
		{
			auto const opt_result = rule.Find(file);
			if (not opt_result) continue;
			return opt_result;
		}
		return std::nullopt;
	}

	auto PreprocessedFindRulesets::GetRulesetByName(std::u8string_view input) const noexcept -> PreprocessedFindRuleset const *
	{
		for (auto const & ruleset : rulesets)
		{
			if (input == ruleset.name)
			{
				return std::addressof(ruleset);
			}
		}
		return nullptr;
	}

	bool PreprocessedFindRulesets::IsValid() const noexcept
	{
		return not rulesets.empty();
	}
}