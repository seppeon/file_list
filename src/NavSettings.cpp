#include "FL/NavSettings.hpp"
#include "FL/FileOps.hpp"
#include <string>
#include <stdexcept>

namespace FL
{
	namespace
	{
		[[nodiscard]] constexpr auto GetDirOptions(bool allow_symlinks) noexcept -> std::filesystem::directory_options
		{
			return (
				std::filesystem::directory_options::skip_permission_denied |
				(allow_symlinks ? std::filesystem::directory_options::follow_directory_symlink : std::filesystem::directory_options{})
			);
		}
	}

	auto NavSettings::ExplorePath(std::filesystem::path const & p) const -> Gen<std::filesystem::path>
	{
		using dir_it = std::filesystem::directory_iterator;
		auto const [fix_applied, fixed_path] = FixLongPath(p);
		if (not FileOk(fixed_path))
		{
			auto const error_msg = u8"Given path '" + p.u8string() + u8"' is either hidden, does not exist or is a symlink.";
			throw std::runtime_error(std::string(error_msg.begin(), error_msg.end()));
		}
		auto it = dir_it(fixed_path, GetDirOptions(allow_symlinks));
		auto const it_end = dir_it();
		while (it != it_end)
		{
			auto const curr = it->path();
			if ( FileOk(curr) )
			{
				if (fix_applied)
				{
					co_yield (fix_applied ? UnfixLongPath(curr) : curr);
				}
				else
				{
					co_yield curr;
				}
			}
			std::error_code ec;
			it = it.increment(ec);
		}
	}

	auto NavSettings::ExplorePathRecurse(std::filesystem::path const & p) const -> Gen<std::filesystem::path>
	{
		auto const [fix_applied, fixed_path] = FixLongPath(p);
		for (auto const & curr : ExplorePath(fixed_path))
		{
			co_yield (fix_applied ? UnfixLongPath(curr) : curr);
			if ( IsDirectory( curr ) )
			{
				for (auto const p : ExplorePathRecurse(curr))
				{
					co_yield (fix_applied ? UnfixLongPath(p) : p);
				}
			}
		}
	}

	auto NavSettings::FileOk(std::filesystem::path const & p) const noexcept -> bool
	{
		if (not FL::FileExists(p)) return false;
		if (not allow_symlinks and FL::IsSymlink(p)) return false;
		if (not show_hidden and FL::IsFileHidden(p)) return false;
		return true;
	}

	auto NavSettings::IsOkDirectory(std::filesystem::path const & p) const noexcept -> bool
	{
		return FileOk(p) and IsDirectory(p);
	}

	auto NavSettings::IsOkRegularFile(std::filesystem::path const & p) const noexcept -> bool
	{
		return FileOk(p) and IsRegularFile(p);
	}

	auto NavSettings::GetPrintablePath(std::filesystem::path const & p) const noexcept -> std::filesystem::path
	{
		return absolute_paths
			? std::filesystem::absolute(p)
			: p;
	}
}