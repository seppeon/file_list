#include "FL/FileSearch.hpp"
#include "FL/Cmd.hpp"
#include "FL/FileOps.hpp"
#include "FL/StrOps.hpp"
#include "PlatformDetect.hpp"
#include <cstddef>
#include <filesystem>
#include <limits>
#include <optional>
#include <ranges>
#include <set>
#include <string>
#include <string_view>
#include <system_error>
#include <array>

#if defined(PLATFORM_IS_WINDOWS)
#include "WindowsRegistry.hpp"
#include "WindowsHeader.hpp"
#elif defined(PLATFORM_IS_LINUX) || defined(PLATFORM_IS_MAC)
#include <dlfcn.h>
#endif

namespace FL
{
	namespace
	{
		[[nodiscard]] auto SplitMultiString(std::u8string_view str, char8_t delim) -> std::vector<std::u8string_view>
		{
			auto const to_string = [](auto&& r)
			{
				return std::u8string_view(&*r.begin(), r.size());
			};
			auto parts = str | std::ranges::views::split(delim) | std::ranges::views::transform(to_string);
			return std::vector<std::u8string_view>{ parts.begin(), parts.end() };
		}

		[[nodiscard]] auto GetEnv(std::u8string_view var) -> std::u8string
		{
			auto const var_string = std::string(reinterpret_cast<const char *>(var.data()), var.size());
			auto const * env_var = std::getenv(var_string.data());
			if (not env_var) return u8"";
			auto env_view = std::string_view(env_var);
			return std::u8string(env_view.begin(), env_view.end());
		}

		[[nodiscard]] auto GetDirsFromEnv(std::u8string_view var, char8_t delim) -> std::vector<std::filesystem::path>
		{
			auto const to_string = [](std::u8string_view r) -> std::filesystem::path
			{
				if (r.empty()) return u8".";
				return std::filesystem::path(r);
			};
			auto env = GetEnv(var);
			if (env.empty()) return {};
			auto parts = SplitMultiString(env, delim) | std::ranges::views::transform(to_string);
			return std::vector<std::filesystem::path>{ parts.begin(), parts.end() };
		}

		[[nodiscard]] auto GetExtsFromEnv(std::u8string const & var, char8_t delim) -> std::vector<std::u8string>
		{
			auto env = GetEnv(var);
			if (env.empty()) return {};
			auto parts = SplitMultiString(env, delim);
			return std::vector<std::u8string>{ parts.begin(), parts.end() };
		}

#if defined(PLATFORM_IS_WINDOWS)
		[[nodiscard]] auto GetRegKeyStringData(RegKey::iterator::value_type const & value) -> std::vector<std::u8string_view>
		{
			auto data = std::u8string(value.data.begin(), value.data.end());	
			if (value.type == FL::RegDataType::Sz)
			{
				// String
				return { data };
			}
			if (value.type == FL::RegDataType::ExpandSz)
			{
				// Need to expand environmental variables.
				std::u8string temp;
				auto const * src = reinterpret_cast<LPCSTR>(data.data());
				char t;
				auto * dst = &t;
				auto const required_chars = ExpandEnvironmentStringsA(src, dst, 0);
				// Skip if the env string fails to run.
				if (required_chars == 0) return {};
				temp.resize(required_chars);
				auto const written_chars = ExpandEnvironmentStringsA(src, reinterpret_cast<LPSTR>(temp.data()), temp.size() + 1);
				if (written_chars == 0) return {};
				data = std::move(temp);
			}
			return SplitMultiString(data, CurrentEnvVarDelim());
		};
#endif

		std::u8string_view Trim(std::u8string_view str, std::u8string_view whitespace = u8" \t\r\n")
		{
			const auto strBegin = str.find_first_not_of(whitespace);
			if (strBegin == std::u8string_view::npos) { return u8""; }
			const auto strEnd = str.find_last_not_of(whitespace);
			const auto strRange = strEnd - strBegin + 1;
			return str.substr(strBegin, strRange);
		}

		struct SplitResult
		{
			std::u8string_view lhs;
			std::u8string_view rhs;
		};
		SplitResult Split(std::u8string_view haystack, std::u8string_view needle)
		{
			auto const res = haystack.find(needle);
			if (res == haystack.npos) return {};
			return SplitResult
			{
				.lhs = haystack.substr(0, res),
				.rhs = haystack.substr(res + needle.size()),
			};
		}

		struct LdConfigEntry
		{
			std::u8string name;
			std::u8string lib;
			std::u8string arch;
			std::filesystem::path path;
		};

		[[nodiscard]] auto GetLdConfigMapping(std::u8string_view ld_config_output) -> std::vector<LdConfigEntry>
		{
			auto lines = GetLines(ld_config_output);

			// Split lines by arrow "=>"
			std::vector<LdConfigEntry> entries;
			entries.reserve(lines.size());
			for (auto & line : lines)
            {
                auto [lhs, rhs] = Split(line, u8"=>");
                if (lhs.data() and rhs.data())
                {
                    // Remove leading and trailing whitespace
                    lhs = Trim(lhs);
                    rhs = Trim(rhs);
                    
                    // Find the library and version.
                    auto const open_bracket = lhs.find(u8'(');
                    if (open_bracket == lhs.npos) continue;
                    auto const comma = lhs.find(u8',', open_bracket);
                    if (comma == lhs.npos) continue;
                    auto const close_bracket = lhs.find(u8')', comma);
                    if (close_bracket == lhs.npos) continue;

                    // Name is before bracket.
                    auto const name = Trim(lhs.substr(0, open_bracket));
                    auto const lib = Trim(lhs.substr(open_bracket + 1, comma - (open_bracket + 1)));
                    auto const arch = Trim(lhs.substr(comma + 1, close_bracket - (comma + 1)));

                    entries.push_back(LdConfigEntry{
                        .name = std::u8string(name),
                        .lib = std::u8string(lib),
                        .arch = std::u8string(arch),
                        .path = rhs,
                    });
                }
            }
			return entries;
		}
	}

	auto FileSearchDirectory(std::filesystem::path const & file, std::filesystem::path const & directory) -> FileSearchResult
	{
		if (not FL::FileExists(directory)) return std::nullopt;
		if (not FL::IsDirectory(directory)) return std::nullopt;
		auto const result = directory / file;
		if (not FL::FileExists(result)) return std::nullopt;
		return std::filesystem::weakly_canonical(result);
	}

	auto FileSearchEnvironmentVariableDirs(std::filesystem::path const & file, std::u8string_view var, char8_t delim) -> FileSearchResult
	{
		for (auto const & dir : GetDirsFromEnv(var, delim))
		{
			auto opt_result = FileSearchDirectory(file, dir);
			if (not opt_result) continue;
			return opt_result;
		}
		return std::nullopt;
	}

	auto FileSearchRegistryKey(std::filesystem::path const & file, std::u8string_view entry, std::filesystem::path const & relative) -> FileSearchResult
	{
#if defined(PLATFORM_IS_WINDOWS)
		auto const is_key_string = [](RegDataType type)
		{
			return (
				(type == FL::RegDataType::Sz) or 
				(type == FL::RegDataType::ExpandSz)
			);
		};
		auto const get_string_data = [](RegKey::iterator::value_type const & value) -> std::u8string
		{
			if (value.type == FL::RegDataType::Sz)
			{
				// String
				return std::u8string(value.data.begin(), value.data.end());
			}
			else if (value.type == FL::RegDataType::ExpandSz)
			{
				// Need to expand environmental variables.
				std::u8string temp;
				auto const * src = reinterpret_cast<LPCSTR>(value.data.data());
				char t;
				auto * dst = &t;
				auto const required_chars = ExpandEnvironmentStringsA(src, dst, 0);
				// Skip if the env string fails to run.
				if (required_chars == 0) return {};
				temp.resize(required_chars);
				auto const written_chars = ExpandEnvironmentStringsA(src, reinterpret_cast<LPSTR>(temp.data()), temp.size() + 1);
				if (written_chars == 0) return {};
				return temp;
			}
			return {};
		};

		auto const need_key_name = file.stem().u8string();
		auto const reg_key = RegKey{RegPath{entry}};
		for (auto value : reg_key)
		{
			auto key_name = value.name;
			if (not is_key_string(value.type)) continue;
			if (not file.has_filename()) continue;
			if (not FL::Equal(key_name, need_key_name, false)) continue;

			for (auto data : GetRegKeyStringData(value))
			{
				auto const found_path = [&]
				{
					if (relative.empty())
					{
						return std::filesystem::weakly_canonical(std::filesystem::path(data));
					}
					else
					{
						return std::filesystem::weakly_canonical(std::filesystem::path(relative / data));
					}
				}();
				if (not FL::Equal(found_path.extension().u8string(), file.extension().u8string(), false)) continue;
				if (not FL::Equal(found_path.filename().u8string(), file.filename().u8string(), false)) continue;
				return found_path;
			}
		}
#endif
		return std::nullopt;
	}

	auto FileSearchRegistryKeyValue(std::filesystem::path const & file, std::u8string_view entry, std::u8string_view value, std::filesystem::path const & relative) -> FileSearchResult
	{
#if defined(PLATFORM_IS_WINDOWS)
		auto const is_key_string = [](RegDataType type)
		{
			return (
				(type == FL::RegDataType::Sz) or 
				(type == FL::RegDataType::ExpandSz)
			);
		};

		auto const need_key_name = file.stem().u8string();
		auto const reg_key = RegKey{RegPath{entry}};
		for (auto key_value : reg_key)
		{
			auto key_name = key_value.name;
			if (not is_key_string(key_value.type)) continue;
			if (not file.has_filename()) continue;
			if (not FL::Equal(std::u8string_view(key_name), value, false)) continue;

			auto const dirs = GetRegKeyStringData(key_value);
			for (auto const & dir : dirs)
			{
				auto opt_result = FileSearchDirectory(file, dir);
				if (not opt_result) continue;
				return opt_result;
			}
		}
#endif
		return std::nullopt;
	}

	auto FileSearchForDynamicLibrary(std::filesystem::path const & file) -> FileSearchResult
	{
#if defined(PLATFORM_IS_WINDOWS)
		{
			// Attempt 1: Using the system resolver.
			struct Raii
			{
				Raii(const char * path) : hmodule{ LoadLibraryA(path) } {}
				~Raii() { FreeLibrary(hmodule); }
				HMODULE hmodule;
			};
			auto raii = Raii(reinterpret_cast<const char *>(file.u8string().data()));
			if (raii.hmodule == NULL) return {};
			char name_path[std::numeric_limits<int16_t>::max()];
			auto const filename_length = GetModuleFileNameA(raii.hmodule, name_path, std::numeric_limits<int16_t>::max());
			if (filename_length > 0) { return std::string{std::data(name_path), filename_length}; }
		}
#elif defined(PLATFORM_IS_LINUX) || defined(PLATFORM_IS_MAC)
		{
			auto const [return_code, result_string] = FL::Cmd{
				.cmd = u8"ldconfig",
				.args = {
					{"-p"}
				}
			}.Run();
			if (return_code != 0) return std::nullopt;
			auto const mapping = FL::GetLdConfigMapping(result_string);
			auto test = file;
			if (not test.filename().u8string().starts_with(u8"lib"))
			{
				test.replace_filename(u8"lib" + test.filename().u8string());
			}
			for (auto const & [name, lib, arch, path] : mapping)
			{
				auto mapping_name = std::filesystem::path(name);
				auto required_name = test.filename();
				if (FL::Equal(mapping_name.u8string(), required_name.u8string(), false))
				{
					return { path };
				}
				auto mapping_name_no_ver = mapping_name.stem();
				auto required_name_no_ver = required_name.stem();
				if (FL::Equal(mapping_name_no_ver.u8string(), required_name_no_ver.u8string(), false))
				{
					return { path };
				}
				auto mapping_name_no_ext = mapping_name_no_ver.stem();
				auto required_name_no_ext = required_name_no_ver.stem();
				if (FL::Equal(mapping_name_no_ext.u8string(), required_name_no_ext.u8string(), false))
				{
					return { path };
				}
			}
		}
#endif
		return std::nullopt;
	}

	auto FileSearchForExecutable(std::filesystem::path const & file) -> FileSearchResult
	{
		std::set<std::filesystem::path> search_root_paths{};
		auto add_source_root_path = [](auto & dst, std::u8string env_var, auto mutator)
		{
			if (not env_var.empty())
			{
				dst.insert(mutator(env_var));
			}
		};
#if defined(PLATFORM_IS_WINDOWS)
		char name_path[std::numeric_limits<int16_t>::max()];
		auto const u8str = file.u8string();
		auto valid_extension = GetExtsFromEnv(u8"PATHEXT", u8';');
		if (file.has_extension())
		{
			auto const result = SearchPathA(NULL, reinterpret_cast<LPCSTR>(u8str.data()), NULL, std::size(name_path), std::data(name_path), NULL);
			if (result > 0)
			{
				return std::filesystem::weakly_canonical(std::filesystem::path(std::u8string(std::data(name_path), std::data(name_path) + result)));
			}
		}
		else
		{
			for (auto const & exts : valid_extension)
			{
				auto const result = SearchPathA(NULL, reinterpret_cast<LPCSTR>(u8str.data()), reinterpret_cast<LPCSTR>(exts.data()), std::size(name_path), std::data(name_path), NULL);
				if (result > 0)
				{
					return std::filesystem::weakly_canonical(std::filesystem::path(std::u8string(std::data(name_path), std::data(name_path) + result)));
				}
			}
		}
		// Attempt 2: Using common paths.
		std::set<std::filesystem::path> source_root_paths;
		add_source_root_path( source_root_paths, GetEnv(u8"ProgramW6432"), std::identity{} );
		add_source_root_path( source_root_paths, GetEnv(u8"ProgramFiles"), std::identity{} );
		add_source_root_path( source_root_paths, GetEnv(u8"SystemDrive"), [](std::u8string var) { return var + u8"/Program Files"; } );
		add_source_root_path( source_root_paths, GetEnv(u8"SystemDrive"), [](std::u8string var) { return var + u8"/Program Files (x86)"; } );

		// Get the possible directories from the above, list them all as seach root paths.
		for (auto const & source_root_path : source_root_paths)
		{
			std::error_code ec;
			auto it = std::filesystem::directory_iterator(source_root_path, ec);
			auto const end_it = std::filesystem::directory_iterator{};
			search_root_paths.insert(it, end_it);
		}
#elif defined(PLATFORM_IS_LINUX) or defined(PLATFORM_IS_MAC)
		// Search directories included in path.
		auto opt_path_result = FileSearchEnvironmentVariableDirs(file, u8"PATH");
		if (opt_path_result) { return *opt_path_result; }
		// Attempt 2: Using common paths.
		add_source_root_path( search_root_paths, u8"/", std::identity{} );
		add_source_root_path( search_root_paths, u8"/usr/", std::identity{} );
		add_source_root_path( search_root_paths, GetEnv(u8"HOME"), std::identity{} );
		add_source_root_path( search_root_paths, GetEnv(u8"HOME"), [](std::u8string var) { return var + u8"/.local"; } );
		auto valid_extension = std::vector<std::u8string>{u8"", u8".sh", u8".exe"};
#endif
		// This is sort of a last resort, and can be vastly slower.
		for (auto path_transform : {+[](std::filesystem::path a){ return a; }, +[](std::filesystem::path a){ return a / "bin"; }})
		{
			for (auto const & raw_path : search_root_paths)
			{
				auto const path = path_transform(raw_path);
				if (FL::FileExists(path) and FL::IsDirectory(path))
				{
					if (file.has_extension())
					{
						auto const exe_path = path / file;
						if (FL::FileExists(exe_path))
						{
							return std::filesystem::weakly_canonical(exe_path);
						}
					}
					else
					{
						auto test = file;
						for (auto const & ext : valid_extension)
						{
							if (ext.empty())
							{
								auto const exe_path = path / test;
								if (FL::FileExists(exe_path))
								{
									return std::filesystem::weakly_canonical(exe_path);
								}
							}
							else
							{
								auto const exe_path = path / test.replace_extension(ext);
								if (FL::FileExists(exe_path))
								{
									return std::filesystem::weakly_canonical(exe_path);
								}
							}
						}
					}
				}
			}
		}
		return std::nullopt;
	}
}