#include "FL/FindRuleset.hpp"
#include "FL/Cmd.hpp"
#include "FL/FileSearch.hpp"
#include "Json.hpp"
#include <algorithm>
#include <fstream>
#include <stdexcept>
#include <string>
#include <string_view>

namespace FL
{
	namespace
	{
		constexpr auto ParseIdentifier(std::u8string_view input) -> std::u8string_view
		{
			if (input.empty()) return {};
			auto const is_dollar = input.front() == '$';
			if (not is_dollar) return {};
			input.remove_prefix(1);

			auto const is_leading_char = [](char8_t c){
				return
				(
					(u8'a' <= c and c <= u8'z') or
					(u8'A' <= c and c <= u8'Z') or 
					(u8'_' == c)
				);
			};
			auto const is_trailing_char = [&](char8_t c){
				return 
				(
					is_leading_char(c) or 
					(u8'0' <= c and c <= u8'9')
				);
			};

			auto it = input.begin();
			auto const end_it = input.end();
			if (it == end_it) return {};
			auto const leading_char = *it++;
			if (not is_leading_char(leading_char)) return {};
			while (it != end_it and is_trailing_char(*it)) ++it;
			return { it, end_it };
		}
		static_assert(ParseIdentifier(u8"$2oawijef") == std::u8string_view{});
		static_assert(ParseIdentifier(u8"$o213aw") == u8"");
		static_assert(ParseIdentifier(u8"o213aw") == std::u8string_view{});
		static_assert(ParseIdentifier(u8"$o213aw crayon") == u8" crayon");

		struct IdentifierRange
		{
			std::size_t index = ~std::size_t{};
			std::size_t length = 0;

			[[nodiscard]] bool IsValid() const noexcept 
			{
				return index != ~std::size_t{};
			}

			friend bool operator==(IdentifierRange const &, IdentifierRange const &) = default;
		};
		constexpr auto FindFirstIdentifier(std::u8string_view input) -> IdentifierRange
		{
			constexpr std::size_t min_length = 5;
			if (input.size() < min_length) return {};
			auto const avalible_length = (input.size() - min_length);
			auto const end_it = input.data() + input.size();
			for (std::size_t i = 0; i <= avalible_length; ++i)
			{
				auto const before = input.data() + i;
				auto const parse_result = ParseIdentifier({before, end_it});
				auto const after = parse_result.data();
				if (parse_result.data() != nullptr)
				{
					return IdentifierRange{
						.index = i,
						.length = static_cast<std::size_t>(after - before)
					};
				}
			}
			return {};
		}
		static_assert(FindFirstIdentifier(u8"$hello this is cool") == IdentifierRange{ 0, 6 });
		static_assert(FindFirstIdentifier(u8" hello $this is cool") == IdentifierRange{ 7, 5 });
		static_assert(FindFirstIdentifier(u8" hello this is $cool") == IdentifierRange{ 15, 5 });

		auto GetAllIdentifierRanges(std::u8string_view input) -> std::vector<IdentifierRange>
		{
			std::vector<IdentifierRange> output;
			std::size_t index = 0;
			auto const * const it = input.data();
			auto const * const end_it = input.data() + input.size();
			while (end_it != it)
			{
				auto const result = FindFirstIdentifier({it + index, end_it});
				auto const start_index = index + result.index;
				auto const end_index = start_index + result.length;
				if (result.IsValid())
				{
					index = end_index;
					output.push_back(IdentifierRange{
						.index = start_index,
						.length = result.length,
					});
				}
				else break;
			}
			return output;
		}

		auto Ruleset(nlohmann::json const & data) -> std::optional<FindRulesets>
		{
			FindRulesets output;

			auto const get_arg_name = [](std::u8string_view arg)
			{
				auto const * begin_ptr = arg.data();
				auto const * end_ptr = begin_ptr + arg.size();
				return std::u8string_view{ std::min(begin_ptr + 1, end_ptr), end_ptr };
			};
			auto const get_prev_rule = [&](std::u8string_view name) -> FindRuleset const *
			{
				for (auto & ruleset : output.rulesets)
				{
					if (name == ruleset.name) { return &ruleset; }
				}
				return nullptr;
			};
			auto const report_error = [&](std::u8string_view str)
			{
				throw std::runtime_error(std::string(str.begin(), str.end()));
			};

			auto opt_args = GetT<FindRulesets>(data);
			if (not opt_args)
			{
				return NullOpt();
			}
			return { std::move(opt_args) };
		}

		static void UnsupportedArrayArg(std::u8string arg)
		{
			throw std::runtime_error("The argument '" + std::string(arg.begin(), arg.end()) + "' doesn't support multiple arguments.");
		}

		static void MissingArg(std::u8string arg)
		{
			throw std::runtime_error("The identifier '" + std::string(arg.begin(), arg.end()) + "' has not been provided as an argument.");
		}

		static std::u8string SubInString(std::u8string src, FindRulesetArgsSubstitution const & sub)
		{
			auto all_identifiers = GetAllIdentifierRanges(src);
			std::ranges::reverse(all_identifiers);
			auto output = src;
			for (auto const & [index, length] : all_identifiers)
			{
				auto const identifier_name = std::u8string( output.data() + index, length );
				auto const sub_it = sub.find(identifier_name);
				if (sub_it == sub.end()) { MissingArg(identifier_name); }
				auto const & sub_ref = sub_it->second;
				if (sub_ref.size() > 1) { UnsupportedArrayArg(identifier_name); }
				if (sub_ref.empty()) { MissingArg(identifier_name); }
				auto const & front = sub_ref.front();
				output.erase(index, length);
				output.insert(output.begin() + index, front.begin(), front.end());
			}
			return output;
		}
	}

	auto ToString(FindRuleType type) -> std::string_view
	{
		switch (type)
		{
		case FindRuleType::InDirs: return "find_in_dirs";
		case FindRuleType::InEnvVarPaths: return "find_in_env_var_paths";
		case FindRuleType::UsingRegKey: return "find_using_reg_key";
		case FindRuleType::UsingRegKeyVal: return "find_using_reg_key_val";
		case FindRuleType::Library: return "find_library";
		case FindRuleType::Executable: return "find_executable";
		case FindRuleType::Pipe: return "pipe";
		default: return "unknown_type";
		}
	}

	auto ToU8String(FindRuleType type) -> std::u8string_view
	{
		switch (type)
		{
		case FindRuleType::InDirs: return u8"find_in_dirs";
		case FindRuleType::InEnvVarPaths: return u8"find_in_env_var_paths";
		case FindRuleType::UsingRegKey: return u8"find_using_reg_key";
		case FindRuleType::UsingRegKeyVal: return u8"find_using_reg_key_val";
		case FindRuleType::Library: return u8"find_library";
		case FindRuleType::Executable: return u8"find_executable";
		case FindRuleType::Pipe: return u8"pipe";
		default: return u8"unknown_type";
		}
	}

	auto ToString(FindArgBaseType type) -> std::string_view
	{
		switch (type)
		{
		case FindArgBaseType::Int: return "int";
		case FindArgBaseType::Uint: return "uint";
		case FindArgBaseType::String: return "string";
		case FindArgBaseType::File: return "file";
		case FindArgBaseType::Directory: return "directory";
		case FindArgBaseType::Executable: return "executable";
		case FindArgBaseType::List: return "list";
		default: return "unknown_type";
		}
	}

	auto ToU8String(FindArgBaseType type) -> std::u8string_view
	{
		switch (type)
		{
		case FindArgBaseType::Int: return u8"int";
		case FindArgBaseType::Uint: return u8"uint";
		case FindArgBaseType::String: return u8"string";
		case FindArgBaseType::File: return u8"file";
		case FindArgBaseType::Directory: return u8"directory";
		case FindArgBaseType::Executable: return u8"executable";
		case FindArgBaseType::List: return u8"list";
		default: return u8"unknown_type";
		}
	}

	auto FindArgType::IsInt() const noexcept -> bool
	{
		return GetBaseType() == FindArgBaseType::Int;
	}

	auto FindArgType::IsUint() const noexcept -> bool
	{
		return GetBaseType() == FindArgBaseType::Uint;
	}

	auto FindArgType::IsString() const noexcept -> bool
	{
		return GetBaseType() == FindArgBaseType::String;
	}

	auto FindArgType::IsFile() const noexcept -> bool
	{
		return GetBaseType() == FindArgBaseType::File;
	}

	auto FindArgType::IsDirectory() const noexcept -> bool
	{
		return GetBaseType() == FindArgBaseType::Directory;
	}

	auto FindArgType::IsExecutable() const noexcept -> bool
	{
		return GetBaseType() == FindArgBaseType::Executable;
	}

	auto FindArgType::IsList() const noexcept -> bool
	{
		return GetBaseType() == FindArgBaseType::List;
	}

	auto FindArgType::Is1dArray() const noexcept -> bool
	{
		return GetDimensionCount() == 1;
	}

	auto FindArgType::IsValid() const noexcept -> bool
	{
		return not m_type.empty();
	}

	auto FindArgType::GetDimensionCount() const noexcept -> std::size_t
	{
		return std::max(std::size_t(1), m_type.size()) - std::size_t(1);
	}

	auto FindArgType::GetBaseType() const noexcept -> FindArgBaseType
	{
		return m_type.back();
	}

	auto FindArgType::ToString() const noexcept -> std::string
	{
		std::string output;
		for (auto const & part : m_type) output += std::string(FL::ToString(part)) + ":"; 
		if (not m_type.empty())
		{
			output.pop_back();
		}
		return output;
	}

	auto FindArgType::ToU8String() const noexcept -> std::u8string
	{
		std::u8string output;
		for (auto const & part : m_type) output += std::u8string(FL::ToU8String(part)) + u8":"; 
		if (not m_type.empty())
		{
			output.pop_back();
		}
		return output;
	}

	auto ParseArgType(std::u8string_view input) -> FindArgType
	{
		if (input.empty()) return {};
		FindArgType output;
		if (input.front() != u8'$') return {};
		// Remove '$'
		input.remove_prefix(1);
		// Attempt to consume all list prefixes.
		auto const list_string = ToU8String(FindArgBaseType::List);
		for (;;)
		{
			if (input.starts_with(list_string))
			{
				input.remove_prefix(list_string.size());
				output.m_type.push_back(FindArgBaseType::List);
				if (not input.starts_with(u8":"))
				{
					// A list must be a list of something, you cannot just have a list of nothings...
					return {};
				}
				// Remove ':'
				input.remove_prefix(1);
			}
			else break;
		}

		// Add the final type.
		for (auto const & arg_type : possible_base_arg_types)
		{
			auto const str = ToU8String(arg_type);
			if (input.starts_with(str))
			{
				// Remove '<typename>'
				input.remove_prefix(str.size());
				output.m_type.push_back(arg_type);
				if (not input.starts_with(u8":"))
				{
					return output;
				}
				// Remove ':'
				input.remove_prefix(1);
			}
		}
		return {};
	}

	auto ParseArgTypeResult::IsValid() const noexcept -> bool
	{
		return result.IsValid();
	}

	auto FindRulesets::GetRulesetByName(std::u8string_view input) const noexcept -> FindRuleset const *
	{
		for (auto const & ruleset : rulesets)
		{
			if (input == ruleset.name)
			{
				return std::addressof(ruleset);
			}
		}
		return nullptr;
	}

	bool FindRulesets::IsValid() const noexcept
	{
		return not rulesets.empty();
	}

	auto FindRulesetsFromJson(std::u8string_view str) -> std::optional<FindRulesets>
	{
		return Ruleset(nlohmann::json::parse(str));
	}

	auto FindRulesetsFromFile(std::filesystem::path path) -> std::optional<FindRulesets>
	{
		// TODO: This would crash with a unicode path, fix that.
		std::ifstream f(path.string());
		return Ruleset(nlohmann::json::parse(f));
	}

	auto FindInDirs::Find(std::filesystem::path const & file) const -> FileSearchResult
	{
		for (auto const & dir : dirs)
		{
			auto const opt_result = FileSearchDirectory(file, dir);
			if (opt_result) { return opt_result; }
		}
		return std::nullopt;
	}

	auto FindInEnvVarPaths::Find(std::filesystem::path const & file) const -> FileSearchResult
	{
		return FileSearchEnvironmentVariableDirs(file, variable, delim);
	}

	auto FindUsingRegKey::Find(std::filesystem::path const & file) const -> FileSearchResult
	{
		return FileSearchRegistryKey(file, entry, relative);
	}

	auto FindUsingRegKeyVal::Find(std::filesystem::path const & file) const -> FileSearchResult
	{
		return FileSearchRegistryKeyValue(file, entry, value, relative);
	}

	auto FindLibrary::Find(std::filesystem::path const & file) const -> FileSearchResult
	{
		return FileSearchForDynamicLibrary(file);
	}

	auto FindExecutable::Find(std::filesystem::path const & file) const -> FileSearchResult
	{
		return FileSearchForExecutable(file);
	}

	auto FindPipe::Find(std::filesystem::path const & file) const -> FileSearchResult
	{
		auto it = cmds.begin();
		auto const end_it = cmds.end();
		if (it == end_it) return {};
		auto cmd = *it++;
		while (it != end_it) { cmd = cmd | *it++; }
		auto const [return_code, std_out] = cmd.Run();
		if (expect_error and (return_code == 0)) return {};
		if (not expect_error and (return_code != 0)) return {};
		return FindInDirs{ filters(std_out) }.Find(file);
	}

	auto FindInDirs::Sub(FindRulesetArgsSubstitution const & sub) const -> FindInDirs
	{
		FindInDirs output;
		for (auto const & dir : dirs)
		{
			std::u8string temp = dir;
			auto all_identifiers = GetAllIdentifierRanges(dir);
			if (all_identifiers.size() > 1)
			{
				throw std::runtime_error("Only a single identifier can be present in FindInDirs's dir field.");
			}
			if (not all_identifiers.empty())
			{
				auto const [index, length] = all_identifiers.front();
				auto const identifier_name = std::u8string( dir.data() + index, length );
				auto const sub_it = sub.find(identifier_name);
				if (sub_it == sub.end()) { MissingArg(identifier_name); }
				auto const & sub_ref = sub_it->second;
				output.dirs.insert(output.dirs.end(), sub_ref.begin(), sub_ref.end());
			}
			else
			{
				output.dirs.push_back(temp);
			}
		}
		return output;
	}

	auto FindInEnvVarPaths::Sub(FindRulesetArgsSubstitution const & sub) const -> FindInEnvVarPaths
	{
		return FindInEnvVarPaths{
			.variable = SubInString(variable, sub),
			.delim = delim,
		};
	}

	auto FindUsingRegKey::Sub(FindRulesetArgsSubstitution const & sub) const -> FindUsingRegKey
	{
		return FindUsingRegKey{
			.entry = SubInString(entry, sub),
			.relative = SubInString(entry, sub),
		};
	}

	auto FindUsingRegKeyVal::Sub(FindRulesetArgsSubstitution const & sub) const -> FindUsingRegKeyVal
	{
		return FindUsingRegKeyVal{
			.entry = SubInString(entry, sub),
			.value = SubInString(value, sub),
			.relative = SubInString(relative, sub),
		};
	}

	auto FindLibrary::Sub(FindRulesetArgsSubstitution const & sub) const -> FindLibrary
	{
		return *this;
	}

	auto FindExecutable::Sub(FindRulesetArgsSubstitution const & sub) const -> FindExecutable
	{
		return *this;
	}

	auto FindPipe::Sub(FindRulesetArgsSubstitution const & sub) const -> FindPipe
	{
		auto output = *this;
		for (auto & cmd : output.cmds)
		{
			cmd.cmd = SubInString(cmd.cmd, sub);
			for (auto & arg : cmd.args)
			{
				arg.data = SubInString(arg.data, sub);
			}
		}
		return output;
	}

	// The type erased form.
	auto FindRule::Find(std::filesystem::path const & path) const -> FL::FileSearchResult
	{
		return m_rule->Find(path);
	}

	auto FindRule::Sub(FindRulesetArgsSubstitution const & sub) const -> FindRule
	{
		return m_rule->Sub(sub);
	}
}