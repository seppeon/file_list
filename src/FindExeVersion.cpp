#include "FL/FindExeVersion.hpp"
#include "FL/Cmd.hpp"

namespace FL
{
	namespace
	{
		auto GetVersion(std::filesystem::path exe ) -> FL::Semver
		{
			Cmd cmd{ .cmd = exe.u8string(), .args = { { "--version" } } };
			auto const [return_code, text] = cmd.Run();
			if (return_code != 0) return {};
			auto const result = FindFirstSemverString({ reinterpret_cast<const char *>(text.data()), text.size() });
			return result.semver;
		}
	}

	auto GetCmakeVersion(std::filesystem::path exe ) -> FL::Semver { return GetVersion(exe); }
	auto GetGccVersion(std::filesystem::path exe ) -> FL::Semver { return GetVersion(exe); }
	auto GetGppVersion(std::filesystem::path exe ) -> FL::Semver { return GetVersion(exe); }
	auto GetClangVersion(std::filesystem::path exe ) -> FL::Semver { return GetVersion(exe); }
	auto GetClangPpVersion(std::filesystem::path exe ) -> FL::Semver { return GetVersion(exe); }
	auto GetNinjaVersion(std::filesystem::path exe ) -> FL::Semver { return GetVersion(exe); }
}