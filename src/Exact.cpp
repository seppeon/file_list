#include "FL/Exact.hpp"
#include "FL/StrOps.hpp"

namespace FL
{
	auto Exact::RequiresExactMatch() const noexcept -> bool
	{
		return true;
	}

	auto Exact::Describe() const -> std::string
	{
		return "('" + std::string(m_contents.begin(), m_contents.end()) + "')";
	}

	auto Exact::Parse(std::u8string_view input, bool case_sensitive) const -> FL::Pattern
	{
		return Equal(m_contents, input, case_sensitive) ? FL::Pattern{input} : FL::Pattern{};
	}

	Exact::Exact(std::u8string contents)
		: m_contents( std::move(contents) )
	{}
}